// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class TTbarPDF : public Analysis {
    public:

      enum JetFlav { B, L };

      /// Constructor
      DEFAULT_RIVET_ANALYSIS_CTOR(TTbarPDF);

      /// @name Analysis methods
      //@{

      /// Book histograms and initialise projections before the run
      void init() {

        // Initialise and register projections

        // Jet clustering
        IdentifiedFinalState nu_id;
        nu_id.acceptNeutrinos();
        PromptFinalState neutrinos(nu_id);
        FinalState fs(Cuts::abseta < 4.9);

        // Get photons to dress leptons
        IdentifiedFinalState photons(fs);
        photons.acceptIdPair(PID::PHOTON);

        // Get dressed leptons
        IdentifiedFinalState el_id(fs);
        el_id.acceptIdPair(PID::ELECTRON);
        PromptFinalState electrons(el_id);
        DressedLeptons fulldressedelectrons(photons, electrons, 0.1, Cuts::abseta < 2.5 && Cuts::pT > 28.0*GeV, true);
        addProjection(fulldressedelectrons, "DressedElectrons");
        IdentifiedFinalState mu_id(fs);
        mu_id.acceptIdPair(PID::MUON);
        PromptFinalState muons(mu_id);
        DressedLeptons fulldressedmuons(photons, muons, 0.1, Cuts::abseta < 2.5 && Cuts::pT > 28.0*GeV, true);
        addProjection(fulldressedmuons, "DressedMuons"); 

        VetoedFinalState vfs;
        vfs.addVetoOnThisFinalState(fulldressedelectrons);
        vfs.addVetoOnThisFinalState(fulldressedmuons);
        vfs.addVetoOnThisFinalState(neutrinos);  // jets.useInvisibles(true);                                                                                         
        declare(FastJets(vfs, FastJets::ANTIKT, 0.4), "CaloJets");

        // Book histograms
        h_j1eta = bookHisto1D("j1eta", 50, -2.5, 2.5);
        h_j2eta = bookHisto1D("j2eta", 50, -2.5, 2.5);
        h_j1pt = bookHisto1D("j1pt", 50, 0, 500*GeV);
        h_j2pt = bookHisto1D("j2pt", 50, 0, 500*GeV);
        h_b1eta = bookHisto1D("b1eta", 50, -2.5, 2.5);
        h_b2eta = bookHisto1D("b2eta", 50, -2.5, 2.5);
        h_b1pt = bookHisto1D("b1pt", 50, 0, 500*GeV);
        h_b2pt = bookHisto1D("b2pt", 50, 0, 500*GeV);
        h_l1eta = bookHisto1D("l1eta", 50, -2.5, 2.5);
        h_l2eta = bookHisto1D("l2eta", 50, -2.5, 2.5);
        h_l1pt = bookHisto1D("l1pt", 50, 0, 500*GeV);
        h_l2pt = bookHisto1D("l2pt", 50, 0, 500*GeV);

        h_mll = bookHisto1D("mll", 50, 0, 250*GeV);
        h_ptll = bookHisto1D("ptll", 50, 0, 250*GeV);
        h_detall = bookHisto1D("detall", 50, 0, 5);

        h_mljbb = bookHisto1D("mljbb", 50, 0, 250*GeV);
        h_mljbl = bookHisto1D("mljbl", 50, 0, 250*GeV);
        h_mljlb = bookHisto1D("mljlb", 50, 0, 250*GeV);
        h_mljll = bookHisto1D("mljll", 50, 0, 250*GeV);
        h_mlj = bookHisto1D("mlj", 50, 0, 250*GeV);
      }


      /// Perform the per-event analysis
      void analyze(const Event& event) {

        double weight = event.weight();

        // Get jets and dressed leptons
        const Jets& jets = apply<FastJets>(event, "CaloJets").jetsByPt(Cuts::pT > 20*GeV && Cuts::abseta < 2.5);
        const vector<DressedLepton>& es = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();
        const vector<DressedLepton>& mus = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();


        // cout << "n elec: " << es.size() << endl;
        // cout << "n muon: " << mus.size() << endl;
        // cout << "n jets: " << jets.size() << endl;

        // Require exactly one of each lepton flavour and exactly 2 central jets.
        if (es.size() != 1 || mus.size() != 1) {
          MSG_INFO("fail emu cut");
          vetoEvent;
        }

        if (jets.size() != 2) {
          MSG_INFO("fail njets cut");
          vetoEvent;
        }

        MSG_INFO("pass cuts");

        const Particle& el = es[0];
        const Particle& mu = mus[0];
        const Jet& j1 = jets[0];
        const Jet& j2 = jets[1];

        const FourMomentum pll = el.momentum() + mu.momentum();

        h_mll->fill(pll.mass(), weight);
        h_ptll->fill(pll.pt(), weight);

        h_detall->fill(abs(el.eta() - mu.eta()), weight);

        h_j1eta->fill(j1.eta(), weight);
        h_j2eta->fill(j2.eta(), weight);
        h_j1pt->fill(j1.pt(), weight);
        h_j2pt->fill(j2.pt(), weight);


        JetFlav j1flav, j2flav;
        if (j1.bTagged()) {
          j1flav = B;
          h_b1eta->fill(j1.eta(), weight);
          h_b1pt->fill(j1.pt(), weight);
        } else {
          j1flav = L;
          h_l1eta->fill(j1.eta(), weight);
          h_l1pt->fill(j1.pt(), weight);
        }

        if (j2.bTagged()) {
          j2flav = B;
          h_b2eta->fill(j2.eta(), weight);
          h_b2pt->fill(j2.pt(), weight);
        } else {
          j2flav = L;
          h_l2eta->fill(j2.eta(), weight);
          h_l2pt->fill(j2.pt(), weight);
        }

        // find the mlj variable for the event...

        double mej1 = (el.momentum() + j1.momentum()).mass2();
        double mmj1 = (mu.momentum() + j1.momentum()).mass2();
        double mej2 = (el.momentum() + j2.momentum()).mass2();
        double mmj2 = (mu.momentum() + j2.momentum()).mass2();

        double mlj;
        if (mej1 + mmj2 < mej2 + mmj1)
          mlj = sqrt(min(mej1, mmj2));
        else 
          mlj = sqrt(min(mej2, mmj1));

        h_mlj->fill(mlj, weight);
        if (j1flav == B && j2flav == B)
          h_mljbb->fill(mlj, weight);
        else if (j1flav == B && j2flav == L)
          h_mljbl->fill(mlj, weight);
        else if (j1flav == L && j2flav == B)
          h_mljlb->fill(mlj, weight);
        else
          h_mljll->fill(mlj, weight);


        return;
      }


      void finalize() {
        normalize(
            { h_j1eta, h_j2eta, h_j1pt, h_j2pt, h_b1eta
            , h_b2eta, h_b1pt, h_b2pt, h_l1eta, h_l2eta
            , h_l1pt, h_l2pt, h_mll, h_ptll, h_detall
            , h_mljbb, h_mljbl, h_mljlb, h_mljll, h_mlj
            } );
      }


    private:

      Histo1DPtr h_j1eta, h_j2eta, h_j1pt, h_j2pt, h_b1eta,
        h_b2eta, h_b1pt, h_b2pt, h_l1eta, h_l2eta, h_l1pt,
        h_l2pt, h_mll, h_ptll, h_detall, h_mljbb, h_mljbl,
        h_mljlb, h_mljll, h_mlj;


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(TTbarPDF);


}
