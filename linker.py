import os,sys
import argparse
import rucio.client


parser=argparse.ArgumentParser("Analysis of the better isolation for ttbar")
parser.add_argument("-r","--rse",dest="rse",action="store",help="RSE")
parser.add_argument("-d","--datasets",dest="datasets",action="store",help="Datasets identifiers")
parser.add_argument("-o","--output",dest="output",action="store",default="/eos/user/a/alopezso/testIsolationFTAG/",help="eos folder")
args=parser.parse_args()

rucio_client = rucio.client.didclient.DIDClient()

if ',' in args.datasets:
    print("Cannot handle lists here. Remove the commas.")
    sys.exit(0)
scope=args.datasets.split(':')[0]
name_filter=args.datasets.split(':')[-1]

if name_filter == "":
    print("Do not recognize the dataset identifier. It should have a form of <scope>:<dataset>. Exiting")
    sys.exit(0)
if scope == "" or scope == args.datasets :
    print("Do not recognize the dataset identifier. It should have a form of <scope>:<dataset>. Exiting")
    sys.exit(0)

datasets_list = list(rucio_client.list_dids( scope, { 'name':name_filter} ))

for dataset in datasets_list:
    print("Linking dataset %s"%dataset)
    os.system("mkdir -p %s/%s"%(args.output,dataset))
    os.chdir(args.output+"/"+dataset)
    os.system("rucio list-file-replicas --rse CERN-PROD_PERF-FLAVTAG --link /eos/:/eos/  %s "%(dataset))




