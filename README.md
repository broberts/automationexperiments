Method in short
---------------
1. select exactly two jets and 1 electron + 1 muon with opposite signs (clean in $t \bar t$ events). 
2. define m_lj per jet as the invariant mass of the lepton + jet combinations minimizing the squared sum of both m_lj combinations in the event. So callculate the two possible ways of combining lepton + jet and take the one which has the minimum m_{lj1}²+m_{lj2}² 
3. Analise Strategy 1 (cut based):
  - veto events with one m_lj above the top mass.
  - take lf-miss-tag rate from MC.
  - take flavor fractions (bb, bl, lb, ll) from MC.
  - extract b-tagging efficiency from data (fit). 
4. Analise Strategy 2 (combined fit):
	- include events with m_{lj} above the top mass as CR regions and extract flavor fractions here (template fit).
	- take lf-miss-tag rate and m_{lj} templates from MC.
	- extract b-tagging efficiency from data in one big combined fit. 

Working with this Repository
----------------------------
Since we had some bad experience with to many people working on seperate branches wich could not be merged in the end and where never intend to and our git repo got much to large, we encurage you to fork this repo if you want to work with it. 

1. Press the Fork butten in the upper left corner. Select your namespace and wait for your fork.
2. Clone into your fork.
  ```
  setupATLAS
  lsetup git
  git clone https://gitlab.cern.ch/CERN_USER/bjets_ttbardilepton_PDF_cleanup.git
  ```
3. And start your work here.
4. In case you want to pull the recent updates to your fork:
  ```
  git remote add upstream https://gitlab.cern.ch/atlas-ftag-calibration/bjets_ttbardilepton_PDF_cleanup.git
  git pull upstream master 
  ```
5. In cases you want your changes in the main repo, make a merge request.

This calibration code is structerd as follows:
-----------------------------------------
each folder contains a own, seperate Readme with detailed instructions.

1. [__AnalysisTop-21.2.X__](AnalysisTop-21.2.X/):
  This Folder Contains The Rootcorepackage For the customization of AnalysisTop and stuff for running this on the Grid. The steering File with our main selection can be found in the [grid directory](AnalysisTop-21.2.X/grid/). Here we run on FTAG2 sample, apply a first event selection and produce ntuples.

2. [__TTbar-b-calib-final-selection__](TTbar-b-calib-final-selection-r21/) runs the final selection on the Outputs of AnalysisTop and produces control-plots. (work in progress)
    here we basicly create the input histrograms for the fit we want to do later. At the moment we are focusing on the emu+2j channel. A cleaning cut (m_lj_cut) has been developed and shows much cleaner results (should be used!)
    The nice thing here -  out of the box the script creates the inputs for all available fit configurations

    1. the one without m_lj_cut is using all events. We get a significant higher systematic uncertainty then for the other configurations.

    2. m_lj_cut - much cleaner results. Since we only use the signal region we get rid of a lot non bb events.

    3. combined_fit (we use m_lj tamplates in the control regions to constrain the light jet fraction). This gives by far the best results at least for calo jets.


3. [__likelihood-fit__](likelihood-fit/) can calculate the scale factors after the final selection. (work in progress)
    in this folder there are all the script you need to run the fits and calculate the scalefactors.


![alt text](likelihood-fit/m_lj_region_plot.png "Region definition with m_lj of both jets in the event")



#### Boot_strap weights
sometimes in this little mess here, you will read options about boot_strap weights. They are needed in order to calculate MC-stat uncertainty. This is only important in case you want to use the combined fitting method in the end. As a start- I would recommend to get rid of this option for a first run. If you decide later, you really want to use the combined method, do so. You will see that the systematic uncertainty will be bumpy in some cases. I would recommend to rerun only this special systematic uncertainties with bootstrap options, since this takes a lot of computing time. Also for alternative ttbar samples the MC statistical unc plays an important roll, since they are produced with less statistics in average.
