import os
import sys
import subprocess
import argparse
import shutil
import ROOT
import os
import datetime

now = datetime.datetime.now()

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *



parser = argparse.ArgumentParser(
    description='creates plots for all fits for all Tagger, workingpoint and cahnnel and updtaes website.')

parser.add_argument('-r',"--rlTag",default="r21",
                    help='releaseTag')
parser.add_argument('-d',"--dataName",default=options.data_name,
                    help='dataName')
parser.add_argument('-t',"--dateTag",default=now.strftime("%y%m%d"),
                    help='dateTag')
parser.add_argument('-v',"--versionNumber",default="1_4_mc16d",
                    help='versionNumber')
parser.add_argument('-a',"--anaVersion",default="21_2_29",
                    help='analysis Top version Number')
parser.add_argument("--plot_dir",default=options.plot_dir,
                    help='use in order to change working dir, usefull for combination of different years.')

args = parser.parse_args()
plot_dir=args.plot_dir
outname=args.dateTag+"_"+args.rlTag+"_"+args.dataName+"_anaTop_"+args.anaVersion+"_v"+args.versionNumber
temp_dir=plot_dir+outname+"/"
if not os.path.exists(temp_dir):
    os.makedirs(temp_dir)
gStyle.SetPaintTextFormat("1.3f");
gStyle.SetNumberContours(12);

combined_fit=channel("emu_OS_J2_combined_fit")
combined_fit.origin_file_folder=plot_dir+ "FitPlots_3SB/"
combined_fit.origin_channel_name="emu_OS_J2"
combined_fit.addition_to_name=""

fit_m_lj_cut=channel("emu_OS_J2_m_lj_cut")
fit_m_lj_cut.origin_file_folder=plot_dir+ "FitPlots/"
fit_m_lj_cut.origin_channel_name="emu_OS_J2_m_lj_cut"
fit_m_lj_cut.addition_to_name="_fconst"

fit_original=channel("emu_OS_J2")
fit_original.origin_file_folder=plot_dir+ "FitPlots/"
fit_original.origin_channel_name="emu_OS_J2"
fit_original.addition_to_name="_fconst"
# channels=["emu_OS_J2","emu_OS_J2_m_lj_cut","emu_OS_J2_combined_fit"]


channels=[combined_fit,fit_m_lj_cut,fit_original]
taggers=["MV2c10","DL1"]#,"MV2c10Flip_MV2c10"]
workingPointNames=["FixedCutBEff","HybBEff"]
p_list=[]
fitplots_to_do=["nominal", "FTAG2_ttbar_Sherpa221","FTAG2_ttbar_PowHW7","FTAG2_ttbar_aMcPy8"]
for channel in channels:
    chan_dir=temp_dir+channel.name+"/"
    if not os.path.exists(chan_dir):
        os.makedirs(chan_dir)
    index_file = open(chan_dir+"index.html","w")
    index_file.write("<html><head><title>Plots in Folder: "+outname +"</title></head><body>\n")
    index_file.write("<h3> "+outname + " </h3>\n")
    for tagger in taggers:
        for workingPointName in workingPointNames:
            if True:
                if channel.name=="emu_OS_J2_combined_fit":
                    p=subprocess.Popen("python calculate_all_fits.py"+ " -r "+args.rlTag+" -c "+channel.origin_channel_name+" -t "+tagger+" -w "+workingPointName+" -d "+args.dataName+ " --combined_fit --plot_dir "+plot_dir , shell=True)
                else:
                    p=subprocess.Popen("python calculate_all_fits.py -r "+args.rlTag+" -c "+channel.origin_channel_name+" -t "+tagger+" -w "+workingPointName+" -d "+args.dataName +" --plot_dir "+plot_dir , shell=True)
                p.wait()
            dir_name="FitPlots_"+args.rlTag+"_"+channel.name+"_"+tagger+"_"+workingPointName
            Fits_dir=chan_dir+dir_name+"/"
            index_file.write("<h3><a href='"+dir_name +"/index.html'>"+dir_name+"</a></h3>\n")
            if not os.path.exists(Fits_dir):
                os.makedirs(Fits_dir)
            sub_dir_index_file = open(Fits_dir+"index.html","w")
            sub_dir_index_file.write("<html><head><title>Plots in Folder: "+dir_name +"</title></head><body>\n")
            sub_dir_index_file.write("<h3> "+dir_name + " </h3><p><a href='../index.html'>Back to index</a></p>\n")
            fit_names_for_plotting=""
            for fit_plot in fitplots_to_do:
                sub_dir_index_file.write("<h3><a href='"+"FitPlot_"+args.rlTag+"_"+args.dataName+"_"+tagger+"_"+workingPointName+"_"+channel.origin_channel_name+channel.addition_to_name+"_"+fit_plot+"/index.html'>"+ fit_plot+" individual FitPlots"+"</a></h3>\n")
                fitPlot_fileName="FitPlot_"+args.rlTag+"_"+args.dataName+"_"+tagger+"_"+workingPointName+"_"+channel.origin_channel_name+channel.addition_to_name+"_" +fit_plot+ ".root"
                shutil.copy2(channel.origin_file_folder+fitPlot_fileName, Fits_dir)
                fit_names_for_plotting=fit_names_for_plotting+Fits_dir+fitPlot_fileName+" "
            sub_dir_index_file.write("<h3><a href='"+"FinalFitPlots_"+args.rlTag+"_"+args.dataName+"_"+tagger+"_"+workingPointName+"_"+channel.origin_channel_name+channel.addition_to_name+"/index.html'>"+"Fitplots with all the Systematic Errors"+"</a></h3>\n")
            sub_dir_index_file.write("</body></html>\n")
            sub_dir_index_file.close()
            finalFitPlot_fileName="FinalFitPlots_"+args.rlTag+"_"+args.dataName+"_"+tagger+"_"+workingPointName+"_"+channel.origin_channel_name+channel.addition_to_name+".root"
            shutil.copy2(channel.origin_file_folder+finalFitPlot_fileName, Fits_dir)
            #
            p=subprocess.Popen("python draw_all_plots_in_file.py"+" "+Fits_dir+finalFitPlot_fileName+" " +fit_names_for_plotting , shell=True)
            p_list.append(p)
    index_file.write("</body></html>\n")
    index_file.close()

for p in p_list:
     p.wait()
print "now copying to website:"
if not os.path.exists(options.www_dir):
    os.makedirs(options.www_dir)
shutil.copytree(temp_dir,options.www_dir+outname)
