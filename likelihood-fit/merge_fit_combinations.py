import ROOT
import os
from os import listdir
from os.path import isfile, join

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *


def combine_files(file1_name,file2_name,out_name):
    command="hadd -f "+out_name+" "+file1_name+" "+file2_name
    print command
    os.system(command)


combi_dir_1="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-29_18-07-03_mc16a/plots-ttbar-PhPy8/"+"combination_sys_"+options.ttb_sample.name+"/"
combi_dir_2="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-29_18-07-10_mc16d/plots-ttbar-PhPy8/"+"combination_sys_"+options.ttb_sample.name+"/"
merged_dir="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-29-merged/"+"combination_sys_"+options.ttb_sample.name+"/"

if not os.path.exists(merged_dir):
    os.makedirs(merged_dir)

num_of_cores_to_use=8
tp = ThreadPool(num_of_cores_to_use)
file_list=listdir(combi_dir_1)
for file_name in file_list:
    file1_name=join(combi_dir_1, file_name)
    file2_name=join(combi_dir_2, file_name)
    if isfile(file1_name):
        if isfile(file2_name):
            out_name=join(merged_dir, file_name)
            print 'combining ',file_name
            tp.apply_async(combine_files, (file1_name,file2_name,out_name))
        else:
            print 'Error: ',file2_name, "does not exist!"

tp.close()
tp.join()
print "Ende gut alles gut! "
