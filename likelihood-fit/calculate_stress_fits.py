import os
import sys
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import subprocess
import array
import math
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *

class arrays_of_TH1:
    def  __init__(self, hist,bin_size=0.02,shift=0.0):
        self._hist=hist
        l_x_nom=[]
        l_x_down=[]
        l_x_up=[]
        l_y_nom=[]
        l_y_up=[]
        l_y_down=[]
        l_zero=[]
        N_bins=hist.GetNbinsX()
        for i_bin in xrange(1,N_bins+1):
            x_center=hist.GetXaxis().GetBinCenter(i_bin)
            if not shift==0:                
                x_center=10**(math.log10(x_center)+shift)
            y_center=hist.GetBinContent(i_bin)
            y_unc=hist.GetBinError(i_bin)
            l_x_nom.append(x_center)
            if bin_size==0:
                l_x_down.append(0)
                l_x_up.append(0)
            else:
                l_x_down.append(-((10**(math.log10(x_center)-bin_size))-x_center))
                l_x_up.append((10**(math.log10(x_center)+bin_size))-x_center)
                #print x_center, math.log10(x_center),(math.log10(x_center)-bin_size),(10**(math.log10(x_center)-bin_size)),(10**(math.log10(x_center)-bin_size))-x_center
            l_y_nom.append(y_center)
            l_y_up.append(y_unc)
            l_y_down.append(y_unc)
        self.a_x_nom=array.array("d", l_x_nom)
        self.a_y_nom=array.array("d", l_y_nom)
        self.a_x_down=array.array("d", l_x_down)
        self.a_x_up=array.array("d", l_x_up)
        self.a_y_up=array.array("d", l_y_up)
        self.a_y_down=array.array("d", l_y_down)
        self.N_bins=N_bins

  # ./2jet_pt_fit [OPTION...]
  #
  # -i, --input input_file     Input
  # -o, --output base_name     Final Outputfile_name will be added together:
  #                            base_name _ dataName _ rlTag _ channelName _
  #                            taggerName _ systName _ fitConfig (default: Workspace)
  # -r, --rlTag rlTag          rlTag (default: r21)
  # -d, --dataName dataName    dataName (default: data18)
  # -t, --tagger taggerName    taggerName (default: MV2c10)
  # -w, --workingPoint name    Name of the Working point: FixedCutBEff, HybBEff
  #                            (default: FixedCutBEff)
  # -s, --syst systName        name of systematics (default: nominal)
  # -c, --channel channelName  name of channel to fit (default: emu_OS_J2)
  # -h, --help                 Print help
  #     --closure_test         take mc as data
  #     --varry_fs             varry the f_xx fractions
#   Inputfile opend! :  /nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-19_18-03-08_4D_si/plots-ttbar-PhPy8/combination_sys_FTAG2_ttbar_PhPy8/FTAG2_ttbar_Sherpa221_combination_for_fit_nominal.root

def work(input_file,work_dir,rlTag,nom_sample,syst,channel,tagger,workingPointName,additional_options=[]):
	out_name= workspace_dir+ "/Workspace_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_" + nom_sample + "_" + syst
	print("Py: Calculating Fit for Systematic:\t" + str(syst) + " with options:\t" + str(additional_options) + " with output filename:\n" + str(out_name))
	s_additional_options=''
	for s_additional_options_single in additional_options:
		s_additional_options=s_additional_options+"_"+s_additional_options_single
	s_additional_options=s_additional_options.replace("-","").replace("+","")
	log=open(out_name+s_additional_options+".log","w")
	print("Over to 2jet_pt_(combined_)fit for " + str(syst) + " ... ")
	if args.combined_fit:
		p=subprocess.Popen(["./build/2jet_pt_combined_fit","-i",input_file,"-o", work_dir, "-r", rlTag,"-d", dataName,"-n", nom_sample, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log) #,stdout=log "--allow_neg_sf_xx"
	else:
		# NB: Havent implemented "-n" in .cxx yet so you probably should if you need it
		p=subprocess.Popen(["./build/2jet_pt_fit", "-i",input_file,"-o", work_dir, "-r", rlTag, "-d", dataName,"-n", nom_sample, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log)
	p.wait()

parser = argparse.ArgumentParser(
    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
parser.add_argument('-c',"--channel",default="emu_OS_J2",  #
                    help='Channel to run over')
parser.add_argument('-t',"--tagger",default="DL1r",
                    help='tagger to run over')
parser.add_argument('-w',"--workingPointName",default="FixedCutBEff",
                    help='workingPointName to run over')
parser.add_argument('-r',"--rlTag",default="r21",
                    help='releaseTag')
parser.add_argument('--rrF', action='store_true',
                    help='rerun all the Fits (default: false)')
parser.add_argument('-d',"--dataName",default=options.data_name,
                    help='dataName')
parser.add_argument('--combined_fit',action='store_true',default=True,
                    help='run with combined fit method.')

# Take info from options file
args = parser.parse_args()
channel=args.channel
tagger=args.tagger
workingPointName=args.workingPointName
rlTag=args.rlTag
dataName=options.data_name
output_dir = options.plot_dir
print("Starting ...")

# A few values for hist plotting
fillColors=[ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
legend_x      = 0.60#0.65
legend_y      = 0.90
legend_width  = 0.28
legend_height = 0.42#0.34

# Declare alt ttbar generator
ttbar_file_to_create_model="FTAG2_ttbar_Sherpa221"
ttbar_samples=options.syst_samples[:]
ttbar_samples.append(options.ttb_sample)
print("We loop over these saamples: ")
for sample in ttbar_samples:
	print(sample.name)

# Set up the directories
workspace_dir = output_dir+"/Workspaces"
fitplots_dir = output_dir+"/FitPlots"
fit_config='_fconst'
if args.combined_fit:
    workspace_dir=workspace_dir+"_3SB"
    fitplots_dir=fitplots_dir+"_3SB"
    fit_config=''
if not os.path.exists(workspace_dir):
    os.makedirs(workspace_dir)
if not os.path.exists(fitplots_dir):
    os.makedirs(fitplots_dir)

# Create the fit plots if they dont exist already
if args.rrF:
	# Run this for first run
    num =1#None # None  # set to the number of workers you want (it defaults to the cpu count of your machine)
    tp = ThreadPool(num)
    for syst_sample in ttbar_samples:
    	# I guess here we create fit plots for the stress test? Who knows, there's no comments any where!!
        if syst_sample.systematic== "ttbar":
            syst_sample.systematic="nominal"
        # Do we need to create this at combine_hists level? Seriously, where does this come from? Theres no stressTest at combine-hists level any where!
        # input_file=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_sample.systematic+"_stressTestMoMc_"+ttbar_file_to_create_model+".root"
        # Otherwise we will use the alt ttbar genator files
        input_file=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+syst_sample.name+"_combination_for_fit_"+syst_sample.systematic+".root"
        syst_name=syst_sample.systematic+"_stressTestMoMc_"+ttbar_file_to_create_model+"_nstat"
        print "calculating Fit for Systematic:", syst_name
        if args.combined_fit:
            p=subprocess.Popen(["./build/2jet_pt_combined_fit","-i",input_file,"-o", output_dir, "-r", rlTag,"-d", dataName,"-n",syst_sample.name, "-s",syst_name,"-c", channel,"-t",tagger,"-w",workingPointName,"--allow_neg_sf_xx"]) #,stdout=log "--allow_neg_sf_xx"
        else:
            print "./build/2jet_pt_fit", "-i",input_file,"-o", output_dir, "-r", rlTag, "-d", dataName, "-s",syst_name,"-c", channel,"-t",tagger,"-w",workingPointName
            p=subprocess.Popen(["./build/2jet_pt_fit", "-i",input_file,"-o", output_dir, "-r", rlTag, "-d", dataName, "-s",syst_name,"-c", channel,"-t",tagger,"-w",workingPointName])
        p.wait()
    print("Finished creating pseudo data for stress test, I think? I have no idea, tip 1, put comments in your code when you write it!")

print("Back in py ... ")
# Open up nominal measurement
nominal_measurement_name = fitplots_dir+"/FinalFitPlots_r21_"+dataName+"_"+tagger+"_FixedCutBEff_"+channel+fit_config+".root"
f_measurment=TFile(nominal_measurement_name,"read") 
print("Getting nominal measurent from:\t" + nominal_measurement_name)
f_measurment.ls()

# Create an output file for the stress test
output_file_name = "stressFits_"+ttbar_file_to_create_model+"_"+tagger+"_"+channel+fit_config+"_combination.root"
f_out=TFile(output_file_name,"recreate")

# Stupid code only works for one WP at a time
wps=["70"]
# Wow what helpful variable names, well done
hists=[]
arrays=[]
for wp in wps:
    col=0
    h_list=[]
    for ttb_sample in ttbar_samples:
        print("Adding:\t" + str(ttb_sample.name))
        if "ttbar" in ttb_sample.systematic:
        	# I am guessing this is what you mean ^^ otherwise your code makes no sense, actually it already does make no sense
            ttb_sample.systematic="nominal"
        # Read the fit plot file created at the start of the code
        f_ttb_path=fitplots_dir+"/FitPlot_r21_"+dataName+"_"+tagger+"_FixedCutBEff_"+channel+fit_config+"_" +ttb_sample.name +"_"+ttb_sample.systematic+"_stressTestMoMc_"+ttbar_file_to_create_model+"_nstat.root"
        f_ttb_sample=TFile(f_ttb_path,"read")
        # Copy a histogram and rename it
        h_e =f_ttb_sample.Get("sf_and_beff/e_b_"+wp+"_Postfit").Clone("e_b_"+wp+"_Postfit_"+ttb_sample.name)
        f_out.cd()
        h_e.SetLineColor(fillColors[col]);
        h_e.SetMarkerColor(fillColors[col]);
        print("bin 9 error " + str(h_e.GetBinError(9)) + " <--- who cares? Seriously what does this achieve? Of all the comments to put ... ")
        if ttb_sample.name==ttbar_file_to_create_model:
            print("Found model sample:\t" + str(ttb_sample.name))
            col=col+1
            h_e.Write()
            h_list.append(f_out.Get(h_e.GetName()))
            h_e_inputfile_model_post=f_out.Get(h_e.GetName())
            # Get the prefit hist from the original file and rename it
            h_e_inputfile_model_pre_name="e_b_"+wp+"_Prefit_"+ttb_sample.name
            h_e_inputfile_model_pre_temp=f_ttb_sample.Get("sf_and_beff/e_b_"+wp+"_Prefit").Clone(h_e_inputfile_model_pre_name)
            f_out.cd()
            h_e_inputfile_model_pre_temp.SetLineWidth(2)
            h_e_inputfile_model_pre_temp.SetLineStyle(2)
            h_e_inputfile_model_pre_temp.Write()
            h_e_inputfile_model_pre=f_out.Get(h_e_inputfile_model_pre_name)
        elif ttb_sample.systematic== "nominal" and not ttb_sample.name=="FTAG2_ttbar_PowHW7":
        	# This all the alternate generators have there systematic as nominal!! This logic makes no sense!?
            h_e.SetLineColor(1);
            h_e.SetMarkerColor(1);
            h_e.Write()
            h_e_nominal_post=f_out.Get(h_e.GetName())
        elif ttb_sample.name=="FTAG2_ttbar_PowHW7":
            print("found sample:\t" + str(ttb_sample.name))
            col=col+1
            h_e.Write()
            h_e_HW7=f_out.Get(h_e.GetName())
            h_list.append(h_e_HW7)
            ROOT.SetOwnership(h_e_HW7,False)
        else:
            col=col+1
            h_e.Write()
            h_list.append(f_out.Get(h_e.GetName()))
            ROOT.SetOwnership(h_e,False)
    # Really impressed with the number of comments, thanks for explaining your logic
    # So after your excellent logic we are left with our model sample and PowHW7, I dont even know if thats what
    # you wanted to achieve because theres no explanation!!!
    f_out.cd()
    h_sf_list=[]
    print("ttbar samples that remain after some weird unexplained logic ... ")
    for h_e in h_list:
    	print(h_e.GetName())
        h_sf=h_e.Clone("sf_"+h_e.GetName());
        ROOT.SetOwnership(h_sf,False)
        h_sf.Add(h_e_inputfile_model_pre,-1);
        h_sf.Divide(h_e_inputfile_model_pre);
        h_sf.SetYTitle("(#epsilon_{b,estimated}-#epsilon_{b,true})-/ #epsilon_{b,true}")
        h_sf.Write()
        h_sf_list.append(h_sf)
    c_obs=TCanvas("c_e_b_"+wp+"_Postfit")
    c_obs.SetLogx()
    c_obs.SetLeftMargin(0.15);
    c_obs.SetRightMargin(0.15);
    c_obs.SetBottomMargin(0.15);
    legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)
    legend.SetTextFont(42)
    c_obs.cd()
    h_e_inputfile_model_post.Draw("LPE")
    h_e_inputfile_model_pre.Draw("HISTSAME");
    legend.AddEntry(h_e_inputfile_model_pre,h_e_inputfile_model_pre.GetName().replace("e_b_"+wp+"_","").replace("_nominal","").replace("FTAG2_ttbar_",""),"L")
    legend.AddEntry(h_e_inputfile_model_post,h_e_inputfile_model_post.GetName().replace("e_b_"+wp+"_Postfit_FTAG2_ttbar_","").replace("_nominal",""),"LPE")
    for h_e in h_list:
        c_obs.cd()
        legend.AddEntry(h_e,h_e.GetName().replace("e_b_"+wp+"_Postfit_FTAG2_ttbar_","").replace("_nominal",""),"L")
        h_e.Draw("HISTSAME");
    legend.Draw()
    h_e_inputfile_model_post.Draw("LPESAME")
    h_e_inputfile_model_pre.Draw("HISTSAME");
    f_out.cd()
    c_obs.RedrawAxis();
    c_obs.Write()
    print("c_e_finished" + " <-- what c_e? and what even is c_e?!!? canvas_something? Thanks for the great variable name")

    c_obs=TCanvas("c_sf_b_"+wp+"_Postfit")
    c_obs.SetLogx()
    c_obs.SetLeftMargin(0.15);
    c_obs.SetRightMargin(0.15);
    c_obs.SetBottomMargin(0.15);
    legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)
    legend.SetTextFont(42)
    c_obs.cd()
    h_f_nominal_stat=h_e_nominal_post.Clone("h_mc_stat_"+h_e_nominal_post.GetName())
    h_f_nominal_stat.Add(h_e_inputfile_model_pre,-1);
    h_f_nominal_stat.Divide(h_e_inputfile_model_pre)
    hists.append(h_f_nominal_stat)
    ROOT.SetOwnership(h_f_nominal_stat,False)
    h_stat=h_e_inputfile_model_post.Clone("h_stat_band")
    h_stat.Reset()
    for i_bin in xrange(1,h_stat.GetNbinsX()+1):
        h_stat.SetBinContent(i_bin,h_e_inputfile_model_post.GetBinError(i_bin)/h_e_inputfile_model_pre.GetBinContent(i_bin))
    h_stat.SetFillColor(ROOT.kGreen -8)
    h_stat.SetLineColor(ROOT.kGreen -8)
    h_stat.SetYTitle("(#epsilon_{b,estimated} - #epsilon_{b,true})/ #epsilon_{b,true}")
    h_stat.GetYaxis().SetRangeUser(-0.2,0.2)
    h_stat_neg=h_stat.Clone("h_stat_band_neg")
    h_stat_neg.Scale(-1)
    h_stat.Draw("HIST")
    h_stat_neg.Draw("HISTSAME")

    #load from file 
    # ttbar_mod_unc=f_measurment.Get("unc_summary_plots/e_b_wp_"+wp+"/e_b_"+wp+"_ttbar_modeling_syst_Error_combi_rel")
    ttbar_mod_unc=f_measurment.Get("unc_summary_plots/e_b_wp_"+wp+"/h_sym_ttbar_combined_"+wp)
    ROOT.SetOwnership(ttbar_mod_unc,False)
    mc_stat_nom=f_measurment.Get("other_systematics/e_b_wp_"+wp+"/e_b_"+wp+"_MC_stat_nominal_syst_Error_rel") 
    ROOT.SetOwnership(mc_stat_nom,False)
    h_f_nominal_tot=h_f_nominal_stat.Clone("h_tot_unc_"+h_e_nominal_post.GetName())
    ROOT.SetOwnership(h_f_nominal_tot,False)
    hists.append(h_f_nominal_tot)

    for i_bin in xrange(1,h_f_nominal_stat.GetNbinsX()+1):
        h_f_nominal_stat.SetBinError(i_bin,mc_stat_nom.GetBinContent(i_bin) )
        tot_unc=mc_stat_nom.GetBinContent(i_bin)**2+ttbar_mod_unc.GetBinContent(i_bin)**2
        h_f_nominal_tot.SetBinError(i_bin,math.sqrt(tot_unc))
    a_nom_mc_stat=arrays_of_TH1(h_f_nominal_stat, shift=-0.03)
    g_nom_mc_stat=TGraphAsymmErrors(a_nom_mc_stat.N_bins, a_nom_mc_stat.a_x_nom, a_nom_mc_stat.a_y_nom, a_nom_mc_stat.a_x_down, a_nom_mc_stat.a_x_up, a_nom_mc_stat.a_y_down, a_nom_mc_stat.a_y_up)
    g_nom_mc_stat.SetName("g_nom_mc_stat_"+wp)
    ROOT.SetOwnership(g_nom_mc_stat,False)
    # Yeah put a ? mark because I have no idea what on earth this code is
    print("?")
    print("^^ I didnt even put this in but it summerises this code .. ")
    g_nom_mc_stat.SetLineWidth(2)
    g_nom_mc_stat.SetFillStyle(0)
    g_nom_mc_stat.Draw("2")
    g_nom_mc_stat.Write()
    g_nom_mc_stat=f_out.Get(g_nom_mc_stat.GetName())
    
    hists.append(g_nom_mc_stat)
    
    #h_f_nominal_tot.Draw("SAME")
    a_nom_tot=arrays_of_TH1(h_f_nominal_tot,bin_size=0,shift=-0.03)
    g_nom_tot=TGraphAsymmErrors(a_nom_tot.N_bins, a_nom_tot.a_x_nom, a_nom_tot.a_y_nom, a_nom_tot.a_x_down, a_nom_tot.a_x_up, a_nom_tot.a_y_down, a_nom_tot.a_y_up)
    g_nom_tot.SetName("g_nom_tot_"+wp)
    ROOT.SetOwnership(g_nom_tot,False)
    g_nom_tot.SetLineWidth(2)
    g_nom_tot.SetMarkerStyle(20)
    g_nom_tot.Draw("P")
    g_nom_tot.Write()
    g_nom_tot=f_out.Get(g_nom_tot.GetName())
    hists.append(g_nom_tot)
    
    legend.AddEntry(h_stat_neg,"simulated data stat","F")
    h_f_HW7_stat=h_e_HW7.Clone("h_mc_stat_"+h_e_HW7.GetName())
    ROOT.SetOwnership(h_f_HW7_stat,False)
    h_f_HW7_stat.Add(h_e_inputfile_model_pre,-1);
    h_f_HW7_stat.Divide(h_e_inputfile_model_pre);
    h_f_HW7_stat.SetYTitle("(#epsilon_{b,estimated}-#epsilon_{b,true})-/ #epsilon_{b,true}")
    h_f_HW7_tot=h_f_HW7_stat.Clone("h_tot_unc_blu"+h_e_HW7.GetName())
    ROOT.SetOwnership(h_f_HW7_tot,False)
    h_f_HW7_stat.Write()
    f_out.ls()
    print "saving:",h_f_HW7_stat.GetName()
    h_f_HW7_stat=f_out.Get(h_f_HW7_stat.GetName()) 
    
    
    mc_stat_HW7=f_measurment.Get("ttbar_systematics/e_b_wp_"+wp+"/smoothing_FTAG2_ttbar_PowHW7/e_b_"+wp+"_FTAG2_ttbar_PowHW7_syst_up_Error_rel_unsmoothed") 
    for i_bin in xrange(1,h_f_HW7_stat.GetNbinsX()+1):
        stat_unc=mc_stat_HW7.GetBinError(i_bin)
        h_f_HW7_stat.SetBinError(i_bin, stat_unc)
        tot_unc=stat_unc**2+ttbar_mod_unc.GetBinContent(i_bin)**2
        h_f_HW7_tot.SetBinError(i_bin,math.sqrt(tot_unc))
    
    legend.AddEntry(h_f_nominal_tot,h_f_nominal_tot.GetName().replace("sf_e_b_"+wp+"_ttbar_","").replace("_nominal",""),"LPE")
    for h_sf in h_sf_list:
        c_obs.cd()
        legend.AddEntry(h_sf,h_sf.GetName().replace("sf_e_b_"+wp+"_Postfit_FTAG2_ttbar_","").replace("_nominal",""),"L")
        h_sf.Draw("HISTSAME");
    h_f_nominal_tot.Draw("HISTSAME")
    legend.Draw()
    line1 = TLine(h_e.GetXaxis().GetXmin(),0,h_e.GetXaxis().GetXmax(),0)
    line1.SetLineWidth(2)
    line1.SetLineStyle(2)
    line1.Draw("SAME")
    f_out.cd()
    c_obs.RedrawAxis();
    c_obs.Write()

print("Finished :)! ")
print("Gz, you have a root file with a bunch of crap which no one knows what it is!")
