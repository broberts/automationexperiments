import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *



#Modification to allow arguments for output directory and plot directory to be provided

parser = argparse.ArgumentParser(
    description='Combines output histograms of final selection stage')
parser.add_argument('-o',"--outdir",default=options.output_dir,
                    help='Directory where final selection output was written to')
parser.add_argument('-p',"--plotdir",default=options.plot_dir,
                    help='Directory for output plots')

args = parser.parse_args()
outdir = args.outdir
plotdir = args.plotdir
dataName=options.data_name


    
#do_bootstrap_global=False
do_bootstrap_global=False
do_bootstrap_ttbar_only=False

# Usage:
#   ./Combine_histograms_for_fit [OPTION...]
#
#       --ttbar_sf file         Nominal ttbar file for sf computation
#       --ttbar_file file       ttbar MC file used for this variation
#       --single_top_file file  single_top MC file used for this variation
#       --diboson_file file     diboson MC file used for this variation
#       --Zjets_file file       Zjets MC file used for this variation
#       --Wjets_file file       Wjets MC file used for this variation
#       --output_file file      output_file name
#       --data_file file        data_file
#       --do_bootstrap          combine with bootstrap weights
#   -h, --help                  Print help


def combine_sample_files(data_file,ttbar_sf,ttbar_file,single_top_file,diboson_file,Zjets_file,Wjets_file,output_file,do_bootstrap=False,do_bootstrap_ttbar_only=False):
    print "combination for outputfile:",  output_file
    if do_bootstrap:
        if do_bootstrap_ttbar_only:
            command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap_ttbar_only"])
            print "command",command
            p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap_ttbar_only"])
        else:
            command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap"])
            print "command",command
            p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap"])
    else:
        command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file])
        print "command",command
        p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file]) #,stdout=log
    p.wait()




if not os.path.exists(plotdir+"combination_sys_"+options.ttb_sample.name+"/"):
    os.makedirs(plotdir+"combination_sys_"+options.ttb_sample.name+"/")
data_name= outdir+dataName+"/"+dataName+"_data_combination.root"
ttb_sf_name= outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"



num_of_cores_to_use=1
tp = ThreadPool(num_of_cores_to_use)

syst="nominal"
ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst+".root"

ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,do_bootstrap_global,do_bootstrap_ttbar_only))

#
# #combine for all tree and inner systemtaics:
syst_to_run_over=options.inner_systematics_in_nominal_tree[:]+options.tree_systematics
if options.fake_estimation:
    syst_to_run_over.append(options.fake_estimation)
for syst_struc in syst_to_run_over:
    syst=syst_struc.name
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst+".root"

    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    if "PunchThrough" in syst:
        singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_nominal_combination.root"
    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(syst_struc.do_bootstrap and do_bootstrap_global),do_bootstrap_ttbar_only))

#combination for ttbar_systemtics:
syst="nominal"
for ttb_syst_sample in options.syst_samples:
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+ttb_syst_sample.name+"_combination_for_fit_"+syst+".root"
    ttb_sample=outdir+ttb_syst_sample.name+"/"+ttb_syst_sample.name+"_"+syst+"_combination.root"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(do_bootstrap_global and ttb_syst_sample.boot_strap_available), do_bootstrap_ttbar_only))


# # #combination for options.rad_samples:
for ttb_syst_sample in options.rad_samples:
    syst=ttb_syst_sample.systematic
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+ttb_syst_sample.name+"_combination_for_fit_"+syst+".root"
    ttb_sample=outdir+ttb_syst_sample.name+"/"+ttb_syst_sample.name+"_"+syst+"_combination.root"
    syst="nominal"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(do_bootstrap_global and ttb_syst_sample.boot_strap_available), do_bootstrap_ttbar_only))

# #combination for options.fsr_samples:
for ttb_syst_sample in options.fsr_samples:
    syst=ttb_syst_sample.systematic
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+ttb_syst_sample.name+"_combination_for_fit_"+syst+".root"
    ttb_sample=outdir+ttb_syst_sample.name+"/"+ttb_syst_sample.name+"_"+syst+"_combination.root"
    syst="nominal"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"
    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(do_bootstrap_global and ttb_syst_sample.boot_strap_available), do_bootstrap_ttbar_only))




# #combination for pdf systemtics::
for syst_ttbar in options.pdf_systematics:
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst_ttbar.name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst_ttbar.name+"_combination.root"
    syst="nominal"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,False))

# #combination for pdf systemtics::
for syst_st in options.pdf_systematics:
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.singeTop_sample.name+"_combination_for_fit_"+syst_st.name+".root"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst_st.name+"_combination.root"
    syst="nominal"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,False))



# #combination for syst_samples_ZJets::
syst="nominal"
for Zjets_sample in options.syst_samples_ZJets:
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+Zjets_sample.name+".root"

    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+Zjets_sample.name+"/"+Zjets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,False))


# #combination for syst_samples_singletop::
syst="nominal"
for singeTop_sample in options.syst_samples_singletop:
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+singeTop_sample.name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
    singeTop_sample=outdir+singeTop_sample.name+"/"+singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,False))


# #combination for syst_samples_Diboson::
syst="nominal"
for syst_sample in options.syst_samples_Diboson:
    ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst_sample.name+".root"

    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
    singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
    Diboson_sample=outdir+syst_sample.name+"/"+syst_sample.name+"_"+syst+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,False))
tp.close()
tp.join()
print "Ende gut alles gut! "
