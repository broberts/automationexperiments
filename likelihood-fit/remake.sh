rm -rf build
mkdir build
cd build
mkdir FitResults/
mkdir FitResults/Workspaces/
mkdir FitResults/FitPlots/
mkdir FitResults/Workspaces_3SB/
mkdir FitResults/FitPlots_3SB/
cmake ..
make
