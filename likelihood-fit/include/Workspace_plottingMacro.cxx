#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TCanvas.h"
#include <string>
#include <sstream>
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "RooFit.h"
#include "TObjString.h"
#include "RooArgList.h"
#include "RooFitResult.h"

#include "../../atlasstyle-00-03-05/AtlasStyle.C"
using namespace std;
int Workspace_plottingMacro(std::string fileName, std::string outputFile_str)
{
	// get latest AtlasStyle package and include it in the repo
	// can be downloaded here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle#ROOT_Style_for_official_ATLAS_pl
	SetAtlasStyle();
	gStyle->SetPaintTextFormat("1.3f");
	gStyle->SetNumberContours(12);

	// ATLAS label, sqrt(s), lumi
	TPaveText *pt = new TPaveText(0.228354, 0.841772, 0.348735, 0.909283, "brNDC");
	pt->SetBorderSize(0);
	pt->SetFillColor(0);
	pt->SetTextSize(0.05);
	pt->SetTextFont(72);
	pt->AddText("ATLAS");

	TPaveText *pt2 = new TPaveText(0.34384, 0.840717, 0.464183, 0.910338, "brNDC");
	pt2->SetBorderSize(0);
	pt2->SetFillColor(0);
	pt2->SetTextSize(0.05);
	pt2->SetTextFont(42);
	pt2->AddText("Internal");

	TPaveText *pt3 = new TPaveText(0.56447, 0.840717, 0.683381, 0.910338, "brNDC");
	pt3->SetBorderSize(0);
	pt3->SetFillColor(0);
	pt3->SetTextSize(0.05);
	pt3->SetTextFont(42);
	pt3->AddText("#sqrt{s} = 13 TeV, 36.1 fb^{-1}");

	TPaveText *pt4 = new TPaveText(0.570201, 0.704641, 0.689112, 0.829114, "brNDC");
	pt4->SetBorderSize(0);
	pt4->SetFillColor(0);
	pt4->SetTextSize(0.04);
	pt4->SetTextFont(42);

	TPaveText *pt5 = new TPaveText(0.501433, 0.71308, 0.620344, 0.820675, "brNDC");
	pt5->SetBorderSize(0);
	pt5->SetFillColor(0);
	pt5->SetTextSize(0.04);
	pt5->SetTextFont(42);

	TPaveText *pt_2d = new TPaveText(0.200401, 0.843882, 0.360745, 0.913502, "brNDC");
	pt_2d->SetBorderSize(0);
	pt_2d->SetFillColor(0);
	pt_2d->SetTextSize(0.05);
	pt_2d->SetTextFont(72);
	pt_2d->AddText("ATLAS");

	TPaveText *pt2_2d = new TPaveText(0.340974, 0.844937, 0.461318, 0.914557, "brNDC");
	pt2_2d->SetBorderSize(0);
	pt2_2d->SetFillColor(0);
	pt2_2d->SetTextSize(0.05);
	pt2_2d->SetTextFont(42);
	pt2_2d->AddText("Internal");

	TPaveText *pt3_2d = new TPaveText(0.548711, 0.847046, 0.667622, 0.916667, "brNDC");
	pt3_2d->SetBorderSize(0);
	pt3_2d->SetFillColor(0);
	pt3_2d->SetTextSize(0.04);
	pt3_2d->SetTextFont(42);
	pt3_2d->AddText("#sqrt{s} = 13 TeV, 36.1 fb^{-1}");

	TPaveText *pt4_2d = new TPaveText(0.342407, 0.695148, 0.461318, 0.81962, "brNDC");
	pt4_2d->SetBorderSize(0);
	pt4_2d->SetFillColor(0);
	pt4_2d->SetTextSize(0.04);
	pt4_2d->SetTextFont(42);

	// Legend - pT plots (prefit/postfit)
	TLegend *legend_fit = new TLegend(0.206304, 0.214135, 0.482808, 0.351266);
	legend_fit->SetTextFont(42);
	legend_fit->SetBorderSize(0);   // no border
	legend_fit->SetFillColor(0);    // Legend background should be white
	legend_fit->SetTextSize(0.04);  // Increase entry font size!

	TLegend *legend_fit_bb = new TLegend(0.206304, 0.214135, 0.482808, 0.351266);
	legend_fit_bb->SetTextFont(42);
	legend_fit_bb->SetBorderSize(0);        // no border
	legend_fit_bb->SetFillColor(0);         // Legend background should be white
	legend_fit_bb->SetTextSize(0.04);       // Increase entry font size!

	TLegend *legend_fit_2d = new TLegend(0.19914, 0.508439, 0.434097, 0.632911);
	legend_fit_2d->SetTextFont(42);
	legend_fit_2d->SetBorderSize(0);        // no border
	legend_fit_2d->SetFillColor(0);         // Legend background should be white
	legend_fit_2d->SetTextSize(0.04);       // Increase entry font size!

	// Legend - tagger output plots (b, l)
	TLegend *legend_data = new TLegend(0.206304, 0.214135, 0.482808, 0.451266);
	legend_data->SetTextFont(42);
	legend_data->SetBorderSize(0);  // no border
	legend_data->SetFillColor(0);   // Legend background should be white
	legend_data->SetTextSize(0.04); // Increase entry font size!

	// List of names
	std::vector<std::string> w_names = { "Prefit", "Postfit" };
	std::vector<std::string> wp_names = { "85", "77", "70", "60" };

	// Load workspace and RooFitResult
	TFile *f_input = new TFile(fileName.c_str(), "READ");
	if (!f_input) {
		std::cerr << "Input file: " << fileName << " not found ... exiting." << std::endl;
		return -1;
	}

	RooFitResult *fitResult = (RooFitResult *)f_input->Get("fitResult");
	RooWorkspace *w_prefit = (RooWorkspace *)f_input->Get("Workspace_before_fit");
	RooWorkspace *w_postfit = (RooWorkspace *)f_input->Get("Workspace_after_fit");
	std::vector<RooWorkspace *> w = { w_prefit, w_postfit };

	// Create output file and directories
	std::string releaseName = ((TObjString *)w_prefit->obj("rlTag"))->GetString().Data();
	std::string taggerName = ((TObjString *)w_prefit->obj("taggerName"))->GetString().Data();
	std::string WPName = ((TObjString *)w_prefit->obj("workingPoint"))->GetString().Data();
	std::string ChannelName = ((TObjString *)w_prefit->obj("channelName"))->GetString().Data();
	std::string fitConfigName = ((TObjString *)w_prefit->obj("dataName"))->GetString().Data();
	std::string systName = ((TObjString *)w_prefit->obj("systName"))->GetString().Data();

	//std::string outputFile_str = "FitPlots_" + releaseName + "_" + taggerName + "_" + WPName + "_" + ChannelName + "_" + fitConfigName + "_" + systName + ".root";
	TFile *f_output = new TFile(outputFile_str.c_str(), "RECREATE");

	TDirectory *d_MCeff = f_output->mkdir("MCeff");
	TDirectory *d_fraction = f_output->mkdir("fractions");
	TDirectory *d_sf_and_beff = f_output->mkdir("sf_and_beff");
	TDirectory *d_pdf = f_output->mkdir("pdf");
	TDirectory *d_tagger_fraction = f_output->mkdir("fraction_times_PDF");
	TDirectory *d_data_MC = f_output->mkdir("data_MC");

	// Get tagger and pT binning used in the fit
	TH2D *h_combinedMC_tagger_pt_l = (TH2D *)w_prefit->obj("hff_MC_combined_light");
	TH2D *h_combinedMC_tagger_pt_b = (TH2D *)w_prefit->obj("hff_MC_combined_b");
	TH2D *h_ttbarMC_tagger_pt_b = (TH2D *)w_prefit->obj("hff_MC_ttb_b");

	TH2D *h_combinedMC_tagger_pt_bb = ((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_bb"))->Projection(3, 1, "E");
	h_combinedMC_tagger_pt_bb->Add(((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_bb"))->Projection(2, 0, "E"));

	TH2D *h_combinedMC_tagger_pt_singleb = ((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_bl"))->Projection(2, 0, "E");
	h_combinedMC_tagger_pt_singleb->Add(((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_lb"))->Projection(3, 1, "E"));

	int Nbin_tagger = h_combinedMC_tagger_pt_l->GetNbinsX();
	int Nbin_pt = h_combinedMC_tagger_pt_l->GetNbinsY();
	int Nwp = Nbin_tagger - 1;

	TH1D *h_tmp_pt = h_combinedMC_tagger_pt_l->ProjectionY();

	double pt_binning[Nbin_pt + 1];
	for (int i_pt = 0; i_pt <= Nbin_pt; i_pt++) pt_binning[i_pt] = h_tmp_pt->GetBinLowEdge(i_pt + 1);
  bool pt_binning_as_eta= (pt_binning[0]<0);
  std::string pt_label_jet1="p_{T,1} [GeV]";
  std::string pt_label_jet2="p_{T,2} [GeV]";
  std::string pt_label_single="p_{T} [GeV]";
  if (pt_binning_as_eta){
      pt_label_jet1="#eta_{1}";
      pt_label_jet2="#eta_{2}";
      pt_label_single="#eta";
  }

	TH2D *h_tmp_2d_pt = new TH2D("h_tmp_2d_pt", "", Nbin_pt, 0., Nbin_pt, Nbin_pt, 0, Nbin_pt);
	for (int i_pt = 0; i_pt < Nbin_pt; i_pt++) {
		h_tmp_2d_pt->GetXaxis()->SetBinLabel(i_pt + 1, (std::to_string((int)pt_binning[i_pt]) + "-" + std::to_string((int)pt_binning[i_pt + 1])).c_str());
		h_tmp_2d_pt->GetYaxis()->SetBinLabel(i_pt + 1, (std::to_string((int)pt_binning[i_pt]) + "-" + std::to_string((int)pt_binning[i_pt + 1])).c_str());
	}

	double tagger_binning[10] = { 100, 85, 77, 70, 60, 0 };
	TH1D *h_tmp_tagger = new TH1D("h_tmp_tagger", "", Nbin_tagger, 0., Nbin_tagger);
	TH1D *h_data_tmp = new TH1D("h_data_tmp", "", Nbin_tagger, 0., Nbin_tagger);

	for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
		h_tmp_tagger->GetXaxis()->SetBinLabel(i_tagger + 1, (std::to_string((int)tagger_binning[i_tagger]) + "-" + std::to_string((int)tagger_binning[i_tagger + 1]) + "%").c_str());
		h_data_tmp->GetXaxis()->SetBinLabel(i_tagger + 1, (std::to_string((int)tagger_binning[i_tagger]) + "-" + std::to_string((int)tagger_binning[i_tagger + 1]) + "%").c_str());
	}

	// get prefit l, b, and b in ttbar efficiencies for each WP with MC stat. unc.
	std::vector<TH1D *> hv_prefit_effMC_l;
	std::vector<TH1D *> hv_prefit_effMC_b;
	std::vector<TH1D *> hv_prefit_effMC_ttbar_b;

	TH1D *h_tot_l = h_combinedMC_tagger_pt_l->ProjectionY("h_tot_l", 0, -1, "E");
	TH1D *h_tot_b = h_combinedMC_tagger_pt_b->ProjectionY("h_tot_b", 0, -1, "E");
	TH1D *h_tot_ttbar_b = h_ttbarMC_tagger_pt_b->ProjectionY("h_tot_ttbar_b", 0, -1, "E");

	for (int i_wp = 0; i_wp < Nwp; i_wp++) {
		TH2D *h_combinedMC_tagger_pt_l_pass = (TH2D *)h_combinedMC_tagger_pt_l->Clone(("h_combinedMC_tagger_pt_l_" + wp_names.at(i_wp) + "wp").c_str());
		TH2D *h_combinedMC_tagger_pt_b_pass = (TH2D *)h_combinedMC_tagger_pt_b->Clone(("h_combinedMC_tagger_pt_b_" + wp_names.at(i_wp) + "wp").c_str());
		TH2D *h_ttbarMC_tagger_pt_b_pass = (TH2D *)h_ttbarMC_tagger_pt_b->Clone(("h_ttbarMC_tagger_pt_b_" + wp_names.at(i_wp) + "wp").c_str());

		// set to 0 bins out from WP definition
		for (int i_tagger = 0; i_tagger < Nbin_tagger - (Nwp - i_wp); i_tagger++) {
			for (int i_pt = 0; i_pt < Nbin_pt; i_pt++) {
				h_combinedMC_tagger_pt_l_pass->SetBinContent(i_tagger + 1, i_pt + 1, 0);
				h_combinedMC_tagger_pt_b_pass->SetBinContent(i_tagger + 1, i_pt + 1, 0);
				h_ttbarMC_tagger_pt_b_pass->SetBinContent(i_tagger + 1, i_pt + 1, 0);
				h_combinedMC_tagger_pt_l_pass->SetBinError(i_tagger + 1, i_pt + 1, 0);
				h_combinedMC_tagger_pt_b_pass->SetBinError(i_tagger + 1, i_pt + 1, 0);
				h_ttbarMC_tagger_pt_b_pass->SetBinError(i_tagger + 1, i_pt + 1, 0);
			}
		}

		TH1D *h_effMC_l = h_combinedMC_tagger_pt_l_pass->ProjectionY(("h_effMC_l_" + wp_names.at(i_wp) + "wp").c_str(), 0, -1, "E");
		TH1D *h_effMC_b = h_combinedMC_tagger_pt_b_pass->ProjectionY(("h_effMC_b_" + wp_names.at(i_wp) + "wp").c_str(), 0, -1, "E");
		TH1D *h_effMC_ttbar_b = h_ttbarMC_tagger_pt_b_pass->ProjectionY(("h_effMC_ttbar_b_" + wp_names.at(i_wp) + "wp").c_str(), 0, -1, "E");

		h_effMC_l->Divide(h_effMC_l, h_tot_l, 1, 1, "B");
		h_effMC_b->Divide(h_effMC_b, h_tot_b, 1, 1, "B");
		h_effMC_ttbar_b->Divide(h_effMC_ttbar_b, h_tot_ttbar_b, 1, 1, "B");

		hv_prefit_effMC_l.push_back(h_effMC_l);
		hv_prefit_effMC_b.push_back(h_effMC_b);
		hv_prefit_effMC_ttbar_b.push_back(h_effMC_ttbar_b);

		f_output->cd();
		d_MCeff->cd();
		h_effMC_l->Write();
		h_effMC_b->Write();
		h_effMC_ttbar_b->Write();
	}

	// get prefit for ll, bb, bl and lb fractions in pT1,pT2
	TH2D *h_ll = ((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_ll"))->Projection(3, 2, "E");
	TH2D *h_bb = ((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_bb"))->Projection(3, 2, "E");
	TH2D *h_bl = ((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_bl"))->Projection(3, 2, "E");
	TH2D *h_lb = ((THnSparseT<TArrayD> *)w_prefit->obj("hf4_MC_combined_lb"))->Projection(3, 2, "E");
	h_ll->Sumw2();
	h_bb->Sumw2();
	h_bl->Sumw2();
	h_lb->Sumw2();

	// get data counts in 4D (tagger_2, tagger_1, pT_2, pT_1)
	THnSparseT<TArrayD> *h_data = (THnSparseT<TArrayD> *)w_prefit->obj("hf4_data");
	TH2D *h_data_pt1_pt2_tmp = (TH2D *)h_data->Projection(3, 2, "E");
	TH2D *h_data_pt1_pt2 = (TH2D *)h_tmp_2d_pt->Clone("h_data_pt1_pt2");
	h_data_pt1_pt2->SetStats(0);
	h_data_pt1_pt2->Reset();
	h_data_pt1_pt2->SetTitle("");
	h_data_pt1_pt2->SetXTitle(pt_label_jet1.c_str());
	h_data_pt1_pt2->SetYTitle(pt_label_jet2.c_str());
	h_data_pt1_pt2->SetZTitle("N_{data}");
	h_data_pt1_pt2->GetXaxis()->SetLabelSize(0.05);
	h_data_pt1_pt2->GetXaxis()->SetTitleSize(0.05);
	h_data_pt1_pt2->GetXaxis()->SetTitleOffset(1.55);
	h_data_pt1_pt2->GetXaxis()->SetLabelOffset(0.01);
	h_data_pt1_pt2->GetYaxis()->SetLabelSize(0.05);
	h_data_pt1_pt2->GetYaxis()->SetTitleSize(0.05);
	h_data_pt1_pt2->GetYaxis()->SetTitleOffset(1.55);
	h_data_pt1_pt2->GetZaxis()->SetTitleOffset(1.3);
	h_data_pt1_pt2->SetMarkerColor(kBlack);
	h_data_pt1_pt2->SetMarkerSize(1);
	h_data_pt1_pt2->SetFillColor(kBlack);
	h_data_pt1_pt2->SetLineColor(kBlack);
	h_data_pt1_pt2->SetBarOffset(+0.22);


	TH2D *h_mc_prefit_pt1_pt2_tmp = (TH2D *)w_prefit->obj("hf4_MC_tot_proj_2_3")->Clone("h_mc_prefit_pt1_pt2");
	TH2D *h_mc_prefit_pt1_pt2 = (TH2D *)h_tmp_2d_pt->Clone("h_mc_prefit_pt1_pt2");
	h_mc_prefit_pt1_pt2->SetStats(0);
	h_mc_prefit_pt1_pt2->Reset();
	h_mc_prefit_pt1_pt2->SetTitle("");
	h_mc_prefit_pt1_pt2->SetXTitle(pt_label_jet1.c_str());
	h_mc_prefit_pt1_pt2->SetYTitle(pt_label_jet2.c_str());
	h_mc_prefit_pt1_pt2->SetZTitle("N_{mc_prefit}");
	h_mc_prefit_pt1_pt2->GetXaxis()->SetLabelSize(0.05);
	h_mc_prefit_pt1_pt2->GetXaxis()->SetTitleSize(0.05);
	h_mc_prefit_pt1_pt2->GetXaxis()->SetTitleOffset(1.55);
	h_mc_prefit_pt1_pt2->GetXaxis()->SetLabelOffset(0.01);
	h_mc_prefit_pt1_pt2->GetYaxis()->SetLabelSize(0.05);
	h_mc_prefit_pt1_pt2->GetYaxis()->SetTitleSize(0.05);
	h_mc_prefit_pt1_pt2->GetYaxis()->SetTitleOffset(1.55);
	h_mc_prefit_pt1_pt2->GetZaxis()->SetTitleOffset(1.3);
	h_mc_prefit_pt1_pt2->SetMarkerColor(kBlack);
	h_mc_prefit_pt1_pt2->SetMarkerSize(1);
	h_mc_prefit_pt1_pt2->SetFillColor(kBlack);
	h_mc_prefit_pt1_pt2->SetLineColor(kBlack);
	h_mc_prefit_pt1_pt2->SetBarOffset(+0.22);

	TH2D *h_mc_postfit_pt1_pt2 = (TH2D *)h_tmp_2d_pt->Clone("h_mc_postfit_pt1_pt2");
	h_mc_postfit_pt1_pt2->SetStats(0);
	h_mc_postfit_pt1_pt2->Reset();
	h_mc_postfit_pt1_pt2->SetTitle("");
	h_mc_postfit_pt1_pt2->SetXTitle(pt_label_jet1.c_str());
	h_mc_postfit_pt1_pt2->SetYTitle(pt_label_jet2.c_str());
	h_mc_postfit_pt1_pt2->SetZTitle("N_{mc_postfit}");
	h_mc_postfit_pt1_pt2->GetXaxis()->SetLabelSize(0.05);
	h_mc_postfit_pt1_pt2->GetXaxis()->SetTitleSize(0.05);
	h_mc_postfit_pt1_pt2->GetXaxis()->SetTitleOffset(1.55);
	h_mc_postfit_pt1_pt2->GetXaxis()->SetLabelOffset(0.01);
	h_mc_postfit_pt1_pt2->GetYaxis()->SetLabelSize(0.05);
	h_mc_postfit_pt1_pt2->GetYaxis()->SetTitleSize(0.05);
	h_mc_postfit_pt1_pt2->GetYaxis()->SetTitleOffset(1.55);
	h_mc_postfit_pt1_pt2->GetZaxis()->SetTitleOffset(1.3);
	h_mc_postfit_pt1_pt2->SetMarkerColor(kRed + 1);
	h_mc_postfit_pt1_pt2->SetMarkerSize(1);
	h_mc_postfit_pt1_pt2->SetFillColor(kRed + 1);
	h_mc_postfit_pt1_pt2->SetLineColor(kRed + 1);
	h_mc_postfit_pt1_pt2->SetBarOffset(-0.22);

	for (int i_pt1 = 0; i_pt1 < Nbin_pt; i_pt1++) {
		for (int i_pt2 = 0; i_pt2 <= i_pt1; i_pt2++) {
			h_data_pt1_pt2->SetBinContent(i_pt1 + 1, i_pt2 + 1, h_data_pt1_pt2_tmp->GetBinContent(i_pt1 + 1, i_pt2 + 1));
			h_data_pt1_pt2->SetBinError(i_pt1 + 1, i_pt2 + 1, h_data_pt1_pt2_tmp->GetBinError(i_pt1 + 1, i_pt2 + 1));

			h_mc_prefit_pt1_pt2->SetBinContent(i_pt1 + 1, i_pt2 + 1, h_mc_prefit_pt1_pt2_tmp->GetBinContent(i_pt1 + 1, i_pt2 + 1));
			h_mc_prefit_pt1_pt2->SetBinError(i_pt1 + 1, i_pt2 + 1, h_mc_prefit_pt1_pt2_tmp->GetBinError(i_pt1 + 1, i_pt2 + 1));

			h_mc_postfit_pt1_pt2->SetBinContent(i_pt1 + 1, i_pt2 + 1, w_postfit->var(("N_pt_" + to_string(i_pt1 + 1) + "_" + to_string(i_pt2 + 1)).c_str())->getValV());
			h_mc_postfit_pt1_pt2->SetBinError(i_pt1 + 1, i_pt2 + 1, w_postfit->var(("N_pt_" + to_string(i_pt1 + 1) + "_" + to_string(i_pt2 + 1)).c_str())->getPropagatedError(*fitResult));
		}
	}

	// record histogram
	f_output->cd();
	h_data_pt1_pt2->Write();
	h_mc_prefit_pt1_pt2->Write();
	h_mc_postfit_pt1_pt2->Write();

	TCanvas *c_data_pt1_pt2 = new TCanvas("c_data_pt1_pt2");
	c_data_pt1_pt2->SetLeftMargin(0.15);
	c_data_pt1_pt2->SetRightMargin(0.20);
	h_data_pt1_pt2->Draw("TEXTCOLZE");
	h_mc_postfit_pt1_pt2->Draw("TEXTESAME");
	h_data_pt1_pt2->Draw("AXISSAME");
	pt_2d->Draw();
	pt2_2d->Draw();
	pt3_2d->Draw();
	pt4_2d->Clear();
	pt4_2d->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
	pt4_2d->Draw();
	legend_fit_2d->Clear();
	legend_fit_2d->AddEntry(h_data_pt1_pt2, "Data", "F");
	legend_fit_2d->AddEntry(h_mc_postfit_pt1_pt2, "MC Postfit", "F");
	legend_fit_2d->Draw();
	f_output->cd();
	c_data_pt1_pt2->Write();

	TCanvas *c_mc_pt1_pt2 = new TCanvas("c_mc_pt1_pt2");
	c_mc_pt1_pt2->SetLeftMargin(0.15);
	c_mc_pt1_pt2->SetRightMargin(0.20);
	h_mc_prefit_pt1_pt2->Draw("TEXTCOLZE");
	h_mc_postfit_pt1_pt2->Draw("TEXTESAME");
	h_mc_prefit_pt1_pt2->Draw("AXISSAME");
	pt_2d->Draw();
	pt2_2d->Draw();
	pt3_2d->Draw();
	pt4_2d->Clear();
	pt4_2d->AddText((releaseName + ", " + ChannelName + ", " + fitConfigName).c_str());
	pt4_2d->Draw();
	legend_fit_2d->Clear();
	legend_fit_2d->AddEntry(h_mc_prefit_pt1_pt2, "MC Prefit", "F");
	legend_fit_2d->AddEntry(h_mc_postfit_pt1_pt2, "MC Postfit", "F");
	legend_fit_2d->Draw();
	f_output->cd();
	c_mc_pt1_pt2->Write();
	legend_fit_2d->Clear();

	// symmetrization of the diagonal
	for (int i_pt = 0; i_pt < Nbin_pt; i_pt++) {
		double diagonal_value = h_bl->GetBinContent(i_pt + 1, i_pt + 1) + h_lb->GetBinContent(i_pt + 1, i_pt + 1);
		double diagonal_unc = sqrt(h_bl->GetBinError(i_pt + 1, i_pt + 1) * h_bl->GetBinError(i_pt + 1, i_pt + 1) +
					   h_lb->GetBinError(i_pt + 1, i_pt + 1) * h_lb->GetBinError(i_pt + 1, i_pt + 1));

		h_bl->SetBinContent(i_pt + 1, i_pt + 1, diagonal_value / 2.);
		h_bl->SetBinError(i_pt + 1, i_pt + 1, diagonal_unc / sqrt(2.));

		h_lb->SetBinContent(i_pt + 1, i_pt + 1, diagonal_value / 2.);
		h_lb->SetBinError(i_pt + 1, i_pt + 1, diagonal_unc / sqrt(2.));
	}

	TH2D *h_sum = (TH2D *)h_ll->Clone("h_sum");
	h_sum->Add(h_bb);
	h_sum->Add(h_bl);
	h_sum->Add(h_lb);

	h_ll->Divide(h_ll, h_sum, 1, 1, "B");
	h_bb->Divide(h_bb, h_sum, 1, 1, "B");
	h_bl->Divide(h_bl, h_sum, 1, 1, "B");
	h_lb->Divide(h_lb, h_sum, 1, 1, "B");

	// 1D and 2D plots vs pT and [pT1,pT2] (prefit, postfit): 1D-> SF-b, eff-b, 2D-> f_bb, f_bl, f_lb, f_ll
	std::cout << "pT plots ...";
	std::vector<std::string> obs_pt_names = { "sf_b", "e_b" };
	std::vector<std::string> obs_pt_labels = { "b-efficiency SF (data / t#bar{t} MC ratio)", "b-efficiency" };
	std::vector<std::string> obs_2d_pt_names = { "f_bb", "f_bl", "f_lb", "f_ll" };
	std::vector<std::string> obs_2d_pt_labels = { "bb event fraction", "bl + bc event fraction", "lb + cb event fraction", "ll + cc fraction" };

	for (int i_wp = 0; i_wp < Nwp; i_wp++) {
		// 2D plots
		for (int i_2d_plots = 0; i_2d_plots < obs_2d_pt_names.size(); i_2d_plots++) {
			// no WP needed
			if (i_wp != 0) continue;

			// create new template 2D pT canvas (combine prefit+postfit)
			std::string canvasname = "c_" + obs_2d_pt_names.at(i_2d_plots);
			TCanvas *c_obs = new TCanvas(canvasname.c_str());
			//c_obs->SetLogx();
			//c_obs->SetLogy();
			c_obs->SetLeftMargin(0.15);
			c_obs->SetRightMargin(0.15);

			for (int i_w = 0; i_w < w.size(); i_w++) { // prefit+postfit
				// create new template 2D pT histogram
				std::string histname = obs_2d_pt_names.at(i_2d_plots) + "_" + w_names.at(i_w);
				TH2D *h_obs = (TH2D *)h_tmp_2d_pt->Clone(histname.c_str());
				h_obs->SetStats(0);
				h_obs->Reset();
				h_obs->SetTitle("");
				h_obs->SetXTitle(pt_label_jet1.c_str());
				h_obs->SetYTitle(pt_label_jet2.c_str());
				h_obs->SetZTitle(obs_2d_pt_labels.at(i_2d_plots).c_str());
				h_obs->GetXaxis()->SetLabelSize(0.05);
				h_obs->GetXaxis()->SetTitleSize(0.05);
				h_obs->GetXaxis()->SetTitleOffset(1.55);
				h_obs->GetXaxis()->SetLabelOffset(0.01);
				h_obs->GetYaxis()->SetLabelSize(0.05);
				h_obs->GetYaxis()->SetTitleSize(0.05);
				h_obs->GetYaxis()->SetTitleOffset(1.55);
				h_obs->GetZaxis()->SetTitleOffset(0.95);

				if (w_names.at(i_w) == "Prefit") {
					h_obs->SetMarkerColor(kBlack);
					h_obs->SetFillColor(kBlack);
					h_obs->SetLineColor(kBlack);
					h_obs->SetBarOffset(+0.22);
				} else {
					h_obs->SetMarkerColor(kRed + 1);
					h_obs->SetFillColor(kRed + 1);
					h_obs->SetLineColor(kRed + 1);
					h_obs->SetBarOffset(-0.22);
				}

				if (i_w == 0 && i_2d_plots == 0) legend_fit_2d->AddEntry(h_obs, "Prefit", "F");
				else if (i_w == 1 && i_2d_plots == 0) legend_fit_2d->AddEntry(h_obs, "Postfit", "F");

				// fill histogram
				for (int i_pt1 = 0; i_pt1 < Nbin_pt; i_pt1++) {
					for (int i_pt2 = 0; i_pt2 <= i_pt1; i_pt2++) {
						// obs
						std::string obs_name = obs_2d_pt_names.at(i_2d_plots) + "_pt_" + std::to_string(i_pt1 + 1) + "_" + std::to_string(i_pt2 + 1);
						RooFormulaVar *f_xx = (RooFormulaVar *)w.at(i_w)->function(obs_name.c_str());

						double obs = f_xx->getValV();
						double obs_unc = 0.00000001;

						if (i_w == 0 || !(f_xx->getPropagatedError(*fitResult))) {
							if (obs_2d_pt_names.at(i_2d_plots).find("_ll") != std::string::npos) obs_unc = h_ll->GetBinError(i_pt1 + 1, i_pt2 + 1);
							else if (obs_2d_pt_names.at(i_2d_plots).find("_bb") != std::string::npos) obs_unc = h_bb->GetBinError(i_pt1 + 1, i_pt2 + 1);
							else if (obs_2d_pt_names.at(i_2d_plots).find("_bl") != std::string::npos) obs_unc = h_bl->GetBinError(i_pt1 + 1, i_pt2 + 1);
							else if (obs_2d_pt_names.at(i_2d_plots).find("_lb") != std::string::npos) obs_unc = h_lb->GetBinError(i_pt1 + 1, i_pt2 + 1);
						} else if (i_w == 1) {
							obs_unc = f_xx->getPropagatedError(*fitResult);
						}

						h_obs->SetBinContent(i_pt1 + 1, i_pt2 + 1, obs);
						h_obs->SetBinError(i_pt1 + 1, i_pt2 + 1, obs_unc);

						//if(obs_2d_pt_names.at(i_2d_plots).find("_lb") !=std::string::npos) std::cout << i_pt1+1 << " " << i_pt2+1 << " " << h_lb->GetBinContent(i_pt1+1, i_pt2+1) - obs << std::endl;
					}
				}

				// record histogram
				f_output->cd();
				d_fraction->cd();
				h_obs->Write();

				// draw on canvas
				c_obs->cd();
				if (w_names.at(i_w) == "Prefit") h_obs->Draw("TEXTCOLZE");
				else h_obs->Draw("TEXTESAME");
				h_obs->Draw("AXISSAME");
			} // end prefit+postfit

			// tune and record canvas
			c_obs->cd();
			pt_2d->Draw();
			pt2_2d->Draw();
			pt3_2d->Draw();
			pt4_2d->Clear();
			pt4_2d->AddText((releaseName + ", " + ChannelName + ", " + systName + ", " + fitConfigName).c_str());
			pt4_2d->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_names.at(i_wp) + "%").c_str());
			pt4_2d->Draw();
			legend_fit_2d->Draw();
			f_output->cd();
			d_fraction->cd();
			c_obs->Write();
		} // end 2D plots

		// 1D plots
		for (int i_plots = 0; i_plots < obs_pt_names.size(); i_plots++) {
			// create new template pT canvas (combine prefit+postfit)
			std::string canvasname = "c_" + obs_pt_names.at(i_plots) + "_" + wp_names.at(i_wp);
			TCanvas *c_obs = new TCanvas(canvasname.c_str());
            if (h_tmp_pt->GetBinLowEdge(1)>0){
                c_obs->SetLogx();
            }

			for (int i_w = 0; i_w < w.size(); i_w++) { // prefit+postfit
				// create new template 1D pT histogram
				std::string histname = obs_pt_names.at(i_plots) + "_" + wp_names.at(i_wp) + "_" + w_names.at(i_w);
				TH1D *h_obs = (TH1D *)h_tmp_pt->Clone(histname.c_str());
				h_obs->SetStats(0);
				h_obs->Reset();
				h_obs->SetTitle("");
				h_obs->SetXTitle(pt_label_single.c_str());
				h_obs->SetYTitle(obs_pt_labels.at(i_plots).c_str());
				h_obs->SetMaximum(1.5);
				h_obs->SetMinimum(0);
				h_obs->GetXaxis()->SetLabelSize(0.05);
				h_obs->GetXaxis()->SetTitleSize(0.05);
				h_obs->GetXaxis()->SetTitleOffset(1.4);
				h_obs->SetMarkerStyle(20);

				if (i_w == 0 && i_plots == 0 && i_wp == 0) {
					legend_fit->AddEntry(h_obs, "Prefit", "LPE");
					legend_fit_bb->AddEntry(h_obs, "Prefit", "LPE");
				} else if (i_w == 1 && i_plots == 0 && i_wp == 0) {
					legend_fit->AddEntry(h_obs, "Postfit", "LPE");
					legend_fit_bb->AddEntry(h_obs, "Postfit", "LPE");
				}

				if (w_names.at(i_w) == "Prefit") {
					h_obs->SetLineColor(kGreen - 8);
					h_obs->SetMarkerColor(kGreen - 8);
				} else {
					h_obs->SetLineColor(kRed + 1);
					h_obs->SetMarkerColor(kRed + 1);
				}

				// fill histogram
				for (int i_pt = 0; i_pt < Nbin_pt; i_pt++) {
					std::string obs_name = obs_pt_names.at(i_plots) + "_pt_" + std::to_string(i_pt + 1) + "_mv2c10_" + std::to_string(i_wp + 2);
					double obs = ((RooFormulaVar *)w.at(i_w)->function(obs_name.c_str()))->getValV();

					double obs_unc = 0.00000001;
					if (i_w == 0 || !((RooFormulaVar *)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult)) obs_unc = obs * hv_prefit_effMC_b.at(i_wp)->GetBinError(i_pt + 1) / hv_prefit_effMC_b.at(i_wp)->GetBinContent(i_pt + 1);
					else if (i_w == 1) obs_unc = ((RooFormulaVar *)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult);

					h_obs->SetBinContent(i_pt + 1, obs);
					h_obs->SetBinError(i_pt + 1, obs_unc);
				}

				// record histogram
				f_output->cd();
				d_sf_and_beff->cd();
				h_obs->Write();

				// draw on canvas
				c_obs->cd();
				h_obs->Draw("LPESAME");
			} // end prefit+postfit

			// tune and record canvas
			c_obs->cd();
			pt->Draw();
			pt2->Draw();
			pt3->Draw();
			pt4->Clear();
			pt4->AddText((releaseName + ", " + ChannelName + ", " + systName + ", " + fitConfigName).c_str());
			pt4->AddText((taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp_names.at(i_wp) + "%").c_str());
			pt4->Draw();
			legend_fit->Draw();
			f_output->cd();
			d_sf_and_beff->cd();
			c_obs->Write();
		}       // end 1D plots
	}               // end wp

	std::cout << " done." << std::endl;

	// Plots vs tagger output (prefit, postfit): b PDF, l+c PDF
	std::cout << "tagger output plots ...";
	std::vector<std::string> obs_tagger_names = { "p_b", "p_l" };
	std::vector<std::string> obs_tagger_labels = { "b-jet PDF", "c- + light-jet PDF" };

	for (int i_pt = 0; i_pt < Nbin_pt; i_pt++) {
		// average b-PDF from bb MC events for extra reference
		std::string histname_bb = "p_b_bb_pt_" + std::to_string(i_pt + 1) + "_MC";
		TH1D *p_b_bb_tmp = h_combinedMC_tagger_pt_bb->ProjectionX((histname_bb + "_tmp").c_str(), i_pt + 1, i_pt + 1, "E");

		TH1D *p_b_bb_tmp_all = (TH1D *)p_b_bb_tmp->Clone((histname_bb + "_tmp_all").c_str());

		double error = 0;
		double integral = p_b_bb_tmp->IntegralAndError(1, p_b_bb_tmp->GetNbinsX(), error);
		for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
			p_b_bb_tmp_all->SetBinContent(i_tagger + 1, integral);
			p_b_bb_tmp_all->SetBinError(i_tagger + 1, error);
		}
		p_b_bb_tmp->Divide(p_b_bb_tmp, p_b_bb_tmp_all, 1., 1., "B");

		TH1D *p_b_bb = (TH1D *)h_tmp_tagger->Clone(histname_bb.c_str());
		p_b_bb->SetStats(0);
		p_b_bb->Reset();
		p_b_bb->SetLineStyle(2);
		p_b_bb->SetLineColor(kGray + 2);
		p_b_bb->SetMarkerStyle(20);
		p_b_bb->SetMarkerSize(0);

		// average b-PDF from single b (bl+lb) MC events for extra reference
		std::string histname_singleb = "p_b_singleb_pt_" + std::to_string(i_pt + 1) + "_MC";
		TH1D *p_b_singleb_tmp = h_combinedMC_tagger_pt_singleb->ProjectionX((histname_singleb + "_tmp").c_str(), i_pt + 1, i_pt + 1, "E");

		TH1D *p_b_singleb_tmp_all = (TH1D *)p_b_singleb_tmp->Clone((histname_singleb + "_tmp_all").c_str());

		error = 0;
		integral = p_b_singleb_tmp->IntegralAndError(1, p_b_singleb_tmp->GetNbinsX(), error);
		for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
			p_b_singleb_tmp_all->SetBinContent(i_tagger + 1, integral);
			p_b_singleb_tmp_all->SetBinError(i_tagger + 1, error);
		}
		p_b_singleb_tmp->Divide(p_b_singleb_tmp, p_b_singleb_tmp_all, 1., 1., "B");

		TH1D *p_b_singleb = (TH1D *)h_tmp_tagger->Clone(histname_singleb.c_str());
		p_b_singleb->SetStats(0);
		p_b_singleb->Reset();
		p_b_singleb->SetLineStyle(2);
		p_b_singleb->SetLineColor(kBlue + 1);
		p_b_singleb->SetMarkerStyle(20);
		p_b_singleb->SetMarkerSize(0);

		// average b-PDF from ttbar MC events (flavor of accompanying jet does not matter)
		// for extra reference
		std::string histname_ttb = "p_b_ttb_pt_" + std::to_string(i_pt + 1) + "_MC";
		TH1D *p_b_ttb_tmp = h_ttbarMC_tagger_pt_b->ProjectionX((histname_ttb + "_tmp").c_str(), i_pt + 1, i_pt + 1, "E");
		TH1D *p_b_ttb_tmp_all = (TH1D *)p_b_ttb_tmp->Clone((histname_ttb + "_tmp_all").c_str());

		error = 0;
		integral = p_b_ttb_tmp->IntegralAndError(1, p_b_ttb_tmp->GetNbinsX(), error);

		for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
			p_b_ttb_tmp_all->SetBinContent(i_tagger + 1, integral);
			p_b_ttb_tmp_all->SetBinError(i_tagger + 1, error);
		}
		p_b_ttb_tmp->Divide(p_b_ttb_tmp, p_b_ttb_tmp_all, 1., 1., "B");

		TH1D *p_b_ttb = (TH1D *)h_tmp_tagger->Clone(histname_ttb.c_str());
		p_b_ttb->SetStats(0);
		p_b_ttb->Reset();
		p_b_ttb->SetLineStyle(2);
		p_b_ttb->SetLineColor(kOrange - 8);
		p_b_ttb->SetMarkerStyle(20);
		p_b_ttb->SetMarkerSize(0);

		// Fill of all 3 histograms
		for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
			p_b_bb->SetBinContent(i_tagger + 1, p_b_bb_tmp->GetBinContent(i_tagger + 1));
			p_b_bb->SetBinError(i_tagger + 1, p_b_bb_tmp->GetBinError(i_tagger + 1));

			p_b_singleb->SetBinContent(i_tagger + 1, p_b_singleb_tmp->GetBinContent(i_tagger + 1));
			p_b_singleb->SetBinError(i_tagger + 1, p_b_singleb_tmp->GetBinError(i_tagger + 1));

			p_b_ttb->SetBinContent(i_tagger + 1, p_b_ttb_tmp->GetBinContent(i_tagger + 1));
			p_b_ttb->SetBinError(i_tagger + 1, p_b_ttb_tmp->GetBinError(i_tagger + 1));
		}

		f_output->cd();
		d_pdf->cd();
		p_b_bb->Write();
		p_b_singleb->Write();
		p_b_ttb->Write();

		for (int i_plots = 0; i_plots < obs_tagger_names.size(); i_plots++) {
			// create new template tagger canvas (combine prefit+postfit)
			std::string canvasname = "c_" + obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt + 1);
			TCanvas *c_obs = new TCanvas(canvasname.c_str());
			c_obs->SetLogy();

			for (int i_w = 0; i_w < w.size(); i_w++) { // prefit+postfit
				// create new template tagger histogram
				std::string histname = obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt + 1) + "_" + w_names.at(i_w);
				TH1D *h_obs = (TH1D *)h_tmp_tagger->Clone(histname.c_str());
				h_obs->SetStats(0);
				h_obs->Reset();
				h_obs->SetTitle("");
				h_obs->SetXTitle((taggerName + " output").c_str());
				h_obs->SetYTitle(obs_tagger_labels.at(i_plots).c_str());
				h_obs->SetMaximum(15);
				if (obs_tagger_names.at(i_plots) == "p_b") h_obs->SetMaximum(1.5);
				//h_obs->SetMinimum(0);
				h_obs->GetXaxis()->SetLabelSize(0.05);
				h_obs->GetXaxis()->SetTitleSize(0.05);
				h_obs->GetXaxis()->SetTitleOffset(1.4);
				h_obs->SetMarkerStyle(20);

				//if(i_w==0 && i_plots==0) legend_fit->AddEntry(h_obs,"Prefit", "LPE");
				//else if(i_w==1 && i_plots==0) legend_fit->AddEntry(h_obs,"Postfit", "LPE");
				if (i_pt == 0 && i_w == 0 && i_plots == 0) legend_fit_bb->AddEntry(p_b_bb, "Prefit from bb events", "L");
				if (i_pt == 0 && i_w == 0 && i_plots == 0) legend_fit_bb->AddEntry(p_b_singleb, "Prefit from single b events", "L");
				if (i_pt == 0 && i_w == 0 && i_plots == 0) legend_fit_bb->AddEntry(p_b_ttb, "Prefit from t#bar{t} events", "L");

				if (w_names.at(i_w) == "Prefit") {
					h_obs->SetLineColor(kGreen - 8);
					h_obs->SetMarkerColor(kGreen - 8);
				} else {
					h_obs->SetLineColor(kRed + 1);
					h_obs->SetMarkerColor(kRed + 1);
				}

				// fill histogram
				for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
					std::string obs_name = obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt + 1) + "_mv2c10_" + std::to_string(i_tagger + 1);
					double obs = ((RooFormulaVar *)w.at(i_w)->function(obs_name.c_str()))->getValV();

					double obs_unc = 0.00000001;
					if (i_w == 0 || !((RooFormulaVar *)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult)) {
						if (obs_tagger_names.at(i_plots) == "p_b") obs_unc = obs * h_combinedMC_tagger_pt_b->GetBinError(i_tagger + 1, i_pt + 1) / h_combinedMC_tagger_pt_b->GetBinContent(i_tagger + 1, i_pt + 1);
						else if (obs_tagger_names.at(i_plots) == "p_l") obs_unc = obs * h_combinedMC_tagger_pt_l->GetBinError(i_tagger + 1, i_pt + 1) / h_combinedMC_tagger_pt_l->GetBinContent(i_tagger + 1, i_pt + 1);
					} else if (i_w == 1) {
						obs_unc = ((RooFormulaVar *)w.at(i_w)->function(obs_name.c_str()))->getPropagatedError(*fitResult);
					}

					h_obs->SetBinContent(i_tagger + 1, obs / h_obs->GetBinWidth(i_tagger + 1));     // PDF
					h_obs->SetBinError(i_tagger + 1, obs_unc / h_obs->GetBinWidth(i_tagger + 1));   // PDF
				}

				// record histogram
				f_output->cd();
				d_pdf->cd();
				h_obs->Write();

				// draw on canvas
				c_obs->cd();
				h_obs->Draw("LPESAME");
				if (w_names.at(i_w) == "Prefit" && obs_tagger_names.at(i_plots) == "p_b") {
					p_b_bb->Draw("LESAME");         // b-PDF from bb event in MC as extra reference
					p_b_singleb->Draw("LESAME");    // b-PDF from single event in MC as extra reference
					p_b_ttb->Draw("LESAME");        // b-PDF from ttbar event in MC as extra reference
				}
				h_obs->Draw("LPESAME");
			} // end prefit+postfit

			// tune and record canvas
			c_obs->cd();
			pt->Draw();
			pt2->Draw();
			pt3->Draw();
			pt4->Clear();
			pt4->AddText((releaseName + ", " + ChannelName + ", " + systName + ", " + fitConfigName).c_str());
			pt4->AddText((taggerName + ", " + WPName + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt + 1)) + " GeV < p_{T}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt + 2)) + " GeV").c_str());
			pt4->Draw();
			if (obs_tagger_names.at(i_plots) == "p_b") legend_fit_bb->Draw();
			else legend_fit->Draw();
			f_output->cd();
			d_pdf->cd();
			c_obs->Write();
		}       // end plots
	}               // end pT

	// Plots vs tagger output (prefit, postfit) - YIELDS (pT1, pT2): b yield, l+c yield
	std::vector<std::string> obs_tagger_yield_names = { "n_b", "n_l" };
	std::vector<std::string> obs_tagger_yield_labels = { "b-fraction * PDF", "c- + light-jet fraction * PDF" };

	for (int i_pt1 = 0; i_pt1 < Nbin_pt; i_pt1++) {
		for (int i_pt2 = 0; i_pt2 <= i_pt1; i_pt2++) {
			for (int i_plots = 0; i_plots < obs_tagger_names.size(); i_plots++) {
				// create new template tagger canvas (combine prefit+postfit), pT1 and pT2
				std::string canvasname_1 = "c_" + obs_tagger_yield_names.at(i_plots) + "_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_1";
				TCanvas *c_obs_1 = new TCanvas(canvasname_1.c_str());
				c_obs_1->SetLogy();

				std::string canvasname_2 = "c_" + obs_tagger_yield_names.at(i_plots) + "_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_2";
				TCanvas *c_obs_2 = new TCanvas(canvasname_2.c_str());
				c_obs_2->SetLogy();

				for (int i_w = 0; i_w < w.size(); i_w++) { // prefit+postfit
					// get PDF in pT1 and pT2 bins
					std::string histname_PDF_1 = std::string(d_pdf->GetName()) + "/" + obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt1 + 1) + "_" + w_names.at(i_w);
					TH1D *h_PDF_1 = (TH1D *)f_output->Get(histname_PDF_1.c_str());

					std::string histname_PDF_2 = std::string(d_pdf->GetName()) + "/" + obs_tagger_names.at(i_plots) + "_pt_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w);
					TH1D *h_PDF_2 = (TH1D *)f_output->Get(histname_PDF_2.c_str());

					// get fractions in this pT1/pT2 bin
					f_output->cd();
					double bbFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										obs_2d_pt_names.at(0) + "_" +
										w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);
					double blFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										obs_2d_pt_names.at(1) + "_" +
										w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);
					double lbFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										obs_2d_pt_names.at(2) + "_" +
										w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);
					double llFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										obs_2d_pt_names.at(3) + "_" +
										w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);

					double bbFrac_unc = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										    obs_2d_pt_names.at(0) + "_" +
										    w_names.at(i_w)).c_str()))->GetBinError(i_pt1 + 1, i_pt2 + 1);
					double blFrac_unc = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										    obs_2d_pt_names.at(1) + "_" +
										    w_names.at(i_w)).c_str()))->GetBinError(i_pt1 + 1, i_pt2 + 1);
					double lbFrac_unc = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										    obs_2d_pt_names.at(2) + "_" +
										    w_names.at(i_w)).c_str()))->GetBinError(i_pt1 + 1, i_pt2 + 1);
					double llFrac_unc = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
										    obs_2d_pt_names.at(3) + "_" +
										    w_names.at(i_w)).c_str()))->GetBinError(i_pt1 + 1, i_pt2 + 1);

					double frac1 = 0;
					double frac2 = 0;
					double frac1_unc = 0;
					double frac2_unc = 0;
					if (obs_tagger_yield_names.at(i_plots).find("_l") != std::string::npos) {
						frac1 = lbFrac + llFrac;
						frac2 = blFrac + llFrac;
						frac1_unc = sqrt(lbFrac_unc * lbFrac_unc + llFrac_unc * llFrac_unc);
						frac2_unc = sqrt(blFrac_unc * blFrac_unc + llFrac_unc * llFrac_unc);
					} else if (obs_tagger_yield_names.at(i_plots).find("_b") != std::string::npos) {
						frac1 = blFrac + bbFrac;
						frac2 = lbFrac + bbFrac;
						frac1_unc = sqrt(blFrac_unc * blFrac_unc + bbFrac_unc * bbFrac_unc);
						frac2_unc = sqrt(lbFrac_unc * lbFrac_unc + bbFrac_unc * bbFrac_unc);
					}

					// create new template tagger histogram, pT1 and pT2
					std::string histname_1 = obs_tagger_yield_names.at(i_plots) + "_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_1";
					TH1D *h_obs_1 = (TH1D *)h_tmp_tagger->Clone(histname_1.c_str());
					h_obs_1->SetStats(0);
					h_obs_1->Reset();
					h_obs_1->SetTitle("");
					h_obs_1->SetXTitle((taggerName + " output (1)").c_str());
					h_obs_1->SetYTitle(obs_tagger_yield_labels.at(i_plots).c_str());
					//h_obs_1->SetMaximum(15);
					//h_obs_1->SetMinimum(0);
					h_obs_1->GetXaxis()->SetLabelSize(0.05);
					h_obs_1->GetXaxis()->SetTitleSize(0.05);
					h_obs_1->GetXaxis()->SetTitleOffset(1.4);
					h_obs_1->SetMarkerStyle(20);

					//if(i_w==0 && i_plots==0 && i_wp==0) legend_fit->AddEntry(h_obs,"Prefit", "LPE");
					//else if(i_w==1 && i_plots==0 && i_wp==0) legend_fit->AddEntry(h_obs,"Postfit", "LPE");

					if (w_names.at(i_w) == "Prefit") {
						h_obs_1->SetLineColor(kGreen - 8);
						h_obs_1->SetMarkerColor(kGreen - 8);
					} else {
						h_obs_1->SetLineColor(kRed + 1);
						h_obs_1->SetMarkerColor(kRed + 1);
					}

					std::string histname_2 = obs_tagger_yield_names.at(i_plots) + "_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_2";
					TH1D *h_obs_2 = (TH1D *)h_obs_1->Clone(histname_2.c_str());
					h_obs_2->SetXTitle((taggerName + " output (2)").c_str());

					// fill histogram
					for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
						h_obs_1->SetBinContent(i_tagger + 1, frac1 * h_PDF_1->GetBinContent(i_tagger + 1) * h_PDF_1->GetBinWidth(i_tagger + 1));
						h_obs_2->SetBinContent(i_tagger + 1, frac2 * h_PDF_2->GetBinContent(i_tagger + 1) * h_PDF_2->GetBinWidth(i_tagger + 1));

						double unc_1 = 0;
						double unc_2 = 0;

						if (frac1 > 0 && h_PDF_1->GetBinContent(i_tagger + 1) > 0) {
							unc_1 = h_obs_1->GetBinContent(i_tagger + 1) *
								sqrt((frac1_unc / frac1) * (frac1_unc / frac1) +
								     (h_PDF_1->GetBinError(i_tagger + 1) / h_PDF_1->GetBinContent(i_tagger + 1)) *
								     (h_PDF_1->GetBinError(i_tagger + 1) / h_PDF_1->GetBinContent(i_tagger + 1)));
						}

						if (frac2 > 0 && h_PDF_2->GetBinContent(i_tagger + 1) > 0) {
							unc_2 = h_obs_2->GetBinContent(i_tagger + 1) *
								sqrt((frac2_unc / frac2) * (frac2_unc / frac2) +
								     (h_PDF_2->GetBinError(i_tagger + 1) / h_PDF_2->GetBinContent(i_tagger + 1)) *
								     (h_PDF_2->GetBinError(i_tagger + 1) / h_PDF_2->GetBinContent(i_tagger + 1)));
						}

						h_obs_1->SetBinError(i_tagger + 1, unc_1);
						h_obs_2->SetBinError(i_tagger + 1, unc_2);
					}

					// record histogram
					f_output->cd();
					d_tagger_fraction->cd();
					h_obs_1->Write();
					h_obs_2->Write();

					// draw on canvas
					c_obs_1->cd();
					h_obs_1->Draw("LPESAME");
					c_obs_2->cd();
					h_obs_2->Draw("LPESAME");
				} // end prefit+postfit

				// tune and record canvas
				c_obs_1->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt5->Clear();
				pt5->AddText((releaseName + ", " + ChannelName + ", " + systName + ", " + fitConfigName).c_str());
				pt5->AddText((taggerName + ", " + WPName + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 2)) + " GeV").c_str());
				pt5->Draw();
				legend_fit->Draw();
				f_output->cd();
				d_tagger_fraction->cd();
				c_obs_1->Write();

				c_obs_2->cd();
				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt5->Clear();
				pt5->AddText((releaseName + ", " + systName + ", " + fitConfigName).c_str());
				pt5->AddText((taggerName + ", " + WPName + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 2)) + " GeV").c_str());
				pt5->Draw();
				legend_fit->Draw();
				f_output->cd();
				d_tagger_fraction->cd();
				c_obs_2->Write();
			}       // end obs
		}               // end pt1
	}                       // end pt2

	// Plots vs tagger output (prefit, postfit) - data/MC (pT1, pT2 + inclusive)
	// for inclusive
	std::vector<TH1D *> h1_b_incl;
	std::vector<TH1D *> h2_b_incl;
	std::vector<TH1D *> h1_l_incl;
	std::vector<TH1D *> h2_l_incl;
	std::vector<TH1D *> h1_tot_incl;
	std::vector<TH1D *> h2_tot_incl;
	std::vector<TH1D *> h1_data_incl;
	std::vector<TH1D *> h2_data_incl;

	// (pT1, pT2)

	// chi2
	double chi2_Prefit = 0;                                         // chi2 before fit
	double chi2_Postfit = 0;                                        // chi2 after fit
	double ndf_Prefit = Nbin_tagger * Nbin_tagger * Nbin_pt * (Nbin_pt + 1) / 2. - Nbin_pt * (Nbin_pt + 1) / 2. - Nbin_tagger
	;                                                               // number of exclusive data bin - Number of N_pt_i_j [chi2 normalized in pT1/pT2]
	double ndf_Postfit = ndf_Prefit - (Nbin_tagger - 1) * Nbin_pt;  // number of data bin - N (p_b[pT])

	double chi2_pt_Prefit[Nbin_pt][Nbin_pt];
	double chi2_pt_Postfit[Nbin_pt][Nbin_pt];

	for (int i_pt1 = 0; i_pt1 < Nbin_pt; i_pt1++) {
		for (int i_pt2 = 0; i_pt2 <= i_pt1; i_pt2++) {
			// get data and scale to 1
			std::string dataname_1 = "data_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_1";
			std::string dataname_2 = "data_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_2";

			h_data->GetAxis(2)->SetRange(i_pt1 + 1, i_pt1 + 1);     // pT1 axis
			h_data->GetAxis(3)->SetRange(i_pt2 + 1, i_pt2 + 1);     // pT2 axis
			TH1D *h1_data_tmp = (TH1D *)h_data->Projection(0, "E");
			TH1D *h2_data_tmp = (TH1D *)h_data->Projection(1, "E");

			h1_data_tmp->SetName((dataname_1 + "_tmp").c_str());
			h2_data_tmp->SetName((dataname_2 + "_tmp").c_str());

			TH1D *h1_data_tmp_all = (TH1D *)h1_data_tmp->Clone((dataname_1 + "_tmp_all").c_str());
			TH1D *h2_data_tmp_all = (TH1D *)h2_data_tmp->Clone((dataname_2 + "_tmp_all").c_str());

			double error1 = 0;
			double integral1 = h1_data_tmp->IntegralAndError(1, h1_data_tmp->GetNbinsX(), error1);

			double error2 = 0;
			double integral2 = h2_data_tmp->IntegralAndError(1, h2_data_tmp->GetNbinsX(), error2);

			for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
				h1_data_tmp_all->SetBinContent(i_tagger + 1, integral1);
				h1_data_tmp_all->SetBinError(i_tagger + 1, error1);

				h2_data_tmp_all->SetBinContent(i_tagger + 1, integral2);
				h2_data_tmp_all->SetBinError(i_tagger + 1, error2);
			}
			h1_data_tmp->Divide(h1_data_tmp, h1_data_tmp_all, 1., 1., "B");
			h2_data_tmp->Divide(h2_data_tmp, h2_data_tmp_all, 1., 1., "B");

			TH1D *h1_data = (TH1D *)h_data_tmp->Clone(dataname_1.c_str());
			h1_data->SetStats(0);
			h1_data->Reset();

			TH1D *h2_data = (TH1D *)h_data_tmp->Clone(dataname_2.c_str());
			h2_data->SetStats(0);
			h2_data->Reset();

			for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
				h1_data->SetBinContent(i_tagger + 1, h1_data_tmp->GetBinContent(i_tagger + 1));
				h2_data->SetBinContent(i_tagger + 1, h2_data_tmp->GetBinContent(i_tagger + 1));
				h1_data->SetBinError(i_tagger + 1, h1_data_tmp->GetBinError(i_tagger + 1));
				h2_data->SetBinError(i_tagger + 1, h2_data_tmp->GetBinError(i_tagger + 1));
			}

			for (int i_w = 0; i_w < w.size(); i_w++) { // prefit+postfit
				// get b and l+c event fraction
				std::string histname_b_1 = std::string(d_tagger_fraction->GetName()) + "/n_b_pt1_" +
							   std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_1";
				std::string histname_b_2 = std::string(d_tagger_fraction->GetName()) + "/n_b_pt1_" +
							   std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_2";
				std::string histname_l_1 = std::string(d_tagger_fraction->GetName()) + "/n_l_pt1_" +
							   std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_1";
				std::string histname_l_2 = std::string(d_tagger_fraction->GetName()) + "/n_l_pt1_" +
							   std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_2";

				TH1D *h1_b = (TH1D *)f_output->Get(histname_b_1.c_str());
				TH1D *h1_l = (TH1D *)f_output->Get(histname_l_1.c_str());

				TH1D *h2_b = (TH1D *)f_output->Get(histname_b_2.c_str());
				TH1D *h2_l = (TH1D *)f_output->Get(histname_l_2.c_str());

				std::string histname_tot_1 = "n_tot_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_1";
				std::string histname_tot_2 = "n_tot_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_2";

				TH1D *h1_tot = (TH1D *)h1_b->Clone(histname_tot_1.c_str());
				h1_tot->Add(h1_l);
				TH1D *h2_tot = (TH1D *)h2_b->Clone(histname_tot_2.c_str());
				h2_tot->Add(h2_l);

				// Cosmetics
				h2_tot->SetYTitle("Tagger distribution normalised to 1");
				h1_tot->SetYTitle("Tagger distribution normalised to 1");

				h1_tot->SetLineColor(kBlue + 1);
				h2_tot->SetLineColor(kBlue + 1);

				h1_b->SetLineColor(kRed + 1);
				h2_b->SetLineColor(kRed + 1);

				h1_l->SetLineColor(kGreen - 8);
				h2_l->SetLineColor(kGreen - 8);

				h1_b->SetLineStyle(2);
				h2_b->SetLineStyle(2);

				h1_l->SetLineStyle(2);
				h2_l->SetLineStyle(2);

				h1_l->SetMaximum(10);
				h2_l->SetMaximum(10);

				// for inclusive plots
				if (i_pt2 == 0) {
					std::string suffix = "_pt1_" + std::to_string(i_pt1 + 1) + "_" + w_names.at(i_w);

					h1_b_incl.push_back((TH1D *)h1_b->Clone(("h1_b_incl" + suffix).c_str()));
					h1_l_incl.push_back((TH1D *)h1_l->Clone(("h1_l_incl" + suffix).c_str()));
					h1_tot_incl.push_back((TH1D *)h1_tot->Clone(("h1_tot_incl" + suffix).c_str()));

					if (i_w == 0) h1_data_incl.push_back((TH1D *)h1_data->Clone(("h1_data_incl_pt1_" + std::to_string(i_pt1 + 1)).c_str()));
				} else {
					h1_b_incl.at(w.size() * i_pt1 + i_w)->Add(h1_b);
					h1_l_incl.at(w.size() * i_pt1 + i_w)->Add(h1_l);
					h1_tot_incl.at(w.size() * i_pt1 + i_w)->Add(h1_tot);
					if (i_w == 0) h1_data_incl.at(i_pt1)->Add(h1_data);
				}

				if (i_pt1 == i_pt2) {
					std::string suffix = "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w);

					h2_b_incl.push_back((TH1D *)h2_b->Clone(("h2_b_incl" + suffix).c_str()));
					h2_l_incl.push_back((TH1D *)h2_l->Clone(("h2_l_incl" + suffix).c_str()));
					h2_tot_incl.push_back((TH1D *)h2_tot->Clone(("h2_tot_incl" + suffix).c_str()));

					if (i_w == 0) h2_data_incl.push_back((TH1D *)h2_data->Clone(("h2_data_incl_pt2_" + std::to_string(i_pt2 + 1)).c_str()));
				} else {
					h2_b_incl.at(w.size() * i_pt2 + i_w)->Add(h2_b);
					h2_l_incl.at(w.size() * i_pt2 + i_w)->Add(h2_l);
					h2_tot_incl.at(w.size() * i_pt2 + i_w)->Add(h2_tot);
					if (i_w == 0) h2_data_incl.at(i_pt2)->Add(h2_data);
				}

				// create new template tagger canvas, pT1 and pT2
				std::string canvasname_1 = "c_data_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_1";
				TCanvas *c_obs_1 = new TCanvas(canvasname_1.c_str());
				c_obs_1->SetLogy();

				std::string canvasname_2 = "c_data_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w) + "_2";
				TCanvas *c_obs_2 = new TCanvas(canvasname_2.c_str());
				c_obs_2->SetLogy();

				if (i_w == 0 && i_pt1 == 0 && i_pt2 == 0) {
					legend_data->AddEntry(h1_data, "Data normalized to 1", "LPE");
					legend_data->AddEntry(h1_tot, "Full model", "L");
					legend_data->AddEntry(h1_b, "b-jet fraction", "L");
					legend_data->AddEntry(h1_l, "light + c-jet fraction", "L");
				}


				for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
					// set error to 0 for total MC (not correct anyway)
					h1_tot->SetBinError(i_tagger + 1, 0.);
					h2_tot->SetBinError(i_tagger + 1, 0.);
					// 2D data inputs (not used at the end, 4D required)
					// set error to 1 if bin empty
					if (h1_data->GetBinContent(i_tagger + 1) == 0) h1_data->SetBinError(i_tagger + 1, 1);
					if (h2_data->GetBinContent(i_tagger + 1) == 0) h2_data->SetBinError(i_tagger + 1, 1);
				}

				// 2D tagger1-tagger2 histograms
				// MC
				std::string histoname_2d_mc = "h_2d_mc_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w);
				TH2D *h_2d_mc = new TH2D(histoname_2d_mc.c_str(), "", Nbin_tagger, 0, Nbin_tagger, Nbin_tagger, 0, Nbin_tagger);

				// data
				h_data->GetAxis(2)->SetRange(i_pt1 + 1, i_pt1 + 1);     // pT1 axis
				h_data->GetAxis(3)->SetRange(i_pt2 + 1, i_pt2 + 1);     // pT2 axis

				std::string histoname_2d_data = "h_2d_data_pt1_" + std::to_string(i_pt1 + 1) + "_pt2_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w);
				TH2D *h_2d_data = (TH2D *)h_data->Projection(1, 0, "E");
				h_2d_data->SetName(histoname_2d_data.c_str());

				// fractions (again ...)
				double bbFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
									obs_2d_pt_names.at(0) + "_" +
									w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);
				double blFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
									obs_2d_pt_names.at(1) + "_" +
									w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);
				double lbFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
									obs_2d_pt_names.at(2) + "_" +
									w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);
				double llFrac = ((TH2D *)f_output->Get((std::string(d_fraction->GetName()) + "/" +
									obs_2d_pt_names.at(3) + "_" +
									w_names.at(i_w)).c_str()))->GetBinContent(i_pt1 + 1, i_pt2 + 1);

				// b- and l+c-PDF
				std::string histname_bPDF_1 = std::string(d_pdf->GetName()) + "/p_b_pt_" + std::to_string(i_pt1 + 1) + "_" + w_names.at(i_w);
				TH1D *h_bPDF_1 = (TH1D *)f_output->Get(histname_bPDF_1.c_str());

				std::string histname_bPDF_2 = std::string(d_pdf->GetName()) + "/p_b_pt_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w);
				TH1D *h_bPDF_2 = (TH1D *)f_output->Get(histname_bPDF_2.c_str());

				std::string histname_lPDF_1 = std::string(d_pdf->GetName()) + "/p_l_pt_" + std::to_string(i_pt1 + 1) + "_" + w_names.at(i_w);
				TH1D *h_lPDF_1 = (TH1D *)f_output->Get(histname_lPDF_1.c_str());

				std::string histname_lPDF_2 = std::string(d_pdf->GetName()) + "/p_l_pt_" + std::to_string(i_pt2 + 1) + "_" + w_names.at(i_w);
				TH1D *h_lPDF_2 = (TH1D *)f_output->Get(histname_lPDF_2.c_str());

				if (i_w == 0) {
					// MC - prefit
					for (int i_tagger1 = 0; i_tagger1 < Nbin_tagger; i_tagger1++) {
						for (int i_tagger2 = 0; i_tagger2 < Nbin_tagger; i_tagger2++) {
							double N_pt1_pt2 = h_mc_prefit_pt1_pt2->GetBinContent(i_pt1 + 1, i_pt2 + 1);
							double N_tagger1_tagger2 = N_pt1_pt2 *
										   (bbFrac * h_bPDF_1->GetBinContent(i_tagger1 + 1) * h_bPDF_2->GetBinContent(i_tagger2 + 1) +
										    blFrac * h_bPDF_1->GetBinContent(i_tagger1 + 1) * h_lPDF_2->GetBinContent(i_tagger2 + 1) +
										    lbFrac * h_lPDF_1->GetBinContent(i_tagger1 + 1) * h_bPDF_2->GetBinContent(i_tagger2 + 1) +
										    llFrac * h_lPDF_1->GetBinContent(i_tagger1 + 1) * h_lPDF_2->GetBinContent(i_tagger2 + 1));

							if (N_tagger1_tagger2 > 0) {
								h_2d_mc->SetBinContent(i_tagger1 + 1, i_tagger2 + 1, N_tagger1_tagger2);
							} else {
								std::cout << "WARNING: some prefit MC predictions are negative, filling with 0 instead ... but check your workspace! " << std::endl;
								h_2d_mc->SetBinContent(i_tagger1 + 1, i_tagger2 + 1, 0);
							}
							h_2d_mc->SetBinError(i_tagger1 + 1, i_tagger2 + 1, 0.); // error of model set to 0 for chi2

							/*
							 * std::cout << std::endl;
							 * std::cout << h_mc_prefit_pt1_pt2->GetBinContent(i_pt1+1, i_pt2+1) << " " << h_data_pt1_pt2->GetBinContent(i_pt1+1, i_pt2+1) << " {pT1, pT2, Tagger1, Tagger2, data, MC} : " << i_pt1+1 << " " << i_pt2+1 << " " << i_tagger1+1 << " " << i_tagger2+1 << " " << h_2d_data->GetBinContent(i_tagger1+1,i_tagger2+1) << " " << h_2d_mc->GetBinContent(i_tagger1+1,i_tagger2+1) << " " << h_2d_data->GetBinContent(i_tagger1+1,i_tagger2+1)/h_2d_mc->GetBinContent(i_tagger1+1,i_tagger2+1) << std::endl;
							 */
						}
					}

					double chi2_pt1_pt2 = h_2d_data->Chi2Test(h_2d_mc, "CHI2UWP");
					chi2_Prefit += chi2_pt1_pt2;
					chi2_pt_Prefit[i_pt1][i_pt2] = chi2_pt1_pt2;
				} else if (i_w == 1) {
					// MC - postfit
					for (int i_tagger1 = 0; i_tagger1 < Nbin_tagger; i_tagger1++) {
						for (int i_tagger2 = 0; i_tagger2 < Nbin_tagger; i_tagger2++) {
							double N_pt1_pt2 = h_mc_postfit_pt1_pt2->GetBinContent(i_pt1 + 1, i_pt2 + 1);
							double N_tagger1_tagger2 = N_pt1_pt2 *
										   (bbFrac * h_bPDF_1->GetBinContent(i_tagger1 + 1) * h_bPDF_2->GetBinContent(i_tagger2 + 1) +
										    blFrac * h_bPDF_1->GetBinContent(i_tagger1 + 1) * h_lPDF_2->GetBinContent(i_tagger2 + 1) +
										    lbFrac * h_lPDF_1->GetBinContent(i_tagger1 + 1) * h_bPDF_2->GetBinContent(i_tagger2 + 1) +
										    llFrac * h_lPDF_1->GetBinContent(i_tagger1 + 1) * h_lPDF_2->GetBinContent(i_tagger2 + 1));

							if (N_tagger1_tagger2 > 0) {
								h_2d_mc->SetBinContent(i_tagger1 + 1, i_tagger2 + 1, N_tagger1_tagger2);
							} else {
								std::cout << "WARNING: some postfit MC predictions are negative, filling with 0 instead ... but check your workspace! " << std::endl;
								h_2d_mc->SetBinContent(i_tagger1 + 1, i_tagger2 + 1, 0);
							}
							h_2d_mc->SetBinError(i_tagger1 + 1, i_tagger2 + 1, 0.); // error of model set to 0 for chi2

							/*
							 * std::cout << h_mc_postfit_pt1_pt2->GetBinContent(i_pt1+1, i_pt2+1) << " " << h_data_pt1_pt2->GetBinContent(i_pt1+1, i_pt2+1) << " {pT1, pT2, Tagger1, Tagger2, data, MC} : " << i_pt1+1 << " " << i_pt2+1 << " " << i_tagger1+1 << " " << i_tagger2+1 << " " << h_2d_data->GetBinContent(i_tagger1+1,i_tagger2+1) << " " << h_2d_mc->GetBinContent(i_tagger1+1,i_tagger2+1) << " " << h_2d_data->GetBinContent(i_tagger1+1,i_tagger2+1)/h_2d_mc->GetBinContent(i_tagger1+1,i_tagger2+1) << std::endl;
							 */
						}
					}

					double chi2_pt1_pt2 = h_2d_data->Chi2Test(h_2d_mc, "CHI2UW");
					chi2_Postfit += chi2_pt1_pt2;
					chi2_pt_Postfit[i_pt1][i_pt2] = chi2_pt1_pt2;
				}

				// tune and record canvas
				c_obs_1->cd();
				h1_tot->Draw("HIST");
				h1_l->Draw("HISTSAME");
				h1_b->Draw("HISTSAME");
				h1_tot->Draw("HISTSAME");
				h1_tot->Draw("AXISSAME");
				h1_data->Draw("LPESAME");

				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt5->Clear();
				pt5->AddText((releaseName + ", " + systName + ", " + fitConfigName).c_str());
				pt5->AddText((taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 2)) + " GeV").c_str());
				pt5->Draw();
				legend_data->Draw();

				f_output->cd();
				d_data_MC->cd();
				c_obs_1->Write();

				c_obs_2->cd();
				h2_tot->Draw("HIST");
				h2_l->Draw("HISTSAME");
				h2_b->Draw("HISTSAME");
				h2_tot->Draw("HISTSAME");
				h2_tot->Draw("AXISSAME");
				h2_data->Draw("LPESAME");

				pt->Draw();
				pt2->Draw();
				pt3->Draw();
				pt5->Clear();
				pt5->AddText((releaseName + ", " + systName + ", " + fitConfigName).c_str());
				pt5->AddText((taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt1 + 2)) + " GeV, " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt2 + 2)) + " GeV").c_str());
				pt5->Draw();
				legend_data->Draw();

				f_output->cd();
				d_data_MC->cd();
				c_obs_2->Write();
			} // prefit+postfit
		}
	}

	// inclusive
	for (int i_pt = 0; i_pt < Nbin_pt; i_pt++) {
		// data rescaled to 1
		TH1D *h1_data_incl_tmp_all = (TH1D *)h1_data_incl.at(i_pt)->Clone(("h1_data_incl_" + to_string(i_pt) + "_tmp_all").c_str());
		TH1D *h2_data_incl_tmp_all = (TH1D *)h2_data_incl.at(i_pt)->Clone(("h2_data_incl_" + to_string(i_pt) + "_tmp_all").c_str());

		double error1 = 0;
		double integral1 = h1_data_incl_tmp_all->IntegralAndError(1, h1_data_incl_tmp_all->GetNbinsX(),
									  error1);

		double error2 = 0;
		double integral2 = h2_data_incl_tmp_all->IntegralAndError(1, h2_data_incl_tmp_all->GetNbinsX(),
									  error2);

		for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
			h1_data_incl_tmp_all->SetBinContent(i_tagger + 1, integral1);
			h1_data_incl_tmp_all->SetBinError(i_tagger + 1, error1);

			h2_data_incl_tmp_all->SetBinContent(i_tagger + 1, integral2);
			h2_data_incl_tmp_all->SetBinError(i_tagger + 1, error2);
		}
		h1_data_incl.at(i_pt)->Divide(h1_data_incl.at(i_pt), h1_data_incl_tmp_all, 1., 1., "B");
		h2_data_incl.at(i_pt)->Divide(h2_data_incl.at(i_pt), h2_data_incl_tmp_all, 1., 1., "B");

		for (int i_w = 0; i_w < w.size(); i_w++) {
			// model rescaled to 1
			TH1D *h1_tot_incl_tmp_all = (TH1D *)h1_tot_incl.at(w.size() * i_pt + i_w)->Clone(("h1_tot_incl_" + to_string(i_pt) + "_" + w_names.at(i_w) + "_tmp_all").c_str());
			TH1D *h2_tot_incl_tmp_all = (TH1D *)h2_tot_incl.at(w.size() * i_pt + i_w)->Clone(("h2_tot_incl_" + to_string(i_pt) + "_" + w_names.at(i_w) + "_tmp_all").c_str());

			double error1_model = 0;
			double integral1_model = h1_tot_incl_tmp_all->IntegralAndError(1,
										       h1_tot_incl_tmp_all->GetNbinsX(),
										       error1_model);

			double error2_model = 0;
			double integral2_model = h2_tot_incl_tmp_all->IntegralAndError(1,
										       h2_tot_incl_tmp_all->GetNbinsX(),
										       error2_model);

			for (int i_tagger = 0; i_tagger < Nbin_tagger; i_tagger++) {
				h1_tot_incl_tmp_all->SetBinContent(i_tagger + 1, integral1_model);
				h1_tot_incl_tmp_all->SetBinError(i_tagger + 1, error1_model);

				h2_tot_incl_tmp_all->SetBinContent(i_tagger + 1, integral2_model);
				h2_tot_incl_tmp_all->SetBinError(i_tagger + 1, error2_model);
			}
			h1_tot_incl.at(w.size() * i_pt + i_w)->Divide(h1_tot_incl.at(w.size() * i_pt + i_w),
								      h1_tot_incl_tmp_all, 1., 1., "B");

			h2_tot_incl.at(w.size() * i_pt + i_w)->Divide(h2_tot_incl.at(w.size() * i_pt + i_w),
								      h2_tot_incl_tmp_all, 1., 1., "B");

			double norm1_b = (h1_tot_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) -
					  h1_tot_incl.at(w.size() * i_pt + i_w)->GetBinContent(2) *
					  h1_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) /
					  h1_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(2)) /
					 (h1_b_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) -
					  h1_b_incl.at(w.size() * i_pt + i_w)->GetBinContent(2) *
					  h1_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) /
					  h1_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(2));

			double norm1_l = (h1_tot_incl.at(w.size() * i_pt + i_w)->GetBinContent(2) -
					  norm1_b * h1_b_incl.at(w.size() * i_pt + i_w)->GetBinContent(2)) /
					 h1_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(2);

			double norm2_b = (h2_tot_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) -
					  h2_tot_incl.at(w.size() * i_pt + i_w)->GetBinContent(2) *
					  h2_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) /
					  h2_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(2)) /
					 (h2_b_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) -
					  h2_b_incl.at(w.size() * i_pt + i_w)->GetBinContent(2) *
					  h2_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(1) /
					  h2_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(2));

			double norm2_l = (h2_tot_incl.at(w.size() * i_pt + i_w)->GetBinContent(2) -
					  norm2_b * h2_b_incl.at(w.size() * i_pt + i_w)->GetBinContent(2)) /
					 h2_l_incl.at(w.size() * i_pt + i_w)->GetBinContent(2);

			// ERROR NORMALIZATION FOR INDIVIDUAL B AND L COMPONENT NOT 100% ACCURATE (error overstimated)
			h1_b_incl.at(w.size() * i_pt + i_w)->Scale(norm1_b);
			h1_l_incl.at(w.size() * i_pt + i_w)->Scale(norm1_l);

			h2_b_incl.at(w.size() * i_pt + i_w)->Scale(norm2_b);
			h2_l_incl.at(w.size() * i_pt + i_w)->Scale(norm2_l);

			h1_b_incl.at(w.size() * i_pt + i_w)->SetMaximum(10);
			h1_l_incl.at(w.size() * i_pt + i_w)->SetMaximum(10);

			h2_b_incl.at(w.size() * i_pt + i_w)->SetMaximum(10);
			h2_l_incl.at(w.size() * i_pt + i_w)->SetMaximum(10);

			// canvas
			std::string suffix = "_pt1_" + std::to_string(i_pt + 1) + "_" + w_names.at(i_w);
			TCanvas *c1_incl = new TCanvas(("c_incl" + suffix).c_str());
			c1_incl->SetLogy();

			h1_tot_incl.at(w.size() * i_pt + i_w)->Draw("HIST");
			h1_l_incl.at(w.size() * i_pt + i_w)->Draw("HISTSAME");
			h1_b_incl.at(w.size() * i_pt + i_w)->Draw("HISTSAME");
			h1_tot_incl.at(w.size() * i_pt + i_w)->Draw("HISTSAME");
			h1_data_incl.at(i_pt)->Draw("LPESAME");
			h1_tot_incl.at(w.size() * i_pt + i_w)->Draw("AXISSAME");

			pt->Draw();
			pt2->Draw();
			pt3->Draw();
			pt5->Clear();
			pt5->SetTextSize(0.04);
			pt5->AddText((releaseName + ", " + systName + ", " + fitConfigName).c_str());
			pt5->AddText((taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt + 1)) + " GeV < p_{T,1}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt + 2)) + " GeV").c_str());
			pt5->Draw();
			legend_data->Draw();

			f_output->cd();
			d_data_MC->cd();
			c1_incl->Write();

			suffix = "_pt2_" + std::to_string(i_pt + 1) + "_" + w_names.at(i_w);
			TCanvas *c2_incl = new TCanvas(("c_incl" + suffix).c_str());
			c2_incl->SetLogy();

			h2_tot_incl.at(w.size() * i_pt + i_w)->Draw("HIST");
			h2_l_incl.at(w.size() * i_pt + i_w)->Draw("HISTSAME");
			h2_b_incl.at(w.size() * i_pt + i_w)->Draw("HISTSAME");
			h2_tot_incl.at(w.size() * i_pt + i_w)->Draw("HISTSAME");
			h2_data_incl.at(i_pt)->Draw("LPESAME");
			h2_tot_incl.at(w.size() * i_pt + i_w)->Draw("AXISSAME");

			pt->Draw();
			pt2->Draw();
			pt3->Draw();
			pt5->Clear();
			pt5->SetTextSize(0.04);
			pt5->AddText((taggerName + ", " + WPName + ", " + w_names.at(i_w) + ", " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt + 1)) + " GeV < p_{T,2}^{jet} < " + std::to_string((int)h_tmp_pt->GetBinLowEdge(i_pt + 2)) + " GeV").c_str());
			pt5->Draw();
			legend_data->Draw();

			f_output->cd();
			d_data_MC->cd();
			c2_incl->Write();
		}       // end prefit+postfit
	}               // end pT loop

	std::cout << " done." << std::endl;


	// ---------------chi2 ----------
	// global
	std::cout << "########## global chi2 ##########" << std::endl;
	std::cout << "PreFit: chi2, " << chi2_Prefit << ", ndf, " << ndf_Prefit << ", chi2/ndf, " << chi2_Prefit / ndf_Prefit << std::endl;
	std::cout << "PostFit: chi2, " << chi2_Postfit << ", ndf, " << ndf_Postfit << ", chi2/ndf, " << chi2_Postfit / ndf_Postfit << std::endl;

	// plot
	h_tmp_2d_pt->SetStats(0);
	h_tmp_2d_pt->Reset();
	h_tmp_2d_pt->SetTitle("");
	h_tmp_2d_pt->SetXTitle(pt_label_jet1.c_str());
	h_tmp_2d_pt->SetYTitle(pt_label_jet2.c_str());
	h_tmp_2d_pt->SetZTitle("% of #chi^{2}");
	h_tmp_2d_pt->GetXaxis()->SetLabelSize(0.05);
	h_tmp_2d_pt->GetXaxis()->SetTitleSize(0.05);
	h_tmp_2d_pt->GetXaxis()->SetTitleOffset(1.55);
	h_tmp_2d_pt->GetXaxis()->SetLabelOffset(0.01);
	h_tmp_2d_pt->GetYaxis()->SetLabelSize(0.05);
	h_tmp_2d_pt->GetYaxis()->SetTitleSize(0.05);
	h_tmp_2d_pt->GetYaxis()->SetTitleOffset(1.55);
	h_tmp_2d_pt->GetZaxis()->SetTitleOffset(1.05);

	std::string histname_chi2Frac_Prefit = "chi2Frac_Prefit";
	TH2D *h_chi2Frac_Prefit = (TH2D *)h_tmp_2d_pt->Clone(histname_chi2Frac_Prefit.c_str());

	std::string histname_chi2Frac_Postfit = "chi2Frac_Postfit";
	TH2D *h_chi2Frac_Postfit = (TH2D *)h_tmp_2d_pt->Clone(histname_chi2Frac_Postfit.c_str());

	h_chi2Frac_Prefit->SetMarkerColor(kBlack);
	h_chi2Frac_Prefit->SetFillColor(kBlack);
	h_chi2Frac_Prefit->SetLineColor(kBlack);
	h_chi2Frac_Prefit->SetBarOffset(+0.22);

	h_chi2Frac_Postfit->SetMarkerColor(kRed + 1);
	h_chi2Frac_Postfit->SetFillColor(kRed + 1);
	h_chi2Frac_Postfit->SetLineColor(kRed + 1);
	h_chi2Frac_Postfit->SetBarOffset(-0.22);

	for (int i_pt1 = 0; i_pt1 < Nbin_pt; i_pt1++) {
		for (int i_pt2 = 0; i_pt2 <= i_pt1; i_pt2++) {
			h_chi2Frac_Prefit->SetBinContent(i_pt1 + 1, i_pt2 + 1, chi2_pt_Prefit[i_pt1][i_pt2] / chi2_Prefit);
			h_chi2Frac_Postfit->SetBinContent(i_pt1 + 1, i_pt2 + 1, chi2_pt_Postfit[i_pt1][i_pt2] / chi2_Postfit);
			h_chi2Frac_Prefit->SetBinError(i_pt1 + 1, i_pt2 + 1, 0.);
			h_chi2Frac_Postfit->SetBinError(i_pt1 + 1, i_pt2 + 1, 0.);
		}
	}

	std::string canvasname_chi2 = "c_chi2";
	TCanvas *c_chi2 = new TCanvas(canvasname_chi2.c_str());
	c_chi2->SetLeftMargin(0.15);
	c_chi2->SetRightMargin(0.15);

	c_chi2->cd();
	h_chi2Frac_Prefit->Draw("TEXTCOLZ");
	h_chi2Frac_Postfit->Draw("TEXTSAME");
	h_chi2Frac_Prefit->Draw("AXISSAME");

	c_chi2->cd();
	pt_2d->Draw();
	pt2_2d->Draw();
	pt3_2d->Draw();
	pt4_2d->Clear();
	pt4_2d->AddText((releaseName + ", " + ChannelName + ", " + systName + ", " + fitConfigName).c_str());

	std::stringstream chi2_Prefit_string;
	std::stringstream chi2_Postfit_string;
	chi2_Prefit_string << std::setprecision(3) << chi2_Prefit / ndf_Prefit;
	chi2_Postfit_string << std::setprecision(3) << chi2_Postfit / ndf_Postfit;
	pt4_2d->AddText((taggerName + ", " + WPName + ", " + ", #chi^{2}/ndf = " + chi2_Prefit_string.str() + " (Postfit, " + chi2_Postfit_string.str() + ")").c_str());
	pt4_2d->Draw();
	legend_fit_2d->Draw();
	f_output->cd();
	c_chi2->Write();
	fitResult->Write();

	//

	return 0;
}
