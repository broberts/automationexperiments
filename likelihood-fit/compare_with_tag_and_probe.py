import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse



gStyle.SetPaintTextFormat("1.3f");
gStyle.SetNumberContours(12);

input_file_finalSystPlots_name="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-13_18-01-22/plots-ttbar-PhPy8/FitPlots/FinalFitPlots_r21_d1516_DL1_FixedCutBEff_emu_OS_J2_2lepcut_fconst.root"

tagNProbe_folder="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/TagAndProbe/"

input_file_finalSystPlots=ROOT.TFile(input_file_finalSystPlots_name,"read")
output_file=ROOT.TFile("comparison_TagNProbe_DL1.root","recreate")
legend_x      = 0.60#0.65
legend_y      = 0.40
legend_width  = 0.28
legend_height = 0.24#0.34
legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
#legend.SetFillStyle(0)
legend.SetLineColor(0)
legend.SetBorderSize(0)
legend.SetTextFont(42)
legend.SetTextSize(0.04)


nice_colours=[ROOT.kRed+1, ROOT.kBlue+1, ROOT.kOrange-1, ROOT.kGray+1, ROOT.kCyan+1] #(kRed + 1), (kOrange - 1), (kBlue + 1), (kGray + 1), (kGreen - 8), (kCyan + 1), (kBlack)
input_file_finalSystPlots.cd()
hists=[]
keyList=input_file_finalSystPlots.GetListOfKeys ()
for i in xrange(1, keyList.GetSize()):
    print i,keyList.At(i).GetName()
    obj=input_file_finalSystPlots.Get(keyList.At(i).GetName())
    className=obj.ClassName()
    oname=obj.GetName()
    if className== "TCanvas":
        if "c_e_b_sf_" in oname and not "log" in oname and oname.find("_nominal")>0:
            wp=oname.replace("c_e_b_sf_","").replace("_nominal","")
            canvas=obj
            canvas.cd()
            output_file.cd()
            canvas.Draw()
            print "adding on Canvas:",oname, "wp: ",wp
            data_h=input_file_finalSystPlots.Get("sf_b_"+wp+"_Postfit").Clone()
            data_h.SetDirectory(0)
            hists.append(data_h)
            legend.Clear()
            legend.AddEntry(data_h,"data1516 - PDF","LPE")
            #now adding tag and probe method.
            filename_tp=tagNProbe_folder+"dl1/"+"SF"+wp+".root"
            print "adding Tag n probe: ",filename_tp
            c_file=ROOT.TFile(filename_tp,"read")
            print "loaded file"
            output_file.cd()
            if not c_file:
                print filename_tp," not found!"
            sf_tp_total=c_file.Get("sf_total").Clone("sf_tp_total"+"_wp_"+wp)
            sf_tp_stat=c_file.Get("sf_stat").Clone("sf_tp_stat"+"_wp_"+wp)
            output_file.cd()
            hists.append(sf_tp_total)
            hists.append(sf_tp_stat)
            print "loaded tgraphs"
            sf_tp_total.SetLineColor(ROOT.kRed+1)
            sf_tp_total.SetMarkerColor(ROOT.kRed+1)
            sf_tp_total.SetFillColor(ROOT.kRed+1);
            sf_tp_total.SetFillStyle(3004);
            sf_tp_stat.SetLineColor(ROOT.kRed+1)
            sf_tp_stat.SetMarkerColor(ROOT.kRed+1)
            output_file.cd()
            sf_tp_total.Write()
            sf_tp_stat.Write()
            canvas.cd()
            sf_tp_total.Draw("2SAME")
            sf_tp_stat.Draw("pSAME")
            print "hist drawen"
            legend.AddEntry(sf_tp_stat,"data1516 - Tag and Probe","LPE")
            print "added to legend"
            c_file.Close()
            print "closed file"
            output_file.cd()
            canvas.cd()
            data_h.Draw("LPESAME")
            legend.Draw()
            output_file.cd()
            canvas.Write()
        # elif "c_e_b_" in oname and not "log" in oname and oname.find("_nominal")>0:
        #     wp=oname.replace("c_e_b_","").replace("_nominal","")
        #     canvas=obj
        #     canvas.cd()
        #     output_file.cd()
        #     canvas.Draw()
        #     print "adding on Canvas:",oname, "wp: ",wp
        #     data_h=input_file_finalSystPlots.Get("e_b_"+wp+"_Postfit").Clone()
        #     data_h.SetDirectory(0)
        #     hists.append(data_h)
        #     legend.Clear()
        #     legend.AddEntry(data_h,"data1516","LPE")
        #     i=0
        #     for compare_name in samples_to_compare_with:
        #         print "adding",compare_name,":",samples_to_compare_with[compare_name]
        #         c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
        #         print "loaded file"
        #         output_file.cd()
        #         if not c_file:
        #             print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
        #         sf_hist=c_file.Get("sf_and_beff/"+"e_b_"+wp+"_Postfit").Clone("e_"+compare_name+"_wp_"+wp)
        #         output_file.cd()
        #         sf_hist.SetDirectory(0)
        #         hists.append(sf_hist)
        #         print "loaded hist"
        #         sf_hist.SetLineColor(nice_colours[i])
        #         sf_hist.SetMarkerColor(nice_colours[i])
        #         output_file.cd()
        #         sf_hist.Write()
        #         canvas.cd()
        #         sf_hist.Draw("LPESAME")
        #         print "hist drawen"
        #         legend.AddEntry(sf_hist,compare_name,"LPE")
        #         print "added to legend"
        #         c_file.Close()
        #         print "closed file"
        #         i=i+1
        #         print  "i=",i
        #     output_file.cd()
        #     canvas.cd()
        #     data_h.Draw("LPESAME")
        #     legend.Draw()
        #     output_file.cd()
        #     canvas.Write()
output_file.Close()

print "outputfile closed."
