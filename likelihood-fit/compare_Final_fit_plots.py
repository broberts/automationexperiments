import ROOT
import os
import math
from array import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse
ROOT.gROOT.LoadMacro("AtlasStyle.C") 


gStyle.SetPaintTextFormat("1.3f");
gStyle.SetNumberContours(12);


releaseName= '21_2_56'
ChannelName= 'emu_OS_J2'
fitConfigName= 'combined_fit'
fit_folder='emu_OS_J2_combined_fit'
taggerNames=['MV2c10','DL1']
WPNames=["FixedCutBEff"]
#taggerNames=['MV2c10','DL1']
#WPNames=["FixedCutBEff","HybBEff"]

output_file_name="combined_vs_yr_comp_"+releaseName+'_'+ChannelName+'_'+fitConfigName+'.root'
#output_file_name="pflow_emtopo_comparison_"+releaseName+'_'+ChannelName+'_'+fitConfigName+'.root'
output_file=ROOT.TFile(output_file_name,"recreate")


legend_x      = 0.55#0.65
legend_y      = 0.31
legend_width  = 0.28
legend_height = 0.06#0.34
legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
#legend.SetFillStyle(0)
legend.SetLineColor(0)
legend.SetBorderSize(0)
legend.SetTextFont(42)
legend.SetTextSize(0.04)

legend_x      = 0.52#0.65
legend_y      = 0.31
legend_width  = 0.32
legend_height = 0.12#0.34
legend_div = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
#legend.SetFillStyle(0)
legend_div.SetLineColor(0)
legend_div.SetBorderSize(0)
legend_div.SetTextFont(42)
legend_div.SetTextSize(0.04)

# ATLAS label, sqrt(s), lumi
pt = TPaveText(0.228354, 0.821772, 0.348735, 0.889283, "brNDC");
pt.SetBorderSize(0);
pt.SetFillColor(0);
pt.SetTextSize(0.05);
pt.SetTextFont(72);
pt.AddText("ATLAS");

pt2 = TPaveText(0.34384, 0.820717, 0.464183, 0.890338, "brNDC");
pt2.SetBorderSize(0);
pt2.SetFillColor(0);
pt2.SetTextSize(0.05);
pt2.SetTextFont(42);
pt2.AddText("Internal");

pt3 = TPaveText(0.56447, 0.820717, 0.683381, 0.890338, "brNDC");
pt3.SetBorderSize(0);
pt3.SetFillColor(0);
pt3.SetTextSize(0.05);
pt3.SetTextFont(42);
pt3.AddText("#sqrt{s} = 13 TeV");

pt4 = TPaveText(0.570201, 0.704641, 0.689112, 0.829114, "brNDC");
pt4.SetBorderSize(0);
pt4.SetFillColor(0);
pt4.SetTextSize(0.04);
pt4.SetTextFont(42);

pt5 = TPaveText(0.501433, 0.71308, 0.620344, 0.820675, "brNDC");
pt5.SetBorderSize(0);
pt5.SetFillColor(0);
pt5.SetTextSize(0.04);
pt5.SetTextFont(42);


nice_colours=[ROOT.kRed+1, ROOT.kBlue+1, ROOT.kOrange-1, ROOT.kGray+1, ROOT.kCyan+1] #(kRed + 1), (kOrange - 1), (kBlue + 1), (kGray + 1), (kGreen - 8), (kCyan + 1), (kBlack)
for taggerName in taggerNames:
    for WPName in WPNames:

        output_dir= output_file.mkdir(taggerName+ "_" +WPName);
        input_file_finalSystPlots_name="/atlas/jhall/bjet_cali/finalSelection/rel_21.2.5X_Combination/plots-21.2.5X_Combination/FitPlots_3SB/FinalFitPlots_r21_data15161718_" + taggerName + "_" + WPName + "_emu_OS_J2.root"
        samples_to_compare_with={
            "Data-1516" : "comparison_dir/mc16a_d1516_results/FinalFitPlots_r21_data1516_" + taggerName + "_" + WPName + "_emu_OS_J2.root",
            "Data-17" : "comparison_dir/mc16d_d17_results/FinalFitPlots_r21_data17_" + taggerName + "_" + WPName + "_emu_OS_J2.root",
			"Data-18" : "comparison_dir/mc16e_d18_results/FinalFitPlots_r21_data18_" + taggerName + "_" + WPName + "_emu_OS_J2.root"
        }
        input_file_finalSystPlots=ROOT.TFile(input_file_finalSystPlots_name,"read")
        results_dir= input_file_finalSystPlots.Get("results_comulative")
        hists=[]
        keyList=results_dir.GetListOfKeys ()
        for i in xrange(1, keyList.GetSize()):
            print i,keyList.At(i).GetName()
            obj=results_dir.Get(keyList.At(i).GetName())
            className=obj.ClassName()
            oname=obj.GetName()
            if className== "TCanvas":
                if "c_e_b_sf_" in oname and oname.find("_nominal")>0: #and not "log" in oname
                    full_c_name=oname
                    wp=oname.replace("c_e_b_sf_","").replace("_nominal","").replace("_log","")
                    canvas=obj
                    canvas.cd()
                    output_dir.cd()
                    canvas.Draw()
                    print "adding on Canvas:",oname, "wp: ",wp
                    data_h=results_dir.Get("g_e_b_sf_"+wp+"_nominal").Clone()
                    hists.append(data_h)
                    legend.Clear()
                    #legend.AddEntry(data_h,"data1516","LPE")
                    i=0
                    for compare_name in samples_to_compare_with:
                        print "adding",compare_name,":",samples_to_compare_with[compare_name]
                        c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                        print "loaded file"
                        output_dir.cd()
                        if not c_file:
                            print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                        sf_hist=c_file.Get("results_comulative/"+"g_e_b_sf_"+wp+"_nominal").Clone("sf_"+compare_name+"_wp_"+wp)
                        output_dir.cd()

                        #sf_hist.SetDirectory(0)
                        hists.append(sf_hist)
                        print "loaded hist"
                        sf_hist.SetLineColor(nice_colours[i])
                        sf_hist.SetMarkerColor(nice_colours[i])
                        output_dir.cd()
                        sf_hist.Write()
                        canvas.cd()
                        sf_hist.Draw("pSAME")
                        # sf_syst_high.Draw("histSAME")
                        # sf_syst_low.Draw("histSAME")
                        print "hist drawen"
                        legend.AddEntry(sf_hist,compare_name,"LPE")
                        print "added to legend"
                        c_file.Close()
                        print "closed file"
                        i=i+1
                        print  "i=",i
                    output_dir.cd()
                    canvas.cd()
                    data_h.Draw("pSAME")
                    legend.Draw()
                    output_dir.cd()
                    canvas.Write()
                    #lets make a division plot:
                    if 'log' in full_c_name:
                        continue
                    # for compare_name in samples_to_compare_with:
                    #     print "adding",compare_name,":",samples_to_compare_with[compare_name]
                    #     c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                    #     print "loaded file"
                    #     output_dir.cd()
                    #     if not c_file:
                    #         print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                    #     divisor_hist=input_file_finalSystPlots.Get("sf_b_"+wp+"_Postfit").Clone("sf_divisor_"+"data1516"+"_wp_"+wp)
                    #     numerator_hist=c_file.Get("results_comulative/"+"sf_b_"+wp+"_Postfit").Clone("sf_numerator_"+compare_name+"_wp_"+wp)
                    #     divison_hist=numerator_hist.Clone("sf_divison_"+compare_name+"_div_data1516"+"_wp_"+wp)
                    #     divison_hist.Divide(divisor_hist)
                    #     divison_hist.SetYTitle("b-eff-sf data17 / data1516")
                    #     divison_hist.GetYaxis().SetRangeUser(0.7,1.3)
                    #     divison_hist.Write()
                    #     output_dir.cd()
                    #     c_obs=TCanvas("c_div_sf_b_"+wp+"_Postfit")
                    #     c_obs.SetLogx()
                    #     c_obs.SetLeftMargin(0.15);
                    #     c_obs.SetRightMargin(0.15);
                    #     c_obs.SetBottomMargin(0.15);
                    #     c_obs.cd()
                    #     divison_hist.Draw()
                    #     output_dir.cd()
                    #     #so now we have to calculate the correlated systematics:
                    #     sys_dir=input_file_finalSystPlots.Get("e_b_systematics_wp_"+wp)
                    #     sys_keyList=sys_dir.GetListOfKeys()
                    #     total_sys_unc_rel=0
                    #     h_one= divison_hist.Clone("h_1_"+wp)
                    #     h_one.Reset()
                    #     for bin in xrange(0, h_one.GetNbinsX()+1):
                    #         h_one.SetBinContent(bin,1)
                    #         h_one.SetBinError(bin, 0)
                    #     for ikey in xrange(1, sys_keyList.GetSize()):
                    #         obj=sys_dir.Get(sys_keyList.At(ikey).GetName())
                    #         className=obj.ClassName()
                    #         if className[:2]=="TH":
                    #             hist_name=obj.GetName();
                    #             if hist_name.find("syst_Error_rel")>0 and hist_name.find(wp)>0 and hist_name.find("unused")==-1:
                    #                 print "Reading syst histogam:\t"+hist_name
                    #                 output_dir.cd()
                    #                 hist_divisor= obj.Clone("numerator"+hist_name)
                    #                 hist_numerator=c_file.Get("results_comulative/"+"e_b_systematics_wp_"+wp+"/"+hist_name)
                    #                 if hist_numerator== None:
                    #                     print "did not find syst histogam:\t"+hist_name," in ",samples_to_compare_with[compare_name]
                    #                     continue
                    #                 hist_divisor=hist_divisor.Clone("h_divisor_"+hist_name)
                    #                 hist_divisor.Add(h_one)
                    #                 hist_numerator.Add(h_one)
                    #                 hist_numerator.Divide(hist_divisor)
                    #                 hist_numerator.Scale(-1)
                    #                 hist_numerator.Add(h_one)
                    #                 h_squared=hist_numerator.Clone(hist_name+"_div_squared")
                    #                 h_squared.Multiply(h_squared)
                    #                 if total_sys_unc_rel==0:
                    #                     total_sys_unc_rel=h_squared.Clone("sf_total_syst_div_rel_wp_"+wp)
                    #                 else:
                    #                     total_sys_unc_rel.Add(h_squared)
                    #     for bin in xrange(0, total_sys_unc_rel.GetNbinsX()+1):
                    #         total_sys_unc_rel.SetBinContent(bin,math.sqrt(total_sys_unc_rel.GetBinContent(bin)) )
                    #         total_sys_unc_rel.SetBinError(bin, 0)
                    #     total_sys_unc_rel.Multiply(divison_hist)
                    #     total_sys_unc_rel.Write()
                    #     n=0
                    #     x=[]
                    #     y=[]
                    #     exl=[]
                    #     exh=[]
                    #     eyl=[]
                    #     eyh=[]
                    #     for bin in xrange(1, total_sys_unc_rel.GetNbinsX()+1):
                    #         n=n+1
                    #         x.append(divison_hist.GetBinCenter(bin))
                    #         y.append(divison_hist.GetBinContent(bin))
                    #         exl.append(divison_hist.GetBinCenter(bin)-divison_hist.GetBinLowEdge(bin))
                    #         exh.append(divison_hist.GetBinWidth(bin)/2.)
                    #         stat_p_syst=(total_sys_unc_rel.GetBinContent(bin) *total_sys_unc_rel.GetBinContent(bin)) +(divison_hist.GetBinError(bin)*divison_hist.GetBinError(bin))
                    #         stat_p_syst=math.sqrt(stat_p_syst)
                    #         eyl.append(stat_p_syst)
                    #         eyh.append(stat_p_syst)
                    #     a_x=array('d',x)
                    #     t_graph=TGraphAsymmErrors(n,a_x , array('d',y), array('d',exl), array('d',exh), array('d',eyl), array('d',eyh))
                    #     c_obs.cd()
                    #     t_graph.SetFillColor(ROOT.kGreen-8);
                    #     t_graph.Draw("2SAME");
                    #     h_line1=TLine(-1., 1, 30000, 1);
                    # 	h_line1.SetLineStyle(2);
                    # 	h_line1.SetLineColorAlpha(1, 0.7);
                    #     h_line1.DrawLine(divison_hist.GetXaxis().GetXmin(), 1., divison_hist.GetXaxis().GetXmax(), 1);
                    #     legend_div.Clear()
                    #     legend_div.AddEntry(divison_hist,"sf data17 / data1516 ","LPE");
                    #     dummy_er=TH1D();
                    #     dummy_er.SetFillColor(kGreen-8);
                    #     dummy_er.SetLineColor(kGreen-8);
                    #     legend_div.AddEntry(dummy_er, "stat + syst unc", "F");
                    #     legend_div.Draw()
                    #     pt.Draw();
                    #     pt2.Draw();
                    #     pt3.Draw();
                    #     pt4.Clear();
                    #     pt4.AddText(releaseName + ", " + ChannelName + ", " + ", " + fitConfigName);
                    #     pt4.AddText(taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp + "%");
                    #     pt4.Draw();
                    #     divison_hist.Draw("LPESAME");
                    #     divison_hist.Draw("AXISSAME");
                    #     c_obs.Write()
                    #     c_file.Close()



                elif "c_e_b_" in oname and oname.find("_nominal")>0:
                    wp=oname.replace("c_e_b_","").replace("_nominal","").replace("_log","")
                    canvas=obj
                    canvas.cd()
                    output_dir.cd()
                    canvas.Draw()
                    print "adding on Canvas:",oname, "wp: ",wp
                    data_h=results_dir.Get("g_e_b_"+wp+"_nominal").Clone()
                    hists.append(data_h)
                    legend.Clear()
                    #legend.AddEntry(data_h,"data1516","LPE")
                    i=0
                    for compare_name in samples_to_compare_with:
                        print "adding",compare_name,":",samples_to_compare_with[compare_name]
                        c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                        print "loaded file"
                        output_dir.cd()
                        if not c_file:
                            print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                        sf_hist=c_file.Get("results_comulative/"+"g_e_b_"+wp+"_nominal").Clone("e_"+compare_name+"_wp_"+wp)
                        output_dir.cd()
                        #sf_hist.SetDirectory(0)
                        hists.append(sf_hist)
                        print "loaded hist"
                        sf_hist.SetLineColor(nice_colours[i])
                        sf_hist.SetMarkerColor(nice_colours[i])
                        output_dir.cd()
                        sf_hist.Write()
                        canvas.cd()
                        sf_hist.Draw("pSAME")
                        print "hist drawen"
                        legend.AddEntry(sf_hist,compare_name,"LPE")
                        print "added to legend"
                        c_file.Close()
                        print "closed file"
                        i=i+1
                        print  "i=",i
                    output_dir.cd()
                    canvas.cd()
                    data_h.Draw("pSAME")
                    legend.Draw()
                    output_dir.cd()
                    canvas.Write()
                    #lets make a division plot:
                    # for compare_name in samples_to_compare_with:
                    #     print "adding",compare_name,":",samples_to_compare_with[compare_name]
                    #     c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                    #     print "loaded file"
                    #     output_dir.cd()
                    #     if not c_file:
                    #         print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                    #     divisor_hist=input_file_finalSystPlots.Get("e_b_"+wp+"_Postfit").Clone("e_divisor_"+"data1516"+"_wp_"+wp)
                    #     numerator_hist=c_file.Get("results_comulative/"+"e_b_"+wp+"_Postfit").Clone("e_numerator_"+compare_name+"_wp_"+wp)
                    #     divison_hist=numerator_hist.Clone("e_divison_"+compare_name+"_div_data1516"+"_wp_"+wp)
                    #     divison_hist.Divide(divisor_hist)
                    #     divison_hist.SetYTitle("b-eff data17 / data1516 ")
                    #     divison_hist.GetYaxis().SetRangeUser(0.7,1.3)
                    #     divison_hist.Write()
                    #     output_dir.cd()
                    #     c_obs=TCanvas("c_div_e_b_"+wp+"_Postfit")
                    #     c_obs.SetLogx()
                    #     c_obs.SetLeftMargin(0.15);
                    #     c_obs.SetRightMargin(0.15);
                    #     c_obs.SetBottomMargin(0.15);
                    #     c_obs.cd()
                    #     divison_hist.Draw()
                    #     output_dir.cd()
                    #     #so now we have to calculate the correlated systematics:
                    #     sys_dir=input_file_finalSystPlots.Get("e_b_systematics_wp_"+wp)
                    #     sys_keyList=sys_dir.GetListOfKeys()
                    #     total_sys_unc_rel=0
                    #     h_one= divison_hist.Clone("h_1_"+wp)
                    #     h_one.Reset()
                    #     for bin in xrange(0, h_one.GetNbinsX()+1):
                    #         h_one.SetBinContent(bin,1)
                    #         h_one.SetBinError(bin, 0)
                    #     for ikey in xrange(1, sys_keyList.GetSize()):
                    #         obj=sys_dir.Get(sys_keyList.At(ikey).GetName())
                    #         className=obj.ClassName()
                    #         if className[:2]=="TH":
                    #             hist_name=obj.GetName();
                    #             if hist_name.find("syst_Error_rel")>0 and hist_name.find(wp)>0 and hist_name.find("unused")==-1:
                    #                 print "Reading syst histogam:\t"+hist_name
                    #                 output_dir.cd()
                    #                 hist_numerator=c_file.Get("results_comulative/"+"e_b_systematics_wp_"+wp+"/"+hist_name)
                    #                 if hist_numerator== None:
                    #                     print "did not find syst histogam:\t"+hist_name," in ",samples_to_compare_with[compare_name]
                    #                     continue
                    #                 hist_numerator=hist_numerator.Clone("h_numerator_"+hist_name)
                    #                 hist_divisor= obj.Clone("h_divisor"+hist_name)
                    #                 hist_divisor.Add(h_one)
                    #                 hist_numerator.Add(h_one)
                    #                 hist_numerator.Divide(hist_divisor)
                    #                 hist_numerator.Scale(-1)
                    #                 hist_numerator.Add(h_one)
                    #                 h_squared=hist_numerator.Clone(hist_name+"_div_squared")
                    #                 h_squared.Multiply(h_squared)
                    #                 if total_sys_unc_rel==0:
                    #                     total_sys_unc_rel=h_squared.Clone("e_b_total_syst_div_rel_wp_"+wp)
                    #                 else:
                    #                     total_sys_unc_rel.Add(h_squared)
                    #     for bin in xrange(0, total_sys_unc_rel.GetNbinsX()+1):
                    #         total_sys_unc_rel.SetBinContent(bin,math.sqrt(total_sys_unc_rel.GetBinContent(bin)) )
                    #         total_sys_unc_rel.SetBinError(bin, 0)
                    #     total_sys_unc_rel.Write()
                    #     n=0
                    #     x=[]
                    #     y=[]
                    #     exl=[]
                    #     exh=[]
                    #     eyl=[]
                    #     eyh=[]
                    #     for bin in xrange(1, total_sys_unc_rel.GetNbinsX()+1):
                    #         n=n+1
                    #         x.append(divison_hist.GetBinCenter(bin))
                    #         y.append(divison_hist.GetBinContent(bin))
                    #         exl.append(divison_hist.GetBinCenter(bin)-divison_hist.GetBinLowEdge(bin))
                    #         exh.append(divison_hist.GetBinWidth(bin)/2.)
                    #         stat_p_syst=(total_sys_unc_rel.GetBinContent(bin) *total_sys_unc_rel.GetBinContent(bin)) +(divison_hist.GetBinError(bin)*divison_hist.GetBinError(bin))
                    #         stat_p_syst=math.sqrt(stat_p_syst)
                    #         eyl.append(stat_p_syst)
                    #         eyh.append(stat_p_syst)
                    #     a_x=array('d',x)
                    #     t_graph=TGraphAsymmErrors(n,a_x , array('d',y), array('d',exl), array('d',exh), array('d',eyl), array('d',eyh))
                    #     c_obs.cd()
                    #     t_graph.SetFillColor(ROOT.kGreen-8);
                    #     t_graph.Draw("2SAME");
                    #     h_line1=TLine(-1., 1, 30000, 1);
                    # 	h_line1.SetLineStyle(2);
                    # 	h_line1.SetLineColorAlpha(1, 0.6);
                    #     h_line1.DrawLine(divison_hist.GetXaxis().GetXmin(), 1., divison_hist.GetXaxis().GetXmax(), 1);
                    #     legend_div.Clear()
                    #     legend_div.AddEntry(divison_hist,"#epsilon_{b}  data17 / data1516","LPE");
                    #     dummy_er=TH1D();
                    #     dummy_er.SetFillColor(kGreen-8);
                    #     dummy_er.SetLineColor(kGreen-8);
                    #     legend_div.AddEntry(dummy_er, "stat + syst unc", "F");
                    #     legend_div.Draw()
                    #     pt.Draw();
                    #     pt2.Draw();
                    #     pt3.Draw();
                    #     pt4.Clear();
                    #     pt4.AddText(releaseName + ", " + ChannelName + ", " + ", " + fitConfigName);
                    #     pt4.AddText(taggerName + ", " + WPName + ", #epsilon_{b}^{MC} = " + wp + "%");
                    #     pt4.Draw();
                    #     divison_hist.Draw("LPESAME");
                    #     divison_hist.Draw("AXISSAME");
                    #     c_obs.Write()
                    #     c_file.Close()
                elif "c_p_b_sf_" in oname and oname.find("_nominal")>0: #and not "log" in oname
                    full_c_name=oname
                    wp=oname.replace("c_p_b_sf_","").replace("_nominal","").replace("_log","")
                    canvas=obj
                    canvas.cd()
                    output_dir.cd()
                    canvas.Draw()
                    print "adding on Canvas:",oname, "wp: ",wp
                    data_h=results_dir.Get("p_b_sf_"+wp+"_nominal").Clone()
                    #data_h.SetDirectory(0)
                    hists.append(data_h)
                    legend.Clear()
                    #legend.AddEntry(data_h,"data1516","LPE")
                    i=0
                    for compare_name in samples_to_compare_with:
                        print "adding",compare_name,":",samples_to_compare_with[compare_name]
                        c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                        print "loaded file"
                        output_dir.cd()
                        if not c_file:
                            print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                        sf_hist=c_file.Get("results_comulative/"+"p_b_sf_"+wp+"_nominal").Clone("c_p_b_sf_"+compare_name+"_wp_"+wp)
                        output_dir.cd()
                        sf_syst_rel_unc=c_file.Get("results_comulative/"+"p_b_sf_"+wp+"_syst_Error_combined_rel").Clone("sf_"+compare_name+"_wp_"+wp+"_syst_Error_combined_rel")
                        sf_syst_high=sf_syst_rel_unc.Clone("p_b_sf_"+compare_name+"_wp_"+wp+"_syst_Error_high")
                        sf_syst_low=sf_syst_rel_unc.Clone("p_b_sf_"+compare_name+"_wp_"+wp+"_syst_Error_low")
                        sf_syst_low.Scale(-1)
                        #sf_syst_high.Divide(sf_hist)
                        sf_syst_high.Add(sf_hist)
                        sf_syst_low.Add(sf_hist)
                        for bin in xrange(0,sf_syst_low.GetNbinsX()+1):
                            sf_syst_low.SetBinError(bin,0)
                            sf_syst_high.SetBinError(bin,0)
                        #sf_hist.SetDirectory(0)
                        hists.append(sf_hist)
                        print "loaded hist"
                        sf_hist.SetLineColor(nice_colours[i])
                        sf_hist.SetMarkerColor(nice_colours[i])
                        output_dir.cd()
                        sf_hist.Write()
                        canvas.cd()
                        sf_hist.Draw("pSAME")
                        # sf_syst_high.Draw("histSAME")
                        # sf_syst_low.Draw("histSAME")
                        print "hist drawen"
                        legend.AddEntry(sf_hist,compare_name,"LPE")
                        print "added to legend"
                        c_file.Close()
                        print "closed file"
                        i=i+1
                        print  "i=",i
                    output_dir.cd()
                    canvas.cd()
                    data_h.Draw("pSAME")
                    legend.Draw()
                    output_dir.cd()
                    canvas.Write()
                    #lets make a division plot:
                    # if 'log' in full_c_name:
                    #     continue
                    # for compare_name in samples_to_compare_with:
                    #     print "adding",compare_name,":",samples_to_compare_with[compare_name]
                    #     c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                    #     print "loaded file"
                    #     output_dir.cd()
                    #     if not c_file:
                    #         print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                    #     divisor_hist=input_file_finalSystPlots.Get("p_b_sf_"+wp+"_nominal").Clone("p_b_sf_divisor_"+"data1516"+"_wp_"+wp)
                    #     numerator_hist=c_file.Get("results_comulative/"+"p_b_sf_"+wp+"_nominal").Clone("p_b_sf_numerator_"+compare_name+"_wp_"+wp)
                    #     divison_hist=numerator_hist.Clone("p_b_sf_divison_"+compare_name+"_div_data1516"+"_wp_"+wp)
                    #     divison_hist.Divide(divisor_hist)
                    #     divison_hist.SetYTitle("b-eff-sf data17 / data1516")
                    #     divison_hist.GetYaxis().SetRangeUser(0.6,1.5)
                    #     divison_hist.Write()
                    #     output_dir.cd()
                    #     c_obs=TCanvas("c_div_p_b_sf_"+wp+"_nominal")
                    #     c_obs.SetLeftMargin(0.15);
                    #     c_obs.SetRightMargin(0.15);
                    #     c_obs.SetBottomMargin(0.15);
                    #     c_obs.cd()
                    #     divison_hist.Draw()
                    #     output_dir.cd()
                    #     #so now we have to calculate the correlated systematics:
                    #     sys_dir=input_file_finalSystPlots.Get("p_b_systematics_"+wp)
                    #     sys_keyList=sys_dir.GetListOfKeys()
                    #     total_sys_unc_rel=0
                    #     h_one= divison_hist.Clone("h_1_"+wp)
                    #     h_one.Reset()
                    #     for bin in xrange(0, h_one.GetNbinsX()+1):
                    #         h_one.SetBinContent(bin,1)
                    #         h_one.SetBinError(bin, 0)
                    #     for ikey in xrange(1, sys_keyList.GetSize()):
                    #         obj=sys_dir.Get(sys_keyList.At(ikey).GetName())
                    #         className=obj.ClassName()
                    #         if className[:2]=="TH":
                    #             hist_name=obj.GetName();
                    #             if hist_name.find("syst_Error_rel")>0 and hist_name.find(wp)>0 and hist_name.find("unused")==-1:
                    #                 print "Reading syst histogam:\t"+hist_name
                    #                 output_dir.cd()
                    #                 hist_divisor= obj.Clone("numerator"+hist_name)
                    #                 hist_numerator=c_file.Get("results_comulative/"+"p_b_systematics_"+wp+"/"+hist_name)
                    #                 if hist_numerator== None:
                    #                     print "did not find syst histogam:\t"+hist_name," in ",samples_to_compare_with[compare_name]
                    #                     continue
                    #                 hist_divisor=hist_divisor.Clone("h_divisor_"+hist_name)
                    #                 hist_divisor.Add(h_one)
                    #                 hist_numerator.Add(h_one)
                    #                 hist_numerator.Divide(hist_divisor)
                    #                 hist_numerator.Scale(-1)
                    #                 hist_numerator.Add(h_one)
                    #                 h_squared=hist_numerator.Clone(hist_name+"_div_squared")
                    #                 h_squared.Multiply(h_squared)
                    #                 if total_sys_unc_rel==0:
                    #                     total_sys_unc_rel=h_squared.Clone("p_b_systematics_"+wp+"_total_syst_div_rel_wp_"+wp)
                    #                 else:
                    #                     total_sys_unc_rel.Add(h_squared)
                    #     for bin in xrange(0, total_sys_unc_rel.GetNbinsX()+1):
                    #         total_sys_unc_rel.SetBinContent(bin,math.sqrt(total_sys_unc_rel.GetBinContent(bin)) )
                    #         total_sys_unc_rel.SetBinError(bin, 0)
                    #     total_sys_unc_rel.Multiply(divison_hist)
                    #     total_sys_unc_rel.Write()
                    #     n=0
                    #     x=[]
                    #     y=[]
                    #     exl=[]
                    #     exh=[]
                    #     eyl=[]
                    #     eyh=[]
                    #     for bin in xrange(1, total_sys_unc_rel.GetNbinsX()+1):
                    #         n=n+1
                    #         x.append(divison_hist.GetBinCenter(bin))
                    #         y.append(divison_hist.GetBinContent(bin))
                    #         exl.append(divison_hist.GetBinCenter(bin)-divison_hist.GetBinLowEdge(bin))
                    #         exh.append(divison_hist.GetBinWidth(bin)/2.)
                    #         stat_p_syst=(total_sys_unc_rel.GetBinContent(bin) *total_sys_unc_rel.GetBinContent(bin)) +(divison_hist.GetBinError(bin)*divison_hist.GetBinError(bin))
                    #         stat_p_syst=math.sqrt(stat_p_syst)
                    #         eyl.append(stat_p_syst)
                    #         eyh.append(stat_p_syst)
                    #     a_x=array('d',x)
                    #     t_graph=TGraphAsymmErrors(n,a_x , array('d',y), array('d',exl), array('d',exh), array('d',eyl), array('d',eyh))
                    #     c_obs.cd()
                    #     t_graph.SetFillColor(ROOT.kGreen-8);
                    #     t_graph.Draw("2SAME");
                    #     h_line1=TLine(-1., 1, 30000, 1);
                    #     h_line1.SetLineStyle(2);
                    #     h_line1.SetLineColorAlpha(1, 0.7);
                    #     h_line1.DrawLine(divison_hist.GetXaxis().GetXmin(), 1., divison_hist.GetXaxis().GetXmax(), 1);
                    #     legend_div.Clear()
                    #     legend_div.AddEntry(divison_hist,"sf data17 / data1516 ","LPE");
                    #     dummy_er=TH1D();
                    #     dummy_er.SetFillColor(kGreen-8);
                    #     dummy_er.SetLineColor(kGreen-8);
                    #     legend_div.AddEntry(dummy_er, "stat + syst unc", "F");
                    #     legend_div.Draw()
                    #     pt.Draw();
                    #     pt2.Draw();
                    #     pt3.Draw();
                    #     pt4.Clear();
                    #     pt4.AddText(releaseName + ", " + ChannelName + ", " + ", " + fitConfigName);
                    #     pt4.AddText(taggerName + ", " + WPName + ", " + wp );
                    #     pt4.Draw();
                    #     divison_hist.Draw("LPESAME");
                    #     divison_hist.Draw("AXISSAME");
                    #     c_obs.Write()
                    #     c_file.Close()

output_file.Close()

print "outputfile closed."
