import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse

def draw_all_plots_in_dir(directory,path):
    gStyle.SetPaintTextFormat("1.3f");
    gStyle.SetNumberContours(12);
    print "currently we are in ", path, " name: ",directory.GetName()
    if not os.path.exists(path):
        os.makedirs(path)
    index_file = open(path+"index.html","w")
    head, name = os.path.split(path)
    name = name or os.path.basename(head)
    index_file.write("<html><head><title>Plots in Folder: "+name +"</title></head><body>\n")
    index_file.write("<h3> "+name + " </h3><p><a href='../index.html'>Back to index</a></p>\n")
    directory.cd()
    keyList=directory.GetListOfKeys ()
    for i in xrange(0, keyList.GetSize()):
        obj_name=keyList.At(i).GetName()
        print i,obj_name
        obj=directory.Get(obj_name)
        className=obj.ClassName()
        if className=="TDirectoryFile":
          index_file.write("<h3><a href='"+ obj_name +"/index.html'>"+obj_name+"</a></h3>\n")
          newpath=path+obj_name+"/"
          draw_all_plots_in_dir(obj,newpath)
        if className== "TCanvas":
          index_file.write("<a href='"+obj_name +".pdf'><img src='"+obj_name+".png' width='200' style='margin:1em'/></a></a>\n")
          a=obj.Clone(obj_name+"_a")
          a.SaveAs(path+obj_name+'.png')
          #gPad.Update()
          b=obj.Clone(obj_name+"_b")
          b.SaveAs(path+obj_name+'.pdf')
          # b.SaveAs(path+obj_name+'.root')
          

    index_file.write("</body></html>\n")
    index_file.close()


parser = argparse.ArgumentParser(
    description='Draw all plots in file.')
parser.add_argument('input_files',nargs='*',
                    help='input files')


gStyle.SetPaintTextFormat("1.3f");
gStyle.SetNumberContours(12);

args = parser.parse_args()
for inFile_name in args.input_files:
    #  gStyle->SetPaintTextFormat("1.3f");
    #  gStyle->SetNumberContours(12);
    inFile=ROOT.TFile(inFile_name,"read")
    if inFile:
        outdir=inFile_name[:-5]+'/'
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        draw_all_plots_in_dir(inFile,outdir)
