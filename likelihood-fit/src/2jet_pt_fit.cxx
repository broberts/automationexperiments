#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TChain.h"
#include <string>
#include "TObjString.h"
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "TRandom3.h"
#include "RooFit.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "cxxopts.hpp"
#include "Workspace_plottingMacro.h"
#include <cstdlib>
using namespace std;
using namespace RooFit;
void custom_print_p_xx_matrix(RooWorkspace* w,int n_pt_bins,string name="p_bb"){
        const int numWidth      = 12;
        const char separator    = ' ';
        cout<<name<<"(pt1,pt2) used for fit:"<<endl;
        cout << left << setw(numWidth) << setfill(separator) << "pt"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "1"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "2"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "3"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "4"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "5"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "6"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "7"<<',';
        cout << endl;
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                cout << left << setw(numWidth) << setfill(separator) << pt_jet1<<',';
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        //be carefull with this! we only estimate the half of the matrix, since jets are pt ordered!
                        if (pt_jet2>pt_jet1) {
                                cout << left << setw(numWidth) << setfill(separator) << "-" <<',';
                                continue;
                        }
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        RooAbsReal * p_bb_bin = w->function((name+"_"+suffix_pt12).c_str());
                        cout << left << setw(numWidth) << setfill(separator) << p_bb_bin->getValV() <<',';
                }
                cout << endl;
        }
        cout<<"**************************************************************************************************************************"<<endl;
}
void custom_print_workspace(RooWorkspace* w,int n_pt_bins,int n_mv2c10_bins,THnSparseD* data_input_hist,RooFitResult* fitResult=NULL){
        const int numWidth      = 12;
        const char separator    = ' ';
        RooRealVar *f_bb=w->var("f_bb");
        RooRealVar *f_ll=w->var("f_ll");
        RooAbsReal * f_bl = w->function("f_bl");
        cout<<"**************************************************************************************************************************"<<endl;
        if (fitResult) {cout<<"Parameters postfit:"<<endl; }
        else {cout<<"Parameters prefit:"<<endl; }
        cout << left << setw(numWidth) << setfill(separator) << "f_XX"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "Value"<<',';
        cout << left << setw(numWidth) << setfill(separator) << "Error"<<',';
        cout << endl;
        cout << left << setw(numWidth) << setfill(separator) << "f_bb" <<',';
        cout << left << setw(numWidth) << setfill(separator) << f_bb->getValV()<<',';
        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << f_bb->getPropagatedError(*fitResult)<<','; }
        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
        cout << endl;
        cout << left << setw(numWidth) << setfill(separator) << "f_bl" <<',';
        cout << left << setw(numWidth) << setfill(separator) << f_bl->getValV()<<',';
        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << f_bl->getPropagatedError(*fitResult)<<','; }
        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
        cout << endl;
        cout << left << setw(numWidth) << setfill(separator) << "f_ll" <<',';
        cout << left << setw(numWidth) << setfill(separator) << f_ll->getValV()<<',';
        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << f_ll->getPropagatedError(*fitResult)<<','; }
        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
        cout << endl;
        cout<<"**************************************************************************************************************************"<<endl;


        for (int mv2c10_bin=1; mv2c10_bin<=n_mv2c10_bins; mv2c10_bin++) {
                double lower_mv2c10_bound,upper_mv2c10_bound,lower_pt_bound,upper_pt_bound;
                cout << left << setw(numWidth) << setfill(separator) << "mv2c10_bin"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "pt_bin"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "lower_pt"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "upper_pt"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "p_b"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "p_b_error"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "e_b"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "e_b_error"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "sf_b"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "sf_b_error"<<',';
                cout << left << setw(numWidth) << setfill(separator) << "p_l"<<',';
                cout << endl;
                // Loop on mv2c10 bins
                for (int pt_bin=1; pt_bin<=n_pt_bins; pt_bin++) {
                        string suffix_pt = "pt_";
                        suffix_pt = suffix_pt + to_string(pt_bin);
                        string suffix_mv = suffix_pt+"_mv2c10_";
                        suffix_mv = suffix_mv + to_string(mv2c10_bin);
                        lower_pt_bound=data_input_hist->GetAxis(3)->GetBinLowEdge ( pt_bin);
                        upper_pt_bound=data_input_hist->GetAxis(3)->GetBinUpEdge ( pt_bin);
                        cout << left << setw(numWidth) << setfill(separator) << mv2c10_bin <<',';
                        cout << left << setw(numWidth) << setfill(separator) << pt_bin<<',';
                        cout << left << setw(numWidth) << setfill(separator) << lower_pt_bound<<',';
                        cout << left << setw(numWidth) << setfill(separator) << upper_pt_bound<<',';
                        RooAbsReal * p_b_bin = w->function(("p_b_"+suffix_mv).c_str());
                        cout << left << setw(numWidth) << setfill(separator) << p_b_bin->getValV() <<',';
                        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << w->function(("p_b_"+suffix_mv).c_str())->getPropagatedError(*fitResult)<<','; }
                        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
                        cout << left << setw(numWidth) << setfill(separator) <<  w->function(("e_b_"+suffix_mv).c_str())->getValV() <<',';
                        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << w->function(("e_b_"+suffix_mv).c_str())->getPropagatedError(*fitResult)<<','; }
                        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
                        cout << left << setw(numWidth) << setfill(separator) <<  w->function(("sf_b_"+suffix_mv).c_str())->getValV() <<',';
                        if (fitResult) {cout << left << setw(numWidth) << setfill(separator) << w->function(("sf_b_"+suffix_mv).c_str())->getPropagatedError(*fitResult)<<','; }
                        else {cout << left << setw(numWidth) << setfill(separator) << "-" <<','; }
                        cout << left << setw(numWidth) << setfill(separator) <<  w->function(("p_l_"+suffix_mv).c_str())->getValV() <<',';
                        cout << endl;
                }
                cout << endl;
        }
        cout<<"**************************************************************************************************************************"<<endl;
        if (fitResult)
        {
                custom_print_p_xx_matrix(w,n_pt_bins,"f_bb");
                custom_print_p_xx_matrix(w,n_pt_bins,"f_bl");
                custom_print_p_xx_matrix(w,n_pt_bins,"f_lb");
                custom_print_p_xx_matrix(w,n_pt_bins,"f_ll");
        }
}

int main (int argc, char *argv[])
{
        string inputfile_name, out_dir,outputfile_name, config_string_name, dataName, rlTag, taggerName, workingPoint, systName, channelName, fitConfig, boot_strap_name;
        bool closure_test = false;
        bool do_plots=true;
        int closure_test_seed = -1;
        bool varry_fs=false; //set to true if you want to let f_bl,f_bb,f_ll and f_lb float.
        bool NConst=false;
        bool pl_up=false;
        int boot_strap_weight=-1;
        try
        {
                cxxopts::Options options(argv[0], " - example command line options");
                options.positional_help("[input output]");
                options.add_options()
                        ("i,input", "Input", cxxopts::value<std::string>(),"input_file")
                        ("o,output", "Final Outputfile_name will be added together: out_dir/Workspaces/Workspace _ rlTag _ dataName _ taggerName _ workingPoint _ channelName  _ fitConfig _ systName ", cxxopts::value<std::string>()
                        ->default_value("FitResults"), "out_dir") //name
                        ("r,rlTag", "rlTag", cxxopts::value<std::string>()->default_value("r21"), "rlTag")
                        ("d,dataName", "dataName", cxxopts::value<std::string>()->default_value("d1516"), "dataName")
                        ("t,tagger", "taggerName", cxxopts::value<std::string>()->default_value("MV2c10"), "taggerName")
                        ("w,workingPoint", "Name of the Working point: FixedCutBEff, HybBEff", cxxopts::value<std::string>()->default_value("FixedCutBEff"),"name" ) //
                        ("s,syst", "name of systematics", cxxopts::value<std::string>()->default_value("nominal"), "systName")
                        ("c,channel", "name of channel to fit", cxxopts::value<std::string>()->default_value("emu_OS_J2"), "channelName")
                        ("closure_test", "take mc as data", cxxopts::value<bool>(closure_test))
                        ("boot_strap", "run over a boot strap weight", cxxopts::value<int>(), "weight")
                        ("closure_test_seed", "set seed for closure_test random generator", cxxopts::value<int>(), "seed")
                                ("pl_up", "simulate mistagrate of light up by factor 2", cxxopts::value<bool>(pl_up))
                                ("varry_fs", "varry the f_xx fractions", cxxopts::value<bool>(varry_fs))
                                ("NConst", "differnet normalisation of f_ll(p1,p2) fractions. no N(p1,p2) but one single N_tot for all pt Bins.", cxxopts::value<bool>(NConst))
                                ("h, help", "Print help")
                                ;

                                options.parse(argc, argv);

                if (options.count("help"))
                {
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                if (options.count("input"))
                {
                        std::cout << "Input = " << options["input"].as<std::string>()<< std::endl;
                        inputfile_name=options["input"].as<std::string>();
                }
                else {
                        std::cout << "please give input file!"<< std::endl;
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                std::cout << "Output = " << options["output"].as<std::string>()<< std::endl;
                out_dir=options["output"].as<std::string>();
                std::cout << "tagger_name = " << options["tagger"].as<std::string>()<< std::endl;
                taggerName=options["tagger"].as<std::string>();
                std::cout << "workingPoint = " << options["workingPoint"].as<std::string>()<< std::endl;
                workingPoint=options["workingPoint"].as<std::string>();
                rlTag=options["rlTag"].as<std::string>();
                dataName=options["dataName"].as<std::string>();
                systName=options["syst"].as<std::string>();
                channelName=options["channel"].as<std::string>();
                if (options.count("closure_test_seed"))
                {

                        closure_test_seed=options["closure_test_seed"].as<int>();
                        cout<< "got cl seed "<<closure_test_seed <<endl;
                        closure_test=true;
                }
                if (options.count("boot_strap"))
                {

                        boot_strap_weight=options["boot_strap"].as<int>();
                        cout<< "got boot_strap"<<boot_strap_weight <<endl;
                }
        } catch (const cxxopts::OptionException& e)
        {
                std::cout << "error parsing options: " << e.what() << std::endl;
                exit(1);
        }
        fitConfig="";
        //hf4_data_emu_OS_J2_hist_for_fit_MV2c10_HybBEff
        string combined_tagger_name=channelName+"_hist_for_fit_"+taggerName+"_"+workingPoint;
        int set_n_pt_bins=0; //set to number of the highest pt bin you want to fit to. not completly supported, use carefull! (good for testing :))
        int fix_p_l=1;
        cout<< "hello World!"<<endl;
        boot_strap_name="";
        if (boot_strap_weight>=0){
            boot_strap_name="_boot_strap_"+to_string(boot_strap_weight);
            cout<<"running on: "<< boot_strap_name<<endl;
        }

        // RooWorkspace, RooCategory, RooSimultaneous building
        //idea to use mv2c10 as variable and pt1pt2 as category
        RooWorkspace* w = new RooWorkspace("w","w");
        RooCategory pt_jet1_jet2("pt_jet1_jet2","pt_jet1_jet2");
        RooSimultaneous full_model("full_model","full_model",pt_jet1_jet2);
        //datalist to insert categorys
        std::map<std::string, RooDataHist*> datalist;
        datalist.clear();

        //opening input file:
        cout<<"opening input File ... "<<endl;
        //TFile *f = new TFile("../../TTbar-b-calib-final-selection/rl20.7-170831/out/combination_for_fit_ttb_powpy6.root","read");
        //TFile *f = new TFile("../../TTbar-b-calib-final-selection/rl20.7-170831/out/testdata_for_fit.root","read");
        //TFile *f = new TFile("/afs/desy.de/user/j/jgeisen/public/combination_for_fit_21-2-4_old.root","read");
        TFile *f = new TFile(inputfile_name.c_str(),"read");
        //Loading MC:
        cout<<"loading MC from File ... "<<endl;
        THnSparseD* h_mc_input_hist_ll=(THnSparseD*)f->Get(("hf4_MC_combined_"+combined_tagger_name+"_ll"+boot_strap_name).c_str());
        h_mc_input_hist_ll=(THnSparseD*) h_mc_input_hist_ll->Clone("hf4_MC_combined_ll");
        THnSparseD* h_mc_input_hist_bb=(THnSparseD*)f->Get(("hf4_MC_combined_"+combined_tagger_name+"_bb"+boot_strap_name).c_str());
        h_mc_input_hist_bb=(THnSparseD*) h_mc_input_hist_bb->Clone("hf4_MC_combined_bb");
        THnSparseD* h_mc_input_hist_bl=(THnSparseD*)f->Get(("hf4_MC_combined_"+combined_tagger_name+"_bl"+boot_strap_name).c_str());
        h_mc_input_hist_bl=(THnSparseD*) h_mc_input_hist_bl->Clone("hf4_MC_combined_bl");
        THnSparseD* h_mc_input_hist_lb=(THnSparseD*)f->Get(("hf4_MC_combined_"+combined_tagger_name+"_lb"+boot_strap_name).c_str());
        h_mc_input_hist_lb=(THnSparseD*) h_mc_input_hist_lb->Clone("hf4_MC_combined_lb");
        // TH2D*       h_mc_input_hist_l_n= (TH2D*)f->Get(("hff_MC_combined_"+combined_tagger_name+"_light").c_str());
        // h_mc_input_hist_l_n=(TH2D*) h_mc_input_hist_l_n->Clone("hff_MC_combined_light");
        TH2D* h_mc_input_hist_l_n=(TH2D*) h_mc_input_hist_ll->Projection(2,0)->Clone("hff_MC_combined_light");
        h_mc_input_hist_l_n->Add( h_mc_input_hist_ll->Projection(3,1));
        h_mc_input_hist_l_n->Add(h_mc_input_hist_bl->Projection(3,1));
        h_mc_input_hist_l_n->Add( h_mc_input_hist_lb->Projection(2,0));


        TH2D*       h_mc_input_hist_b_n= (TH2D*)f->Get(("hff_MC_combined_"+combined_tagger_name+"_b").c_str());
        h_mc_input_hist_b_n=(TH2D*) h_mc_input_hist_b_n->Clone("hff_MC_combined_b");
        TH2D*       h_mc_input_hist_b_ttb= (TH2D*)f->Get(("hff_MC_ttb_"+combined_tagger_name+"_b").c_str());
        h_mc_input_hist_b_ttb=(TH2D*) h_mc_input_hist_b_ttb->Clone("hff_MC_ttb_b");
        THnSparseD* h_mc_tot=(THnSparseD*) h_mc_input_hist_ll->Clone("hf4_MC_tot");
        h_mc_tot->Add(h_mc_input_hist_lb);
        h_mc_tot->Add(h_mc_input_hist_bl);
        h_mc_tot->Add(h_mc_input_hist_bb);
        // TH1D*       h_mc_input_hist_l_shape=h_mc_input_hist_l_n->ProjectionX();
        // h_mc_input_hist_l_shape->Scale(1./h_mc_input_hist_l_shape->Integral());
        // TH1D*       h_mc_input_hist_b_shape=h_mc_input_hist_b_n->ProjectionX();
        // h_mc_input_hist_b_shape->Scale(1./h_mc_input_hist_b_shape->Integral());

        THnSparseD* data_input_hist;
        if (!closure_test) {
                cout<<"loading Data from File ... "<<"hf4_data_"+combined_tagger_name <<endl;
                data_input_hist=(THnSparseD*)f->Get(("hf4_data_"+combined_tagger_name).c_str());
                data_input_hist=(THnSparseD*) data_input_hist->Clone("hf4_data");
        }
        else{
                cout<<"closure_test ...!!!!!!! Data from MC!! "<<endl;
                if (systName=="nominal"||systName=="clTestMoMc") {
                        systName="clTestMoMc";
                }
                else if (systName!="clTestMoMo") systName="clTestMoMc";
                data_input_hist=(THnSparseD*) h_mc_input_hist_ll->Clone("hf4_data");
                data_input_hist->Add(h_mc_input_hist_lb);
                data_input_hist->Add(h_mc_input_hist_bl);
                data_input_hist->Add(h_mc_input_hist_bb);
                int tot_data_bins = data_input_hist->GetNbins();
                if (closure_test_seed==-1)
                {
                        closure_test_seed=101;
                }
                else if (closure_test_seed==-2) {
                        cout<<"closure_test ...!!!!!!! Data from MC withou stat!! "<<endl;
                        if (systName!="clTestMoMc_nstat") systName=systName+"_nstat";
                }
                else{
                        //we are doing cl tests for
                        systName=systName+"S"+to_string(closure_test_seed);
                        //do_plots=false;
                }
                if (closure_test_seed>=0)
                {
                        TRandom3* rGenerator=new TRandom3(closure_test_seed);
                        for(int bin=1; bin <= tot_data_bins; bin++) {
                                double old_value =data_input_hist->GetBinContent(bin);
                                int bin_value=rGenerator->Poisson(old_value );
                                data_input_hist->SetBinContent(bin, bin_value);
                                data_input_hist->SetBinError(bin,sqrt(bin_value));
                                cout<<"setted bin "<<bin<< " Old value: "<< old_value << " new value "<< bin_value<< " error "<< data_input_hist->GetBinError(bin) <<endl;
                        }
                }
        }
        int n_pt_bins=data_input_hist->GetAxis(3)->GetNbins();
        if (pl_up) {
                double factor=2.0;
                cout<<"Simulating pl up by factor "<< factor <<" !!!!!!! "<<endl;
                //fitConfig=fitConfig+"_pl_up";
                systName="misstagLight_up";

                //int bin[4] ={0,0,0,0}
                //h_mc_input_hist_l_n
                //double untagged_light = h_mc_input_hist_ll->GetBinContent(bin);
                for (int pt_bin=1; pt_bin<=n_pt_bins; pt_bin++)
                {
                        cout<<"pt_bin: "<<pt_bin<<endl;
                        double untagged = h_mc_input_hist_l_n->GetBinContent(1,pt_bin);
                        double tagged = h_mc_input_hist_l_n->Integral(2,5,pt_bin,pt_bin);
                        cout<<"untagged: "<<untagged<<" tagged: "<<tagged<<endl;
                        h_mc_input_hist_l_n->SetBinContent(1,pt_bin,(untagged - tagged*(factor-1.)));
                        for (int mv_bin=2; mv_bin<=5; mv_bin++)
                        {
                                h_mc_input_hist_l_n->SetBinContent(mv_bin,pt_bin,h_mc_input_hist_l_n->GetBinContent(mv_bin,pt_bin)*factor);
                        }
                        untagged = h_mc_input_hist_l_n->GetBinContent(1,pt_bin);
                        tagged = h_mc_input_hist_l_n->Integral(2,5,pt_bin,pt_bin);
                        cout<<"later: untagged: "<<untagged<<" tagged: "<<tagged<<endl;
                }
        }
        if(NConst) {
                fitConfig=fitConfig+"_NConst";
                cout<<"running with NConst-configuration!!";
        }

        if (set_n_pt_bins) {
                cout<<"Setting max pt bins to: "<<set_n_pt_bins<<endl;
                n_pt_bins=set_n_pt_bins;
                data_input_hist->GetAxis(2)->SetRange(1, set_n_pt_bins);
                data_input_hist->GetAxis(3)->SetRange(1, set_n_pt_bins);
        }
        //importing histograms to workspace>
        w->import(*data_input_hist);
        w->import(*h_mc_input_hist_ll);
        w->import(*h_mc_input_hist_bb);
        w->import(*h_mc_input_hist_bl);
        w->import(*h_mc_input_hist_lb);
        w->import(*h_mc_input_hist_l_n);
        w->import(*h_mc_input_hist_b_n);
        w->import(*h_mc_input_hist_b_ttb);
        //importing TStings to workspace in order to have them as labels on the plots :)
        TObjString* dataName_help=new TObjString(dataName.c_str());
        w->import(*dataName_help,"dataName");
        TObjString* rlTag_help=new TObjString(rlTag.c_str());
        w->import(*rlTag_help,"rlTag");
        TObjString* taggerName_help=new TObjString(taggerName.c_str());
        w->import(*taggerName_help,"taggerName");
        TObjString* workingPoint_help=new TObjString(workingPoint.c_str());
        w->import(*workingPoint_help,"workingPoint");
        TObjString* systName_help=new TObjString(systName.c_str());
        w->import(*systName_help,"systName");
        TObjString* channelName_help=new TObjString(channelName.c_str());
        w->import(*channelName_help,"channelName");

        //setting the 4 Observables:
        int n_mv2c10_bins=data_input_hist->GetAxis(0)->GetNbins();
        cout<<"Data loaded: n_pt_bins: "<<n_pt_bins<<" n_mv2c10_bins: "<<n_mv2c10_bins<<endl;
        //use constant binning for mv2c10 bins. bins 0,1,2...
        RooBinning* r_mv2c10_bins= new RooBinning (n_mv2c10_bins,  0., n_mv2c10_bins,"r_mv2c10_bins");
        //2-jets->2 w variables
        w->import(RooRealVar("w_jet1", "w_jet1", 0.5,0,n_mv2c10_bins));
        RooRealVar* w_jet1=w->var("w_jet1");
        w_jet1->setBinning(*r_mv2c10_bins);
        w->import(RooRealVar("w_jet2", "w_jet2", 0.5,0,n_mv2c10_bins));
        RooRealVar* w_jet2=w->var("w_jet2");
        w_jet2->setBinning(*r_mv2c10_bins);


        //for fbb f_ll and so on some helpfull projections.. (don't project something twice, otherwise root complains!)
        TH1D* data_input_hist_proj0=data_input_hist->Projection(0);
        TH2D* data_input_hist_proj32=data_input_hist->Projection(3,2);
        TH1D* h_mc_input_hist_ll_proj0 =h_mc_input_hist_ll->Projection(0);
        TH2D* h_mc_input_hist_ll_proj32=h_mc_input_hist_ll->Projection(3,2);
        TH1D* h_mc_input_hist_lb_proj0 =h_mc_input_hist_lb->Projection(0);
        TH2D* h_mc_input_hist_lb_proj23=h_mc_input_hist_lb->Projection(2,3);
        TH1D* h_mc_input_hist_bl_proj0 =h_mc_input_hist_bl->Projection(0);
        TH2D* h_mc_input_hist_bl_proj32=h_mc_input_hist_bl->Projection(3,2);
        TH1D* h_mc_input_hist_bb_proj0 =h_mc_input_hist_bb->Projection(0);
        TH2D* h_mc_input_hist_bb_proj32=h_mc_input_hist_bb->Projection(3,2);
        TH2D* h_mc_tot_proj32=h_mc_tot->Projection(3,2);
        w->import(*h_mc_tot_proj32);
        TH2D* h_mc_input_hist_bl_combi=(TH2D*)h_mc_input_hist_bl_proj32->Clone("h_mc_input_hist_bl_combi");
        h_mc_input_hist_bl_combi->Add(h_mc_input_hist_lb_proj23);

        //defining helping doubles:
        double n_bb_mc,n_ll_mc,n_bl_mc_s,n_lb_mc_s, d_n_data,n_bl_mc_combi,n_mc;
        n_ll_mc=h_mc_input_hist_ll_proj0->Integral();
        n_lb_mc_s=h_mc_input_hist_lb_proj0->Integral();
        n_bb_mc=h_mc_input_hist_bb_proj0->Integral();
        n_bl_mc_s=h_mc_input_hist_bl_proj0->Integral();
        n_bl_mc_combi=n_bl_mc_s+n_lb_mc_s;
        n_mc=n_bb_mc+n_bl_mc_combi+n_ll_mc;
        d_n_data=data_input_hist_proj0->Integral();

        //defining variables to fit:
        //we dont need N_tot if we do a extended fit!
        w->import(RooRealVar("N_tot","N_tot", d_n_data,d_n_data-d_n_data/5, d_n_data+d_n_data/5 ));
        RooRealVar* N_tot=w->var("N_tot");

        w->import( RooRealVar("f_bb","f_bb",n_bb_mc/n_mc,n_bb_mc/n_mc-n_bb_mc/n_mc/10,n_bb_mc/n_mc+n_bb_mc/n_mc/10));
        RooRealVar *f_bb=w->var("f_bb");


        w->import(RooRealVar("f_ll","f_ll",n_ll_mc/n_mc,n_ll_mc/n_mc-n_ll_mc/n_mc/10,n_ll_mc/n_mc+n_ll_mc/n_mc/10));
        RooRealVar *f_ll=w->var("f_ll");

        w->import( RooFormulaVar("f_bl","f_bl","1-(@0+@1)", RooArgList(*f_bb, *f_ll)));
        RooAbsReal * f_bl = w->function("f_bl");
        if (!varry_fs) {
                fitConfig=fitConfig+"_fconst";
                f_ll->setConstant(kTRUE);
                f_bb->setConstant(kTRUE);
        }
        //set to put constrains into
        RooArgSet* ex_constraints=new RooArgSet("ex_constraints");
        //for (int bin=1;bin<=n_mv2c10_bins;bin++){
        //cout<<"bin: "<<bin<<endl;
        //string suffix=suffix_list_single_jet.at(bin-1);
        //RooFormulaVar *s_l=new RooFormulaVar(("s_l_"+suffix).c_str(),"shape of light","@0/@1", RooArgList(*w->var(("N_l_"+suffix).c_str()),*w->arg("N_l_sum")));
        //RooGaussian* p_l_constraint = new RooGaussian(("p_l_constraint_"+suffix).c_str(),"p_l constraint per bin",*w->var(("p_l_"+suffix).c_str()), RooConst(h_mc_n_l->GetBinContent(bin)/n_l_mc) ,RooConst(h_mc_n_l->GetBinContent(bin)/n_l_mc/100 ) );
        //w->import(*p_l_constraint);
        //ex_constraints->add(*w->arg(("p_l_constraint_"+suffix).c_str()));
        //}
//first loop over pt bins to define p_b_pt and p_l_pt for jet1 and jet2 which are used to define pdf_b_for a given pt
        for (int pt_bin=1; pt_bin<=n_pt_bins; pt_bin++) {

                string suffix_pt = "pt_";
                int mv2c10_bin;
                double lower_mv2c10_bound,upper_mv2c10_bound,lower_pt_bound,upper_pt_bound;
                lower_pt_bound=data_input_hist->GetAxis(3)->GetBinLowEdge ( pt_bin);
                upper_pt_bound=data_input_hist->GetAxis(3)->GetBinUpEdge ( pt_bin);
                suffix_pt = suffix_pt + to_string(pt_bin);

                RooArgSet* p_b_all_mv2c10=new RooArgSet("p_b_all_mv2c10");
                RooArgSet* p_l_all_mv2c10=new RooArgSet("p_l_all_mv2c10");
                double pt_bin_integral_b=h_mc_input_hist_b_n->Integral(0,n_mv2c10_bins,pt_bin,pt_bin); //how many b jets in given pt bin?
                double pt_bin_integral_b_ttb=h_mc_input_hist_b_ttb->Integral(0,n_mv2c10_bins,pt_bin,pt_bin); //how many b jets in given pt bin?
                double pt_bin_integral_l=h_mc_input_hist_l_n->Integral(0,n_mv2c10_bins,pt_bin,pt_bin); //how many l jets in given pt bin?
                cout<<"pt_Bin: "<<pt_bin<<" from: "<<lower_pt_bound<<" to: "<<upper_pt_bound<<" Suffix: "<< suffix_pt<<endl;
                // Loop on mv2c10 bins
                for (mv2c10_bin=1; mv2c10_bin<n_mv2c10_bins; mv2c10_bin++) {
                        lower_mv2c10_bound=data_input_hist->GetAxis(0)->GetBinLowEdge ( mv2c10_bin);
                        upper_mv2c10_bound=data_input_hist->GetAxis(0)->GetBinUpEdge ( mv2c10_bin);
                        // suffix used for categories
                        string suffix_mv = suffix_pt+"_mv2c10_";
                        suffix_mv = suffix_mv + to_string(mv2c10_bin);
                        w->import( RooRealVar(("p_b_"+suffix_mv).c_str(),("p_b_"+suffix_mv).c_str(),0.2,0.,1.));
                        RooRealVar * p_b_bin = w->var(("p_b_"+suffix_mv).c_str());
                        p_b_bin->setVal((h_mc_input_hist_b_n->GetBinContent(mv2c10_bin,pt_bin))/pt_bin_integral_b);
                        //  p_b_bin ->	setConstant(kTRUE);
                        p_b_all_mv2c10->add(*p_b_bin);
                        w->import( RooRealVar(("p_l_"+suffix_mv).c_str(),("p_l_"+suffix_mv).c_str(),0.2,0.,1.));
                        RooRealVar * p_l_bin = w->var(("p_l_"+suffix_mv).c_str());
                        double p_l_val=(h_mc_input_hist_l_n->GetBinContent(mv2c10_bin,pt_bin))/pt_bin_integral_l;
                        p_l_bin->setVal(p_l_val);
                        if (fix_p_l) {
                                //if (mv2c10_bin<4){p_l_bin ->	setConstant(kTRUE);}
                                //if (pt_bin<3){p_l_bin ->	setConstant(kTRUE);}
                                p_l_bin->setConstant(kTRUE);
                        }
                        else{
                                RooGaussian* p_l_constraint = new RooGaussian(("p_l_constraint_"+suffix_mv).c_str(),"p_l constraint per bin",*p_l_bin, RooConst(p_l_val),RooConst(p_l_val/100 ) );
                                w->import(*p_l_constraint);
                                ex_constraints->add(*w->arg(("p_l_constraint_"+suffix_mv).c_str()));
                        }
                        p_l_all_mv2c10->add(*p_l_bin);
                        cout<<"Bin: "<<mv2c10_bin<<" from: "<<lower_mv2c10_bound<<" to: "<<upper_mv2c10_bound<<" Suffix: "<< suffix_mv<<endl;
                        cout<<"p_b_bin: "<<mv2c10_bin<<endl;
                }
                //for each pt bin we define RooParametricStepFunctions  as pdfs
                //RooParametricStepFunction * pdf_b=new RooParametricStepFunction ("pdf_b", "pdf_b", w_je1, const RooArgList &coefList, TArrayD &limits, Int_t nBins=1)
                TArrayD* limits=new TArrayD (n_mv2c10_bins+1,r_mv2c10_bins->array()); //limits = [0,1,2,...] which are the bin boundrys...
                w->import(RooParametricStepFunction (("pdf_b_jet1_"+suffix_pt).c_str(), ("pdf_b_jet1_"+suffix_pt).c_str(), *w_jet1, *p_b_all_mv2c10,*limits, n_mv2c10_bins ));
                w->import(RooParametricStepFunction (("pdf_b_jet2_"+suffix_pt).c_str(), ("pdf_b_jet2_"+suffix_pt).c_str(), *w_jet2, *p_b_all_mv2c10,*limits, n_mv2c10_bins ));
                w->import(RooParametricStepFunction (("pdf_l_jet1_"+suffix_pt).c_str(), ("pdf_l_jet1_"+suffix_pt).c_str(), *w_jet1, *p_l_all_mv2c10,*limits, n_mv2c10_bins ));
                w->import(RooParametricStepFunction (("pdf_l_jet2_"+suffix_pt).c_str(), ("pdf_l_jet2_"+suffix_pt).c_str(), *w_jet2, *p_l_all_mv2c10,*limits, n_mv2c10_bins ));
                // last mv2c10 bin seperatly:, Just for printout!!!
                lower_mv2c10_bound=data_input_hist->GetAxis(0)->GetBinLowEdge ( mv2c10_bin);
                upper_mv2c10_bound=data_input_hist->GetAxis(0)->GetBinUpEdge ( mv2c10_bin);
                string suffix_mv = suffix_pt+"_mv2c10_";
                suffix_mv = suffix_mv + to_string(mv2c10_bin);
                cout<<"Last Bin: "<<mv2c10_bin<<" from: "<<lower_mv2c10_bound<<" to: "<<upper_mv2c10_bound<<" Suffix: "<< suffix_mv<<endl;
                w->import( RooFormulaVar(("p_b_"+suffix_mv).c_str(),("p_b_"+suffix_mv).c_str(),"1-(@0+@1+@2+@3)",*p_b_all_mv2c10));
                w->import( RooFormulaVar(("p_l_"+suffix_mv).c_str(),("p_l_"+suffix_mv).c_str(),"1-(@0+@1+@2+@3)",*p_l_all_mv2c10));
                p_b_all_mv2c10->add(*w->function(("p_b_"+suffix_mv).c_str()));
                p_l_all_mv2c10->add(*w->function(("p_l_"+suffix_mv).c_str()));
                if (!(fix_p_l)) {
                        double p_l_val=(h_mc_input_hist_l_n->GetBinContent(mv2c10_bin,pt_bin))/pt_bin_integral_l;
                        RooGaussian* p_l_constraint = new RooGaussian(("p_l_constraint_"+suffix_mv).c_str(),"p_l constraint per bin",*w->function(("p_l_"+suffix_mv).c_str()), RooConst(p_l_val),RooConst(p_l_val/100 ) );
                        w->import(*p_l_constraint);
                        ex_constraints->add(*w->arg(("p_l_constraint_"+suffix_mv).c_str()));
                }
                //e_bs (also just for print out!):
                w->import( RooFormulaVar(("e_b_"+suffix_pt+"_mv2c10_5").c_str(),("e_b_"+suffix_pt+"_mv2c10_5").c_str(),"(@4)",*p_b_all_mv2c10));
                w->import( RooFormulaVar(("e_b_"+suffix_pt+"_mv2c10_4").c_str(),("e_b_"+suffix_pt+"_mv2c10_4").c_str(),"(@3+@4)",*p_b_all_mv2c10));
                w->import( RooFormulaVar(("e_b_"+suffix_pt+"_mv2c10_3").c_str(),("e_b_"+suffix_pt+"_mv2c10_3").c_str(),"(@2+@3+@4)",*p_b_all_mv2c10));
                w->import( RooFormulaVar(("e_b_"+suffix_pt+"_mv2c10_2").c_str(),("e_b_"+suffix_pt+"_mv2c10_2").c_str(),"(@1+@2+@3+@4)",*p_b_all_mv2c10));
                w->import( RooFormulaVar(("e_b_"+suffix_pt+"_mv2c10_1").c_str(),("e_b_"+suffix_pt+"_mv2c10_1").c_str(),"(@0+@1+@2+@3+@4)",*p_b_all_mv2c10));
                //scalefactors:
                for (mv2c10_bin=1; mv2c10_bin<=n_mv2c10_bins; mv2c10_bin++) {
                        double e_b_pt_mv;
                        e_b_pt_mv=(h_mc_input_hist_b_ttb->Integral(mv2c10_bin,n_mv2c10_bins,pt_bin,pt_bin))/pt_bin_integral_b_ttb;
                        w->import( RooFormulaVar(("sf_b_"+suffix_pt+"_mv2c10_"+to_string(mv2c10_bin)).c_str(),("sf_b_"+suffix_pt+"_mv2c10_"+to_string(mv2c10_bin)).c_str(),"(@0/@1)",RooArgSet(*w->function(("e_b_"+suffix_pt+"_mv2c10_"+to_string(mv2c10_bin)).c_str()),RooConst(e_b_pt_mv))));
                }
        }
//end loop oder pt bin

//loop over both pt bins to define p_bb_pt1_pt2 (the probability to get an event with pt1 and pt2 given that it is bb)
//we have to be very carefull here when we want to let p_bb float! Sum over all p_bb has to be 1!
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        //be carefull with this! we only estimate the half of the matrix, since jets are pt ordered!
                        if (pt_jet2>pt_jet1) {continue; }
                        string suffix_pt12 = "pt_";
                        string f_or_p="f";
                        if (NConst) {
                                f_or_p="p";
                        }
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        double mc_value_bb;
                        if (!NConst) {
                                mc_value_bb=(h_mc_input_hist_bb_proj32->GetBinContent(pt_jet1,pt_jet2)/ h_mc_tot_proj32->GetBinContent(pt_jet1,pt_jet2)); //f_bb-> getValV();
                        }
                        else mc_value_bb = h_mc_input_hist_bb_proj32->GetBinContent(pt_jet1,pt_jet2)/n_bb_mc;
                        w->import( RooRealVar((f_or_p + "_bb_"+suffix_pt12).c_str(),(f_or_p + "_bb_"+suffix_pt12).c_str(),mc_value_bb,0,1));
                        cout<< "Suffix: "<<suffix_pt12<<" p_bb: "<<mc_value_bb;
                        RooRealVar *p_bb=w->var((f_or_p + "_bb_"+suffix_pt12).c_str());
                        //p_bb->setVal(1.);
                        p_bb->setConstant(kTRUE);
                        //same thing for bl
                        double mc_value_bl;
                        if (!NConst) {
                                mc_value_bl=(h_mc_input_hist_bl_combi->GetBinContent(pt_jet1,pt_jet2)/ h_mc_tot_proj32->GetBinContent(pt_jet1,pt_jet2)); //f_bl-> getValV();
                        }
                        else mc_value_bl =h_mc_input_hist_bl_combi->GetBinContent(pt_jet1,pt_jet2)/n_bl_mc_combi;
                        //Since we calculate both terms (f_bl and f_lb) if they are on the diagonal (pt1=pt2) we have to normalise these evtens by /2
                        if (pt_jet1==pt_jet2) {mc_value_bl=mc_value_bl/2; }
                        w->import( RooRealVar((f_or_p + "_bl_"+suffix_pt12).c_str(),(f_or_p + "_bl_"+suffix_pt12).c_str(),mc_value_bl,0,1));
                        RooRealVar *p_bl=w->var((f_or_p + "_bl_"+suffix_pt12).c_str());
                        //p_bl->setVal(1.);
                        p_bl->setConstant(kTRUE);
                        //now for lb careful because of symmetrie!
                        double mc_value_lb;
                        if (!NConst) {
                                mc_value_lb=(h_mc_input_hist_bl_combi->GetBinContent(pt_jet2,pt_jet1)/ h_mc_tot_proj32->GetBinContent(pt_jet1,pt_jet2)); //f_bl-> getValV();
                        }
                        else mc_value_lb=h_mc_input_hist_bl_combi->GetBinContent(pt_jet2,pt_jet1)/n_bl_mc_combi;
                        //Since we calculate both terms (f_bl and f_lb) if they are on the diagonal (pt1=pt2) we have to normalise these evtens by /2
                        if (pt_jet1==pt_jet2) {mc_value_lb=mc_value_lb/2; }
                        w->import( RooRealVar((f_or_p + "_lb_"+suffix_pt12).c_str(),(f_or_p + "_lb_"+suffix_pt12).c_str(),mc_value_lb,0,1));
                        RooRealVar *p_lb=w->var((f_or_p + "_lb_"+suffix_pt12).c_str());
                        //p_lb->setVal(1.);
                        p_lb->setConstant(kTRUE);
                        //for f_ll everythin is easy again.
                        double mc_value_ll;
                        if (!NConst) {
                                mc_value_ll=(h_mc_input_hist_ll_proj32->GetBinContent(pt_jet1,pt_jet2)/ h_mc_tot_proj32->GetBinContent(pt_jet1,pt_jet2)); //f_ll-> getValV();
                        }
                        else mc_value_ll = h_mc_input_hist_ll_proj32->GetBinContent(pt_jet1,pt_jet2)/n_ll_mc;
                        w->import( RooRealVar((f_or_p + "_ll_"+suffix_pt12).c_str(),(f_or_p + "_ll_"+suffix_pt12).c_str(),mc_value_ll,0,1));
                        RooRealVar *p_ll=w->var((f_or_p + "_ll_"+suffix_pt12).c_str());
                        //p_ll->setVal(1.);
                        p_ll->setConstant(kTRUE);
                        if (NConst)
                        {
                                w->import( RooFormulaVar(("f_lb_"+suffix_pt12).c_str(),("f_lb_"+suffix_pt12).c_str(),"(@0*@1)", RooArgList(*f_bl, *p_lb)));
                                w->import( RooFormulaVar(("f_ll_"+suffix_pt12).c_str(),("f_ll_"+suffix_pt12).c_str(),"(@0*@1)", RooArgList(*f_ll, *p_ll)));
                                w->import( RooFormulaVar(("f_bl_"+suffix_pt12).c_str(),("f_bl_"+suffix_pt12).c_str(),"(@0*@1)", RooArgList(*f_bl, *p_bl)));
                                w->import( RooFormulaVar(("f_bb_"+suffix_pt12).c_str(),("f_bb_"+suffix_pt12).c_str(),"(@0*@1)", RooArgList(*f_bb, *p_bb)));
                        }

                }
        }


//new loop over both pt bin to define full model.
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        if (pt_jet2>pt_jet1) {continue; }
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        // Defining model:
                        //getting pdf to use:
                        string suffix_jet1 = "pt_"+ to_string(pt_jet1);
                        string suffix_jet2 = "pt_"+ to_string(pt_jet2);
                        RooAbsPdf* pdf_b_jet1=w->pdf(("pdf_b_jet1_"+suffix_jet1).c_str());
                        RooAbsPdf* pdf_b_jet2=w->pdf(("pdf_b_jet2_"+suffix_jet2).c_str());
                        RooAbsPdf* pdf_l_jet1=w->pdf(("pdf_l_jet1_"+suffix_jet1).c_str());
                        RooAbsPdf* pdf_l_jet2=w->pdf(("pdf_l_jet2_"+suffix_jet2).c_str());
                        RooAbsReal* f_bb_pt12=w->function(("f_bb_"+suffix_pt12).c_str());
                        RooAbsReal* f_bl_pt12=w->function(("f_bl_"+suffix_pt12).c_str());
                        RooAbsReal* f_lb_pt12=w->function(("f_lb_"+suffix_pt12).c_str());
                        RooAbsReal* f_ll_pt12=w->function(("f_ll_"+suffix_pt12).c_str());
                        double cutOff = 0; //0.001;
                        RooProdPdf* P_bb= new RooProdPdf(("P_bb_"+suffix_pt12).c_str(),("P_bb_"+suffix_pt12).c_str(),*pdf_b_jet1,*pdf_b_jet2, cutOff );
                        RooProdPdf* P_bl= new RooProdPdf(("P_bl_"+suffix_pt12).c_str(),("P_bl_"+suffix_pt12).c_str(),*pdf_b_jet1,*pdf_l_jet2, cutOff );
                        RooProdPdf* P_lb= new RooProdPdf(("P_lb_"+suffix_pt12).c_str(),("P_lb_"+suffix_pt12).c_str(),*pdf_l_jet1,*pdf_b_jet2, cutOff );
                        RooProdPdf* P_ll= new RooProdPdf(("P_ll_"+suffix_pt12).c_str(),("P_ll_"+suffix_pt12).c_str(),*pdf_l_jet1,*pdf_l_jet2, cutOff );
                        RooAddPdf* P_full= new RooAddPdf(("P_full_"+suffix_pt12).c_str(),("P_full_"+suffix_pt12).c_str(), RooArgList(*P_bb,*P_bl,*P_lb,*P_ll),RooArgList(*f_bb_pt12,*f_bl_pt12,*f_lb_pt12,*f_ll_pt12));
                        RooExtendPdf* P_full_extended;
                        if (!NConst)
                        {
                                double d_n_mc_pt12=h_mc_tot_proj32->GetBinContent(pt_jet1,pt_jet2);
                                //double d_n_data_pt12=data_input_hist_proj32->GetBinContent(pt_jet1,pt_jet2);//h_mc_tot_proj32->GetBinContent(pt_jet1,pt_jet2)*d_n_data;
                                w->import(RooRealVar(("N_"+suffix_pt12).c_str(),("N_"+suffix_pt12).c_str(), d_n_mc_pt12,d_n_mc_pt12*0.01, d_n_mc_pt12*5. ));
                                RooRealVar* N_pt12=w->var(("N_"+suffix_pt12).c_str());
                                P_full_extended=new RooExtendPdf(("P_full_extended_"+suffix_pt12).c_str(), ("P_full_extended_"+suffix_pt12).c_str(), *P_full, *N_pt12);
                        }
                        else P_full_extended=new RooExtendPdf(("P_full_extended_"+suffix_pt12).c_str(), ("P_full_extended_"+suffix_pt12).c_str(), *P_full, *N_tot);
                        w->import(*P_full_extended);
                        full_model.addPdf(*(w->pdf(("P_full_extended_"+suffix_pt12).c_str())),suffix_pt12.c_str());
                        pt_jet1_jet2.defineType(suffix_pt12.c_str());
                }
        }

        w->import(full_model);
//new loop over both pt bin to import data.
        for (int pt_jet1=1; pt_jet1<=n_pt_bins; pt_jet1++) {
                for (int pt_jet2=1; pt_jet2<=n_pt_bins; pt_jet2++) {
                        if (pt_jet2>pt_jet1) {continue; }
                        //hn->GetAxis(12)->SetRange(from_bin, to_bin);
                        string suffix_pt12 = "pt_";
                        suffix_pt12 = suffix_pt12 + to_string(pt_jet1)+"_"+to_string(pt_jet2);
                        THnSparseD* data_input_hist_this=(THnSparseD*) data_input_hist->Clone(("data_input_hist_"+suffix_pt12).c_str());
                        data_input_hist_this->GetAxis(2)->SetRange(pt_jet1, pt_jet1);
                        data_input_hist_this->GetAxis(3)->SetRange(pt_jet2, pt_jet2);
                        TH2D* data_input_hist_this_proj10=data_input_hist_this->Projection(1,0);
                        data_input_hist_this_proj10->GetXaxis()->Set(n_mv2c10_bins,0,n_mv2c10_bins);
                        data_input_hist_this_proj10->GetYaxis()->Set(n_mv2c10_bins,0,n_mv2c10_bins);
                        RooDataHist* dh_data =new RooDataHist (("dh_data_"+suffix_pt12).c_str(),("dh_data"+suffix_pt12).c_str(), RooArgList(*w_jet1,*w_jet2), data_input_hist_this_proj10);
                        w->import(*dh_data);
                        datalist[suffix_pt12] =  (RooDataHist*)w->data(("dh_data_"+suffix_pt12).c_str());
                        int where[4]={2,3,pt_jet1,pt_jet2};
                        cout<<"importing data for bin: "<<suffix_pt12<<" value in bin 2,3 : "<< data_input_hist_this_proj10->GetBinContent(2,3)<<" == "<<data_input_hist_this->GetBinContent(where)<<endl;
                }
        }
        cout<<"importing datalist:"<<endl;
        RooDataHist* combined_data = new RooDataHist("combined_data", "combined_data", RooArgList(*w_jet1,*w_jet2), RooFit::Index(pt_jet1_jet2), RooFit::Import(datalist));
        w->import(*combined_data);


        w->Print();


        //w->Print();
        config_string_name=rlTag+"_"+dataName+"_"+taggerName+"_"+workingPoint+"_"+channelName+fitConfig+"_"+systName+boot_strap_name;
        int dir= system(("mkdir -p "+out_dir).c_str());
        dir= system(("mkdir -p "+out_dir+"/Workspaces").c_str());
        dir= system(("mkdir -p "+out_dir+"/FitPlots").c_str());
        outputfile_name=out_dir+"/Workspaces"+"/Workspace_"+config_string_name;
        outputfile_name=outputfile_name+".root";
        TFile *wsOut = new TFile((outputfile_name).c_str(),"recreate");
        w->Clone("Workspace_before_fit")->Write();
        custom_print_workspace(w,n_pt_bins,n_mv2c10_bins,data_input_hist);
        RooFitResult* fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save(),SumW2Error(kFALSE)); //,Extended(kTRUE)
        //RooFitResult* fitResult = full_model.fitTo(*w->data("combined_data"),ExternalConstraints(*ex_constraints),RooFit::Save());
        //RooAbsPdf* model=w->pdf("full_model");//("P_full");
        //RooFitResult* fitResult = model->fitTo(*w->data("data"),RooFit::Save()); //,SumW2Error(kFALSE)
        //w->import(*fitResult);
        custom_print_workspace(w,n_pt_bins,n_mv2c10_bins,data_input_hist,fitResult);

        w->Clone("Workspace_after_fit")->Write();
        fitResult->SetName("fitResult");
        fitResult->Write();
        wsOut->Close();
        cout<<"Outputfile Closed: "<<outputfile_name<<endl;
        //w->Print();
        f->Close();
        if (do_plots) Workspace_plottingMacro(outputfile_name, out_dir+"/FitPlots/FitPlot_"+config_string_name+".root");
}
