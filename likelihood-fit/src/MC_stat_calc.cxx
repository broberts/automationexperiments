#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TMatrixDSymEigen.h"
#include "TMatrixDEigen.h"
#include <THnSparse.h>
#include "TFile.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TLine.h"
#include "TCanvas.h"
#include <string>
#include "RooGlobalFunc.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooSimultaneous.h"
#include "RooParametricStepFunction.h"
#include "RooConstVar.h"
#include "RooWorkspace.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExtendPdf.h"
#include "RooAddPdf.h"
#include "RooBinning.h"
#include "RooProdPdf.h"
#include "RooFit.h"
#include "TObjString.h"
#include "TVectorD.h"
#include "RooArgList.h"
#include "RooFitResult.h"
#include "TGraphAsymmErrors.h"
#include "cxxopts.hpp"
#include "Check_fit.h"

#include "../../atlasstyle-00-03-05/AtlasStyle.C"
using namespace std;



TH1D* create_boot_strap_unc_syst(std::string plot_path, std::string plot_name, std::string syst_name, TFile *f_nominal, std::vector<TFile*> boot_strap_files, TFile *f_syst, std::vector<TFile*> syst_boot_strap_files,TFile *f_output )
{
    std::cout << "Trying to create_boot_strap_unc_syst for: " << plot_path<< plot_name<< std::endl;
    //create th1 with central values ->set unc later
    f_output->cd();
    //save nominal file:
    TH1D* h_nominal=((TH1D*)f_nominal->Get((plot_path+plot_name).c_str()));
    h_nominal->Write();
    TH1D* h_syst= (TH1D*)f_syst->Get((plot_path+plot_name).c_str())->Clone((plot_name+"_"+syst_name).c_str());
    TH1D* h_syst_unc= (TH1D*)h_syst->Clone((plot_name+"_"+syst_name+"_unc").c_str());
    h_syst_unc->Add(h_nominal, -1);
    h_syst_unc->Write();
    TH1D* h_boot_strap_unc= (TH1D*)h_syst_unc->Clone((plot_name+"_boot_strap_unc_rel").c_str());
    cout <<h_boot_strap_unc->GetNbinsX()<<endl;
    h_boot_strap_unc->Reset();

    //create folder with histos for  each bin. 20 bins 3sigma
    TDirectory *dir = 0 ;
    dir = f_output->GetDirectory((("f_" + plot_name + "_boot_strap_unc_syst_rel").c_str()));
    if (dir == 0)
        dir = f_output->mkdir((("f_" + plot_name + "_boot_strap_unc_syst_rel").c_str()));
    dir->cd();
    //get min and max values for all bins.
    std::vector<double> min_values;
    std::vector<double> max_values;
    for (int f_i=0;f_i<boot_strap_files.size();f_i++){

        TH1D* h_boot_strap_nom= (TH1D*)boot_strap_files.at(f_i)->Get((plot_path+plot_name).c_str());
        TH1D* h_boot_strap_syst= (TH1D*)syst_boot_strap_files.at(f_i)->Get((plot_path+plot_name).c_str());
        for (int i_bin=1; i_bin<=h_nominal->GetNbinsX() ;i_bin++){
            double bin_val_nom=h_boot_strap_nom->GetBinContent(i_bin);
            double bin_val_syst=h_boot_strap_syst->GetBinContent(i_bin);
            double val=(bin_val_syst-bin_val_nom)/bin_val_nom;
            if (min_values.size()<i_bin){
                min_values.push_back(val);
                max_values.push_back(val);
            }
            else{
	      // If the value of the uncertainty is larger than the stored one, save the new.
                if (val<min_values.at(i_bin-1)){
                    min_values.at(i_bin-1)=val;
                }
                if (val>max_values.at(i_bin-1)){
                    max_values.at(i_bin-1)=val;
                }
            }
        }
    }
    //create histograms
    std::vector<TH1D*> boot_strap_hists;
    for (int i_bin=1; i_bin<=h_nominal->GetNbinsX() ;i_bin++){
        TH1D* h_bin=new TH1D ((plot_name+"_boot_strap_unc_rel_bin_"+to_string(i_bin)).c_str(), (plot_name+"_boot_strap_unc_rel_bin_"+to_string(i_bin)).c_str(), 10, min_values.at(i_bin-1)-0.001,max_values.at(i_bin-1)+0.001 );
        boot_strap_hists.push_back(h_bin);
    }
    //fill histograms

    for (int f_i=0;f_i<boot_strap_files.size();f_i++){

        TH1D* h_boot_strap_nom= (TH1D*)boot_strap_files.at(f_i)->Get((plot_path+plot_name).c_str());
        TH1D* h_boot_strap_syst= (TH1D*)syst_boot_strap_files.at(f_i)->Get((plot_path+plot_name).c_str());
        for (int i_bin=1; i_bin<=h_nominal->GetNbinsX() ;i_bin++){
            double bin_val_nom=h_boot_strap_nom->GetBinContent(i_bin);
            double bin_val_syst=h_boot_strap_syst->GetBinContent(i_bin);
            boot_strap_hists.at(i_bin-1)->Fill((bin_val_syst-bin_val_nom)/bin_val_nom);
        }
    }
    //save histograms:
    dir->cd();
    for (const auto &h : boot_strap_hists)
        h->Write();

    //fit histograms with RooGaussian
    for (int i_bin=1; i_bin<=h_boot_strap_unc->GetNbinsX() ;i_bin++){
        double d_center=h_syst_unc->GetBinContent(i_bin)/h_nominal->GetBinContent(i_bin);
        cout << "d_center" << d_center<<endl;
        TF1 *fb = new TF1(("f_gaus_bin_"+to_string(i_bin)).c_str(),"gaus(0)",-5,5);
        fb->SetParameter("Mean", d_center);
        TFitResultPtr f_res=boot_strap_hists.at(i_bin-1)->Fit(("f_gaus_bin_"+to_string(i_bin)).c_str(),"S");
        //f_res->Print();
        double d_sigma=fb->GetParameter("Sigma");
        double d_Mean=fb->GetParameter("Mean");
        dir->cd();
        fb->Write();
        f_res->Write();
        //set unc for central values:
        if (abs(d_Mean-d_center)<0.1){
            h_boot_strap_unc->SetBinError(i_bin, 0);
            h_boot_strap_unc->SetBinContent(i_bin, d_sigma);
        }
        else{
            cout << "d_center" << d_center<< "!="<< d_Mean<< "d_Mean"<<endl;
        }

    }
    //set unc for central values
    f_output->cd();
    h_boot_strap_unc->Write();
    // TH1D* h_boot_strap_unc_rel= (TH1D*)h_boot_strap_unc->Clone((plot_name+"_boot_strap_unc_rel").c_str());
    // h_boot_strap_unc_rel->Divide(h_nominal);
    // h_boot_strap_unc_rel->Write();
    return h_boot_strap_unc;
}

TH1D* create_boot_strap_unc_nominal(std::string plot_path, std::string plot_name, TFile *f_nominal, std::vector<TFile*> boot_strap_files, TFile *f_output )
{
    std::cout << "Trying to create_boot_strap_unc for: " << plot_path<< plot_name<< std::endl;
    //create th1 with central values ->set unc later
    f_output->cd();
    //save nominal file:
    TH1D* h_nominal=((TH1D*)f_nominal->Get((plot_path+plot_name).c_str()));
    h_nominal->Write();
    //f_nominal->ls();
    TH1D* h_boot_strap_unc= (TH1D*)f_nominal->Get((plot_path+plot_name).c_str())->Clone((plot_name+"_boot_strap_unc").c_str());
    cout <<h_boot_strap_unc->GetNbinsX()<<endl;
    h_boot_strap_unc->Reset();

    TH1D* h_syst=((TH1D*)f_nominal->Get((plot_path+plot_name).c_str()));
    //create folder with histos for  each bin. 20 bins 3sigma
    TDirectory *dir = 0 ;
    dir = f_output->GetDirectory((("f_" + plot_name + "_boot_strap_unc").c_str()));
    if (dir == 0)
        dir = f_output->mkdir((("f_" + plot_name + "_boot_strap_unc").c_str()));
    dir->cd();
    //get min and max values for all bins.
    std::vector<double> min_values;
    std::vector<double> max_values;
    for (const auto &f_boot_strap : boot_strap_files){
        TH1D* h_boot_strap= (TH1D*)f_boot_strap->Get((plot_path+plot_name).c_str());
        for (int i_bin=1; i_bin<=h_nominal->GetNbinsX() ;i_bin++){
            double bin_val=h_boot_strap->GetBinContent(i_bin);
            if (min_values.size()<i_bin){
                min_values.push_back(bin_val);
                max_values.push_back(bin_val);
            }
            else{
                if (bin_val<min_values.at(i_bin-1)){
                    min_values.at(i_bin-1)=bin_val;
                }
                if (bin_val>max_values.at(i_bin-1)){
                    max_values.at(i_bin-1)=bin_val;
                }
            }
        }
    }
    //create histograms
    std::vector<TH1D*> boot_strap_hists;
    for (int i_bin=1; i_bin<=h_nominal->GetNbinsX() ;i_bin++){
        double d_center=h_nominal->GetBinContent(i_bin);
        double d_range=h_nominal->GetBinError(i_bin) *0.25;
        TH1D* h_bin=new TH1D ((plot_name+"_boot_strap_unc_bin_"+to_string(i_bin)).c_str(), (plot_name+"_boot_strap_unc_bin_"+to_string(i_bin)).c_str(),  10, min_values.at(i_bin-1)-0.001,max_values.at(i_bin-1)+0.001 );
        boot_strap_hists.push_back(h_bin);
    }
    //fill histograms
    for (const auto &f_boot_strap : boot_strap_files){
        TH1D* h_boot_strap= (TH1D*)f_boot_strap->Get((plot_path+plot_name).c_str());
        for (int i_bin=1; i_bin<=h_nominal->GetNbinsX() ;i_bin++){
            double bin_val=h_boot_strap->GetBinContent(i_bin);
            boot_strap_hists.at(i_bin-1)->Fill(bin_val);
        }
    }
    //save histograms:
    dir->cd();
    for (const auto &h : boot_strap_hists)
        h->Write();

    //fit histograms with RooGaussian
    for (int i_bin=1; i_bin<=h_boot_strap_unc->GetNbinsX() ;i_bin++){
        double d_center=h_nominal->GetBinContent(i_bin);
        cout << "d_center" << d_center<<endl;
        TF1 *fb = new TF1(("f_gaus_bin_"+to_string(i_bin)).c_str(),"gaus(0)",0,10);
        fb->SetParameter("Mean", d_center);
        TFitResultPtr f_res=boot_strap_hists.at(i_bin-1)->Fit(("f_gaus_bin_"+to_string(i_bin)).c_str(),"S");
        //f_res->Print();
        double d_sigma=fb->GetParameter("Sigma");
        double d_Mean=fb->GetParameter("Mean");
        dir->cd();
        fb->Write();
        f_res->Write();
        //set unc for central values:
        if (abs(d_Mean-d_center)<0.1){
            h_boot_strap_unc->SetBinError(i_bin, 0);
            h_boot_strap_unc->SetBinContent(i_bin, d_sigma);
        }
        else{
            cout << "d_center" << d_center<< "!="<< d_Mean<< "d_Mean"<< " In bin "<<i_bin <<endl;
            exit(1);
        }

    }



    //set unc for central values
    f_output->cd();
    h_boot_strap_unc->Write();
    TH1D* h_boot_strap_unc_rel= (TH1D*)h_boot_strap_unc->Clone((plot_name+"_boot_strap_unc_rel").c_str());
    h_boot_strap_unc_rel->Divide(h_nominal);
    h_boot_strap_unc_rel->Write();
    return h_boot_strap_unc;

}

int MC_stat_calc(std::string inputfile_folder, std::string nominal_inputfile_name,std::string syst_file="")
{
    int i_boot_weight_max=100;
    // Loading input files:
    std::string inputFile_str = inputfile_folder + "/" +nominal_inputfile_name;
    std::string syst_inputFile_str = inputfile_folder + "/" +syst_file;
    //main file:
    std::cout << "***************************************************************************" <<std::endl;
    std::cout << "Input file: " << inputFile_str + ".root" <<std::endl;
  	TFile *f_nominal = new TFile((inputFile_str + ".root").c_str(), "READ");
  	if (check_fit( f_nominal, inputFile_str + ".root")) {
  		std::cerr << " ... exiting." << std::endl;
  		return -1;
  	}
    int n_pt_bins=((TH1D*)f_nominal->Get("sf_and_beff/e_b_60_Postfit"))->GetNbinsX();
    TFile *f_syst;
    if (syst_file!=""){
        f_syst = new TFile((syst_inputFile_str + ".root").c_str(), "READ");
        if (check_fit( f_syst, syst_inputFile_str + ".root")) {
            std::cerr << " ... exiting." << std::endl;
            return -1;
        }
    }
    // TFile *f_nominal_combi=0;
    // if (syst_file!=""){
    //     TFile *f_nominal_combi = new TFile((inputFile_str+"_bootStrap_combination" + ".root").c_str(), "READ");
    //     if (!f_nominal_combi) {
    //         std::cerr << "Input file: " << inputFile_str+"_bootStrap_combination" + ".root" << " not found ... exiting." << std::endl;
    //         return -1;
    //     }
    // }
    //oben all bootstrapweight files
    std::vector<TFile*> nominal_boot_strap_files; 
    std::vector<TFile*> syst_boot_strap_files;
    for (int i_boot_weight=0;i_boot_weight<i_boot_weight_max;i_boot_weight++){
        bool drop_this=false;
        TFile *f_boot_i = new TFile((inputFile_str + "_boot_strap_"+to_string(i_boot_weight)+ ".root").c_str(), "READ");
        if (check_fit( f_boot_i, (inputFile_str + "_boot_strap_"+to_string(i_boot_weight)+ ".root"))) {
            // std::cerr << " ... exiting." << std::endl;
            // return -1;
            std::cerr << " dropping bootstrap because of nominal: " << i_boot_weight <<std::endl;
            drop_this=true;
        }
        else if (syst_file!="" ){
            TFile *f_syst_boot_i = new TFile((syst_inputFile_str + "_boot_strap_"+to_string(i_boot_weight)+ ".root").c_str(), "READ");
            if (check_fit( f_syst_boot_i, (syst_inputFile_str + "_boot_strap_"+to_string(i_boot_weight)+ ".root"))) {
                // std::cerr << " ... exiting." << std::endl;
                // return -1;
                std::cerr << " dropping bootstrap because of systematic: " << i_boot_weight <<std::endl;
                drop_this=true;
            }
            else{
                syst_boot_strap_files.push_back(f_syst_boot_i);
            }
        }
        if (!drop_this){
            nominal_boot_strap_files.push_back(f_boot_i);
        }
    }
    std::string out_name;
    if (syst_file==""){
	       out_name = inputFile_str+"_bootStrap_combination" ;
       }
       else{
           out_name = syst_inputFile_str+"_bootStrap_combination" ;
       }
	std::string outputFile_str = out_name + ".root";
	std::cout << "Outputfile: " << outputFile_str << std::endl;
    std::cout << "***************************************************************************" <<std::endl;
	TFile *f_output = new TFile(outputFile_str.c_str(), "RECREATE");
    string plot_path="sf_and_beff/";
    // sf_and_beff/c_e_b_85.pdf
    std::vector<std::string> wp_names = { "85", "77", "70", "60" };
    for (const auto &wp_name : wp_names){
        string plot_name="e_b_"+wp_name+"_Postfit";
        if (syst_file==""){
            TH1D* h_boot_strap_unc= create_boot_strap_unc_nominal(plot_path,plot_name,f_nominal,nominal_boot_strap_files,f_output );
        }
        else{
            TH1D* h_boot_strap_unc= create_boot_strap_unc_syst(plot_path,plot_name,syst_file,f_nominal,nominal_boot_strap_files, f_syst, syst_boot_strap_files ,f_output );
        }
    }
    //"pdf/p_b_" + pt_name + "_Postfit"
    plot_path="pdf/";
    for (int pt_bin=1;pt_bin <= n_pt_bins;pt_bin++){
        string plot_name="p_b_pt_" + to_string(pt_bin) + "_Postfit";
        if (syst_file==""){
            TH1D* h_boot_strap_unc= create_boot_strap_unc_nominal(plot_path,plot_name,f_nominal,nominal_boot_strap_files,f_output );
        }
        else{
            TH1D* h_boot_strap_unc= create_boot_strap_unc_syst(plot_path,plot_name,syst_file,f_nominal,nominal_boot_strap_files, f_syst, syst_boot_strap_files ,f_output );
        }
    }

    f_output->Close();
    for (const auto &f : nominal_boot_strap_files)
        f->Close();
    for (const auto &f : syst_boot_strap_files)
        f->Close();
    f_nominal->Close();
    if (syst_file!=""){
        f_syst->Close();
    }
}

int main (int argc, char *argv[])
{
        string inputfile_folder, nominal_file,syst_file;
        syst_file="";
        std::vector<std::string>systNames;
        try
        {
                cxxopts::Options options(argv[0], " - example command line options");
                options.positional_help("[input output]");
                options.add_options()
                        ("i,input", "inputfile_folder", cxxopts::value<std::string>(),"inputfile_folder")
                        ("n,nominal_file", "file_Name", cxxopts::value<std::string>()->default_value("FitPlot_r21_d1516_MV2c10_FixedCutBEff_emu_OS_J2_nominal"), "fileName")
                        ("s,syst_file", "file_Name", cxxopts::value<std::string>(), "fileName")
                        ("h, help", "Print help")
                        ;

                        options.parse(argc, argv);

                if (options.count("help"))
                {
                        std::cout << options.help({""}) << std::endl;
                        exit(0);
                }
                if (options.count("input"))
                {
                        std::cout << "Input = " << options["input"].as<std::string>()<< std::endl;
                        inputfile_folder=options["input"].as<std::string>();
                }

                std::cout << "nominal_file = " << options["nominal_file"].as<std::string>()<< std::endl;
                nominal_file=options["nominal_file"].as<std::string>();
                std::cout << "syst_file = " << options["syst_file"].as<std::string>()<< std::endl;
                syst_file=options["syst_file"].as<std::string>();

        } catch (const cxxopts::OptionException& e)
        {
                std::cout << "error parsing options: " << e.what() << std::endl;
                exit(1);
        }
        MC_stat_calc(inputfile_folder, nominal_file, syst_file);


}
