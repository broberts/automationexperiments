#### Installation
```bash
source setup.sh
mkdir build
cd build
cmake ..
make
```
ok....

#### combine the input files.
```bash
python combine_histos_for_fit_with_syst.py
```
to combine the relevant histogram for the fits. At the moment You have to uncomment parts you don't want here (no systematics for example).

now you have hopefully everything in place.

#### calculation of nominal fit manually
Perhaps lets start by calculating the nominal fit manually to see how every thing works, before we let condor (or what ever) do the fits with the systematics.
In order to do that:

```bash
cd build
mkdir FitResults/
mkdir FitResults/Worksapces
mkdir FitResults/FitPlots/

./2jet_pt_fit --help
```
and then do run the actual fit with one of your input files:
```bash
./2jet_pt_fit -i $dust/b-jet-calib-syst/r21-2-19_18-03-05/plots-ttbar-PhPy8/combination_sys_FTAG2_ttbar_PhPy8/FTAG2_ttbar_PhPy8_combination_for_fit_nominal.root
```

This will calculate the fit and produce some first fitplots. find them in
```bash
FitResults/FitPlots/FitPlots*.root
```
Try to understand the Plots. You can find yields, scalefactors, efficiencies, data to model comparisons and much more in the dedicated directories of the rootfiles.
If you want to draw them to pdfs, for example to copy them on a webpage you can use the draw all plots in file macro.
```bash
 python draw_all_plots_in_file.py FitResults/FitPlots/FitPlots*.root
```
To do this for the emu_OS_J2_2lepcut channel add the option *-c emu_OS_J2_m_lj_cut* and then you can do it for *-t DL1*.
keep playing!

#### calculation of nominal fit manually using the CR regions
If you want to try to use the CR regions to constrain you background you can try to use the combined fitting method.
*likelihood-fit/src/2jet_pt_combined_fit.cxx* basically works in the same way as *2jet_pt_fit.cxx*. I has its own , dedicated plotting script (which produces a little more plots).
```bash
cd build
mkdir FitResults/
mkdir FitResults/Worksapces_3SB
mkdir FitResults/FitPlots_3SB

./2jet_pt_combined_fit --help
```

and then do run the actual fit with one of your input files:
```bash
./2jet_pt_combined_fit -i $dust/b-jet-calib-syst/r21-2-19_18-03-05/plots-ttbar-PhPy8/combination_sys_FTAG2_ttbar_PhPy8/FTAG2_ttbar_PhPy8_combination_for_fit_nominal.root
```
Of course, the combined fit only makes sense for emu_OS_J2. If we apply the cut there are no events in the CR left.

There are some known issues which can appear when you run this quite huge fit (1260 categorys):
1. high correlations between lb and bl events in some pt_bins pushes the fit to a region with one lb/bl sf beeing negative. This is no problem as such, but roofit can not evaluate the statistical uncertainty probably if a parametter hits it bundary. Use the option *--allow_neg_sf_xx* in this case. And check the fitplots later on.
2. fits a more complicated and do not always what you think. Always check the nominal fitplots and plots of systematics which don look reasonable.  




#### calculation of fits for all systematics
To calculate fits for all systematics just run
```bash
python calculate_all_fits.py -c emu_OS_J2_m_lj_cut -t MV2c10 --rrF
```
for all taggers and Workingpoint definitions. Only do this after you got the nominal fit plots running!

This script will calculate a fit for each systematic individually and compare them to the nominal fit in order to estimate systematic uncertainties.
Since there are many systematic uncertainties and some of them need a dedicated setting (for example some of them need to be compared to the nominal af2 sample) you will have to check the code in *likelihood-fit/include/SystError_plottingMacro.cxx*. We try to keep it general - but you will need some dedicated setting here! Drop the rrF (rerun Fits) option if you just want to rerun the plotting makro.

You can also use the --combined_fit option here. (so same thing for the combined fit.)

The fit plots can be drawn with the *draw_all_plots_in_file.py* to pdf. If you got it working so far you can even have a look at the awesome *update_website.py* makro to save a lot of time. Which uploads all the plots to a DESY -style website.

You can even submit the fits for all systematics to condor if you want to: *htcondor_submit/submit_to_condor.py*


#### Run with Bootstrap Weights:
If you want to do everything above but with bootstrap weights follow these instructions:
	- Need to combine histos again, usually you would use *combine_histos_for_fit_with_syst.py* but thats no good locally, again it will most likely stop as its running due to memory consumption so we need to use the cluster again so use *htcondor_submit/combine_histos_submit.py*. All you have to do to set this up is make sure *combine_histos_4_fits_condor.py* has bootstrap options near the top of the file set to true.
	- Once this is finished you can go through the rubbish of performing nominal fits first
	- Then you will need to run calculate_all_fits but with the --boot_strap option turned on.
	- That should be it, if its not, figure it our yourself.


#### Whats not implemented so far:
 - zJets scalefactors "only some input file name changes an so on.."
 - nice structure for update_website
