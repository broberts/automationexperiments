import os
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *

  # ./2jet_pt_fit [OPTION...]
  #
  # -i, --input input_file     Input
  # -o, --output base_name     Final Outputfile_name will be added together:
  #                            base_name _ dataName _ rlTag _ channelName _
  #                            taggerName _ systName _ fitConfig (default: Workspace)
  # -r, --rlTag rlTag          rlTag (default: r21)
  # -d, --dataName dataName    dataName (default: d1516)
  # -t, --tagger taggerName    taggerName (default: MV2c10)
  # -w, --workingPoint name    Name of the Working point: FixedCutBEff, HybBEff
  #                            (default: FixedCutBEff)
  # -s, --syst systName        name of systematics (default: nominal)
  # -c, --channel channelName  name of channel to fit (default: emu_OS_J2)
  # -h, --help                 Print help
  #     --closure_test         take mc as data
  #     --varry_fs             varry the f_xx fractions

def work(input_file,work_dir,rlTag,nom_sample,syst,channel,tagger,workingPointName,additional_options=[]):
	if "--use_Minos" in additional_options:
		out_name= workspace_dir+ "/Workspace_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_Minos_" + nom_sample + "_" + syst
	else:
		out_name= workspace_dir+ "/Workspace_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_" + nom_sample + "_" + syst
	print("Py: Calculating Fit for Systematic:\t" + str(syst) + " with options:\t" + str(additional_options) + " with output filename:\n" + str(out_name))
	s_additional_options=''
	for s_additional_options_single in additional_options:
		s_additional_options=s_additional_options+"_"+s_additional_options_single
	s_additional_options=s_additional_options.replace("-","").replace("+","")
	# print(s_additional_options)
	log=open(out_name+s_additional_options+".log","w")
	print("Over to 2jet_pt_(combined_)fit for " + str(syst) + " ... ")
	if args.combined_fit:
		p=subprocess.Popen(["./build/2jet_pt_combined_fit","-i",input_file,"-o", work_dir, "-r", rlTag,"-d", dataName,"-n", nom_sample, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log) #,stdout=log "--allow_neg_sf_xx"
	else:
		# NB: Havent implemented "-n" in .cxx yet so you probably should if you need it
		p=subprocess.Popen(["./build/2jet_pt_fit", "-i",input_file,"-o", work_dir, "-r", rlTag, "-d", dataName,"-n", nom_sample, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log)
	p.wait()

def mc_stat_calc(nominal_input_file,syst_input_file,input_dir):
    if syst_input_file=="":
        print "calculating mc stat unc for:", nominal_input_file
        p=subprocess.Popen(["./build/MC_stat_calc", "-i",input_dir,"-n", nominal_input_file])
    else:
        print "calculating mc stat unc for:", syst_input_file
        p=subprocess.Popen(["./build/MC_stat_calc", "-i",input_dir,"-n", nominal_input_file,"-s", syst_input_file])
    p.wait()


parser = argparse.ArgumentParser(
    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
parser.add_argument('-c',"--channel",default="emu_OS_J2",
                    help='Channel to run over')
parser.add_argument('-t',"--tagger",default="DL1r",
                    help='tagger to run over')
parser.add_argument('-w',"--workingPointName",default="FixedCutBEff",
                    help='workingPointName to run over')
parser.add_argument('-r',"--rlTag",default="r21",
                    help='releaseTag')
parser.add_argument('-d',"--dataName",default=options.data_name,
                    help='dataName')
parser.add_argument('--rrF', action='store_true',
                    help='rerun all the Fits (default: false)')
parser.add_argument('--combined_fit',action='store_true',
                    help='run with combined fit method.')
parser.add_argument('--boot_strap',action='store_true',
                    help='run with mc stat systematics takes really long!.')
parser.add_argument("--plot_dir",default=options.plot_dir,
                    help='use in order to change working dir, usefull for combination of different years.')
parser.add_argument("--run_pack",default=0,
                    help='use to run packs of 100 fits on condor. option -1 will help you in finding the number of needed blocks')

num_of_cores_to_use =4 			# set to the number of workers you want (it defaults to the cpu count of your machine)
args = parser.parse_args()
channel=args.channel
tagger=args.tagger
workingPointName=args.workingPointName
rlTag=args.rlTag
dataName=args.dataName
work_dir = args.plot_dir

if args.rrF:
    run_fits=range(1,2000)
if args.run_pack!=0:
    run_fits=range((int(args.run_pack)-1)*100,( int(args.run_pack))*100)

syst_samples=options.syst_samples_singletop[:] # options.singletop_fsr_samples +
systematics = options.inner_systematics_in_nominal_tree[:] + options.tree_systematics + options.pdf_systematics[:] # options.singletop_pdf_systematics + # options.ZJets_weight_mc_systematics +# options.Diboson_weight_mc_systematics

if options.fake_estimation:
    systematics.append(options.fake_estimation)
    print("found fake estimation.")

workspace_dir = work_dir+"/Workspaces"
fitplots_dir = work_dir+"/FitPlots"
#fit_config='combination_for_fit'
fit_config='fconst'
s_data_lumi="{:.1f}".format(options.data_lumi/1000) 
options_for_all_fits=[]
if args.combined_fit:
    options_for_all_fits=options_for_all_fits+["-l", s_data_lumi, "-m", "mc16ade"]#, "--allow_neg_sf_xx" ]
    workspace_dir=workspace_dir+"_3SB"
    fitplots_dir=fitplots_dir+"_3SB"
    fit_config=''
if not os.path.exists(workspace_dir):
    os.makedirs(workspace_dir)
if not os.path.exists(fitplots_dir):
    os.makedirs(fitplots_dir)
job_n=0
#main part of the script. Otherwise we can use it to calculate a single bootstrap untcertainty. Usefull to have shorter jobs. 
if True:
	if args.rrF:
		tp = ThreadPool(num_of_cores_to_use)
		# the nominal fit:
		print("Nominal Fit: ... ")
		syst='nominal'
		input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+"nominal"+".root"
		job_n=job_n+1
		if job_n in run_fits:
			tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,"nominal",channel,tagger,workingPointName, options_for_all_fits[:] + ["--use_Minos"]))
		job_n=job_n+1
		#a nominal version without Minos for the bootstraps
		if job_n in run_fits:        
			tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,syst,channel,tagger,workingPointName, options_for_all_fits[:]))    
		if args.boot_strap:
			print "......Fit for 100 bootstraps..."
			for i_boot_strap_weight in xrange(0,100):
				#print "Fit for bootstrap: ",i_boot_strap_weight
				job_n=job_n+1
				if job_n in run_fits:
					tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,syst,channel,tagger,workingPointName, options_for_all_fits[:] +["--boot_strap",str(i_boot_strap_weight)]))
		#.systematics the Fits:
		#............cl tests:
		print("cl-test Fit: ... " )
		input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+"nominal"+".root"
		job_n=job_n+1
		if job_n in run_fits: 
			tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,"clTestMoMc",channel,tagger,workingPointName, options_for_all_fits[:] +["--closure_test"]))
		print("cl-test Fit n_stat: ... ")
		input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+"nominal"+".root"
		job_n=job_n+1
		if job_n in run_fits: 
			tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,"clTestMoMc_nstat",channel,tagger,workingPointName, options_for_all_fits[:] + ["--closure_test_seed","-2"," --closure_test"] ))
		# Detector, pdf, weight systematics:
		for syst in systematics:
			input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst.name+".root"
			job_n=job_n+1
			print("Job " + str(job_n) + ": Fit for syst: " + str(syst.name))
			if job_n in run_fits: 
				tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,syst.name,channel,tagger,workingPointName,options_for_all_fits[:] ))
			if args.boot_strap and syst.do_bootstrap:
				print "......Fit for 100 bootstraps..."
				for i_boot_strap_weight in xrange(0,100):
					#print "Fit for bootstrap: ",i_boot_strap_weight
					job_n=job_n+1
					if job_n in run_fits: 
						tp.apply_async(work, (input_file,work_dir,rlTag,options.ttb_sample.name,syst.name,channel,tagger,workingPointName, options_for_all_fits[:] + ["--boot_strap",str(i_boot_strap_weight)]))


                # Singletop systematics
                for syst in options.pdf_systematics:
                        input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.singeTop_sample.name+"_combination_for_fit_"+syst.name+".root"
			job_n=job_n+1
			print("Job " + str(job_n) + ": Fit for syst: " + str(syst.name))
			if job_n in run_fits: 
				tp.apply_async(work, (input_file,work_dir,rlTag,options.singeTop_sample.name,syst.name,channel,tagger,workingPointName,options_for_all_fits[:] ))
                                
		# ............simulate misstaglight up:
		# print "Simulate misstag light: "
		# input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"combination_for_fit_"+"nominal"+".root"
		# job_n=job_n+1
		# if job_n in run_fits: 
		#    tp.apply_async(work, (input_file, work_dir, rlTag,"misstagLight_up", channel, tagger, workingPointName, options_for_all_fits[:] + ["--pl_up",]))
		# ............systematic samples:
		for syst_sample in syst_samples:
			# Loop over ttbar systematics (amc, PowHW7, etc..)
			# print("Syst sample: " + str(syst_sample.name) + " with systematic: " + str(syst_sample.systematic))
			input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+syst_sample.name+"_combination_for_fit_"+syst_sample.systematic+".root"
			job_n=job_n+1
			print("Job " + str(job_n) + ": Fit for syst: " + str(syst_sample.name) + " with systematic: " + str(syst_sample.systematic))
			if job_n in run_fits:
					tp.apply_async(work, (input_file, work_dir, rlTag,syst_sample.name,syst_sample.systematic, channel, tagger, workingPointName,options_for_all_fits[:] ))
			if args.boot_strap and syst_sample.boot_strap_available:
				print "......Fit for 100 bootstraps..."
				for i_boot_strap_weight in xrange(0,100):
					print "Fit for bootstrap: ",i_boot_strap_weight
					tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample.name,syst_sample.systematic,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
		# other syst samples:
		for syst_samples_ZJets in options.syst_samples_ZJets[:]+options.syst_samples_Diboson[:]+options.syst_samples_singletop[:]:
			print "Fit for syst_samples_ZJets: ",syst_samples_ZJets.name
			input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst_samples_ZJets.name+".root"
			tp.apply_async(work, (input_file,work_dir,rlTag,syst_samples_ZJets.name,syst_samples_ZJets.systematic,channel,tagger,workingPointName,))
			if args.boot_strap and syst_samples_ZJets.boot_strap_available:
				for i_boot_strap_weight in xrange(0,100):
					print "Fit for bootstrap: ",i_boot_strap_weight
					tp.apply_async(work, (input_file,work_dir,rlTag,syst_samples_ZJets.name,syst_samples_ZJets.systematic,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
		# rad syst samples:
		for syst_sample_rad in options.rad_samples:
			print("Fit for rad syst: " + str(syst_sample_rad.name) + " with systematic: " + str(syst_sample_rad.systematic))
			input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+syst_sample_rad.name+"_combination_for_fit_"+syst_sample_rad.systematic+".root"
			if syst_sample_rad.name=="FTAG2_ttbar_PhPy8_hdamp3mtop":
				syst_sample_rad.systematic="hdamp_mc_rad_UP"
			tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample_rad.name,syst_sample_rad.systematic,channel,tagger,workingPointName,))
			if args.boot_strap and syst_sample_rad.boot_strap_available:
				for i_boot_strap_weight in xrange(0,100):
					print "Fit for bootstrap: ",i_boot_strap_weight
					tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample_rad.name,syst_sample_rad.systematic,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
		for syst_sample_rad in options.fsr_samples:
			print("Fit for fsr syst: " + str(syst_sample_rad.name) + " with systematic: " + str(syst_sample_rad.systematic))
			input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+syst_sample_rad.name+"_combination_for_fit_"+syst_sample_rad.systematic+".root"
			tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample_rad.name,syst_sample_rad.systematic,channel,tagger,workingPointName,))
			if args.boot_strap and syst_sample_rad.boot_strap_available:
				for i_boot_strap_weight in xrange(0,100):
					print "Fit for bootstrap: ",i_boot_strap_weight
					tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample_rad.name,syst_sample_rad.systematic,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
		tp.close()
		tp.join()
	print "total number of jobs: ", job_n
	n_jobs_needed=(job_n/100)
	if job_n%100>0:
		n_jobs_needed=n_jobs_needed+1
	print "suggested number of blocks of 100 for condor: ",  n_jobs_needed
	if args.boot_strap and args.run_pack==0:
		print "lets calculate mc stat unc via MC_stat_calc"
		file_name_start="FitPlot_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_"
		if fit_config != "":
			file_name_start=file_name_start+fit_config+"_"
		tp2 = ThreadPool(1)
		print "nominal:"
		tp2.apply_async( mc_stat_calc,(file_name_start+options.ttb_sample.name+"_"+"nominal","",fitplots_dir))
		for syst in systematics:
			if not bool(syst):
				# Only fake estimation syst is a bool, all the rest are systematic class
				if syst.do_bootstrap:
					print "syst sample:: ",syst.name
					tp2.apply_async( mc_stat_calc,(file_name_start+"nominal",file_name_start+syst.name,fitplots_dir))
		for syst_sample in syst_samples:
			if syst_sample.boot_strap_available:
				print "syst sample:: ",syst_sample.name
				if syst_sample.name=='FTAG2_ttbar_PowHW7' or syst_sample.name=='FTAG2_ttbar_aMcPy8' or syst_sample.name=='FTAG2_ttbar_PhPy8_hdamp3mtop':
					tp2.apply_async( mc_stat_calc,(file_name_start+'FTAG2_ttbar_PhPy8_AF2'+"_nominal",file_name_start+syst_sample.name+"_nominal",fitplots_dir))
				elif syst_sample.name=='FTAG2_singletop_PowHW7' or syst_sample.name=='FTAG2_singletop_aMcPy8' :
					tp2.apply_async( mc_stat_calc,(file_name_start+'FTAG2_singletop_PowPy8_AF2'+"_nominal",file_name_start+syst_sample.name+"_nominal",fitplots_dir))    
				else:
					tp2.apply_async( mc_stat_calc,(file_name_start+options.ttb_sample.name+"_"+"nominal",file_name_start+syst_sample.name+"_nominal",fitplots_dir))
		tp2.close()
		tp2.join()
	print("Now working on final syst plots ...")
	syst_for_final_plot = []
	for syst in options.pdf_systematics:
		syst_for_final_plot.append(syst.name)
	#syst_for_final_plot.append("misstagLight_up")
	#syst_for_final_plot.append("clTestMoMc_nstat")
	for syst in options.tree_systematics:
		syst_for_final_plot.append(syst.name)
	for syst in options.inner_systematics_in_nominal_tree:
		syst_for_final_plot.append(syst.name)
	for syst_sample in options.syst_samples+options.syst_samples_ZJets[:]+options.syst_samples_Diboson[:]+options.syst_samples_singletop[:]:
		syst_for_final_plot.append(syst_sample.name)
	for syst_sample in options.rad_samples[:]+options.fsr_samples:
		if syst_sample.name=="FTAG2_ttbar_PhPy8_hdamp3mtop":
			syst_sample.systematic="hdamp_mc_rad_UP"
		syst_for_final_plot.append(syst_sample.systematic)
	if options.fake_estimation:
	    syst_for_final_plot.append(options.fake_estimation.name)
	print("Over to final_syst_plot.cxx ... ")
	command=" ".join(["./build/final_syst_plot", "-i", fitplots_dir,"-n",work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+"nominal"+".root","-r", rlTag, "-d", dataName, "-c", channel,"-s",options.ttb_sample.name,"-t",tagger,"-w",workingPointName ]+syst_for_final_plot)
	print "command",command
	p=subprocess.Popen(["./build/final_syst_plot", "-i", fitplots_dir,"-n",work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+"nominal"+".root", "-r", rlTag, "-d", dataName, "-c", channel,"-s",options.ttb_sample.name,"-t",tagger,"-w",workingPointName, "-f", fit_config, "-l", "{:.1f}".format(options.data_lumi/1000)]+syst_for_final_plot)
	p.wait()
	print("Finished :)! ")
else:
	tp = ThreadPool(num_of_cores_to_use)
	if args.boot_strap_calc_single == options.rad_up_sample.systematic or args.boot_strap_calc_single == options.rad_down_sample.systematic:
		if args.boot_strap_calc_single == options.rad_up_sample.systematic:
			syst_sample_rad=options.rad_up_sample
		else:
			syst_sample_rad=options.rad_down_sample
		print "Fit for syst_sample_rad: ",syst_sample_rad.name
		input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+syst_sample_rad.name+"_combination_for_fit_"+syst_sample_rad.systematic+".root"
		if syst_sample_rad.name=="FTAG2_ttbar_PhPy8_hdamp3mtop":
			syst_sample_rad.systematic="hdamp_mc_rad_UP"
			args.boot_strap_calc_single="hdamp_mc_rad_UP"
		tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample_rad.name,syst_sample_rad.systematic,channel,tagger,workingPointName,))
		for i_boot_strap_weight in xrange(0,100):
			print "Fit for bootstrap: ",i_boot_strap_weight
			tp.apply_async(work, (input_file,work_dir,rlTag,syst_sample_rad.name, syst_sample_rad.systematic,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
	elif str("FTAG2") in args.boot_strap_calc_single:
		input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+args.boot_strap_calc_single+"_combination_for_fit_nominal.root"
		tp.apply_async(work, (input_file, work_dir, rlTag, args.boot_strap_calc_single, channel, tagger, workingPointName,))
		for i_boot_strap_weight in xrange(0,100):
			print "Fit for bootstrap: ",i_boot_strap_weight
			tp.apply_async(work, (input_file,work_dir,rlTag, args.boot_strap_calc_single,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
	else:
		print "Fit for syst: ",args.boot_strap_calc_single
		input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+args.boot_strap_calc_single+".root"
		tp.apply_async(work, (input_file,work_dir,rlTag,args.boot_strap_calc_single,channel,tagger,workingPointName,))
		for i_boot_strap_weight in xrange(0,100):
			print "Fit for bootstrap: ",i_boot_strap_weight
			tp.apply_async(work, (input_file,work_dir,rlTag, args.boot_strap_calc_single ,channel,tagger,workingPointName,["--boot_strap",str(i_boot_strap_weight)]))
	tp.close()
	tp.join()
	file_name_start="FitPlot_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_"
	if fit_config != "":
		file_name_start=file_name_start+fit_config+"_"
	tp2 = ThreadPool(num_of_cores_to_use)
	if args.boot_strap_calc_single ==str("nominal"):
		print "nominal:"
		tp2.apply_async( mc_stat_calc,(file_name_start+"nominal","",fitplots_dir))
	else:
		print "syst: ",args.boot_strap_calc_single
		tp2.apply_async( mc_stat_calc,(file_name_start+"nominal",file_name_start+args.boot_strap_calc_single+"_nominal",fitplots_dir))
	tp2.close()
	tp2.join()
	print("Ende gut alles gut!")
	
