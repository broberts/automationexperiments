import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/') #takes the options file from there does not seem to cause an error if path does not exist and so should be able to be left for automation if options file is copied to lik-fit directory
from options_file import *

dataName=options.data_name
outdir=options.output_dir
plotdir=options.plot_dir

#Modification to allow first argument to be output directory and second argument plot directory
if len(sys.argv)>1:
    outdir = sys.argv[1]
if len(sys.argv)>2:
    plotdir = sys.argv[2]



#do_bootstrap_global=False
do_bootstrap_global=False
do_bootstrap_ttbar_only=False

# Usage:
#   ./Combine_histograms_for_fit [OPTION...]
#
#       --ttbar_sf file         Nominal ttbar file for sf computation
#       --ttbar_file file       ttbar MC file used for this variation
#       --single_top_file file  single_top MC file used for this variation
#       --diboson_file file     diboson MC file used for this variation
#       --Zjets_file file       Zjets MC file used for this variation
#       --Wjets_file file       Wjets MC file used for this variation
#       --output_file file      output_file name
#       --data_file file        data_file
#       --do_bootstrap          combine with bootstrap weights
#   -h, --help                  Print help


def combine_sample_files(data_file,ttbar_sf,ttbar_file,single_top_file,diboson_file,Zjets_file,Wjets_file,output_file,do_bootstrap=False,do_bootstrap_ttbar_only=False):
    print "combination for outputfile:",  output_file
    if do_bootstrap:
        if do_bootstrap_ttbar_only:
            command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap_ttbar_only"])
            print "command",command
            p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap_ttbar_only"])
        else:
            command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap"])
            print "command",command
            p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file,"--do_bootstrap"])
    else:
        command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file])
        print "command",command
        p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file]) #,stdout=log
    p.wait()




if not os.path.exists(plotdir+"combination_sys_"+options.ttb_sample.name+"/"):
    os.makedirs(plotdir+"combination_sys_"+options.ttb_sample.name+"/")
data_name= outdir+dataName+"/"+dataName+"_data_combination.root"
ttb_sf_name= outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"



num_of_cores_to_use=1
tp = ThreadPool(num_of_cores_to_use)

syst="nominal"
ouputfile_name=plotdir+"combination_sys_"+options.ttb_sample.name+"/"+options.ttb_sample.name+"_combination_for_fit_"+syst+".root"

ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
singeTop_sample=outdir+options.singeTop_sample.name+"/"+options.singeTop_sample.name+"_"+syst+"_combination.root"
ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singeTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,do_bootstrap_global,do_bootstrap_ttbar_only))

#
# #combine for all tree and inner systemtaics:
# deleted

#combination for ttbar_systemtics:

#deleted

# # #combination for options.rad_samples:
# deleted

# #combination for options.fsr_samples:

#deleted




# #combination for pdf systemtics::
#Deleted

# #combination for pdf systemtics::
#Deleted


# #combination for syst_samples_ZJets::

#Deleted

# #combination for syst_samples_singletop::

#deleted


# #combination for syst_samples_Diboson::
#deleted

tp.close()
tp.join()

print "Ende gut alles gut! "
