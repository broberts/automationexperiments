import ROOT
import os
import sys
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import subprocess
import argparse
import shutil
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *
from multiprocessing.pool import ThreadPool



  # ./2jet_pt_fit [OPTION...]
  #
  # -i, --input input_file     Input
  # -o, --output base_name     Final Outputfile_name will be added together:
  #                            base_name _ dataName _ rlTag _ channelName _
  #                            taggerName _ systName _ fitConfig (default: Workspace)
  # -r, --rlTag rlTag          rlTag (default: r21)
  # -d, --dataName dataName    dataName (default: d1516)
  # -t, --tagger taggerName    taggerName (default: MV2c10)
  # -w, --workingPoint name    Name of the Working point: FixedCutBEff, HybBEff
  #                            (default: FixedCutBEff)
  # -s, --syst systName        name of systematics (default: nominal)
  # -c, --channel channelName  name of channel to fit (default: emu_OS_J2)
  # -h, --help                 Print help
  #     --closure_test         take mc as data
  #     --varry_fs             varry the f_xx fractions


print "hello world!"



parser = argparse.ArgumentParser(
    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
parser.add_argument('-c',"--channel",default="emu_OS_J2",
                    help='Channel to run over')
parser.add_argument('-t',"--tagger",default="MV2c10",
                    help='tagger to run over')
parser.add_argument('-w',"--workingPointName",default="FixedCutBEff",
                    help='workingPointName to run over')
parser.add_argument('-r',"--rlTag",default="r21",
                    help='releaseTag')
parser.add_argument('--rrF', action='store_true',
                    help='rerun all the Fits (default: false)')
parser.add_argument('-d',"--dataName",default="d1516",
                    help='dataName')

args = parser.parse_args()
channel=args.channel
tagger=args.tagger
workingPointName=args.workingPointName
rlTag=args.rlTag
dataName=args.dataName
output_dir = "/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-19_18-03-08_4D_si/plots-ttbar-PhPy8/likelihoodsamples/"
input_file="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-19_18-03-08_4D_si/plots-ttbar-PhPy8/combination_sys_FTAG2_ttbar_PhPy8/FTAG2_ttbar_PhPy8_combination_for_fit_clTestMoMo.root"
if not os.path.exists(output_dir+"/Workspaces_3SB"):
    os.makedirs(output_dir+"/Workspaces_3SB")
if not os.path.exists(output_dir+"/FitPlots_3SB"):
    os.makedirs(output_dir+"/FitPlots_3SB")
syst="clTestMoMo"
p_list=[]

# def work(seed):
#     print "Fit for seed: ",seed
#     out_name=output_dir +"/Workspaces_3SB"+ "/Workspace_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_"+ syst+"S"+str(seed)
#     log=open(out_name+".log","w")
#     p=subprocess.Popen(["./build/2jet_pt_combined_fit", "-i",input_file,"-o", output_dir, "-r", rlTag,"-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName,"--closure_test_seed",str(seed)],stdout=log)
#     p.wait()
#
# num = None # None  # set to the number of workers you want (it defaults to the cpu count of your machine)
# tp = ThreadPool(num)
# for seed in xrange(800,1600):
#     print "submitting seed: ",seed
#     tp.apply_async(work, (seed,))
#
# tp.close()
# tp.join()
print "finished. Outpudir: ",output_dir

# # PostFit: chi2,
# # PreFit: chi2, 1065.69, ndf, 1165, chi2/ndf, 0.914755
# # PostFit: chi2, 939.314, ndf, 949, chi2/ndf, 0.989794
f_out=TFile("sampled_likelihood.root","recreate")
file_name_pre_str="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-19_18-03-08_4D_si/plots-ttbar-PhPy8/likelihoodsamples/Workspaces_3SB/Workspace_r21_d1516_MV2c10_FixedCutBEff_emu_OS_J2_clTestMoMoS"
h_prefit=TH1D("h_chi_prefit","chisquared prefit total value", 120,800,1400)
h_postfit=TH1D("h_chi_postfit","chisquared postfit total value", 120,800,1400)
for seed in xrange(200,1200):
    # print "opening log of seed: ",seed
    seed_path=file_name_pre_str+str(seed)+".log"
    chi_prefit=-1
    chi_postfit=-1
    with open(seed_path) as f_in:
        for line in f_in:
            if "PreFit: chi2," in line:
                spli=line.split()
                #print spli
                chi_prefit=float(spli[2].replace(",",""))
            if "PostFit: chi2," in line:
                spli=line.split()
                #print spli
                chi_postfit=float(spli[2].replace(",",""))
    if chi_postfit<1:
        print "Did not find chi in log of seed: ",seed
        print "log: ", seed_path
        print "chi postfit:", chi_postfit
        print "chi prefit:", chi_prefit
    else:
        h_prefit.Fill(chi_prefit);
        h_postfit.Fill(chi_postfit);
f_out.cd()
h_prefit.Write()
h_postfit.Write()
f_out.Close()
