import os
import sys
import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
import subprocess
import array
import math
import argparse
import shutil
from tgraph_help_classes import arrays_of_TH1 
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *


parser = argparse.ArgumentParser(
    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
parser.add_argument('-c',"--channel",default="emu_OS_J2",  #
                    help='Channel to run over')
parser.add_argument('-t',"--tagger",default="DL1r",
                    help='tagger to run over')
parser.add_argument('-w',"--workingPointName",default="FixedCutBEff",
                    help='workingPointName to run over')
parser.add_argument('-r',"--rlTag",default="r21",
                    help='releaseTag')
parser.add_argument('--combined_fit',action='store_true',
                    help='run with combined fit method.')

args = parser.parse_args()
channel=args.channel
tagger=args.tagger
workingPointName=args.workingPointName
rlTag=args.rlTag
dataName=options.data_name
output_dir = options.plot_dir
print "hello world!"
ttbar_file_to_create_model="ttbar_Sherpa221"
# ttbar_file_to_create_model="FTAG2_ttbar_aMcPy8"
ttbar_samples=[options.ttb_sample]+[sample('FTAG2_ttbar_PowHW7',True),sample(ttbar_file_to_create_model,True) ]
workspace_dir = output_dir+"/Workspaces"
fitplots_dir = output_dir+"/FitPlots"
fit_config='_fconst'
reg_name="SR"
if args.combined_fit:
    workspace_dir=workspace_dir+"_3SB"
    fitplots_dir=fitplots_dir+"_3SB"
    fit_config=''
    reg_name="SR+CR"
for ttb_sample in ttbar_samples:
    if ttb_sample.systematic== "ttbar":
        ttb_sample.systematic="nominal"
    f_ttb_path=fitplots_dir+"/FitPlot_r21_"+dataName+"_"+tagger+"_FixedCutBEff_"+channel+fit_config+"_"+ttb_sample.systematic+"_stressTestMoMc_"+ttbar_file_to_create_model+"_nstat.root"
    ttb_sample.file=TFile(f_ttb_path,"read")

f_measurment=TFile(fitplots_dir+"/FinalFitPlots_r21_"+dataName+"_"+tagger+"_FixedCutBEff_"+channel+fit_config+".root","read") 
f_out=TFile("stressFits_combined_"+ttbar_file_to_create_model+"_"+channel+fit_config+"_combination.root","recreate")

hists=[]
arrays=[]
#the wonderfull horizontal line
line = ROOT.TLine(20,0,600,0)
line.SetLineWidth(1)
line.SetLineStyle(2)
# #lets create a legend
legend = ROOT.TLegend(0.289398, 0.181435, 0.659026, 0.417722)
legend.SetName("legend_1")
legend.SetFillStyle(0)
legend.SetLineColor(0)
legend.SetLineWidth(0)
legend.SetTextFont(42)

# #filling the legend:
#stat
f_out.cd()
dummy_er=ROOT.TH1I("d_stat","d_stat",1,0,1);
dummy_er.SetDirectory(0)
dummy_er.SetFillColor(ROOT.kGreen-8);
dummy_er.SetLineColor(ROOT.kGreen-8);
dummy_er.Write()
legend.AddEntry(dummy_er, "expected data stat unc. ("+reg_name+")", "F")
#MC Stat
dummy_grey=ROOT.TH1I("d_mc_stat","d_mc_stat",1,0,1);
dummy_grey.SetDirectory(0)
dummy_grey.SetFillColor(0);
dummy_grey.SetLineWidth(2);
dummy_grey.SetLineColor(ROOT.kGray+1);
dummy_grey.SetMarkerColor(ROOT.kGray+1)
dummy_grey.SetMarkerStyle(20)
dummy_grey.Write()
legend.AddEntry(dummy_grey, "expected MC stat unc. ("+reg_name+")", "F")
dummy_grey.SetMarkerStyle(20)
legend.AddEntry(dummy_grey, "MC stat #oplus estimated t#bar{t}-modeling unc.", "LPE")
dummy_black=ROOT.TH1I("d_black","d_black",1,0,1)
dummy_black.SetLineWidth(2)
dummy_black.SetLineColor(1);
dummy_black.SetMarkerColor(1)
dummy_black.SetMarkerStyle(20)
legend.AddEntry(dummy_black, "PhPy8", "LPE")
dummy_red=dummy_black.Clone("d_red")
dummy_red.SetLineColor(ROOT.kRed+1);
dummy_red.SetMarkerColor(ROOT.kRed+1)
legend.AddEntry(dummy_red, "PowHW7", "LPE")

#text labels
# t_atlas=ROOT.TPaveText(0.228354,0.841772,0.348735,0.909283,"brNDC")
# t_atlas.SetName("ATLAS")
# t_atlas.SetBorderSize(0);
# t_atlas.SetFillColor(0);
# t_atlas.SetTextSize(0.05);
# t_atlas.SetTextFont(72);
# t_atlas.AddText("ATLAS");

# t_internal=ROOT.TPaveText(0.34384,0.840717,0.464183,0.910338,"brNDC");
# t_internal.SetName("Internal")
# t_internal.SetBorderSize(0);
# t_internal.SetFillColor(0);
# t_internal.SetTextSize(0.05);
# t_internal.SetTextFont(42);
# t_internal.AddText("Internal");

wps=["70"]
for wp in wps:
    print "running on wp",wp
    col=0
    h_list=[]
    for ttb_sample in ttbar_samples:
        print "adding",ttb_sample.name,ttb_sample.systematic
        f_out.cd()
        h_e =ttb_sample.file.Get("sf_and_beff/e_b_"+wp+"_Postfit").Clone("e_b_"+wp+"_Postfit_"+ttb_sample.name+"_"+ttb_sample.systematic)
        h_e.Write()
        hists.append(h_e)
        if ttb_sample.name==ttbar_file_to_create_model:
            print "found model sample: ",ttb_sample.name
            col=col+1
            h_e.Write()
            h_e_inputfile_model_post=h_e
            h_e_inputfile_model_pre_name="e_b_"+wp+"_Prefit_"+ttb_sample.name+"_"+ttb_sample.systematic
            h_e_inputfile_model_pre=ttb_sample.file.Get("sf_and_beff/e_b_"+wp+"_Prefit").Clone(h_e_inputfile_model_pre_name)
            h_e_inputfile_model_pre.SetLineWidth(2)
            h_e_inputfile_model_pre.SetLineStyle(2)
            h_e_inputfile_model_pre.Write()
            hists.append(h_e_inputfile_model_pre)
        elif ttb_sample== options.ttb_sample:
            h_e.SetLineColor(1);
            h_e.SetMarkerColor(1);
            h_e.Write()
            h_e_nominal_post=h_e
        elif ttb_sample.name=="ttbar_PowHW7":
            print "found sample: ",ttb_sample.name
            col=col+1
            h_e.Write()
            h_e_HW7=h_e
            h_list.append(h_e_HW7)
        else:
            col=col+1
            h_e.Write()
            h_list.append(h_e)

    f_out.cd()

    c_obs=TCanvas("c_sf_b_"+wp+"_Postfit")
    c_obs.SetLogx()
    c_obs.SetLeftMargin(0.15);
    c_obs.SetRightMargin(0.05);
    c_obs.SetTopMargin(0.05);   
    c_obs.SetBottomMargin(0.15);
    c_obs.cd()
    h_f_nominal_stat=h_e_nominal_post.Clone("h_mc_stat_"+h_e_nominal_post.GetName())
    h_f_nominal_stat.Add(h_e_inputfile_model_pre,-1);
    h_f_nominal_stat.Divide(h_e_inputfile_model_pre)
    hists.append(h_f_nominal_stat)
    h_stat=h_e_inputfile_model_post.Clone("h_stat_band_"+wp)
    h_stat.Reset()
    for i_bin in xrange(1,h_stat.GetNbinsX()+1):
        h_stat.SetBinContent(i_bin,h_e_inputfile_model_post.GetBinError(i_bin)/h_e_inputfile_model_pre.GetBinContent(i_bin))
    h_stat.SetFillColor(ROOT.kGreen -8)
    h_stat.SetLineColor(ROOT.kGreen -8)
    h_stat.SetYTitle("(#epsilon_{b,Estimated}-#epsilon_{b,True})-/ #epsilon_{b,True}")
    h_stat.GetYaxis().SetRangeUser(-0.1,0.1)
    h_stat.GetXaxis().SetTitle("p_{T} [GeV]")
    h_stat_neg=h_stat.Clone("h_stat_band_neg_"+wp)
    h_stat_neg.Scale(-1)
    h_stat.Draw("HIST")
    h_stat_neg.Draw("HISTSAME")

    #load from file 
    ttbar_mod_unc=f_measurment.Get("unc_summary_plots/e_b_wp_"+wp+"/h_sym_ttbar_combined_"+wp)
    mc_stat_nom=f_measurment.Get("other_systematics/e_b_wp_"+wp+"/e_b_"+wp+"_MC_stat_nominal_syst_Error_rel")     
    h_f_nominal_tot=h_f_nominal_stat.Clone("h_tot_unc_"+h_e_nominal_post.GetName())


    for i_bin in xrange(1,h_f_nominal_stat.GetNbinsX()+1):
        h_f_nominal_stat.SetBinError(i_bin,mc_stat_nom.GetBinContent(i_bin) )
        tot_unc=mc_stat_nom.GetBinContent(i_bin)**2+ttbar_mod_unc.GetBinContent(i_bin)**2
        h_f_nominal_tot.SetBinError(i_bin,math.sqrt(tot_unc))
    a_nom_mc_stat=arrays_of_TH1(h_f_nominal_stat, shift=-0.025)
    arrays.append(a_nom_mc_stat)
    g_nom_mc_stat=ROOT.TGraphAsymmErrors(a_nom_mc_stat.N_bins, a_nom_mc_stat.a_x_nom, a_nom_mc_stat.a_y_nom, a_nom_mc_stat.a_x_down, a_nom_mc_stat.a_x_up, a_nom_mc_stat.a_y_down, a_nom_mc_stat.a_y_up)
    g_nom_mc_stat.SetName("g_nom_mc_stat_"+wp)
    hists.append(g_nom_mc_stat)
    print "?"
    g_nom_mc_stat.SetLineWidth(2)
    g_nom_mc_stat.SetFillStyle(0)
    g_nom_mc_stat.Draw("2")
    g_nom_mc_stat.Write()

    a_nom_tot=arrays_of_TH1(h_f_nominal_tot,bin_size=0,shift=-0.025)
    arrays.append(a_nom_tot)
    g_nom_tot=ROOT.TGraphAsymmErrors(a_nom_tot.N_bins, a_nom_tot.a_x_nom, a_nom_tot.a_y_nom, a_nom_tot.a_x_down, a_nom_tot.a_x_up, a_nom_tot.a_y_down, a_nom_tot.a_y_up)
    g_nom_tot.SetName("g_nom_tot_"+wp)
    hists.append(g_nom_tot)
    g_nom_tot.SetLineWidth(2)
    g_nom_tot.SetMarkerStyle(20)
    g_nom_tot.Draw("P")
    g_nom_tot.Write()
    
    
    # legend.AddEntry(h_stat_neg,"simulated data stat","F")
    h_f_HW7_stat=h_e_HW7.Clone("h_mc_stat_"+h_e_HW7.GetName())
    h_f_HW7_stat.Add(h_e_inputfile_model_pre,-1);
    h_f_HW7_stat.Divide(h_e_inputfile_model_pre);
    h_f_HW7_stat.SetYTitle("(#epsilon_{b,Estimated}-#epsilon_{b,True})-/ #epsilon_{b,True}")
    h_f_HW7_tot=h_f_HW7_stat.Clone("h_tot_unc_blu"+h_e_HW7.GetName())
    hists.append(h_f_HW7_stat)
    hists.append(h_f_HW7_tot)
    h_f_HW7_stat.Write() 
    mc_stat_HW7=f_measurment.Get("ttbar_systematics/e_b_wp_"+wp+"/smoothing_ttbar_PowHW7/e_b_"+wp+"_ttbar_PowHW7_syst_up_Error_rel_unsmoothed") 
    for i_bin in xrange(1,h_f_HW7_stat.GetNbinsX()+1):
        stat_unc=mc_stat_HW7.GetBinError(i_bin)
        h_f_HW7_stat.SetBinError(i_bin, stat_unc)
        tot_unc=stat_unc**2+ttbar_mod_unc.GetBinContent(i_bin)**2
        h_f_HW7_tot.SetBinError(i_bin,math.sqrt(tot_unc))
    a_HW7_mc_stat=arrays_of_TH1(h_f_HW7_stat, shift=0.025)
    arrays.append(a_HW7_mc_stat)
    g_HW7_mc_stat=ROOT.TGraphAsymmErrors(a_HW7_mc_stat.N_bins, a_HW7_mc_stat.a_x_nom, a_HW7_mc_stat.a_y_nom, a_HW7_mc_stat.a_x_down, a_HW7_mc_stat.a_x_up, a_HW7_mc_stat.a_y_down, a_HW7_mc_stat.a_y_up)
    g_HW7_mc_stat.SetName("g_HW7_mc_stat_"+wp)
    hists.append(g_HW7_mc_stat)
    print "?"
    g_HW7_mc_stat.SetLineWidth(2)
    g_HW7_mc_stat.SetFillStyle(0)
    g_HW7_mc_stat.SetLineColor(ROOT.kRed+1)
    g_HW7_mc_stat.Draw("2")
    g_HW7_mc_stat.Write()
    # 

    a_HW7_tot=arrays_of_TH1(h_f_HW7_tot,bin_size=0,shift=0.025)
    arrays.append(a_HW7_tot)
    g_HW7_tot=ROOT.TGraphAsymmErrors(a_HW7_tot.N_bins, a_HW7_tot.a_x_nom, a_HW7_tot.a_y_nom, a_HW7_tot.a_x_down, a_HW7_tot.a_x_up, a_HW7_tot.a_y_down, a_HW7_tot.a_y_up)
    g_HW7_tot.SetName("g_HW7_tot_"+wp)
    g_HW7_tot.SetLineWidth(2)
    g_HW7_tot.SetMarkerStyle(20)
    g_HW7_tot.SetLineColor(ROOT.kRed+1)
    g_HW7_tot.SetMarkerColor(ROOT.kRed+1)
    g_HW7_tot.Draw("P")
    g_HW7_tot.Write()
    hists.append(g_HW7_tot)

    legend.Draw()
    line.Draw("SAME")
    # t_atlas.Draw()
    # t_internal.Draw()
    f_out.cd()
    c_obs.RedrawAxis();
    c_obs.Write()

print "Finished :)! "
