import os,glob,ROOT,argparse

ROOT.gROOT.SetBatch(True)

def getXsectionWeight(dsid):

   kfac=1
   crosssec=1
   print("Looking for cross-section from %d"%dsid)
   with open("../AnalysisTop-21.2.X/grid/TopDataPreparation/XSection-MC15-13TeV.data") as f:
        for line in f:
            if str(dsid) in line:
                spli=line.split()
                #print spli                                                                                                                                                                                        
                if not spli[0] == "#":
                    kfac=float(spli[2])
                    crossec=float(spli[1])
                    print("This is the cross-section %.2f"%crossec)
   if kfac == 1:
      print "kfac not found!"
      print "were looking for dsid", dsid
      raise kfacNotFound('kfac in XSection-MC15-13TeV.data not Found!')
   
   
   return crosssec*kfac



def save1Dhists(histos,output,canv,lumi):


   latex=ROOT.TLatex()
   latex.SetTextFont(42)
   latex.SetTextSize(0.035)
   
   legend=ROOT.TLegend(0.5,0.5,0.8,0.75)
   # legend.SetHeader("#bf{#it{ATLAS}} Internal")
   legend.SetBorderSize(0)
   legend.SetTextFont(42)
   legend.SetTextSize(0.02)
   legend.AddEntry(histos[0],"Nominal","l")
   legend.AddEntry(histos[1],"FCLoose Iso","l")
   legend.AddEntry(histos[2],"FCTight Iso","l")
   legend.AddEntry(histos[3],"TightTrackOnly Iso","l")

   histos[0].SetLineColor(ROOT.kBlue)
   histos[1].SetLineColor(ROOT.kRed)
   histos[2].SetLineColor(ROOT.kOrange-3)
   histos[3].SetLineColor(ROOT.kGreen+4)
   
   histos[0].SetLineWidth(2)
   histos[1].SetLineWidth(2)
   histos[2].SetLineWidth(2)
   histos[3].SetLineWidth(2)


   canv.cd()
   canv.SetLogy(1)
   histos[0].Draw("hist")
   histos[1].Draw("hist""SAME")
   histos[2].Draw("hist""SAME")
   histos[3].Draw("hist""SAME")
   
   legend.Draw("SAME")
   latex.DrawLatexNDC(0.5,0.85,"#bf{#it{ATLAS}} Internal")
   latex.DrawLatexNDC(0.5,0.78,"#sqrt{s} = 13 TeV, #int L dt = %.1f fb^{-1}"%(lumi/1000.))
   
   canv.SaveAs(output+".pdf","RECREATE")
   canv.SaveAs(output+".eps","RECREATE")



def save2Dhists(histograms,output,initial,canv):
    
    count=0
    for histogram in  histograms:
        if count < initial:
           count+=1
           continue
        canv.Clear()
        canv.cd()
        canv.SetRightMargin(0.2)
        canv.SetBottomMargin(0.2)
        histogram.SetStats(0)
        histogram.GetXaxis().SetTitle("Leading jet p_{T} [GeV]")
        histogram.GetYaxis().SetTitle("Subleading jet p_{T} [GeV]")
        histogram.Draw("COLZ")
        canv.SaveAs(output+"/"+histogram.GetName()+".pdf","RECREATE")
        canv.SaveAs(output+"/"+histogram.GetName()+".eps","RECREATE")
        count+=1


def classifyEvent(ev):

    mlj_1=0
    mlj_2=0
    
    l1=ROOT.TLorentzVector()
    l2=ROOT.TLorentzVector()
    j1=ROOT.TLorentzVector()
    j2=ROOT.TLorentzVector()
    
    l1.SetPtEtaPhiE(ev.el_pt[0],ev.el_eta[0],ev.el_phi[0],ev.el_e[0])
    l2.SetPtEtaPhiE(ev.mu_pt[0],ev.mu_eta[0],ev.mu_phi[0],ev.mu_e[0])
    j1.SetPtEtaPhiE(ev.jet_pt[0],ev.jet_eta[0],ev.jet_phi[0],ev.jet_e[0])
    j2.SetPtEtaPhiE(ev.jet_pt[1],ev.jet_eta[1],ev.jet_phi[1],ev.jet_e[1])
    
    if ( pow((l1+j1).M(),2) + pow((l2+j2).M(),2)) < ( pow((l1+j2).M(),2) + pow((l2+j1).M(),2)) :
        mlj_1=(l1+j1).M()/1000
        mlj_2=(l2+j2).M()/1000
    else:
        mlj_1=(l1+j2).M()/1000
        mlj_2=(l2+j1).M()/1000
        
    if mlj_1 < 170 and mlj_2 < 170.:
        return "SR"
    elif mlj_1 > 170 and mlj_2 < 170.:
        return "CR_lb"
    elif mlj_1 < 170 and mlj_2 > 170.:
        return "CR_bl"
    elif mlj_1 > 170 and mlj_2 > 170.:
        return "CR_ll"

    return "Unknown"
        
def eventFlavour(ev):

    if ev.jet_truthflav[0] == 5 and ev.jet_truthflav[1] == 5:
        return "bb"
    elif ev.jet_truthflav[0] == 5 and ev.jet_truthflav[1] < 5:
        return "bl"
    elif ev.jet_truthflav[0] < 5 and ev.jet_truthflav[1] == 5:
        return "lb"
    elif ev.jet_truthflav[0] < 5 and ev.jet_truthflav[1] < 5:
        return "ll"
    return "Unknown"



def doPlots(chain,sumWeights,lumi):

    listOfPlots=[]

    n_generated=0
    dsid=0
    for sumWeightsChain in sumWeights:
        n_generated +=  sumWeightsChain.totalEventsWeighted
        if dsid != 0 and dsid != sumWeightsChain.dsid:
            print("Found incompatible DSIDs. Are you using this code to run on two samples at the same time ?")
            sys.exit(0)
        dsid=sumWeightsChain.dsid
    

    lumiWeight=getXsectionWeight(dsid)*lumi/n_generated

    el_real_pt=ROOT.TH1F(chain.GetName()+"elrealpt","",20,20,400)
    el_fakehad_pt=ROOT.TH1F(chain.GetName()+"elfakehadpt","",20,20,400)
    el_fake_pt=ROOT.TH1F(chain.GetName()+"elfakept","",20,20,400)

    mu_real_pt=ROOT.TH1F(chain.GetName()+"murealpt","",20,20,400)
    mu_fakehad_pt=ROOT.TH1F(chain.GetName()+"mufakehadpt","",20,20,400)
    mu_fake_pt=ROOT.TH1F(chain.GetName()+"mufakept","",20,20,400)

    el_type=ROOT.TH1F(chain.GetName()+"eltype","",5,0,5)
    mu_type=ROOT.TH1F(chain.GetName()+"mutype","",5,0,5)


    el_type.SetStats(0)
    mu_type.SetStats(0)

    el_type.GetXaxis().SetBinLabel(1,"True Electron")
    el_type.GetXaxis().SetBinLabel(2,"Hadron fake")
    el_type.GetXaxis().SetBinLabel(3,"Non-Iso Electron")
    el_type.GetXaxis().SetBinLabel(4,"Bkg Electron")
    el_type.GetXaxis().SetBinLabel(5,"Unknown Electron")


    mu_type.GetXaxis().SetBinLabel(1,"True Muon")
    mu_type.GetXaxis().SetBinLabel(2,"Hadron fake")
    mu_type.GetXaxis().SetBinLabel(3,"Non-Iso Muon")
    mu_type.GetXaxis().SetBinLabel(4,"Bkg Muon")
    mu_type.GetXaxis().SetBinLabel(5,"Unknown Muon")


    b_pt=ROOT.TH1F(chain.GetName()+"bpt","",20,20,400)
    c_pt=ROOT.TH1F(chain.GetName()+"lpt","",20,20,400)
    l_pt=ROOT.TH1F(chain.GetName()+"cpt","",20,20,400)

    bb_SR=ROOT.TH2F(chain.GetName()+"bb_SR","",20,20,400,20,20,400)
    bl_SR=ROOT.TH2F(chain.GetName()+"bl_SR","",20,20,400,20,20,400)
    lb_SR=ROOT.TH2F(chain.GetName()+"lb_SR","",20,20,400,20,20,400)
    ll_SR=ROOT.TH2F(chain.GetName()+"ll_SR","",20,20,400,20,20,400)


    bb_CR_bl=ROOT.TH2F(chain.GetName()+"bb_CR_bl","",20,20,400,20,20,400)
    bl_CR_bl=ROOT.TH2F(chain.GetName()+"bl_CR_bl","",20,20,400,20,20,400)
    lb_CR_bl=ROOT.TH2F(chain.GetName()+"lb_CR_bl","",20,20,400,20,20,400)
    ll_CR_bl=ROOT.TH2F(chain.GetName()+"ll_CR_bl","",20,20,400,20,20,400)


    bb_CR_lb=ROOT.TH2F(chain.GetName()+"bb_CR_lb","",20,20,400,20,20,400)
    bl_CR_lb=ROOT.TH2F(chain.GetName()+"bl_CR_lb","",20,20,400,20,20,400)
    lb_CR_lb=ROOT.TH2F(chain.GetName()+"lb_CR_lb","",20,20,400,20,20,400)
    ll_CR_lb=ROOT.TH2F(chain.GetName()+"ll_CR_lb","",20,20,400,20,20,400)


    bb_CR_ll=ROOT.TH2F(chain.GetName()+"bb_CR_ll","",20,20,400,20,20,400)
    bl_CR_ll=ROOT.TH2F(chain.GetName()+"bl_CR_ll","",20,20,400,20,20,400)
    lb_CR_ll=ROOT.TH2F(chain.GetName()+"lb_CR_ll","",20,20,400,20,20,400)
    ll_CR_ll=ROOT.TH2F(chain.GetName()+"ll_CR_ll","",20,20,400,20,20,400)

    countEvents=0
    print(" Building plots for chain %s"%chain.GetName())
    for event in chain:
       countEvents+=1        
       
       if countEvents % 10000 == 0 :
          print("    INFO   ::   %d events processed so far for chain %s (%.2f %%)"%(countEvents, chain.GetName() , countEvents*100.0/chain.GetEntries() ))


       if not hasattr(event,"el_pt"):
          continue


          
       if event.jet_pt.size() != 2 :
          continue
       if event.el_pt.size() == 1 and event.mu_pt.size() == 1 :
          pass
       else:
          continue
        
       weights=event.weight_mc*event.weight_pileup*event.weight_leptonSF*event.weight_jvt
       totWeights=weights*lumiWeight
       

       eventClass=classifyEvent(event)
       eventFlav=eventFlavour(event)

       if eventFlav == "bb":
          b_pt.Fill(event.jet_pt[0]/1000,totWeights)
          b_pt.Fill(event.jet_pt[1]/1000,totWeights)
       if eventFlav == "bl":
          b_pt.Fill(event.jet_pt[0]/1000,totWeights)
          l_pt.Fill(event.jet_pt[1]/1000,totWeights)
       if eventFlav == "lb":
          l_pt.Fill(event.jet_pt[0]/1000,totWeights)
          b_pt.Fill(event.jet_pt[1]/1000,totWeights)
       if eventFlav == "ll":
          l_pt.Fill(event.jet_pt[0]/1000,totWeights)
          l_pt.Fill(event.jet_pt[1]/1000,totWeights)
               
       if event.el_true_type[0] == 2: 
          el_real_pt.Fill(event.el_pt[0]/1000,totWeights)
          el_type.Fill(0.5,totWeights)        
       elif event.el_true_type[0] == 17: 
          el_fakehad_pt.Fill(event.el_pt[0]/1000,totWeights)        
          el_type.Fill(1.5,totWeights)        
       else : 
          el_fake_pt.Fill(event.el_pt[0]/1000,totWeights)
          if event.el_true_type[0] == 3: 
             el_type.Fill(2.5,totWeights)        
          elif event.el_true_type[0] == 4: 
             el_type.Fill(3.5,totWeights)        
          else: 
             el_type.Fill(4.5,totWeights)        


       if event.mu_true_type[0] == 6: 
          mu_real_pt.Fill(event.mu_pt[0]/1000,totWeights)
          mu_type.Fill(0.5,totWeights)        
       elif event.mu_true_type[0] == 17: 
          mu_fakehad_pt.Fill(event.mu_pt[0]/1000,totWeights)        
          mu_type.Fill(1.5,totWeights)        
       else : 
          mu_fake_pt.Fill(event.mu_pt[0]/1000,totWeights)
          if event.mu_true_type[0] == 7: 
             mu_type.Fill(2.5,totWeights)        
          elif event.mu_true_type[0] == 8: 
             mu_type.Fill(3.5,totWeights)        
          else: 
             mu_type.Fill(4.5,totWeights)        



       if eventClass == "SR":
          if eventFlav == "bb":
             bb_SR.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "bl":
             bl_SR.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "lb":
             lb_SR.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "ll":
             ll_SR.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
       elif eventClass == "CR_bl":
          if eventFlav == "bb":
             bb_CR_bl.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "bl":
             bl_CR_bl.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "lb":
             lb_CR_bl.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "ll":
             ll_CR_bl.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
       elif eventClass == "CR_lb":
          if eventFlav == "bb":
             bb_CR_lb.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "bl":
             bl_CR_lb.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "lb":
             lb_CR_lb.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "ll":
             ll_CR_lb.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
       elif eventClass == "CR_ll":
          if eventFlav == "bb":
             bb_CR_ll.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "bl":
             bl_CR_ll.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "lb":
             lb_CR_ll.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)
          elif eventFlav == "ll":
             ll_CR_ll.Fill(event.jet_pt[0]/1000, event.jet_pt[1]/1000,totWeights)


    return [ el_real_pt,el_fakehad_pt,el_fake_pt,el_type, # electrons
             mu_real_pt,mu_fakehad_pt,mu_fake_pt,mu_type, # muons
             b_pt,l_pt, # inclusive pT
             bb_SR,bl_SR,lb_SR,ll_SR, ## Signal region
             bb_CR_bl,bl_CR_bl,lb_CR_bl,ll_CR_bl, # CR_bl 
             bb_CR_lb,bl_CR_lb,lb_CR_lb,ll_CR_lb, # CR_lb 
             bb_CR_ll,bl_CR_ll,lb_CR_ll,ll_CR_ll  # CR_ll
             ]


            
def readFiles(chain,sumchain,filepath):

    files=[]
    if isinstance(filepath,list):
        for filelist in filepath:
            files.extend(glob.glob(filelist))
    else:
        files.extend(glob.glob(filepath))

    for infile in files:
        chain.Add(infile)
        sumchain.Add(infile)
        print(" Adding file %s for chain %s"%(infile,chain.GetName()))



parser=argparse.ArgumentParser("Analysis of the better isolation for ttbar")
parser.add_argument("-n","--nominal",dest="nominal",action="store",help="Nominal list of files")
parser.add_argument("-l","--loose",dest="loose",action="store",help="Loose  list of files")
parser.add_argument("-t","--tight",dest="tight",action="store",help="Tight  list of files")
parser.add_argument("-k","--trkonly",dest="trkonly",action="store",help="TrackOnly list of files")
parser.add_argument("--lumi",dest="lumi",default=36200.,action="store",help="Luminosity")
parser.add_argument("-o","--output",dest="output",action="store",default=os.getcwd()+"/IsolationPlots/",help="Output folder")
args=parser.parse_args()


os.system("mkdir -p %s"%args.output)

nominalChain=ROOT.TChain("nominal","nominal")
looseChain=ROOT.TChain("nominal","nominal")
tightChain=ROOT.TChain("nominal","nominal")
trkChain=ROOT.TChain("nominal","nominal")

nominalSum=ROOT.TChain("sumWeights","sumWeights")
looseSum=ROOT.TChain("sumWeights","sumWeights")
tightSum=ROOT.TChain("sumWeights","sumWeights")
trkSum=ROOT.TChain("sumWeights","sumWeights")

readFiles(nominalChain,nominalSum,args.nominal.split(','))
nominalChain.SetName("Nominal")
plotsNominal=doPlots(nominalChain,nominalSum,args.lumi)
nominalChain.Delete() 

readFiles(looseChain,looseSum,args.loose.split(','))
looseChain.SetName("Loose")
plotsLoose=doPlots(looseChain,looseSum,args.lumi)
looseChain.Delete() 

readFiles(tightChain,tightSum,args.tight.split(','))
tightChain.SetName("Tight")
plotsTight=doPlots(tightChain,tightSum,args.lumi)
tightChain.Delete() 

readFiles(trkChain,trkSum,args.trkonly.split(','))
trkChain.SetName("TrkOnly")
plotsTrk=doPlots(trkChain,trkSum,args.lumi)
trkChain.Delete() 

canv=ROOT.TCanvas("canvas","canvas",700,700)
canv.cd()


plotsNominal[0].SetStats(0)
plotsNominal[0].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[0].GetYaxis().SetTitle("Weighted events")

plotsNominal[1].SetStats(0)
plotsNominal[1].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[1].GetYaxis().SetTitle("Weighted events")

plotsNominal[2].SetStats(0)
plotsNominal[2].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[2].GetYaxis().SetTitle("Weighted events")

plotsNominal[3].SetStats(0)
plotsNominal[3].GetXaxis().SetTitle("")
plotsNominal[3].GetYaxis().SetTitle("Weighted events")

plotsNominal[4].SetStats(0)
plotsNominal[4].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[4].GetYaxis().SetTitle("Weighted events")

plotsNominal[5].SetStats(0)
plotsNominal[5].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[5].GetYaxis().SetTitle("Weighted events")

plotsNominal[6].SetStats(0)
plotsNominal[6].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[6].GetYaxis().SetTitle("Weighted events")

plotsNominal[7].SetStats(0)
plotsNominal[7].GetXaxis().SetTitle("")
plotsNominal[7].GetYaxis().SetTitle("Weighted events")

plotsNominal[8].SetStats(0)
plotsNominal[8].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[8].GetYaxis().SetTitle("Weighted events")

plotsNominal[9].SetStats(0)
plotsNominal[9].GetXaxis().SetTitle("p_{T} [GeV]")
plotsNominal[9].GetYaxis().SetTitle("Weighted events")


save1Dhists(   [plotsNominal[0],plotsLoose[0],plotsTight[0],plotsTrk[0]]   ,args.output+"/"+"realelectronspt"       ,canv,args.lumi)
save1Dhists(   [plotsNominal[1],plotsLoose[1],plotsTight[1],plotsTrk[1]]   ,args.output+"/"+"electronhadronfakespt" ,canv,args.lumi)
save1Dhists(   [plotsNominal[2],plotsLoose[2],plotsTight[2],plotsTrk[2]]   ,args.output+"/"+"electronfakespt"       ,canv,args.lumi)
save1Dhists(   [plotsNominal[3],plotsLoose[3],plotsTight[3],plotsTrk[3]]   ,args.output+"/"+"electrontypes"         ,canv,args.lumi)
save1Dhists(   [plotsNominal[4],plotsLoose[4],plotsTight[4],plotsTrk[4]]   ,args.output+"/"+"realmuonspt"           ,canv,args.lumi)
save1Dhists(   [plotsNominal[5],plotsLoose[5],plotsTight[5],plotsTrk[5]]   ,args.output+"/"+"muonhadronfakespt"     ,canv,args.lumi)
save1Dhists(   [plotsNominal[6],plotsLoose[6],plotsTight[6],plotsTrk[6]]   ,args.output+"/"+"muonfakespt"           ,canv,args.lumi)
save1Dhists(   [plotsNominal[7],plotsLoose[7],plotsTight[7],plotsTrk[7]]   ,args.output+"/"+"muontypes"             ,canv,args.lumi)
save1Dhists(   [plotsNominal[8],plotsLoose[8],plotsTight[8],plotsTrk[8]]   ,args.output+"/"+"bjet_pt_comparison"    ,canv,args.lumi)
save1Dhists(   [plotsNominal[9],plotsLoose[9],plotsTight[9],plotsTrk[9]]   ,args.output+"/"+"ljet_pt_comparison"    ,canv,args.lumi)


canv.SetLogy(0)
save2Dhists(plotsNominal,args.output,10,canv)
save2Dhists(plotsLoose,args.output,10,canv)
save2Dhists(plotsTight,args.output,10,canv)
save2Dhists(plotsTrk,args.output,10,canv)


