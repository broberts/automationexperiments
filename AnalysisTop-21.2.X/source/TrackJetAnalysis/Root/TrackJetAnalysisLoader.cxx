#include "TrackJetAnalysis/TrackJetAnalysisLoader.h"
#include "TrackJetAnalysis/NTrackJetSelector.h"
#include <iostream>

top::EventSelectorBase* TrackJetAnalysisLoader::initTool(
							  const std::string& name,
							  const std::string& line,
							  TFile* outputFile,
							  std::shared_ptr<top::TopConfig> config,
							  EL::Worker* wk
							  ){
  std::istringstream iss(line);
  std::string toolname;
  getline(iss, toolname, ' ');

  std::string param;
  if (line.size() > toolname.size())
    param = line.substr(toolname.size() + 1);

  if (toolname == "TRACKJET_N")
    return new NTrackJetSelector(param);

  return nullptr;
}
