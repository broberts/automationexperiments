#include "TrackJetAnalysis/NTrackJetSelector.h"


NTrackJetSelector::NTrackJetSelector(const std::string& params) :
  SignValueSelector("TRACKJET_N", params, true) {
  checkMultiplicityIsInteger();
}

bool NTrackJetSelector::apply(const top::Event& event) const {
  auto func = [&](const xAOD::Jet* jetPtr){return jetPtr->pt() > value();};
  auto count = std::count_if(event.m_trackJets.begin(), event.m_trackJets.end(), func);
  return checkInt(count, multiplicity());
}
