#include "TrackJetAnalysis/TrackJetObjectLoader.h"
#include "TrackJetAnalysis/TrackJetAnalysisLoader.h"

#ifdef __CINT__
#pragma extra_include "TrackJetAnalysis/TrackJetObjectLoader.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
 
 
// For loading the object selection at run time
#pragma link C++ class TrackJetObjectLoader+;
#endif

#ifdef __CINT__
#pragma extra_include "TrackJetAnalysis/TrackJetAnalysisLoader.h";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
 
 
// For loading the object selection at run time
#pragma link C++ class TrackJetAnalysisLoader+;
#endif
