#ifndef NTRACKJETSELECTOR_H_
#define NTRACKJETSELECTOR_H_

#include "TopEventSelectionTools/SignValueSelector.h"


class NTrackJetSelector : public top::SignValueSelector {
 public:
  explicit NTrackJetSelector(const std::string& params);

  bool apply(const top::Event& event) const override;
};
#endif
