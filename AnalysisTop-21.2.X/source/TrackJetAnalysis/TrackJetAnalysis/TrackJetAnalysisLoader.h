#ifndef TRACKJETANALYSISLOADER
#define TRACKJETANALYSISLOADER

#include "TopEventSelectionTools/ToolLoaderBase.h"

class TrackJetAnalysisLoader: public top::ToolLoaderBase{
public:

  top::EventSelectorBase* initTool(
                                   const std::string& name,
                                   const std::string& line,
                                   TFile* outputFile,
				   std::shared_ptr<top::TopConfig> config,
                                   EL::Worker* wk = nullptr
                                   );

  ClassDef(TrackJetAnalysisLoader, 0)

};

#endif
