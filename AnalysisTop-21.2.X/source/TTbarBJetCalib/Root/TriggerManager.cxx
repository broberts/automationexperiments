#include "TTbarBJetCalib/ObjectManager.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <iostream>

using xAOD::IParticle;

ClassImp(top::TriggerManager);

namespace top {
  TriggerManager::TriggerManager() :
    m_L1Jet_eta            (),
    m_L1Jet_phi            (),
    m_L1Jet_et8x8          (),
    m_HLT_SplitJet_pt      (),
    m_HLT_SplitJet_et      (),
    m_HLT_SplitJet_eta     (),
    m_HLT_SplitJet_phi     (),
    m_HLT_SplitJet_m       (),
    m_HLT_SplitJet_mv2c00  (),
    m_HLT_SplitJet_mv2c10  (),
    m_HLT_SplitJet_mv2c20  (),
    m_jet_onlinemv2c20_split  (),
    m_jet_onlinemv2c10_split  (),
    m_HLT_GSCJet_pt        (),
    m_HLT_GSCJet_et        (),
    m_HLT_GSCJet_eta       (),
    m_HLT_GSCJet_phi       (),
    m_HLT_GSCJet_m         (),
    m_HLT_GSCJet_mv2c00    (),
    m_HLT_GSCJet_mv2c10    (),
    m_HLT_GSCJet_mv2c20    (),
    m_jet_onlinemv2c20_gsc (),
    m_jet_onlinemv2c10_gsc ()
  {
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void TriggerManager::DeclareBranches(top::TreeManager* tree)
  {
    tree->makeOutputVariable(m_L1Jet_eta            , "L1Jet_eta");
    tree->makeOutputVariable(m_L1Jet_phi            , "L1Jet_phi");
    tree->makeOutputVariable(m_L1Jet_et8x8          , "L1Jet_et8x8");
    tree->makeOutputVariable(m_HLT_SplitJet_pt      , "HLT_SplitJet_pt");
    tree->makeOutputVariable(m_HLT_SplitJet_et      , "HLT_SplitJet_et");
    tree->makeOutputVariable(m_HLT_SplitJet_eta     , "HLT_SplitJet_eta");
    tree->makeOutputVariable(m_HLT_SplitJet_phi     , "HLT_SplitJet_phi");
    tree->makeOutputVariable(m_HLT_SplitJet_m       , "HLT_SplitJet_m");
    tree->makeOutputVariable(m_HLT_SplitJet_mv2c00  , "HLT_SplitJet_mv2c00");
    tree->makeOutputVariable(m_HLT_SplitJet_mv2c10  , "HLT_SplitJet_mv2c10");
    tree->makeOutputVariable(m_HLT_SplitJet_mv2c20  , "HLT_SplitJet_mv2c20");
    tree->makeOutputVariable(m_jet_onlinemv2c20_split  , "jet_onlinemv2c20_split");
    tree->makeOutputVariable(m_jet_onlinemv2c10_split  , "jet_onlinemv2c10_split");
    tree->makeOutputVariable(m_HLT_GSCJet_pt        , "HLT_GSCJet_pt");
    tree->makeOutputVariable(m_HLT_GSCJet_et        , "HLT_GSCJet_et");
    tree->makeOutputVariable(m_HLT_GSCJet_eta       , "HLT_GSCJet_eta");
    tree->makeOutputVariable(m_HLT_GSCJet_phi       , "HLT_GSCJet_phi");
    tree->makeOutputVariable(m_HLT_GSCJet_m         , "HLT_GSCJet_m");
    tree->makeOutputVariable(m_HLT_GSCJet_mv2c00    , "HLT_GSCJet_mv2c00");
    tree->makeOutputVariable(m_HLT_GSCJet_mv2c10    , "HLT_GSCJet_mv2c10");
    tree->makeOutputVariable(m_HLT_GSCJet_mv2c20    , "HLT_GSCJet_mv2c20");
    tree->makeOutputVariable(m_jet_onlinemv2c20_gsc , "jet_onlinemv2c20_gsc");
    tree->makeOutputVariable(m_jet_onlinemv2c10_gsc , "jet_onlinemv2c10_gsc");
    
    for (int i=0; i<m_SplitJetTriggerList.size(); i++)
      tree->makeOutputVariable(m_JetTriggerDecisions[i] , m_SplitJetTriggerList[i]);



  }
  ///-- saveEvent - run for every systematic and every event --///
  void TriggerManager::Fill(const top::Event& event,xAOD::TEvent* eventStore)
  {
    m_L1Jet_eta.clear();
    m_L1Jet_phi.clear();
    m_L1Jet_et8x8.clear();
    m_HLT_SplitJet_pt.clear();
    m_HLT_SplitJet_et.clear();
    m_HLT_SplitJet_eta.clear();
    m_HLT_SplitJet_phi.clear();
    m_HLT_SplitJet_m.clear();
    m_HLT_SplitJet_mv2c00.clear();
    m_HLT_SplitJet_mv2c10.clear();
    m_HLT_SplitJet_mv2c20.clear();
    m_jet_onlinemv2c20_split.clear();
    m_jet_onlinemv2c10_split.clear();
    m_HLT_GSCJet_pt.clear();
    m_HLT_GSCJet_et.clear();
    m_HLT_GSCJet_eta.clear();
    m_HLT_GSCJet_phi.clear();
    m_HLT_GSCJet_m.clear();
    m_HLT_GSCJet_mv2c00.clear();
    m_HLT_GSCJet_mv2c10.clear();
    m_HLT_GSCJet_mv2c20.clear();
    m_jet_onlinemv2c20_gsc.clear();
    m_jet_onlinemv2c10_gsc.clear();


    for (const auto* const jp : event.m_jets) {


      //==== RETRIEVE TRIGGER LEVEL VARIABLES ====
      
      std::string m_TriggerChain;

      //Trigger Decisions
      for (unsigned int itrig = 0; itrig < m_SplitJetTriggerList.size(); itrig++) {
	m_TriggerChain = m_SplitJetTriggerList[itrig];
	auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
	if (cg->isPassed()) m_JetTriggerDecisions[itrig]=1;
	else m_JetTriggerDecisions[itrig]=0;
      }
      
      //LV1
      const xAOD::JetRoIContainer* m_L1Jets = nullptr;
      top::check(eventStore->retrieve(m_L1Jets, "LVL1JetRoIs"),"Failed to retrieve container LVL1JetRoIs");
      for(auto jptr : *m_L1Jets){
	m_L1Jet_eta.push_back(jptr->eta());
	m_L1Jet_phi.push_back(jptr->phi());
	m_L1Jet_et8x8.push_back(jptr->et8x8());
      }

      //TRIGGER NAVIGATION

      m_TriggerChain = "";

      //SplitJets
      for (unsigned int isplittrig = 0; isplittrig < m_SplitJetTriggerList.size(); isplittrig++) {
	m_TriggerChain = m_SplitJetTriggerList[isplittrig];
	auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
	if (cg->isPassed()) break;
      }
      std::vector<TLorentzVector> SplitJetVectors;

      Trig::FeatureContainer fcsplit = m_TrigDecisionTool->features(m_TriggerChain);
      Trig::FeatureContainer::combination_const_iterator comb_split   (fcsplit.getCombinations().begin());
      Trig::FeatureContainer::combination_const_iterator combEnd_split(fcsplit.getCombinations().end());

      for( ; comb_split!=combEnd_split ; ++comb_split) {
	std::vector< Trig::Feature<xAOD::JetContainer> >  jetCollections  = comb_split->containerFeature<xAOD::JetContainer>("SplitJet");
	std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb_split->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");

	if(jetCollections.size() > bjetCollections.size()){
	  std::cout << "ERROR Problem in container size SplitJet  -->  jets: "<< jetCollections.size() << " bjets: "<< bjetCollections.size() <<"\n";
	  continue;
	}

	for ( unsigned ifeat=0 ; ifeat<jetCollections.size() ; ifeat++ ) {
	  const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(ifeat));
	  if(!hlt_jet) continue;
	  const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(ifeat));
	  if(!hlt_btag) continue;

	  TLorentzVector v1; v1.SetPtEtaPhiM(hlt_jet->pt(), hlt_jet->eta(), hlt_jet->phi(), hlt_jet->m());
	  bool notsaved = true;
	  for (auto vsaved : SplitJetVectors) if (vsaved.DeltaR(v1) == 0) notsaved = false;

	  if (notsaved) {

	    double MV2c00_mvx = -999, MV2c10_mvx = -999, MV2c20_mvx = -999;
	    hlt_btag->MVx_discriminant("MV2c00" ,MV2c00_mvx);
	    hlt_btag->MVx_discriminant("MV2c10" ,MV2c10_mvx);
	    hlt_btag->MVx_discriminant("MV2c20" ,MV2c20_mvx);

	    m_HLT_SplitJet_pt.push_back(hlt_jet->pt());
	    m_HLT_SplitJet_eta.push_back(hlt_jet->eta());
	    m_HLT_SplitJet_phi.push_back(hlt_jet->phi());
	    m_HLT_SplitJet_m.push_back(hlt_jet->m());
	    m_HLT_SplitJet_et.push_back(v1.Et());
	    m_HLT_SplitJet_mv2c00.push_back(MV2c00_mvx);
	    m_HLT_SplitJet_mv2c10.push_back(MV2c10_mvx);
	    m_HLT_SplitJet_mv2c20.push_back(MV2c20_mvx);

	    SplitJetVectors.push_back(v1);
	  }

	}
      }


      std::vector<uint> matchedsplitjets;
      for (const auto* const jp : event.m_jets) {
	TLorentzVector vjet;
	vjet.SetPtEtaPhiE(jp->pt(), jp->eta(), jp->phi(), jp->e());
	float online_mv2c20 = -2;
	float online_mv2c10 = -2;
	for (uint itrigjet = 0; itrigjet < m_HLT_SplitJet_pt.size(); itrigjet++) {
	  bool isalreadymatched = false;
	  for (auto imtj : matchedsplitjets) if (itrigjet == imtj) isalreadymatched = true;
	  if (isalreadymatched) continue;
	  TLorentzVector vtrigjet;
	  vtrigjet.SetPtEtaPhiM(m_HLT_SplitJet_pt.at(itrigjet), m_HLT_SplitJet_eta.at(itrigjet), m_HLT_SplitJet_phi.at(itrigjet), m_HLT_SplitJet_m.at(itrigjet));
	  if (vjet.DeltaR(vtrigjet) < 0.2) {
	    online_mv2c20=m_HLT_SplitJet_mv2c20.at(itrigjet);
	    online_mv2c10=m_HLT_SplitJet_mv2c10.at(itrigjet);
	    matchedsplitjets.push_back(itrigjet);
	    break;
	  }
	}
	m_jet_onlinemv2c20_split.push_back(online_mv2c20);
	m_jet_onlinemv2c10_split.push_back(online_mv2c10);
      }

      m_TriggerChain = "";

      //GSCJets
      for (unsigned int igsctrig = 0; igsctrig < m_GSCJetTriggerList.size(); igsctrig++) {
	m_TriggerChain = m_GSCJetTriggerList[igsctrig];
	auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
	if (cg->isPassed()) break;
      }

      std::vector<TLorentzVector> GSCJetVectors;

      Trig::FeatureContainer fcgsc = m_TrigDecisionTool->features(m_TriggerChain);
      Trig::FeatureContainer::combination_const_iterator comb_gsc   (fcgsc.getCombinations().begin());
      Trig::FeatureContainer::combination_const_iterator combEnd_gsc(fcgsc.getCombinations().end());

      for( ; comb_gsc!=combEnd_gsc ; ++comb_gsc) {
	std::vector< Trig::Feature<xAOD::JetContainer> >  jetCollections  = comb_gsc->containerFeature<xAOD::JetContainer>("GSCJet");
	std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb_gsc->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");

	if(jetCollections.size() > bjetCollections.size()){
	  std::cout << "ERROR Problem in container size GSCJet  -->  jets: "<< jetCollections.size() << " bjets: "<< bjetCollections.size() <<"\n";
	  continue;
	}

	for ( unsigned ifeat=0 ; ifeat<jetCollections.size() ; ifeat++ ) {
	  const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(ifeat));
	  if(!hlt_jet) continue;
	  const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(ifeat));
	  if(!hlt_btag) continue;

	  TLorentzVector v1; v1.SetPtEtaPhiM(hlt_jet->pt(), hlt_jet->eta(), hlt_jet->phi(), hlt_jet->m());
	  bool notsaved = true;
	  for (auto vsaved : GSCJetVectors) if (vsaved.DeltaR(v1) == 0) notsaved = false;

	  if (notsaved) {

	    double MV2c00_mvx = -999, MV2c10_mvx = -999, MV2c20_mvx = -999;
	    hlt_btag->MVx_discriminant("MV2c00" ,MV2c00_mvx);
	    hlt_btag->MVx_discriminant("MV2c10" ,MV2c10_mvx);
	    hlt_btag->MVx_discriminant("MV2c20" ,MV2c20_mvx);

	    m_HLT_GSCJet_pt.push_back(hlt_jet->pt());
	    m_HLT_GSCJet_eta.push_back(hlt_jet->eta());
	    m_HLT_GSCJet_phi.push_back(hlt_jet->phi());
	    m_HLT_GSCJet_m.push_back(hlt_jet->m());
	    m_HLT_GSCJet_et.push_back(v1.Et());
	    m_HLT_GSCJet_mv2c00.push_back(MV2c00_mvx);
	    m_HLT_GSCJet_mv2c10.push_back(MV2c10_mvx);
	    m_HLT_GSCJet_mv2c20.push_back(MV2c20_mvx);

	    GSCJetVectors.push_back(v1);
	  }

	}
      }


      std::vector<uint> matchedgscjets;
      for (const auto* const jp : event.m_jets) {
	TLorentzVector vjet;
	vjet.SetPtEtaPhiE(jp->pt(), jp->eta(), jp->phi(), jp->e());
	float online_mv2c20 = -2;
	float online_mv2c10 = -2;
	for (uint itrigjet = 0; itrigjet < m_HLT_GSCJet_pt.size(); itrigjet++) {
	  bool isalreadymatched = false;
	  for (auto imtj : matchedgscjets) if (itrigjet == imtj) isalreadymatched = true;
	  if (isalreadymatched) continue;
	  TLorentzVector vtrigjet;
	  vtrigjet.SetPtEtaPhiM(m_HLT_GSCJet_pt.at(itrigjet), m_HLT_GSCJet_eta.at(itrigjet), m_HLT_GSCJet_phi.at(itrigjet), m_HLT_GSCJet_m.at(itrigjet));
	  if (vjet.DeltaR(vtrigjet) < 0.2) {
	    online_mv2c20=m_HLT_GSCJet_mv2c20.at(itrigjet);
	    online_mv2c10=m_HLT_GSCJet_mv2c10.at(itrigjet);
	    matchedgscjets.push_back(itrigjet);
	    break;
	  }
	}
	m_jet_onlinemv2c20_gsc.push_back(online_mv2c20);
	m_jet_onlinemv2c10_gsc.push_back(online_mv2c10);
      }


    }
  }

  TriggerManager::~TriggerManager()
  {
    delete m_TrigConfigTool;
    delete m_TrigDecisionTool;
  }
}
