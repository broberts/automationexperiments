#include "TTbarBJetCalib/ObjectManager.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <iostream>

using xAOD::IParticle;

ClassImp(top::HighTaggerManager);
namespace top {
  ///-- Constrcutor --///
  HighTaggerManager::HighTaggerManager() :
    m_MV2c10Flip           (),
    m_DL1Flip_pb           (),
    m_DL1Flip_pc           (),
    m_DL1Flip_pu           (),
    m_MV2c10FlipACC           (nullptr),
    m_DL1Flip_pbACC           (nullptr),
    m_DL1Flip_pcACC           (nullptr),
    m_DL1Flip_puACC           (nullptr)
  {
    m_MV2c10FlipACC            = new SG::AuxElement::Accessor<double> ("MV2c10Flip_discriminant");
    m_DL1Flip_pbACC            = new SG::AuxElement::Accessor<double> ("DL1Flip_pb");
    m_DL1Flip_pcACC            = new SG::AuxElement::Accessor<double> ("DL1Flip_pc");
    m_DL1Flip_puACC            = new SG::AuxElement::Accessor<double> ("DL1Flip_pu");    
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void HighTaggerManager::DeclareBranches(top::TreeManager* tree)
  {
    tree->makeOutputVariable(m_MV2c10Flip           , "MV2c10Flip");
    tree->makeOutputVariable(m_DL1Flip_pb           , "DL1Flip_pb");
    tree->makeOutputVariable(m_DL1Flip_pc           , "DL1Flip_pc");
    tree->makeOutputVariable(m_DL1Flip_pu           , "DL1Flip_pu");
  }
  ///-- saveEvent - run for every systematic and every event --///
  void HighTaggerManager::Fill(const top::Event& event)
  {
    m_MV2c10Flip.clear();
    m_DL1Flip_pb.clear();
    m_DL1Flip_pc.clear();
    m_DL1Flip_pu.clear();
    float MV2c10Flip;
    float DL1Flip_pb;
    float DL1Flip_pc;
    float DL1Flip_pu;

    for (const auto* const jp : event.m_jets) {
      MV2c10Flip            = -999.;
      DL1Flip_pb            = -999.;
      DL1Flip_pc            = -999.;
      DL1Flip_pu            = -999.;

      // retrieve btagging info
      const xAOD::BTagging* btag = jp->btagging();

      if (m_MV2c10FlipACC            && m_MV2c10FlipACC            ->isAvailable(*btag) ) MV2c10Flip            = (*m_MV2c10FlipACC            )(*btag);
      if (m_DL1Flip_pbACC            && m_DL1Flip_pbACC            ->isAvailable(*btag) ) DL1Flip_pb            = (*m_DL1Flip_pbACC            )(*btag);
      if (m_DL1Flip_pcACC            && m_DL1Flip_pcACC            ->isAvailable(*btag) ) DL1Flip_pc            = (*m_DL1Flip_pcACC            )(*btag);
      if (m_DL1Flip_puACC            && m_DL1Flip_puACC            ->isAvailable(*btag) ) DL1Flip_pu            = (*m_DL1Flip_puACC            )(*btag);

      m_MV2c10Flip.push_back(MV2c10Flip);
      m_DL1Flip_pb.push_back(DL1Flip_pb);
      m_DL1Flip_pc.push_back(DL1Flip_pc);
      m_DL1Flip_pu.push_back(DL1Flip_pu);
    }
  }

  HighTaggerManager::~HighTaggerManager()
  {

    delete m_MV2c10FlipACC;
    delete m_DL1Flip_pbACC;
    delete m_DL1Flip_pcACC;
    delete m_DL1Flip_puACC;

  }
}
