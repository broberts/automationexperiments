#include "TTbarBJetCalib/CustomEventSaver.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <iostream>

using xAOD::IParticle;

namespace top {
  ///-- Constrcutor --///
  CustomEventSaver::CustomEventSaver() :
    m_config(nullptr),
    m_IP2DNeg_ntrk         (),
    m_IP2DNeg_pb           (),
    m_IP2DNeg_pc           (),
    m_IP2DNeg_pu           (),
    m_IP2D_ntrk            (),
    m_IP2D_pb              (),
    m_IP2D_pc              (),
    m_IP2D_pu              (),
    m_IP3DNeg_ntrk         (),
    m_IP3DNeg_pb           (),
    m_IP3DNeg_pc           (),
    m_IP3DNeg_pu           (),
    m_IP3D_ntrk            (),
    m_IP3D_pb              (),
    m_IP3D_pc              (),
    m_IP3D_pu              (),
    m_JFFlip_N2Tpair       (),
    m_JFFlip_dRFlightDir   (),
    m_JFFlip_deltaeta      (),
    m_JFFlip_deltaphi      (),
    m_JFFlip_energyFraction(),
    m_JFFlip_mass          (),
    m_JFFlip_nSingleTracks (),
    m_JFFlip_nTracksAtVtx  (),
    m_JFFlip_nVTX          (),
    m_JFFlip_ntrk          (),
    m_JFFlip_significance3d(),
    m_JF_N2Tpair           (),
    m_JF_dRFlightDir       (),
    m_JF_deltaeta          (),
    m_JF_deltaphi          (),
    m_JF_energyFraction    (),
    m_JF_mass              (),
    m_JF_nSingleTracks     (),
    m_JF_nTracksAtVtx      (),
    m_JF_nVTX              (),
    m_JF_ntrk              (),
    m_JF_significance3d    (),
    m_MV2c10Flip           (),
    m_DL1Flip_pb           (),
    m_DL1Flip_pc           (),
    m_DL1Flip_pu           (),
    m_SV1Flip_L3d          (),
    m_SV1Flip_Lxy          (),
    m_SV1Flip_N2Tpair      (),
    m_SV1Flip_NGTinSvx     (),
    m_SV1Flip_deltaR       (),
    m_SV1Flip_efracsvx     (),
    m_SV1Flip_masssvx      (),
    m_SV1Flip_normdist     (),
    m_SV1Flip_ntrk         (),
    m_SV1_L3d              (),
    m_SV1_Lxy              (),
    m_SV1_N2Tpair          (),
    m_SV1_NGTinSvx         (),
    m_SV1_deltaR           (),
    m_SV1_efracsvx         (),
    m_SV1_masssvx          (),
    m_SV1_normdist         (),
    m_SV1_ntrk             (),
    m_SMT                  (),
    m_nPV                 (0.),
    m_averageIntPerXing   (0.),
    m_HadronConeExclExtendedTruthLabelID	(),
    m_L1Jet_eta            (),
    m_L1Jet_phi            (),
    m_L1Jet_et8x8          (),
    m_HLT_SplitJet_pt      (),
    m_HLT_SplitJet_et      (),
    m_HLT_SplitJet_eta     (),
    m_HLT_SplitJet_phi     (),
    m_HLT_SplitJet_m       (),
    m_HLT_SplitJet_mv2c00  (),
    m_HLT_SplitJet_mv2c10  (),
    m_HLT_SplitJet_mv2c20  (),
    m_jet_onlinemv2c20_split  (),
    m_jet_onlinemv2c10_split  (),
    m_HLT_GSCJet_pt        (),
    m_HLT_GSCJet_et        (),
    m_HLT_GSCJet_eta       (),
    m_HLT_GSCJet_phi       (),
    m_HLT_GSCJet_m         (),
    m_HLT_GSCJet_mv2c00    (),
    m_HLT_GSCJet_mv2c10    (),
    m_HLT_GSCJet_mv2c20    (),
    m_jet_onlinemv2c20_gsc (),
    m_jet_onlinemv2c10_gsc (),
    //track jets
    m_tjet_numConstituents (),
    m_tjet_MV2r (),
    m_tjet_MV2rmu (),
    m_tjet_DL1_pu (),
    m_tjet_DL1_pb (),
    m_tjet_DL1_pc (),
    m_tjet_DL1r_pu (),
    m_tjet_DL1r_pb (),
    m_tjet_DL1r_pc (),
    m_tjet_DL1rmu_pu (),
    m_tjet_DL1rmu_pb (),
    m_tjet_DL1rmu_pc (),
    m_tjet_BHadron_eta (),
    m_tjet_BHadron_pt (),
    m_tjet_BHadron_phi (),
    m_tjet_BHadron_e (),
    m_tjet_Ghosts_BHadron_Final_Count (),
    m_tjet_Ghosts_CHadron_Final_Count (),
    m_tjet_Ghosts_Taus_Final_Count (),
    m_tjet_track_eta (),
    m_tjet_track_pt (),
    m_tjet_track_phi (),
    m_tjet_track_e (),
    m_tjet_HadronConeExclTruthLabelID (),
    m_tjet_HadronConeExclExtendedTruthLabelID (),
    m_IP2D_TPELVecACC         (nullptr),
    m_IP3D_TPELVecACC         (nullptr),
    m_IP2DNeg_TPELVecACC      (nullptr),
    m_IP3DNeg_TPELVecACC      (nullptr),
    m_SV1_TPELVecACC          (nullptr),
    m_SV1Flip_TPELVecACC      (nullptr),
    m_JF_TPELVecACC           (nullptr),
    m_JFFlip_TPELVecACC       (nullptr),
    m_IP2DNeg_pbACC           (nullptr),
    m_IP2DNeg_pcACC           (nullptr),
    m_IP2DNeg_puACC           (nullptr),
    m_IP2D_pbACC              (nullptr),
    m_IP2D_pcACC              (nullptr),
    m_IP2D_puACC              (nullptr),
    m_IP3DNeg_pbACC           (nullptr),
    m_IP3DNeg_pcACC           (nullptr),
    m_IP3DNeg_puACC           (nullptr),
    m_IP3D_pbACC              (nullptr),
    m_IP3D_pcACC              (nullptr),
    m_IP3D_puACC              (nullptr),
    m_JFFlip_N2TpairACC       (nullptr),
    m_JFFlip_dRFlightDirACC   (nullptr),
    m_JFFlip_deltaetaACC      (nullptr),
    m_JFFlip_deltaphiACC      (nullptr),
    m_JFFlip_energyFractionACC(nullptr),
    m_JFFlip_massACC          (nullptr),
    m_JFFlip_nSingleTracksACC (nullptr),
    m_JFFlip_nTracksAtVtxACC  (nullptr),
    m_JFFlip_nVTXACC          (nullptr),
    m_JFFlip_significance3dACC(nullptr),
    m_JF_N2TpairACC           (nullptr),
    m_JF_dRFlightDirACC       (nullptr),
    m_JF_deltaetaACC          (nullptr),
    m_JF_deltaphiACC          (nullptr),
    m_JF_energyFractionACC    (nullptr),
    m_JF_massACC              (nullptr),
    m_JF_nSingleTracksACC     (nullptr),
    m_JF_nTracksAtVtxACC      (nullptr),
    m_JF_nVTXACC              (nullptr),
    m_JF_significance3dACC    (nullptr),
    m_MV2c10FlipACC           (nullptr),
    m_DL1Flip_pbACC           (nullptr),
    m_DL1Flip_pcACC           (nullptr),
    m_DL1Flip_puACC           (nullptr),
    m_SV1Flip_L3dACC          (nullptr),
    m_SV1Flip_LxyACC          (nullptr),
    m_SV1Flip_N2TpairACC      (nullptr),
    m_SV1Flip_NGTinSvxACC     (nullptr),
    m_SV1Flip_deltaRACC       (nullptr),
    m_SV1Flip_efracsvxACC     (nullptr),
    m_SV1Flip_masssvxACC      (nullptr),
    m_SV1Flip_normdistACC     (nullptr),
    m_SV1_L3dACC              (nullptr),
    m_SV1_LxyACC              (nullptr),
    m_SV1_N2TpairACC          (nullptr),
    m_SV1_NGTinSvxACC         (nullptr),
    m_SV1_deltaRACC           (nullptr),
    m_SV1_efracsvxACC         (nullptr),
    m_SV1_masssvxACC          (nullptr),
    m_SV1_normdistACC         (nullptr),
    m_HadronConeExclExtendedTruthLabelIDACC	(nullptr)

  {
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    m_config = config;
    //------------------------
    // SV1 variables
    //------------------------
    // --> Nominal
    m_IP2DNeg_pbACC            = new SG::AuxElement::Accessor<double>  ("IP2DNeg_pb");
    m_IP2DNeg_pcACC            = new SG::AuxElement::Accessor<double>  ("IP2DNeg_pc");
    m_IP2DNeg_puACC            = new SG::AuxElement::Accessor<double>  ("IP2DNeg_pu");
    m_IP2DNeg_TPELVecACC       = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("IP2DNeg_TrackParticleLinks");
    m_IP2D_pbACC               = new SG::AuxElement::Accessor<double>  ("IP2D_pb");
    m_IP2D_pcACC               = new SG::AuxElement::Accessor<double>  ("IP2D_pc");
    m_IP2D_puACC               = new SG::AuxElement::Accessor<double>  ("IP2D_pu");
    m_IP2D_TPELVecACC          = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("IP2D_TrackParticleLinks");
    m_IP3DNeg_pbACC            = new SG::AuxElement::Accessor<double>  ("IP3DNeg_pb");
    m_IP3DNeg_pcACC            = new SG::AuxElement::Accessor<double>  ("IP3DNeg_pc");
    m_IP3DNeg_puACC            = new SG::AuxElement::Accessor<double>  ("IP3DNeg_pu");
    m_IP3DNeg_TPELVecACC       = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("IP3DNeg_TrackParticleLinks");
    m_IP3D_pbACC               = new SG::AuxElement::Accessor<double>  ("IP3D_pb");
    m_IP3D_pcACC               = new SG::AuxElement::Accessor<double>  ("IP3D_pc");
    m_IP3D_puACC               = new SG::AuxElement::Accessor<double>  ("IP3D_pu");
    m_IP3D_TPELVecACC          = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("IP3D_TrackParticleLinks");
    m_JFFlip_N2TpairACC        = new SG::AuxElement::Accessor<int>    ("JetFitterFlip_N2Tpair");
    m_JFFlip_dRFlightDirACC    = new SG::AuxElement::Accessor<float>  ("JetFitterFlip_dRFlightDir");
    m_JFFlip_deltaetaACC       = new SG::AuxElement::Accessor<float>  ("JetFitterFlip_deltaeta");
    m_JFFlip_deltaphiACC       = new SG::AuxElement::Accessor<float>  ("JetFitterFlip_deltaphi");
    m_JFFlip_energyFractionACC = new SG::AuxElement::Accessor<float>  ("JetFitterFlip_energyFraction");
    m_JFFlip_massACC           = new SG::AuxElement::Accessor<float>  ("JetFitterFlip_mass");
    m_JFFlip_nSingleTracksACC  = new SG::AuxElement::Accessor<int>    ("JetFitterFlip_nSingleTracks");
    m_JFFlip_nTracksAtVtxACC   = new SG::AuxElement::Accessor<int>    ("JetFitterFlip_nTracksAtVtx");
    m_JFFlip_nVTXACC           = new SG::AuxElement::Accessor<int>    ("JetFitterFlip_nVTX");
    m_JFFlip_significance3dACC = new SG::AuxElement::Accessor<float>  ("JetFitterFlip_significance3d");
    m_JFFlip_TPELVecACC        = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("JetFitterFlip_tracksAtPVlinks");
    m_JF_N2TpairACC            = new SG::AuxElement::Accessor<int>    ("JetFitter_N2Tpair");
    m_JF_dRFlightDirACC        = new SG::AuxElement::Accessor<float>  ("JetFitter_dRFlightDir");
    m_JF_deltaetaACC           = new SG::AuxElement::Accessor<float>  ("JetFitter_deltaeta");
    m_JF_deltaphiACC           = new SG::AuxElement::Accessor<float>  ("JetFitter_deltaphi");
    m_JF_energyFractionACC     = new SG::AuxElement::Accessor<float>  ("JetFitter_energyFraction");
    m_JF_massACC               = new SG::AuxElement::Accessor<float>  ("JetFitter_mass");
    m_JF_nSingleTracksACC      = new SG::AuxElement::Accessor<int>    ("JetFitter_nSingleTracks");
    m_JF_nTracksAtVtxACC       = new SG::AuxElement::Accessor<int>    ("JetFitter_nTracksAtVtx");
    m_JF_nVTXACC               = new SG::AuxElement::Accessor<int>    ("JetFitter_nVTX");
    m_JF_significance3dACC     = new SG::AuxElement::Accessor<float>  ("JetFitter_significance3d");
    m_JF_TPELVecACC            = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("JetFitter_tracksAtPVlinks");
    m_MV2c10FlipACC            = new SG::AuxElement::Accessor<double> ("MV2c10Flip_discriminant");
    m_DL1Flip_pbACC            = new SG::AuxElement::Accessor<double> ("DL1Flip_pb");
    m_DL1Flip_pcACC            = new SG::AuxElement::Accessor<double> ("DL1Flip_pc");
    m_DL1Flip_puACC            = new SG::AuxElement::Accessor<double> ("DL1Flip_pu");
    m_SV1Flip_L3dACC           = new SG::AuxElement::Accessor<float>  ("SV1Flip_L3d");
    m_SV1Flip_LxyACC           = new SG::AuxElement::Accessor<float>  ("SV1Flip_Lxy");
    m_SV1Flip_N2TpairACC       = new SG::AuxElement::Accessor<int>    ("SV1Flip_N2Tpair");
    m_SV1Flip_NGTinSvxACC      = new SG::AuxElement::Accessor<int>    ("SV1Flip_NGTinSvx");
    m_SV1Flip_deltaRACC        = new SG::AuxElement::Accessor<float>  ("SV1Flip_deltaR");
    m_SV1Flip_efracsvxACC      = new SG::AuxElement::Accessor<float>  ("SV1Flip_efracsvx");
    m_SV1Flip_masssvxACC       = new SG::AuxElement::Accessor<float>  ("SV1Flip_masssvx");
    m_SV1Flip_normdistACC      = new SG::AuxElement::Accessor<float>  ("SV1Flip_normdist");
    m_SV1Flip_TPELVecACC       = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("SV1Flip_TrackParticleLinks");
    m_SV1_L3dACC               = new SG::AuxElement::Accessor<float>  ("SV1_L3d");
    m_SV1_LxyACC               = new SG::AuxElement::Accessor<float>  ("SV1_Lxy");
    m_SV1_N2TpairACC           = new SG::AuxElement::Accessor<int>    ("SV1_N2Tpair");
    m_SV1_NGTinSvxACC          = new SG::AuxElement::Accessor<int>    ("SV1_NGTinSvx");
    m_SV1_deltaRACC            = new SG::AuxElement::Accessor<float>  ("SV1_deltaR");
    m_SV1_efracsvxACC          = new SG::AuxElement::Accessor<float>  ("SV1_efracsvx");
    m_SV1_masssvxACC           = new SG::AuxElement::Accessor<float>  ("SV1_masssvx");
    m_SV1_normdistACC          = new SG::AuxElement::Accessor<float>  ("SV1_normdist");
    m_SV1_TPELVecACC           = new SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t>("SV1_TrackParticleLinks");
    m_HadronConeExclExtendedTruthLabelIDACC	= new SG::AuxElement::Accessor<int>	("HadronConeExclExtendedTruthLabelID");
    m_eventClean               = new SG::AuxElement::Accessor<char>  ("DFCommonJets_eventClean_LooseBad");

    //==== TRIGGER TOOLS INITIALIZATION ====
    //Initialize and configure trigger tools 
    m_TrigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    top::check(m_TrigConfigTool->initialize(),"Failed to initialize TrigConfTool");
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_TrigConfigTool );
    m_TrigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    top::check(m_TrigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ),"Failed to set Property for TrigDecisionTool");
    top::check(m_TrigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ),"Failed to set Property for TrigDecisionTool");
    top::check(m_TrigDecisionTool->setProperty( "AcceptMultipleInstance", "1" ),"Failed to set Property for TrigDecisionTool");
    top::check(m_TrigDecisionTool->initialize(),"Failed to initialize TrigDecisionTool");

    //set lists of triggers
    m_SplitJetTriggerList = {
      "HLT_mu26_ivarmedium_2j35_boffperf_split",
      "HLT_e26_lhtight_nod0_ivarloose_2j35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_2j35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM_2j35_boffperf_split",
      "HLT_2mu14_2j35_boffperf_split",
      "HLT_2e24_lhvloose_nod0_2j35_boffperf_split",
      //"HLT_e28_lhmedium_nod0_L1EM24VHI_mu8noL1_2j35_boffperf_split",
      "HLT_mu26_ivarmedium_j150_boffperf_split_j35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_j150_boffperf_split_j35_boffperf_split",
      "HLT_mu26_ivarmedium_j110_gsc150_boffperf_split_j15_gsc35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_j110_gsc150_boffperf_split_j15_gsc35_boffperf_split",
      "HLT_2mu14_2j15_gsc35_boffperf_split",
      "HLT_2e24_lhvloose_nod0_2j15_gsc35_boffperf_split",
      //"HLT_e28_lhmedium_nod0_L1EM24VHI_mu8noL1_2j15_gsc35_boffperf_split",
      "HLT_mu26_ivarmedium_j110_gsc150_boffperf_split_j35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_j110_gsc150_boffperf_split_j35_boffperf_split",
    };
    m_GSCJetTriggerList = {
      "HLT_mu26_ivarmedium_j110_gsc150_boffperf_split_j15_gsc35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_j110_gsc150_boffperf_split_j15_gsc35_boffperf_split",
      "HLT_2mu14_2j15_gsc35_boffperf_split",
      "HLT_2e24_lhvloose_nod0_2j15_gsc35_boffperf_split",
      //"HLT_e28_lhmedium_nod0_L1EM24VHI_mu8noL1_2j15_gsc35_boffperf_split",
      "HLT_mu26_ivarmedium_j110_gsc150_boffperf_split_j35_boffperf_split",
      "HLT_e28_lhtight_nod0_ivarloose_j110_gsc150_boffperf_split_j35_boffperf_split",
    };
    m_JetTriggerDecisions.resize(m_SplitJetTriggerList.size());

	
    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      systematicTree->makeOutputVariable(m_averageIntPerXing    , "averageIntPerXing");
      systematicTree->makeOutputVariable(m_nPV                  , "nPV");
      systematicTree->makeOutputVariable(m_SMT                  , "jets_SMT");
      systematicTree->makeOutputVariable(m_IP2DNeg_ntrk         , "IP2DNeg_ntrk");
      // systematicTree->makeOutputVariable(m_IP2DNeg_pb           , "IP2DNeg_pb");
      // systematicTree->makeOutputVariable(m_IP2DNeg_pc           , "IP2DNeg_pc");
      // systematicTree->makeOutputVariable(m_IP2DNeg_pu           , "IP2DNeg_pu");
      systematicTree->makeOutputVariable(m_IP2D_ntrk            , "IP2D_ntrk");
      // systematicTree->makeOutputVariable(m_IP2D_pb              , "IP2D_pb");
      // systematicTree->makeOutputVariable(m_IP2D_pc              , "IP2D_pc");
      // systematicTree->makeOutputVariable(m_IP2D_pu              , "IP2D_pu");
      systematicTree->makeOutputVariable(m_IP3DNeg_ntrk         , "IP3DNeg_ntrk");
      // systematicTree->makeOutputVariable(m_IP3DNeg_pb           , "IP3DNeg_pb");
      // systematicTree->makeOutputVariable(m_IP3DNeg_pc           , "IP3DNeg_pc");
      // systematicTree->makeOutputVariable(m_IP3DNeg_pu           , "IP3DNeg_pu");
      systematicTree->makeOutputVariable(m_IP3D_ntrk            , "IP3D_ntrk");
      // systematicTree->makeOutputVariable(m_IP3D_pb              , "IP3D_pb");
      // systematicTree->makeOutputVariable(m_IP3D_pc              , "IP3D_pc");
      // systematicTree->makeOutputVariable(m_IP3D_pu              , "IP3D_pu");
      // systematicTree->makeOutputVariable(m_JFFlip_N2Tpair       , "JFFlip_N2Tpair");
      // systematicTree->makeOutputVariable(m_JFFlip_dRFlightDir   , "JFFlip_dRFlightDir");
      // systematicTree->makeOutputVariable(m_JFFlip_deltaeta      , "JFFlip_deltaeta");
      // systematicTree->makeOutputVariable(m_JFFlip_deltaphi      , "JFFlip_deltaphi");
      // systematicTree->makeOutputVariable(m_JFFlip_energyFraction, "JFFlip_energyFraction");
      // systematicTree->makeOutputVariable(m_JFFlip_mass          , "JFFlip_mass");
      // systematicTree->makeOutputVariable(m_JFFlip_nSingleTracks , "JFFlip_nSingleTracks");
      // systematicTree->makeOutputVariable(m_JFFlip_nTracksAtVtx  , "JFFlip_nTracksAtVtx");
      // systematicTree->makeOutputVariable(m_JFFlip_nVTX          , "JFFlip_nVTX");
      systematicTree->makeOutputVariable(m_JFFlip_ntrk          , "JFFlip_ntrk");
      // systematicTree->makeOutputVariable(m_JFFlip_significance3d, "JFFlip_significance3d");
      // systematicTree->makeOutputVariable(m_JF_N2Tpair           , "JF_N2Tpair");
      // systematicTree->makeOutputVariable(m_JF_dRFlightDir       , "JF_dRFlightDir");
      // systematicTree->makeOutputVariable(m_JF_deltaeta          , "JF_deltaeta");
      // systematicTree->makeOutputVariable(m_JF_deltaphi          , "JF_deltaphi");
      // systematicTree->makeOutputVariable(m_JF_energyFraction    , "JF_energyFraction");
      // systematicTree->makeOutputVariable(m_JF_mass              , "JF_mass");
      // systematicTree->makeOutputVariable(m_JF_nSingleTracks     , "JF_nSingleTracks");
      // systematicTree->makeOutputVariable(m_JF_nTracksAtVtx      , "JF_nTracksAtVtx");
      // systematicTree->makeOutputVariable(m_JF_nVTX              , "JF_nVTX");
      systematicTree->makeOutputVariable(m_JF_ntrk              , "JF_ntrk");
      // systematicTree->makeOutputVariable(m_JF_significance3d    , "JF_significance3d");
      // systematicTree->makeOutputVariable(m_MV2c10Flip           , "MV2c10Flip");
      // systematicTree->makeOutputVariable(m_DL1Flip_pb           , "DL1Flip_pb");
      // systematicTree->makeOutputVariable(m_DL1Flip_pc           , "DL1Flip_pc");
      // systematicTree->makeOutputVariable(m_DL1Flip_pu           , "DL1Flip_pu");
      // systematicTree->makeOutputVariable(m_SV1Flip_L3d          , "SV1Flip_L3d");
      // systematicTree->makeOutputVariable(m_SV1Flip_Lxy          , "SV1Flip_Lxy");
      // systematicTree->makeOutputVariable(m_SV1Flip_N2Tpair      , "SV1Flip_N2Tpair");
      // systematicTree->makeOutputVariable(m_SV1Flip_NGTinSvx     , "SV1Flip_NGTinSvx");
      // systematicTree->makeOutputVariable(m_SV1Flip_deltaR       , "SV1Flip_deltaR");
      // systematicTree->makeOutputVariable(m_SV1Flip_efracsvx     , "SV1Flip_efracsvx");
      // systematicTree->makeOutputVariable(m_SV1Flip_masssvx      , "SV1Flip_masssvx");
      // systematicTree->makeOutputVariable(m_SV1Flip_normdist     , "SV1Flip_normdist");
      systematicTree->makeOutputVariable(m_SV1Flip_ntrk         , "SV1Flip_ntrk");
      // systematicTree->makeOutputVariable(m_SV1_L3d              , "SV1_L3d");
      // systematicTree->makeOutputVariable(m_SV1_Lxy              , "SV1_Lxy");
      // systematicTree->makeOutputVariable(m_SV1_N2Tpair          , "SV1_N2Tpair");
      // systematicTree->makeOutputVariable(m_SV1_NGTinSvx         , "SV1_NGTinSvx");
      // systematicTree->makeOutputVariable(m_SV1_deltaR           , "SV1_deltaR");
      // systematicTree->makeOutputVariable(m_SV1_efracsvx         , "SV1_efracsvx");
      // systematicTree->makeOutputVariable(m_SV1_masssvx          , "SV1_masssvx");
      // systematicTree->makeOutputVariable(m_SV1_normdist         , "SV1_normdist");
      systematicTree->makeOutputVariable(m_SV1_ntrk             , "SV1_ntrk");
      systematicTree->makeOutputVariable(m_HadronConeExclExtendedTruthLabelID	, "HadronConeExclExtendedTruthLabelID");
      // systematicTree->makeOutputVariable(m_L1Jet_eta            , "L1Jet_eta");
      // systematicTree->makeOutputVariable(m_L1Jet_phi            , "L1Jet_phi");
      // systematicTree->makeOutputVariable(m_L1Jet_et8x8          , "L1Jet_et8x8");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_pt      , "HLT_SplitJet_pt");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_et      , "HLT_SplitJet_et");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_eta     , "HLT_SplitJet_eta");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_phi     , "HLT_SplitJet_phi");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_m       , "HLT_SplitJet_m");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_mv2c00  , "HLT_SplitJet_mv2c00");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_mv2c10  , "HLT_SplitJet_mv2c10");
      // systematicTree->makeOutputVariable(m_HLT_SplitJet_mv2c20  , "HLT_SplitJet_mv2c20");
      // systematicTree->makeOutputVariable(m_jet_onlinemv2c20_split  , "jet_onlinemv2c20_split");
      // systematicTree->makeOutputVariable(m_jet_onlinemv2c10_split  , "jet_onlinemv2c10_split");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_pt        , "HLT_GSCJet_pt");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_et        , "HLT_GSCJet_et");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_eta       , "HLT_GSCJet_eta");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_phi       , "HLT_GSCJet_phi");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_m         , "HLT_GSCJet_m");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_mv2c00    , "HLT_GSCJet_mv2c00");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_mv2c10    , "HLT_GSCJet_mv2c10");
      // systematicTree->makeOutputVariable(m_HLT_GSCJet_mv2c20    , "HLT_GSCJet_mv2c20");
      // systematicTree->makeOutputVariable(m_jet_onlinemv2c20_gsc , "jet_onlinemv2c20_gsc");
      // systematicTree->makeOutputVariable(m_jet_onlinemv2c10_gsc , "jet_onlinemv2c10_gsc");
      for (int i=0; i<m_SplitJetTriggerList.size(); i++) systematicTree->makeOutputVariable(m_JetTriggerDecisions[i] , m_SplitJetTriggerList[i]);
      //track jets
      if(m_config->useTrackJets()){
	systematicTree->makeOutputVariable(m_tjet_numConstituents, "tjet_numConstituents");
	//taggers
	systematicTree->makeOutputVariable(m_tjet_MV2r,  "tjet_MV2r");
	systematicTree->makeOutputVariable(m_tjet_MV2rmu,  "tjet_MV2rmu");
	systematicTree->makeOutputVariable(m_tjet_DL1_pu, "tjet_DL1_pu");
	systematicTree->makeOutputVariable(m_tjet_DL1_pb, "tjet_DL1_pb");
	systematicTree->makeOutputVariable(m_tjet_DL1_pc, "tjet_DL1_pc");
	systematicTree->makeOutputVariable(m_tjet_DL1r_pu, "tjet_DL1r_pu");
	systematicTree->makeOutputVariable(m_tjet_DL1r_pb, "tjet_DL1r_pb");
	systematicTree->makeOutputVariable(m_tjet_DL1r_pc, "tjet_DL1r_pc");
	systematicTree->makeOutputVariable(m_tjet_DL1rmu_pu, "tjet_DL1rmu_pu");
	systematicTree->makeOutputVariable(m_tjet_DL1rmu_pb, "tjet_DL1rmu_pb");
	systematicTree->makeOutputVariable(m_tjet_DL1rmu_pc, "tjet_DL1rmu_pc");
	//constituent tracks
	systematicTree->makeOutputVariable(m_tjet_track_eta, "tjet_track_eta");
	systematicTree->makeOutputVariable(m_tjet_track_phi, "tjet_track_phi");
	systematicTree->makeOutputVariable(m_tjet_track_pt, "tjet_track_pt");
	systematicTree->makeOutputVariable(m_tjet_track_e, "tjet_track_e");
	if(m_config->isMC()){
	  //b-hadrons within dR=0.3
	  systematicTree->makeOutputVariable(m_tjet_BHadron_eta, "tjet_BHadron_eta");
	  systematicTree->makeOutputVariable(m_tjet_BHadron_phi, "tjet_BHadron_phi");
	  systematicTree->makeOutputVariable(m_tjet_BHadron_pt, "tjet_BHadron_pt");
	  systematicTree->makeOutputVariable(m_tjet_BHadron_e, "tjet_BHadron_e");
	  systematicTree->makeOutputVariable(m_tjet_Ghosts_BHadron_Final_Count, "tjet_nGhosts_bHadron");
	  systematicTree->makeOutputVariable(m_tjet_Ghosts_CHadron_Final_Count, "tjet_nGhosts_cHadron");
	  systematicTree->makeOutputVariable(m_tjet_Ghosts_Taus_Final_Count, "tjet_nGhosts_Taus");
	  //truth labeling
	  systematicTree->makeOutputVariable(m_tjet_HadronConeExclTruthLabelID, "tjet_HadronConeExclTruthLabelID");
	  systematicTree->makeOutputVariable(m_tjet_HadronConeExclExtendedTruthLabelID, "tjet_HadronConeExclExtendedTruthLabelID");
	}
      }
    }
  }

  ///-- saveEvent - run for every systematic and every event --///
  void CustomEventSaver::saveEvent(const top::Event& event)
  {
    m_IP2DNeg_ntrk.clear();
    m_IP2DNeg_pb.clear();
    m_IP2DNeg_pc.clear();
    m_IP2DNeg_pu.clear();
    m_IP2D_ntrk.clear();
    m_IP2D_pb.clear();
    m_IP2D_pc.clear();
    m_IP2D_pu.clear();
    m_IP3DNeg_ntrk.clear();
    m_IP3DNeg_pb.clear();
    m_IP3DNeg_pc.clear();
    m_IP3DNeg_pu.clear();
    m_IP3D_ntrk.clear();
    m_IP3D_pb.clear();
    m_IP3D_pc.clear();
    m_IP3D_pu.clear();
    m_JFFlip_N2Tpair.clear();
    m_JFFlip_dRFlightDir.clear();
    m_JFFlip_deltaeta.clear();
    m_JFFlip_deltaphi.clear();
    m_JFFlip_energyFraction.clear();
    m_JFFlip_mass.clear();
    m_JFFlip_nSingleTracks.clear();
    m_JFFlip_nTracksAtVtx.clear();
    m_JFFlip_nVTX.clear();
    m_JFFlip_ntrk.clear();
    m_JFFlip_significance3d.clear();
    m_JF_N2Tpair.clear();
    m_JF_dRFlightDir.clear();
    m_JF_deltaeta.clear();
    m_JF_deltaphi.clear();
    m_JF_energyFraction.clear();
    m_JF_mass.clear();
    m_JF_nSingleTracks.clear();
    m_JF_nTracksAtVtx.clear();
    m_JF_nVTX.clear();
    m_JF_ntrk.clear();
    m_JF_significance3d.clear();
    m_MV2c10Flip.clear();
    m_DL1Flip_pb.clear();
    m_DL1Flip_pc.clear();
    m_DL1Flip_pu.clear();
    m_SV1Flip_L3d.clear();
    m_SV1Flip_Lxy.clear();
    m_SV1Flip_N2Tpair.clear();
    m_SV1Flip_NGTinSvx.clear();
    m_SV1Flip_deltaR.clear();
    m_SV1Flip_efracsvx.clear();
    m_SV1Flip_masssvx.clear();
    m_SV1Flip_normdist.clear();
    m_SV1Flip_ntrk.clear();
    m_SV1_L3d.clear();
    m_SV1_Lxy.clear();
    m_SV1_N2Tpair.clear();
    m_SV1_NGTinSvx.clear();
    m_SV1_deltaR.clear();
    m_SV1_efracsvx.clear();
    m_SV1_masssvx.clear();
    m_SV1_normdist.clear();
    m_SV1_ntrk.clear();
    m_averageIntPerXing =0;
    m_nPV=0;
    m_HadronConeExclExtendedTruthLabelID.clear();
    m_L1Jet_eta.clear();
    m_L1Jet_phi.clear();
    m_L1Jet_et8x8.clear();
    m_HLT_SplitJet_pt.clear();
    m_HLT_SplitJet_et.clear();
    m_HLT_SplitJet_eta.clear();
    m_HLT_SplitJet_phi.clear();
    m_HLT_SplitJet_m.clear();
    m_HLT_SplitJet_mv2c00.clear();
    m_HLT_SplitJet_mv2c10.clear();
    m_HLT_SplitJet_mv2c20.clear();
    m_jet_onlinemv2c20_split.clear();
    m_jet_onlinemv2c10_split.clear();
    m_HLT_GSCJet_pt.clear();
    m_HLT_GSCJet_et.clear();
    m_HLT_GSCJet_eta.clear();
    m_HLT_GSCJet_phi.clear();
    m_HLT_GSCJet_m.clear();
    m_HLT_GSCJet_mv2c00.clear();
    m_HLT_GSCJet_mv2c10.clear();
    m_HLT_GSCJet_mv2c20.clear();
    m_jet_onlinemv2c20_gsc.clear();
    m_jet_onlinemv2c10_gsc.clear();
    m_SMT.clear();
    //track jets
    if (m_config->useTrackJets()) {
      m_tjet_numConstituents.clear();
      m_tjet_MV2r.clear();
      m_tjet_MV2rmu.clear();
      m_tjet_DL1_pu.clear();
      m_tjet_DL1_pb.clear();
      m_tjet_DL1_pc.clear();
      m_tjet_DL1r_pu.clear();
      m_tjet_DL1r_pb.clear();
      m_tjet_DL1r_pc.clear();
      m_tjet_DL1rmu_pu.clear();
      m_tjet_DL1rmu_pb.clear();
      m_tjet_DL1rmu_pc.clear();
    }
    /// serious stuff starts here
    // loop on the reco jets in the event
    float IP2DNeg_ntrk;
    float IP2DNeg_pb;
    float IP2DNeg_pc;
    float IP2DNeg_pu;
    float IP2D_ntrk;
    float IP2D_pb;
    float IP2D_pc;
    float IP2D_pu;
    float IP3DNeg_ntrk;
    float IP3DNeg_pb;
    float IP3DNeg_pc;
    float IP3DNeg_pu;
    float IP3D_ntrk;
    float IP3D_pb;
    float IP3D_pc;
    float IP3D_pu;
    float JFFlip_N2Tpair;
    float JFFlip_dRFlightDir;
    float JFFlip_deltaeta;
    float JFFlip_deltaphi;
    float JFFlip_energyFraction;
    float JFFlip_mass;
    float JFFlip_nSingleTracks;
    float JFFlip_nTracksAtVtx;
    float JFFlip_nVTX;
    float JFFlip_ntrk;
    float JFFlip_significance3d;
    float JF_N2Tpair;
    float JF_dRFlightDir;
    float JF_deltaeta;
    float JF_deltaphi;
    float JF_energyFraction;
    float JF_mass;
    float JF_nSingleTracks;
    float JF_nTracksAtVtx;
    float JF_nVTX;
    float JF_ntrk;
    float JF_significance3d;
    float MV2c10Flip;
    float DL1Flip_pb;
    float DL1Flip_pc;
    float DL1Flip_pu;
    float SV1Flip_L3d;
    float SV1Flip_Lxy;
    float SV1Flip_N2Tpair;
    float SV1Flip_NGTinSvx;
    float SV1Flip_deltaR;
    float SV1Flip_efracsvx;
    float SV1Flip_masssvx;
    float SV1Flip_normdist;
    float SV1Flip_ntrk;
    float SV1_L3d;
    float SV1_Lxy;
    float SV1_N2Tpair;
    float SV1_NGTinSvx;
    float SV1_deltaR;
    float SV1_efracsvx;
    float SV1_masssvx;
    float SV1_normdist;
    float SV1_ntrk;
    const xAOD::EventInfo* eventinfo = event.m_info;

    if ( (*m_eventClean)(*eventinfo)==0 )
      return;
    m_averageIntPerXing= eventinfo->averageInteractionsPerCrossing(); // averageIntPerXing
    const xAOD::VertexContainer* vtx = event.m_primaryVertices;
    m_nPV = vtx->size();
    int HadronConeExclExtendedTruthLabelID;
    //  std::cout<<"Number of Vertices: "<<m_nPV << " Average Int per Bunch:"<<m_averageIntPerXing<<std::endl;
    //  for (const auto* const vtx :m_vtx) { m_nPV++; }

    for (const auto* const jp : event.m_jets) {
      IP2DNeg_ntrk          = -999.;
      IP2DNeg_pb            = -999.;
      IP2DNeg_pc            = -999.;
      IP2DNeg_pu            = -999.;
      IP2D_ntrk             = -999.;
      IP2D_pb               = -999.;
      IP2D_pc               = -999.;
      IP2D_pu               = -999.;
      IP3DNeg_ntrk          = -999.;
      IP3DNeg_pb            = -999.;
      IP3DNeg_pc            = -999.;
      IP3DNeg_pu            = -999.;
      IP3D_ntrk             = -999.;
      IP3D_pb               = -999.;
      IP3D_pc               = -999.;
      IP3D_pu               = -999.;
      JFFlip_N2Tpair        = -999.;
      JFFlip_dRFlightDir    = -999.;
      JFFlip_deltaeta       = -999.;
      JFFlip_deltaphi       = -999.;
      JFFlip_energyFraction = -999.;
      JFFlip_mass           = -999.;
      JFFlip_nSingleTracks  = -999.;
      JFFlip_nTracksAtVtx   = -999.;
      JFFlip_nVTX           = -999.;
      JFFlip_ntrk           = -999.;
      JFFlip_significance3d = -999.;
      JF_N2Tpair            = -999.;
      JF_dRFlightDir        = -999.;
      JF_deltaeta           = -999.;
      JF_deltaphi           = -999.;
      JF_energyFraction     = -999.;
      JF_mass               = -999.;
      JF_nSingleTracks      = -999.;
      JF_nTracksAtVtx       = -999.;
      JF_nVTX               = -999.;
      JF_ntrk               = -999.;
      JF_significance3d     = -999.;
      MV2c10Flip            = -999.;
      DL1Flip_pb            = -999.;
      DL1Flip_pc            = -999.;
      DL1Flip_pu            = -999.;
      SV1Flip_L3d           = -999.;
      SV1Flip_Lxy           = -999.;
      SV1Flip_N2Tpair       = -999.;
      SV1Flip_NGTinSvx      = -999.;
      SV1Flip_deltaR        = -999.;
      SV1Flip_efracsvx      = -999.;
      SV1Flip_masssvx       = -999.;
      SV1Flip_normdist      = -999.;
      SV1Flip_ntrk          = -999.;
      SV1_L3d               = -999.;
      SV1_Lxy               = -999.;
      SV1_N2Tpair           = -999.;
      SV1_NGTinSvx          = -999.;
      SV1_deltaR            = -999.;
      SV1_efracsvx          = -999.;
      SV1_masssvx           = -999.;
      SV1_normdist          = -999.;
      SV1_ntrk              = -999.;
      HadronConeExclExtendedTruthLabelID = -999;


      // retrieve btagging info
      const xAOD::BTagging* btag = jp->btagging();

      if (m_IP2D_TPELVecACC          && m_IP2D_TPELVecACC          ->isAvailable(*btag) ) IP2D_ntrk    = static_cast<int>( ((*m_IP2D_TPELVecACC)(*btag)).size() );
      if (m_IP3D_TPELVecACC          && m_IP3D_TPELVecACC          ->isAvailable(*btag) ) IP3D_ntrk    = static_cast<int>( ((*m_IP3D_TPELVecACC)(*btag)).size() );
      if (m_IP2DNeg_TPELVecACC&& m_IP2DNeg_TPELVecACC->isAvailable(*btag) ) IP2DNeg_ntrk = static_cast<int>( ((*m_IP2DNeg_TPELVecACC)(*btag)).size() );
      if (m_IP3DNeg_TPELVecACC&& m_IP3DNeg_TPELVecACC->isAvailable(*btag) ) IP3DNeg_ntrk = static_cast<int>( ((*m_IP3DNeg_TPELVecACC)(*btag)).size() );
      if (m_JFFlip_TPELVecACC&& m_JFFlip_TPELVecACC->isAvailable(*btag) ) JFFlip_ntrk = static_cast<int>( ((*m_JFFlip_TPELVecACC)(*btag)).size() );
      if (m_JF_TPELVecACC&& m_JF_TPELVecACC->isAvailable(*btag) ) JF_ntrk = static_cast<int>( ((*m_JF_TPELVecACC)(*btag)).size() );
      if (m_SV1Flip_TPELVecACC&& m_SV1Flip_TPELVecACC->isAvailable(*btag) ) SV1Flip_ntrk = static_cast<int>( ((*m_SV1Flip_TPELVecACC)(*btag)).size() );
      if (m_SV1_TPELVecACC&& m_SV1_TPELVecACC->isAvailable(*btag) ) SV1_ntrk = static_cast<int>( ((*m_SV1_TPELVecACC)(*btag)).size() );



      if (m_IP2DNeg_pbACC            && m_IP2DNeg_pbACC            ->isAvailable(*btag) ) IP2DNeg_pb            = (*m_IP2DNeg_pbACC            )(*btag);
      if (m_IP2DNeg_pcACC            && m_IP2DNeg_pcACC            ->isAvailable(*btag) ) IP2DNeg_pc            = (*m_IP2DNeg_pcACC            )(*btag);
      if (m_IP2DNeg_puACC            && m_IP2DNeg_puACC            ->isAvailable(*btag) ) IP2DNeg_pu            = (*m_IP2DNeg_puACC            )(*btag);
      if (m_IP2D_pbACC               && m_IP2D_pbACC               ->isAvailable(*btag) ) IP2D_pb               = (*m_IP2D_pbACC               )(*btag);
      if (m_IP2D_pcACC               && m_IP2D_pcACC               ->isAvailable(*btag) ) IP2D_pc               = (*m_IP2D_pcACC               )(*btag);
      if (m_IP2D_puACC               && m_IP2D_puACC               ->isAvailable(*btag) ) IP2D_pu               = (*m_IP2D_puACC               )(*btag);
      if (m_IP3DNeg_pbACC            && m_IP3DNeg_pbACC            ->isAvailable(*btag) ) IP3DNeg_pb            = (*m_IP3DNeg_pbACC            )(*btag);
      if (m_IP3DNeg_pcACC            && m_IP3DNeg_pcACC            ->isAvailable(*btag) ) IP3DNeg_pc            = (*m_IP3DNeg_pcACC            )(*btag);
      if (m_IP3DNeg_puACC            && m_IP3DNeg_puACC            ->isAvailable(*btag) ) IP3DNeg_pu            = (*m_IP3DNeg_puACC            )(*btag);
      if (m_IP3D_pbACC               && m_IP3D_pbACC               ->isAvailable(*btag) ) IP3D_pb               = (*m_IP3D_pbACC               )(*btag);
      if (m_IP3D_pcACC               && m_IP3D_pcACC               ->isAvailable(*btag) ) IP3D_pc               = (*m_IP3D_pcACC               )(*btag);
      if (m_IP3D_puACC               && m_IP3D_puACC               ->isAvailable(*btag) ) IP3D_pu               = (*m_IP3D_puACC               )(*btag);
      if (m_JFFlip_N2TpairACC        && m_JFFlip_N2TpairACC        ->isAvailable(*btag) ) JFFlip_N2Tpair        = (*m_JFFlip_N2TpairACC        )(*btag);
      if (m_JFFlip_dRFlightDirACC    && m_JFFlip_dRFlightDirACC    ->isAvailable(*btag) ) JFFlip_dRFlightDir    = (*m_JFFlip_dRFlightDirACC    )(*btag);
      if (m_JFFlip_deltaetaACC       && m_JFFlip_deltaetaACC       ->isAvailable(*btag) ) JFFlip_deltaeta       = (*m_JFFlip_deltaetaACC       )(*btag);
      if (m_JFFlip_deltaphiACC       && m_JFFlip_deltaphiACC       ->isAvailable(*btag) ) JFFlip_deltaphi       = (*m_JFFlip_deltaphiACC       )(*btag);
      if (m_JFFlip_energyFractionACC && m_JFFlip_energyFractionACC ->isAvailable(*btag) ) JFFlip_energyFraction = (*m_JFFlip_energyFractionACC )(*btag);
      if (m_JFFlip_massACC           && m_JFFlip_massACC           ->isAvailable(*btag) ) JFFlip_mass           = (*m_JFFlip_massACC           )(*btag);
      if (m_JFFlip_nSingleTracksACC  && m_JFFlip_nSingleTracksACC  ->isAvailable(*btag) ) JFFlip_nSingleTracks  = (*m_JFFlip_nSingleTracksACC  )(*btag);
      if (m_JFFlip_nTracksAtVtxACC   && m_JFFlip_nTracksAtVtxACC   ->isAvailable(*btag) ) JFFlip_nTracksAtVtx   = (*m_JFFlip_nTracksAtVtxACC   )(*btag);
      if (m_JFFlip_nVTXACC           && m_JFFlip_nVTXACC           ->isAvailable(*btag) ) JFFlip_nVTX           = (*m_JFFlip_nVTXACC           )(*btag);
      if (m_JFFlip_significance3dACC && m_JFFlip_significance3dACC ->isAvailable(*btag) ) JFFlip_significance3d = (*m_JFFlip_significance3dACC )(*btag);
      if (m_JF_N2TpairACC            && m_JF_N2TpairACC            ->isAvailable(*btag) ) JF_N2Tpair            = (*m_JF_N2TpairACC            )(*btag);
      if (m_JF_dRFlightDirACC        && m_JF_dRFlightDirACC        ->isAvailable(*btag) ) JF_dRFlightDir        = (*m_JF_dRFlightDirACC        )(*btag);
      if (m_JF_deltaetaACC           && m_JF_deltaetaACC           ->isAvailable(*btag) ) JF_deltaeta           = (*m_JF_deltaetaACC           )(*btag);
      if (m_JF_deltaphiACC           && m_JF_deltaphiACC           ->isAvailable(*btag) ) JF_deltaphi           = (*m_JF_deltaphiACC           )(*btag);
      if (m_JF_energyFractionACC     && m_JF_energyFractionACC     ->isAvailable(*btag) ) JF_energyFraction     = (*m_JF_energyFractionACC     )(*btag);
      if (m_JF_massACC               && m_JF_massACC               ->isAvailable(*btag) ) JF_mass               = (*m_JF_massACC               )(*btag);
      if (m_JF_nSingleTracksACC      && m_JF_nSingleTracksACC      ->isAvailable(*btag) ) JF_nSingleTracks      = (*m_JF_nSingleTracksACC      )(*btag);
      if (m_JF_nTracksAtVtxACC       && m_JF_nTracksAtVtxACC       ->isAvailable(*btag) ) JF_nTracksAtVtx       = (*m_JF_nTracksAtVtxACC       )(*btag);
      if (m_JF_nVTXACC               && m_JF_nVTXACC               ->isAvailable(*btag) ) JF_nVTX               = (*m_JF_nVTXACC               )(*btag);
      if (m_JF_significance3dACC     && m_JF_significance3dACC     ->isAvailable(*btag) ) JF_significance3d     = (*m_JF_significance3dACC     )(*btag);
      if (m_MV2c10FlipACC            && m_MV2c10FlipACC            ->isAvailable(*btag) ) MV2c10Flip            = (*m_MV2c10FlipACC            )(*btag);
      if (m_DL1Flip_pbACC            && m_DL1Flip_pbACC            ->isAvailable(*btag) ) DL1Flip_pb            = (*m_DL1Flip_pbACC            )(*btag);
      if (m_DL1Flip_pcACC            && m_DL1Flip_pcACC            ->isAvailable(*btag) ) DL1Flip_pc            = (*m_DL1Flip_pcACC            )(*btag);
      if (m_DL1Flip_puACC            && m_DL1Flip_puACC            ->isAvailable(*btag) ) DL1Flip_pu            = (*m_DL1Flip_puACC            )(*btag);
      if (m_SV1Flip_L3dACC           && m_SV1Flip_L3dACC           ->isAvailable(*btag) ) SV1Flip_L3d           = (*m_SV1Flip_L3dACC           )(*btag);
      if (m_SV1Flip_LxyACC           && m_SV1Flip_LxyACC           ->isAvailable(*btag) ) SV1Flip_Lxy           = (*m_SV1Flip_LxyACC           )(*btag);
      if (m_SV1Flip_N2TpairACC       && m_SV1Flip_N2TpairACC       ->isAvailable(*btag) ) SV1Flip_N2Tpair       = (*m_SV1Flip_N2TpairACC       )(*btag);
      if (m_SV1Flip_NGTinSvxACC      && m_SV1Flip_NGTinSvxACC      ->isAvailable(*btag) ) SV1Flip_NGTinSvx      = (*m_SV1Flip_NGTinSvxACC      )(*btag);
      if (m_SV1Flip_deltaRACC        && m_SV1Flip_deltaRACC        ->isAvailable(*btag) ) SV1Flip_deltaR        = (*m_SV1Flip_deltaRACC        )(*btag);
      if (m_SV1Flip_efracsvxACC      && m_SV1Flip_efracsvxACC      ->isAvailable(*btag) ) SV1Flip_efracsvx      = (*m_SV1Flip_efracsvxACC      )(*btag);
      if (m_SV1Flip_masssvxACC       && m_SV1Flip_masssvxACC       ->isAvailable(*btag) ) SV1Flip_masssvx       = (*m_SV1Flip_masssvxACC       )(*btag);
      if (m_SV1Flip_normdistACC      && m_SV1Flip_normdistACC      ->isAvailable(*btag) ) SV1Flip_normdist      = (*m_SV1Flip_normdistACC      )(*btag);
      if (m_SV1_L3dACC               && m_SV1_L3dACC               ->isAvailable(*btag) ) SV1_L3d               = (*m_SV1_L3dACC               )(*btag);
      if (m_SV1_LxyACC               && m_SV1_LxyACC               ->isAvailable(*btag) ) SV1_Lxy               = (*m_SV1_LxyACC               )(*btag);
      if (m_SV1_N2TpairACC           && m_SV1_N2TpairACC           ->isAvailable(*btag) ) SV1_N2Tpair           = (*m_SV1_N2TpairACC           )(*btag);
      if (m_SV1_NGTinSvxACC          && m_SV1_NGTinSvxACC          ->isAvailable(*btag) ) SV1_NGTinSvx          = (*m_SV1_NGTinSvxACC          )(*btag);
      if (m_SV1_deltaRACC            && m_SV1_deltaRACC            ->isAvailable(*btag) ) SV1_deltaR            = (*m_SV1_deltaRACC            )(*btag);
      if (m_SV1_efracsvxACC          && m_SV1_efracsvxACC          ->isAvailable(*btag) ) SV1_efracsvx          = (*m_SV1_efracsvxACC          )(*btag);
      if (m_SV1_masssvxACC           && m_SV1_masssvxACC           ->isAvailable(*btag) ) SV1_masssvx           = (*m_SV1_masssvxACC           )(*btag);
      if (m_SV1_normdistACC          && m_SV1_normdistACC          ->isAvailable(*btag) ) SV1_normdist          = (*m_SV1_normdistACC          )(*btag);
      if (m_HadronConeExclExtendedTruthLabelIDACC && jp->isAvailable<int>("HadronConeExclExtendedTruthLabelID")) jp->getAttribute("HadronConeExclExtendedTruthLabelID", HadronConeExclExtendedTruthLabelID);


      m_SMT.push_back(jp->btagging()->isAvailable<double>("SMT_discriminant") ?  jp->btagging()->auxdata<double>("SMT_discriminant") : -999);
      m_IP2DNeg_ntrk.push_back(IP2DNeg_ntrk);
      m_IP2DNeg_pb.push_back(IP2DNeg_pb);
      m_IP2DNeg_pc.push_back(IP2DNeg_pc);
      m_IP2DNeg_pu.push_back(IP2DNeg_pu);
      m_IP2D_ntrk.push_back(IP2D_ntrk);
      m_IP2D_pb.push_back(IP2D_pb);
      m_IP2D_pc.push_back(IP2D_pc);
      m_IP2D_pu.push_back(IP2D_pu);
      m_IP3DNeg_ntrk.push_back(IP3DNeg_ntrk);
      m_IP3DNeg_pb.push_back(IP3DNeg_pb);
      m_IP3DNeg_pc.push_back(IP3DNeg_pc);
      m_IP3DNeg_pu.push_back(IP3DNeg_pu);
      m_IP3D_ntrk.push_back(IP3D_ntrk);
      m_IP3D_pb.push_back(IP3D_pb);
      m_IP3D_pc.push_back(IP3D_pc);
      m_IP3D_pu.push_back(IP3D_pu);
      m_JFFlip_N2Tpair.push_back(JFFlip_N2Tpair);
      m_JFFlip_dRFlightDir.push_back(JFFlip_dRFlightDir);
      m_JFFlip_deltaeta.push_back(JFFlip_deltaeta);
      m_JFFlip_deltaphi.push_back(JFFlip_deltaphi);
      m_JFFlip_energyFraction.push_back(JFFlip_energyFraction);
      m_JFFlip_mass.push_back(JFFlip_mass);
      m_JFFlip_nSingleTracks.push_back(JFFlip_nSingleTracks);
      m_JFFlip_nTracksAtVtx.push_back(JFFlip_nTracksAtVtx);
      m_JFFlip_nVTX.push_back(JFFlip_nVTX);
      m_JFFlip_ntrk.push_back(JFFlip_ntrk);
      m_JFFlip_significance3d.push_back(JFFlip_significance3d);
      m_JF_N2Tpair.push_back(JF_N2Tpair);
      m_JF_dRFlightDir.push_back(JF_dRFlightDir);
      m_JF_deltaeta.push_back(JF_deltaeta);
      m_JF_deltaphi.push_back(JF_deltaphi);
      m_JF_energyFraction.push_back(JF_energyFraction);
      m_JF_mass.push_back(JF_mass);
      m_JF_nSingleTracks.push_back(JF_nSingleTracks);
      m_JF_nTracksAtVtx.push_back(JF_nTracksAtVtx);
      m_JF_nVTX.push_back(JF_nVTX);
      m_JF_ntrk.push_back(JF_ntrk);
      m_JF_significance3d.push_back(JF_significance3d);
      m_MV2c10Flip.push_back(MV2c10Flip);
      m_DL1Flip_pb.push_back(DL1Flip_pb);
      m_DL1Flip_pc.push_back(DL1Flip_pc);
      m_DL1Flip_pu.push_back(DL1Flip_pu);
      m_SV1Flip_L3d.push_back(SV1Flip_L3d);
      m_SV1Flip_Lxy.push_back(SV1Flip_Lxy);
      m_SV1Flip_N2Tpair.push_back(SV1Flip_N2Tpair);
      m_SV1Flip_NGTinSvx.push_back(SV1Flip_NGTinSvx);
      m_SV1Flip_deltaR.push_back(SV1Flip_deltaR);
      m_SV1Flip_efracsvx.push_back(SV1Flip_efracsvx);
      m_SV1Flip_masssvx.push_back(SV1Flip_masssvx);
      m_SV1Flip_normdist.push_back(SV1Flip_normdist);
      m_SV1Flip_ntrk.push_back(SV1Flip_ntrk);
      m_SV1_L3d.push_back(SV1_L3d);
      m_SV1_Lxy.push_back(SV1_Lxy);
      m_SV1_N2Tpair.push_back(SV1_N2Tpair);
      m_SV1_NGTinSvx.push_back(SV1_NGTinSvx);
      m_SV1_deltaR.push_back(SV1_deltaR);
      m_SV1_efracsvx.push_back(SV1_efracsvx);
      m_SV1_masssvx.push_back(SV1_masssvx);
      m_SV1_normdist.push_back(SV1_normdist);
      m_SV1_ntrk.push_back(SV1_ntrk);
      m_HadronConeExclExtendedTruthLabelID.push_back(HadronConeExclExtendedTruthLabelID);

    }


    //==== RETRIEVE TRIGGER LEVEL VARIABLES ====
	
    std::string m_TriggerChain;

    //Trigger Decisions
    for (unsigned int itrig = 0; itrig < m_SplitJetTriggerList.size(); itrig++) {
      m_TriggerChain = m_SplitJetTriggerList[itrig];
      auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
      if (cg->isPassed()) m_JetTriggerDecisions[itrig]=1;
      else m_JetTriggerDecisions[itrig]=0;
    }
	
    //LV1Jets
    top::check(evtStore()->retrieve(m_L1Jets, "LVL1JetRoIs"),"Failed to retrieve container LVL1JetRoIs");
    for(auto jptr : *m_L1Jets){
      m_L1Jet_eta.push_back(jptr->eta());
      m_L1Jet_phi.push_back(jptr->phi());
      m_L1Jet_et8x8.push_back(jptr->et8x8());
    }

    //TRIGGER NAVIGATION

    m_TriggerChain = "";

    //SplitJets
    for (unsigned int isplittrig = 0; isplittrig < m_SplitJetTriggerList.size(); isplittrig++) {
      m_TriggerChain = m_SplitJetTriggerList[isplittrig];
      auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
      if (cg->isPassed()) break;
    }
    /*
    std::vector<TLorentzVector> SplitJetVectors;

    Trig::FeatureContainer fcsplit = m_TrigDecisionTool->features(m_TriggerChain);
    Trig::FeatureContainer::combination_const_iterator comb_split   (fcsplit.getCombinations().begin());
    Trig::FeatureContainer::combination_const_iterator combEnd_split(fcsplit.getCombinations().end());

    for( ; comb_split!=combEnd_split ; ++comb_split) {
      std::vector< Trig::Feature<xAOD::JetContainer> >  jetCollections  = comb_split->containerFeature<xAOD::JetContainer>("SplitJet");
      std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb_split->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");

      if(jetCollections.size() > bjetCollections.size()){
	std::cout << "ERROR Problem in container size SplitJet  -->  jets: "<< jetCollections.size() << " bjets: "<< bjetCollections.size() <<"\n";
	continue;
      }

      for ( unsigned ifeat=0 ; ifeat<jetCollections.size() ; ifeat++ ) {
	const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(ifeat));
	if(!hlt_jet) continue;
	const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(ifeat));
	if(!hlt_btag) continue;

	TLorentzVector v1; v1.SetPtEtaPhiM(hlt_jet->pt(), hlt_jet->eta(), hlt_jet->phi(), hlt_jet->m());
	bool notsaved = true;
	for (auto vsaved : SplitJetVectors) if (vsaved.DeltaR(v1) == 0) notsaved = false;

	if (notsaved) {

	  double MV2c00_mvx = -999, MV2c10_mvx = -999, MV2c20_mvx = -999;
	  hlt_btag->MVx_discriminant("MV2c00" ,MV2c00_mvx);
	  hlt_btag->MVx_discriminant("MV2c10" ,MV2c10_mvx);
	  hlt_btag->MVx_discriminant("MV2c20" ,MV2c20_mvx);

	  m_HLT_SplitJet_pt.push_back(hlt_jet->pt());
	  m_HLT_SplitJet_eta.push_back(hlt_jet->eta());
	  m_HLT_SplitJet_phi.push_back(hlt_jet->phi());
	  m_HLT_SplitJet_m.push_back(hlt_jet->m());
	  m_HLT_SplitJet_et.push_back(v1.Et());
	  m_HLT_SplitJet_mv2c00.push_back(MV2c00_mvx);
	  m_HLT_SplitJet_mv2c10.push_back(MV2c10_mvx);
	  m_HLT_SplitJet_mv2c20.push_back(MV2c20_mvx);

	  SplitJetVectors.push_back(v1);
	}

      }
    }


    std::vector<uint> matchedsplitjets;
    for (const auto* const jp : event.m_jets) {
      TLorentzVector vjet;
      vjet.SetPtEtaPhiE(jp->pt(), jp->eta(), jp->phi(), jp->e());
      float online_mv2c20 = -2;
      float online_mv2c10 = -2;
      for (uint itrigjet = 0; itrigjet < m_HLT_SplitJet_pt.size(); itrigjet++) {
	bool isalreadymatched = false;
	for (auto imtj : matchedsplitjets) if (itrigjet == imtj) isalreadymatched = true;
	if (isalreadymatched) continue;
	TLorentzVector vtrigjet;
	vtrigjet.SetPtEtaPhiM(m_HLT_SplitJet_pt.at(itrigjet), m_HLT_SplitJet_eta.at(itrigjet), m_HLT_SplitJet_phi.at(itrigjet), m_HLT_SplitJet_m.at(itrigjet));
	if (vjet.DeltaR(vtrigjet) < 0.2) {
	  online_mv2c20=m_HLT_SplitJet_mv2c20.at(itrigjet);
	  online_mv2c10=m_HLT_SplitJet_mv2c10.at(itrigjet);
	  matchedsplitjets.push_back(itrigjet);
	  break;
	}
      }
      m_jet_onlinemv2c20_split.push_back(online_mv2c20);
      m_jet_onlinemv2c10_split.push_back(online_mv2c10);
    }

    m_TriggerChain = "";

    //GSCJets
    for (unsigned int igsctrig = 0; igsctrig < m_GSCJetTriggerList.size(); igsctrig++) {
      m_TriggerChain = m_GSCJetTriggerList[igsctrig];
      auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
      if (cg->isPassed()) break;
    }

    std::vector<TLorentzVector> GSCJetVectors;

    Trig::FeatureContainer fcgsc = m_TrigDecisionTool->features(m_TriggerChain);
    Trig::FeatureContainer::combination_const_iterator comb_gsc   (fcgsc.getCombinations().begin());
    Trig::FeatureContainer::combination_const_iterator combEnd_gsc(fcgsc.getCombinations().end());

    for( ; comb_gsc!=combEnd_gsc ; ++comb_gsc) {
      std::vector< Trig::Feature<xAOD::JetContainer> >  jetCollections  = comb_gsc->containerFeature<xAOD::JetContainer>("GSCJet");
      std::vector< Trig::Feature<xAOD::BTaggingContainer> > bjetCollections = comb_gsc->containerFeature<xAOD::BTaggingContainer>("HLTBjetFex");

      if(jetCollections.size() > bjetCollections.size()){
	std::cout << "ERROR Problem in container size GSCJet  -->  jets: "<< jetCollections.size() << " bjets: "<< bjetCollections.size() <<"\n";
	continue;
      }

      for ( unsigned ifeat=0 ; ifeat<jetCollections.size() ; ifeat++ ) {
	const xAOD::Jet* hlt_jet = getTrigObject<xAOD::Jet, xAOD::JetContainer>(jetCollections.at(ifeat));
	if(!hlt_jet) continue;
	const xAOD::BTagging* hlt_btag = getTrigObject<xAOD::BTagging, xAOD::BTaggingContainer>(bjetCollections.at(ifeat));
	if(!hlt_btag) continue;

	TLorentzVector v1; v1.SetPtEtaPhiM(hlt_jet->pt(), hlt_jet->eta(), hlt_jet->phi(), hlt_jet->m());
	bool notsaved = true;
	for (auto vsaved : GSCJetVectors) if (vsaved.DeltaR(v1) == 0) notsaved = false;

	if (notsaved) {

	  double MV2c00_mvx = -999, MV2c10_mvx = -999, MV2c20_mvx = -999;
	  hlt_btag->MVx_discriminant("MV2c00" ,MV2c00_mvx);
	  hlt_btag->MVx_discriminant("MV2c10" ,MV2c10_mvx);
	  hlt_btag->MVx_discriminant("MV2c20" ,MV2c20_mvx);

	  m_HLT_GSCJet_pt.push_back(hlt_jet->pt());
	  m_HLT_GSCJet_eta.push_back(hlt_jet->eta());
	  m_HLT_GSCJet_phi.push_back(hlt_jet->phi());
	  m_HLT_GSCJet_m.push_back(hlt_jet->m());
	  m_HLT_GSCJet_et.push_back(v1.Et());
	  m_HLT_GSCJet_mv2c00.push_back(MV2c00_mvx);
	  m_HLT_GSCJet_mv2c10.push_back(MV2c10_mvx);
	  m_HLT_GSCJet_mv2c20.push_back(MV2c20_mvx);

	  GSCJetVectors.push_back(v1);
	}

      }
    }


    std::vector<uint> matchedgscjets;
    for (const auto* const jp : event.m_jets) {
      TLorentzVector vjet;
      vjet.SetPtEtaPhiE(jp->pt(), jp->eta(), jp->phi(), jp->e());
      float online_mv2c20 = -2;
      float online_mv2c10 = -2;
      for (uint itrigjet = 0; itrigjet < m_HLT_GSCJet_pt.size(); itrigjet++) {
	bool isalreadymatched = false;
	for (auto imtj : matchedgscjets) if (itrigjet == imtj) isalreadymatched = true;
	if (isalreadymatched) continue;
	TLorentzVector vtrigjet;
	vtrigjet.SetPtEtaPhiM(m_HLT_GSCJet_pt.at(itrigjet), m_HLT_GSCJet_eta.at(itrigjet), m_HLT_GSCJet_phi.at(itrigjet), m_HLT_GSCJet_m.at(itrigjet));
	if (vjet.DeltaR(vtrigjet) < 0.2) {
	  online_mv2c20=m_HLT_GSCJet_mv2c20.at(itrigjet);
	  online_mv2c10=m_HLT_GSCJet_mv2c10.at(itrigjet);
	  matchedgscjets.push_back(itrigjet);
	  break;
	}
      }
      m_jet_onlinemv2c20_gsc.push_back(online_mv2c20);
      m_jet_onlinemv2c10_gsc.push_back(online_mv2c10);
    }
    */
    //track jets
    if (m_config->useTrackJets()) {

      unsigned int i(0);
	
      //resize
      m_tjet_numConstituents.resize(event.m_trackJets.size());
      m_tjet_MV2r.resize(event.m_trackJets.size());
      m_tjet_MV2rmu.resize(event.m_trackJets.size());
      m_tjet_DL1_pu.resize(event.m_trackJets.size());
      m_tjet_DL1_pb.resize(event.m_trackJets.size());
      m_tjet_DL1_pc.resize(event.m_trackJets.size());
      m_tjet_DL1r_pu.resize(event.m_trackJets.size());
      m_tjet_DL1r_pb.resize(event.m_trackJets.size());
      m_tjet_DL1r_pc.resize(event.m_trackJets.size());
      m_tjet_DL1rmu_pu.resize(event.m_trackJets.size());
      m_tjet_DL1rmu_pb.resize(event.m_trackJets.size());
      m_tjet_DL1rmu_pc.resize(event.m_trackJets.size());
      m_tjet_Ghosts_BHadron_Final_Count.resize(event.m_trackJets.size());
      m_tjet_Ghosts_CHadron_Final_Count.resize(event.m_trackJets.size());
      m_tjet_Ghosts_Taus_Final_Count.resize(event.m_trackJets.size());
      m_tjet_HadronConeExclTruthLabelID.resize(event.m_trackJets.size());
      m_tjet_HadronConeExclExtendedTruthLabelID.resize(event.m_trackJets.size());

      for (const auto* const jp : event.m_trackJets) {

	m_tjet_numConstituents[i]=jp->numConstituents();

	m_tjet_DL1_pu[i] = -999.;
	m_tjet_DL1_pb[i] = -999.;
	m_tjet_DL1_pc[i] = -999.;
	m_tjet_DL1r_pu[i] = -999.;
	m_tjet_DL1r_pb[i] = -999.;
	m_tjet_DL1r_pc[i] = -999.;
	m_tjet_DL1rmu_pu[i] = -999.;
	m_tjet_DL1rmu_pb[i] = -999.;
	m_tjet_DL1rmu_pc[i] = -999.;
	  
	const xAOD::BTagging* btag(nullptr);
	btag = jp->btagging();
	  
	double mvx = -999;
	if (btag) btag->MVx_discriminant("MV2r", mvx);
	m_tjet_MV2r[i] = mvx;
	mvx = -999;
	if (btag) btag->MVx_discriminant("MV2rmu", mvx);
	m_tjet_MV2rmu[i] = mvx;
	  
	double tjet_pu, tjet_pc, tjet_pb = -999;
	double tjet_pu_r, tjet_pc_r, tjet_pb_r = -999;
	double tjet_pu_rmu, tjet_pc_rmu, tjet_pb_rmu = -999;
	if (btag) {
	  btag->pu("DL1",tjet_pu);
	  btag->pb("DL1",tjet_pb);
	  btag->pc("DL1",tjet_pc);
	  btag->pu("DL1r",tjet_pu_r);
	  btag->pb("DL1r",tjet_pb_r);
	  btag->pc("DL1r",tjet_pc_r);
	  btag->pu("DL1rmu",tjet_pu_rmu);
	  btag->pb("DL1rmu",tjet_pb_rmu);
	  btag->pc("DL1rmu",tjet_pc_rmu);
	  m_tjet_DL1_pu[i] = tjet_pu;
	  m_tjet_DL1_pb[i] = tjet_pb;
	  m_tjet_DL1_pc[i] = tjet_pc;
	  m_tjet_DL1r_pu[i] = tjet_pu_r;
	  m_tjet_DL1r_pb[i] = tjet_pb_r;
	  m_tjet_DL1r_pc[i] = tjet_pc_r;
	  m_tjet_DL1rmu_pu[i] = tjet_pu_rmu;
	  m_tjet_DL1rmu_pb[i] = tjet_pb_rmu;
	  m_tjet_DL1rmu_pc[i] = tjet_pc_rmu;
	}

	if (m_config->isMC()) {

	  if(jp->isAvailable<int>("HadronConeExclTruthLabelID")){
	    jp->getAttribute("HadronConeExclTruthLabelID", m_tjet_HadronConeExclTruthLabelID[i]);
	  }

	  if(jp->isAvailable<int>("HadronConeExclExtendedTruthLabelID")){
	    jp->getAttribute("HadronConeExclExtendedTruthLabelID", m_tjet_HadronConeExclExtendedTruthLabelID[i]);
	  }

	  //truth b-hadrons

	  m_tjet_Ghosts_BHadron_Final_Count[i] = jp->auxdata<int>( "GhostBHadronsFinalCount" );
	  m_tjet_Ghosts_CHadron_Final_Count[i] = jp->auxdata<int>( "GhostCHadronsFinalCount" );
	  m_tjet_Ghosts_Taus_Final_Count[i] = jp->auxdata<int>( "GhostTausFinalCount" );

	  std::vector<const IParticle*> tjet_bhadrons;
	  const std::string labelB = "ConeExclBHadronsFinal";
	  jp->getAssociatedObjects<IParticle>(labelB, tjet_bhadrons);

	  m_tjet_BHadron_eta.resize(event.m_trackJets.size(), std::vector<float>());
	  m_tjet_BHadron_pt.resize(event.m_trackJets.size(), std::vector<float>());
	  m_tjet_BHadron_phi.resize(event.m_trackJets.size(), std::vector<float>());
	  m_tjet_BHadron_e.resize(event.m_trackJets.size(), std::vector<float>());
	  m_tjet_BHadron_eta[i].clear();
	  m_tjet_BHadron_phi[i].clear();
	  m_tjet_BHadron_pt[i].clear();
	  m_tjet_BHadron_e[i].clear();

	  for (const auto* const ip : tjet_bhadrons) {
	    const xAOD::TruthParticle * bhadron=(const xAOD::TruthParticle*)(ip);
	    m_tjet_BHadron_eta[i].push_back(bhadron->eta());
	    m_tjet_BHadron_phi[i].push_back(bhadron->phi());
	    m_tjet_BHadron_pt[i].push_back(bhadron->pt());
	    m_tjet_BHadron_e[i].push_back(bhadron->e());
	  }
	}

	std::vector<const IParticle*> tjet_tracks;
	const std::string labelTracks = "constituentLinks";
	jp->getAssociatedObjects<IParticle>(labelTracks, tjet_tracks);
	m_tjet_track_eta.resize(event.m_trackJets.size(), std::vector<float>());
	m_tjet_track_pt.resize(event.m_trackJets.size(), std::vector<float>());
	m_tjet_track_phi.resize(event.m_trackJets.size(), std::vector<float>());
	m_tjet_track_e.resize(event.m_trackJets.size(), std::vector<float>());
	//clear vector
	m_tjet_track_eta[i].clear();
	m_tjet_track_pt[i].clear();
	m_tjet_track_phi[i].clear();
	m_tjet_track_e[i].clear();
	unsigned int trkj(0);
	for (const auto* const ip_trk : tjet_tracks) {
	  const xAOD::TrackParticle * trk=(const xAOD::TrackParticle*)(ip_trk);
	  m_tjet_track_eta[i].push_back(trk->eta());
	  m_tjet_track_phi[i].push_back(trk->phi());
	  m_tjet_track_pt[i].push_back(trk->pt());
	  m_tjet_track_e[i].push_back(trk->e());
	  ++trkj;
	}
	++i;
      }
    }
    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  }

  CustomEventSaver::~CustomEventSaver()
  {
    delete m_IP2D_TPELVecACC;
    delete m_IP3D_TPELVecACC;
    delete m_IP2DNeg_TPELVecACC;
    delete m_IP3DNeg_TPELVecACC;
    delete m_SV1_TPELVecACC;
    delete m_SV1Flip_TPELVecACC;
    delete m_JF_TPELVecACC;
    delete m_JFFlip_TPELVecACC;


    delete m_IP2DNeg_pbACC;
    delete m_IP2DNeg_pcACC;
    delete m_IP2DNeg_puACC;
    delete m_IP2D_pbACC;
    delete m_IP2D_pcACC;
    delete m_IP2D_puACC;
    delete m_IP3DNeg_pbACC;
    delete m_IP3DNeg_pcACC;
    delete m_IP3DNeg_puACC;
    delete m_IP3D_pbACC;
    delete m_IP3D_pcACC;
    delete m_IP3D_puACC;
    delete m_JFFlip_N2TpairACC;
    delete m_JFFlip_dRFlightDirACC;
    delete m_JFFlip_deltaetaACC;
    delete m_JFFlip_deltaphiACC;
    delete m_JFFlip_energyFractionACC;
    delete m_JFFlip_massACC;
    delete m_JFFlip_nSingleTracksACC;
    delete m_JFFlip_nTracksAtVtxACC;
    delete m_JFFlip_nVTXACC;
    delete m_JFFlip_significance3dACC;
    delete m_JF_N2TpairACC;
    delete m_JF_dRFlightDirACC;
    delete m_JF_deltaetaACC;
    delete m_JF_deltaphiACC;
    delete m_JF_energyFractionACC;
    delete m_JF_massACC;
    delete m_JF_nSingleTracksACC;
    delete m_JF_nTracksAtVtxACC;
    delete m_JF_nVTXACC;
    delete m_JF_significance3dACC;
    delete m_MV2c10FlipACC;
    delete m_DL1Flip_pbACC;
    delete m_DL1Flip_pcACC;
    delete m_DL1Flip_puACC;
    delete m_SV1Flip_L3dACC;
    delete m_SV1Flip_LxyACC;
    delete m_SV1Flip_N2TpairACC;
    delete m_SV1Flip_NGTinSvxACC;
    delete m_SV1Flip_deltaRACC;
    delete m_SV1Flip_efracsvxACC;
    delete m_SV1Flip_masssvxACC;
    delete m_SV1Flip_normdistACC;
    delete m_SV1_L3dACC;
    delete m_SV1_LxyACC;
    delete m_SV1_N2TpairACC;
    delete m_SV1_NGTinSvxACC;
    delete m_SV1_deltaRACC;
    delete m_SV1_efracsvxACC;
    delete m_SV1_masssvxACC;
    delete m_SV1_normdistACC;
    delete m_HadronConeExclExtendedTruthLabelIDACC;

    //delete m_TrigConfigTool;
    //delete m_TrigDecisionTool;

  }
}
