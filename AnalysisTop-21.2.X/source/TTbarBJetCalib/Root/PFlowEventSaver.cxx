#include "TTbarBJetCalib/PFlowEventSaver.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <iostream>

using xAOD::IParticle;

namespace top {
  ///-- Constrcutor --///
  PFlowEventSaver::PFlowEventSaver() :
    m_config(nullptr),
    m_nPV                 (0.),
    m_averageIntPerXing   (0.),
    m_saveTriggerInfo     (false),
    m_saveLowTaggers      (false),
    m_saveHighTaggers     (false),
    m_HadronConeExclExtendedTruthLabelID	(),
    m_HadronConeExclExtendedTruthLabelIDACC	(nullptr),
    m_passTightCleaning()
  {
    m_lowTaggerManager = new top::LowTaggerManager();
    m_highTaggerManager = new top::HighTaggerManager();
    m_triggerManager = new top::TriggerManager();
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void PFlowEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///

    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    m_config = config;
    m_HadronConeExclExtendedTruthLabelIDACC	= new SG::AuxElement::Accessor<int>	("HadronConeExclExtendedTruthLabelID");
    m_eventClean               = new SG::AuxElement::Accessor<char>  ("DFCommonJets_eventClean_LooseBad");


    m_configSettings = top::ConfigurationSettings::get();
    //    this->readConfigSettings();

    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {

      if( m_saveLowTaggers)
	m_lowTaggerManager->DeclareBranches(systematicTree.get());
      if( m_saveHighTaggers)
	m_highTaggerManager->DeclareBranches(systematicTree.get());
      if (m_saveTriggerInfo)
	m_triggerManager->DeclareBranches(systematicTree.get());

      systematicTree->makeOutputVariable(m_passTightCleaning    , "PassTightCleaning");
      systematicTree->makeOutputVariable(m_averageIntPerXing    , "averageIntPerXing");
      systematicTree->makeOutputVariable(m_nPV                  , "nPV");
      systematicTree->makeOutputVariable(m_HadronConeExclExtendedTruthLabelID	, "HadronConeExclExtendedTruthLabelID");

    }
  }

  void PFlowEventSaver::readConfigSettings()
  {

    // m_configSettings->registerParameter("PFlowSaveTriggerInfo",
    // 					"Option of PFlowEventSaver to enable/disable the storage of trigger information",
    // 					"False");
    // m_configSettings->registerParameter("PFlowSaveLowTaggers",
    //                                     "Option of PFlowEventSaver to enable/disable the storage of detailed information on low-level taggers",
    //                                     "False");
    // m_configSettings->registerParameter("PFlowSaveHighTaggers",
    //                                     "Option of PFlowEventSaver to enable/disable the storage of detailed information on high-level taggers",
    //                                     "False");
    /// Trigger information
    m_saveTriggerInfo = m_configSettings->value("PFlowSaveTriggerInfo") == "True" || m_configSettings->value("PFlowSaveTriggerInfo") == "1"? true : false;

    if(m_saveTriggerInfo)
      ATH_MSG_INFO(" INFO  ::  PFlowEventSaver  ::  Trigger information  is saved.");
    else
      ATH_MSG_INFO(" INFO  ::  PFlowEventSaver  ::  Trigger information  is not saved.");

    /// Low Taggers
    m_saveLowTaggers  = m_configSettings->value("PFlowSaveLowTaggers")  == "True" || m_configSettings->value("PFlowSaveLowTaggers")  == "1"? true : false;
    if(m_saveLowTaggers)
      ATH_MSG_INFO(" INFO  ::  PFlowEventSaver  ::  Detailed information  on low-level taggers is saved.");
    else
      ATH_MSG_INFO(" INFO  ::  PFlowEventSaver  ::  Detailed information  on low-level taggers is not saved.");

    /// High Taggers
    m_saveHighTaggers = m_configSettings->value("PFlowSaveHighTaggers") == "True" || m_configSettings->value("PFlowSaveHighTaggers") == "1"? true : false;
    if(m_saveHighTaggers)
      ATH_MSG_INFO(" INFO  ::  PFlowEventSaver  ::  Detailed information  on high-level taggers is saved.");
    else
      ATH_MSG_INFO(" INFO  ::  PFlowEventSaver  ::  Detailed information  on high-level taggers is not saved.");


  }

  ///-- saveEvent - run for every systematic and every event --///
  void PFlowEventSaver::saveEvent(const top::Event& event)
  {

    // std :: cout << " *************************************************" << std::endl; 
    // std :: cout << " Here my event saver starts " << std::endl;
    // std :: cout << " *************************************************" << std::endl; 
    m_averageIntPerXing =0;
    m_nPV=0;
    m_HadronConeExclExtendedTruthLabelID.clear();
    m_passTightCleaning.clear();

    const xAOD::EventInfo* eventinfo = event.m_info;
    if ( (*m_eventClean)(*eventinfo)==0 )
      return;
    m_averageIntPerXing= eventinfo->averageInteractionsPerCrossing(); // averageIntPerXing
    const xAOD::VertexContainer* vtx = event.m_primaryVertices;
    m_nPV = vtx->size();
    int HadronConeExclExtendedTruthLabelID;

    for (const auto* const jp : event.m_jets) {
      HadronConeExclExtendedTruthLabelID = -999;
      // retrieve btagging info
      const xAOD::BTagging* btag = jp->btagging();
      if (m_HadronConeExclExtendedTruthLabelIDACC && jp->isAvailable<int>("HadronConeExclExtendedTruthLabelID"))
	jp->getAttribute("HadronConeExclExtendedTruthLabelID", HadronConeExclExtendedTruthLabelID);
      m_HadronConeExclExtendedTruthLabelID.push_back(HadronConeExclExtendedTruthLabelID);
      
      // Checking that the jet is passing tight cleaning
      double chf= jp->auxdata<std::vector<float> >("SumPtChargedPFOPt500").at(0) / jp->jetP4(xAOD::JetConstitScaleMomentum).Pt();
      double fmax=0;
      if( !jp->getAttribute(xAOD::JetAttribute::FracSamplingMax,fmax) ){
	std::cout << "Missing fmax for tight cleaning" << std::endl;
	fmax=0;
      };
      
      if (fmax<DBL_MIN)
	m_passTightCleaning.push_back(0);
      else if(std::fabs(jp->eta())<2.4 && chf/fmax<0.1) 
	m_passTightCleaning.push_back(0);
      else
	m_passTightCleaning.push_back(1);
      


      // if( jp->isAvailable< std::vector<ElementLink<xAOD::TrackParticleContainer>> >("BTagTrackToJetAssociator") ){
      // 	auto trackLinks=jp->auxdata< std::vector<ElementLink<xAOD::TrackParticleContainer>> >("BTagTrackToJetAssociator");

      // 	std::vector<const xAOD::TrackParticle*> selectedTracks; 
      // 	for (unsigned int iT = 0; iT < trackLinks.size(); iT++) { 
      // 	  // std::cout << " .... trk link: " << iT << std::endl;
      // 	  if (!trackLinks.at(iT).isValid())
      // 	    continue;
      // 	  uint8_t getInt(0);
      // 	  const xAOD::TrackParticle *tmpTrk = *(trackLinks.at(iT));
      // 	  tmpTrk->summaryValue(getInt, xAOD::numberOfPixelHits);
      // 	  int nSi = getInt;
      // 	  tmpTrk->summaryValue(getInt, xAOD::numberOfSCTHits);
      // 	  nSi += getInt;
      // 	  if (nSi < 0)
      // 	    continue;
      // 	  selectedTracks.push_back(tmpTrk);
      // 	}

      // 	//	std::cout << " Number of tracks for b-tagging : " << selectedTracks.size() << std::endl;
      // }else{

      //       std::cout << " I cannot access the BTagTrackToJetAssociator. Jet Eta is :  " << jp->eta() << std::endl;

      // }
      // // double DL1_pu,DL1_pc,DL1_pb;
      // jp->btagging()->pu("DL1",DL1_pu);
      // jp->btagging()->pc("DL1",DL1_pc);
      // jp->btagging()->pb("DL1",DL1_pb);
      //      std::cout << "Jet pt : " << jp->pt()/1000 <<  " | jet Eta : " << jp->eta() << " | DL1_pb : " << DL1_pb << " | DL1_pc : " << DL1_pc<<" | DL1_pu : " << DL1_pu <<  std::endl;
    }


    // Save the ntruthjets as a branch for Z+jets theory uncertainties.
    // const xAOD::JetContainer* truthjets = nullptr;
    // if( top::check(evtStore()->retrieve(truthjets,"AntiKt4TruthDressedWZJets")) ,"AntiKt4TruthDressedWZJets is not available in the derivations." );


    // Save detailed information about IP, SV1 and JetFitter 
    if( m_saveLowTaggers)
      m_lowTaggerManager->Fill(event);
    // Save detailed information about the DL1,DL1r,DL1rmu,MV2,MV2r,MV2rmu
    if( m_saveHighTaggers)
      m_highTaggerManager->Fill(event);
    // Save trigger information if needed
    if (m_saveTriggerInfo)
      m_triggerManager->Fill(event,evtStore()->event());

    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  }

  PFlowEventSaver::~PFlowEventSaver()
  {

    delete m_HadronConeExclExtendedTruthLabelIDACC;

    //delete m_TrigConfigTool;
    //delete m_TrigDecisionTool;

  }
}
