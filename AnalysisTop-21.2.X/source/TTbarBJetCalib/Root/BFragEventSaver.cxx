#include "TTbarBJetCalib/BFragEventSaver.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopParticleLevel/ParticleLevelEvent.h"
#include "TopParticleLevel/TruthTools.h"
#include <TRandom3.h>

#include <iostream>
#include "TruthUtils/PIDHelpers.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"
#include <TRandom3.h>
#include <iostream>

using namespace xAOD;
using namespace std;


//typedef vector<const TruthParticle*> tpset;
// // Unnamed namespace for helpers: only visible to this compilation unit 
// namespace {   inline bool isBHadron(const xAOD::TruthParticle* tp) {
//   if (!tp) {
//     cout << "isBHadron: missing truth particle!" << endl;
//     return false;
//   }
//   return MC::PID::isBottomHadron(tp->pdgId());
// }
//   inline bool isFinalB(const xAOD::TruthParticle* tp) {
//     if (!isBHadron(tp)) return false;
//     if (!tp->hasDecayVtx()) return false;
//     for (unsigned int i = 0; i < tp->decayVtx()->nOutgoingParticles(); ++i) {
//       if (isBHadron(tp->decayVtx()->outgoingParticle(i)))
// 	return false;
//     }
//     return true;
//   }
// } 


namespace top {
  ///-- Constrcutor --///
  BFragEventSaver::BFragEventSaver() :
    m_jet_pv_track_origin()
    , m_jet_fcharge()
    , m_SMT()
    , passTightCleaning()
    , jet_passTightCleaning()
    , m_jet_fTile0()
    , m_jet_fEM3()
    , m_jet_Ntrk()
    , m_jet_Wtrk()
    , m_jet_NmuSeg()
    , m_jet_EMFrac()
    , m_jet_charge()
    , m_jet_pv_track_pt()
    , m_jet_pv_track_eta()
    , m_jet_pv_track_phi()
    , m_jet_pv_track_e()
    , m_jet_pv_track_d0()
    , m_jet_pv_track_z0()
    , m_jet_pv_track_d0sig()
    , m_jet_pv_track_z0sintheta()
    , m_jet_pfo_track_pt()
    , m_jet_pfo_track_eta()
    , m_jet_pfo_track_phi()
    , m_jet_pfo_track_e()
    , m_jet_pfo_track_d0()
    , m_jet_pfo_track_z0()
    , m_jet_pfo_track_d0sig()
    , m_jet_pfo_track_z0sintheta()
    , m_jet_pfo_track_isSV1()
    , m_jet_pfo_track_isPV()
    , m_jet_sv1_track_origin()
    , m_jet_sv1_track_pt()
    , m_jet_sv1_track_eta()
    , m_jet_sv1_track_phi()
    , m_jet_sv1_track_e()
    , m_jet_sv1_track_d0()
    , m_jet_sv1_track_z0()
    , m_jet_sv1_track_d0sig()
    , m_jet_sv1_track_z0sintheta()
    , m_tracktool("TrackSelection"),
    m_nPV                   (0.),
    m_averageIntPerXing     (0.),
    m_HadronConeExclExtendedTruthLabelID()
    , m_jet_TruthLabelDeltaR_T()
    , m_jet_TruthLabelDeltaR_B()
    , m_jet_TruthLabelDeltaR_C()
    , m_HadronConeExclTruthLabelID()

  {
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void BFragEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {



    m_tracktool.setProperty("CutLevel", "TightPrimary").isSuccess();
    m_tracktool.initialize().isSuccess();
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    //------------------------
    // SV1 variables
    //------------------------
    // --> Nominal

	
    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      systematicTree->makeOutputVariable(m_averageIntPerXing    , "averageIntPerXing");
      systematicTree->makeOutputVariable(m_nPV                  , "nPV");
      systematicTree->makeOutputVariable(m_HadronConeExclExtendedTruthLabelID	, "HadronConeExclExtendedTruthLabelID");

      systematicTree->makeOutputVariable(m_jet_pv_track_pt, "jet_pv_track_pt");
      systematicTree->makeOutputVariable(m_jet_pv_track_eta, "jet_pv_track_eta");
      systematicTree->makeOutputVariable(m_jet_pv_track_phi, "jet_pv_track_phi");
      systematicTree->makeOutputVariable(m_jet_pv_track_e, "jet_pv_track_e");
      systematicTree->makeOutputVariable(m_jet_pv_track_d0, "jet_pv_track_d0");
      systematicTree->makeOutputVariable(m_jet_pv_track_z0, "jet_pv_track_z0");
      systematicTree->makeOutputVariable(m_jet_pv_track_d0sig, "jet_pv_track_d0sig");
      systematicTree->makeOutputVariable(m_jet_pv_track_z0sintheta, "jet_pv_track_z0sintheta");

      // systematicTree->makeOutputVariable(m_jet_pfo_track_pt, "jet_pfo_track_pt");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_eta, "jet_pfo_track_eta");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_phi, "jet_pfo_track_phi");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_e, "jet_pfo_track_e");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_d0, "jet_pfo_track_d0");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_z0, "jet_pfo_track_z0");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_d0sig, "jet_pfo_track_d0sig");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_z0sintheta, "jet_pfo_track_z0sintheta");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_isSV1, "jet_pfo_track_isSV1");
      // systematicTree->makeOutputVariable(m_jet_pfo_track_isPV, "jet_pfo_track_isPV");

      systematicTree->makeOutputVariable(m_jet_sv1_track_pt,"jet_sv1_track_pt");
      systematicTree->makeOutputVariable(m_jet_sv1_track_eta,"jet_sv1_track_eta");
      systematicTree->makeOutputVariable(m_jet_sv1_track_phi,"jet_sv1_track_phi");
      systematicTree->makeOutputVariable(m_jet_sv1_track_e,"jet_sv1_track_e");
      systematicTree->makeOutputVariable(m_jet_sv1_track_d0,"jet_sv1_track_d0");
      systematicTree->makeOutputVariable(m_jet_sv1_track_z0,"jet_sv1_track_z0");
      systematicTree->makeOutputVariable(m_jet_sv1_track_d0sig,"jet_sv1_track_d0sig");
      systematicTree->makeOutputVariable(m_jet_sv1_track_z0sintheta,"jet_sv1_track_z0sintheta");

      systematicTree->makeOutputVariable(passTightCleaning,"event_passTightCleaning");
      systematicTree->makeOutputVariable(jet_passTightCleaning,"jet_passTightCleaning");
      systematicTree->makeOutputVariable(m_SMT,"jets_SMT");


      systematicTree->makeOutputVariable(m_jet_fcharge,"jet_fcharge");
      systematicTree->makeOutputVariable(m_jet_fTile0,"jet_fTile0");
      systematicTree->makeOutputVariable(m_jet_fEM3,"jet_fEM3");
      systematicTree->makeOutputVariable(m_jet_Ntrk,"jet_ntrk");
      systematicTree->makeOutputVariable(m_jet_Wtrk,"jet_wtrk");
      systematicTree->makeOutputVariable(m_jet_NmuSeg,"jet_NmuonSegment");
      systematicTree->makeOutputVariable(m_jet_charge,"jet_charge");
      systematicTree->makeOutputVariable(m_jet_EMFrac,"jet_EMFrac");

    }

    m_eventClean               = new SG::AuxElement::Accessor<char>  ("DFCommonJets_eventClean_LooseBad");
    
    if ( topConfig()->doTopParticleLevel() )
      setupParticleLevelTreeManager();
  }

  ///-- saveEvent - run for every systematic and every event --///
  void BFragEventSaver::saveEvent(const top::Event& event)
  {

    unsigned int njet = event.m_jets.size();

    m_SMT.clear();
    m_jet_fcharge.clear();
    m_jet_fTile0.clear();
    m_jet_fEM3.clear();
    m_jet_Ntrk.clear();
    m_jet_Wtrk.clear();
    m_jet_NmuSeg.clear();
    m_jet_charge.clear();
    m_jet_EMFrac.clear();

    m_jet_pv_track_pt.resize(njet, vector<float>());
    m_jet_pv_track_eta.resize(njet, vector<float>());
    m_jet_pv_track_phi.resize(njet, vector<float>());
    m_jet_pv_track_e.resize(njet, vector<float>());
    m_jet_pv_track_d0.resize(njet, vector<float>());
    m_jet_pv_track_z0.resize(njet, vector<float>());
    m_jet_pv_track_d0sig.resize(njet, vector<float>());
    m_jet_pv_track_z0sintheta.resize(njet, vector<float>());

    m_jet_pfo_track_pt.resize(njet, vector<float>());
    m_jet_pfo_track_eta.resize(njet, vector<float>());
    m_jet_pfo_track_phi.resize(njet, vector<float>());
    m_jet_pfo_track_e.resize(njet, vector<float>());
    m_jet_pfo_track_d0.resize(njet, vector<float>());
    m_jet_pfo_track_z0.resize(njet, vector<float>());
    m_jet_pfo_track_d0sig.resize(njet, vector<float>());
    m_jet_pfo_track_z0sintheta.resize(njet, vector<float>());
    m_jet_pfo_track_isSV1.resize(njet, vector<int>());
    m_jet_pfo_track_isPV.resize(njet, vector<int>());

    m_jet_sv1_track_pt.resize(njet, vector<float>());
    m_jet_sv1_track_eta.resize(njet, vector<float>());
    m_jet_sv1_track_phi.resize(njet, vector<float>());
    m_jet_sv1_track_e.resize(njet, vector<float>());
    m_jet_sv1_track_d0.resize(njet, vector<float>());
    m_jet_sv1_track_z0.resize(njet, vector<float>());
    m_jet_sv1_track_d0sig.resize(njet, vector<float>());
    m_jet_sv1_track_z0sintheta.resize(njet, vector<float>());
    m_averageIntPerXing =0;
    m_nPV=0;
    m_HadronConeExclExtendedTruthLabelID.clear();
    const xAOD::EventInfo* eventinfo = event.m_info;
    m_averageIntPerXing= eventinfo->averageInteractionsPerCrossing(); // averageIntPerXing
    const xAOD::VertexContainer* vtx = event.m_primaryVertices;
    m_nPV = vtx->size();
    int HadronConeExclExtendedTruthLabelID;
    passTightCleaning=1;
    jet_passTightCleaning.clear();


    if ( (*m_eventClean)(*eventinfo)==0 )
      return;

    unsigned int ij = 0;
  
    for (const auto* const jp : event.m_jets) {
      HadronConeExclExtendedTruthLabelID = -999;
      // retrieve btagging info
      const xAOD::BTagging* btag = jp->btagging();
      // Getting truth information
      if ( jp->isAvailable<int>("HadronConeExclExtendedTruthLabelID") ) jp->getAttribute("HadronConeExclExtendedTruthLabelID", HadronConeExclExtendedTruthLabelID);
      m_HadronConeExclExtendedTruthLabelID.push_back(HadronConeExclExtendedTruthLabelID);



      // Saving GSC variables.
      std::vector<float> ePerSampling=jp->auxdata<std::vector<float> >("EnergyPerSampling");
      m_jet_fcharge.push_back(jp->auxdata<std::vector<float> >("SumPtChargedPFOPt500").at(0) / jp->jetP4(xAOD::JetConstitScaleMomentum).Pt());
      m_jet_fTile0.push_back((ePerSampling.at(12)+ePerSampling.at(18))/jp->jetP4(xAOD::JetConstitScaleMomentum).E()); //layers 12+18 / total
      m_jet_fEM3.push_back((ePerSampling.at(3)+ePerSampling.at(7))/jp->jetP4(xAOD::JetConstitScaleMomentum).E()); // layers 3+7 / total
      m_jet_Ntrk.push_back(jp->auxdata<std::vector<int> >("NumTrkPt1000").at(0));
      m_jet_Wtrk.push_back(jp->auxdata<std::vector<float> >("TrackWidthPt1000").at(0));
      m_jet_NmuSeg.push_back(jp->auxdata<int>("GhostMuonSegmentCount"));
      m_jet_charge.push_back(jp->auxdata<float >("Charge"));
      m_jet_EMFrac.push_back(jp->auxdata<float >("EMFrac"));

      m_SMT.push_back(jp->btagging()->isAvailable<double>("SMT_discriminant") ?  jp->btagging()->auxdata<double>("SMT_discriminant") : -999);
      

      double chf= jp->auxdata<std::vector<float> >("SumPtChargedPFOPt500").at(0) / jp->jetP4(xAOD::JetConstitScaleMomentum).Pt();
      double fmax=0;
      if( !jp->getAttribute(xAOD::JetAttribute::FracSamplingMax,fmax) ){
	std::cout << "Missing fmax for tight cleaning" << std::endl;
	fmax=0;
      };
      
      if (fmax<DBL_MIN)
	jet_passTightCleaning.push_back(0);
      else if(std::fabs(jp->eta())<2.4 && chf/fmax<0.1) 
	jet_passTightCleaning.push_back(0);
      else{
	jet_passTightCleaning.push_back(1);
      }

      //Getting track particles 
      const Vertex* vertexPtr = NULL;
      for (const auto* const vtxPtr : *event.m_primaryVertices) {
	const VxType::VertexType vtype=vtxPtr->vertexType();
	const int vmult=vtxPtr->trackParticleLinks().size();
	// count vertices of type 1 (primary) and 3 (pileup) with >= 5 tracks
	if ((vtype==1) && vmult >= 5) {
	  vertexPtr = vtxPtr;
	  break;
	}
      }


      const vector< ElementLink<TrackParticleContainer> >& SV1_trackParticleLinks = btag->SV1_TrackParticleLinks();

      unsigned int nsv1trks = SV1_trackParticleLinks.size();

      m_jet_sv1_track_pt[ij].clear();
      m_jet_sv1_track_eta[ij].clear();
      m_jet_sv1_track_phi[ij].clear();
      m_jet_sv1_track_e[ij].clear();
      m_jet_sv1_track_d0[ij].clear();
      m_jet_sv1_track_z0[ij].clear();
      m_jet_sv1_track_d0sig[ij].clear();
      m_jet_sv1_track_z0sintheta[ij].clear();

      m_jet_sv1_track_pt[ij].resize(nsv1trks);
      m_jet_sv1_track_eta[ij].resize(nsv1trks);
      m_jet_sv1_track_phi[ij].resize(nsv1trks);
      m_jet_sv1_track_e[ij].resize(nsv1trks);
      m_jet_sv1_track_d0[ij].resize(nsv1trks);
      m_jet_sv1_track_z0[ij].resize(nsv1trks);
      m_jet_sv1_track_d0sig[ij].resize(nsv1trks);
      m_jet_sv1_track_z0sintheta[ij].resize(nsv1trks);


      vector<const TrackParticle*> sv1trks;

      unsigned int itp = 0;
      for (const auto& el : SV1_trackParticleLinks) {
	if (!el.isValid()) {
	  ATH_MSG_ERROR("error: couldn't get an SV1 track");
	  cout << "jet pt: " << jp->pt() << endl;
	  cout << topConfig()->systematicName(event.m_hashValue) << endl;
	  exit(-1);
	}

	const TrackParticle* tp = *el;

	float d0sig = xAOD::TrackingHelpers::d0significance( tp, eventinfo->beamPosSigmaX(), eventinfo->beamPosSigmaY(), eventinfo->beamPosSigmaXY() );
	float delta_z0 = -999;
	float dzsintheta = -999;
	if(vertexPtr != NULL ){
	  delta_z0 = fabs(tp->z0() + tp->vz() - vertexPtr->z());
	  dzsintheta = delta_z0*sin(tp->theta());
	}

	m_jet_sv1_track_pt[ij][itp] = tp->pt();
	m_jet_sv1_track_eta[ij][itp] = tp->eta();
	m_jet_sv1_track_phi[ij][itp] = tp->phi();
	m_jet_sv1_track_e[ij][itp] = tp->e();
	m_jet_sv1_track_d0[ij][itp] = tp->d0();
	m_jet_sv1_track_z0[ij][itp] = tp->z0();
	m_jet_sv1_track_d0sig[ij][itp] = d0sig;
	m_jet_sv1_track_z0sintheta[ij][itp] = dzsintheta;

	sv1trks.push_back(tp);

	itp++;
      }

      const vector< ElementLink<IParticleContainer> >& trackParticleLinks = jp->getAttribute<vector<ElementLink<IParticleContainer> > >("GhostTrack");

      m_jet_pv_track_pt[ij].clear();
      m_jet_pv_track_eta[ij].clear();
      m_jet_pv_track_phi[ij].clear();
      m_jet_pv_track_e[ij].clear();
      m_jet_pv_track_d0[ij].clear();
      m_jet_pv_track_z0[ij].clear();
      m_jet_pv_track_d0sig[ij].clear();
      m_jet_pv_track_z0sintheta[ij].clear();


      itp = 0;

      std::vector<const xAOD::TrackParticle*> pvtracks;
      for (const auto& el : trackParticleLinks) {
	if (!el.isValid()) {
	  cout << "missing track particle link!!" << endl;
	  cout << "jet pt: " << jp->pt() << endl;
	  cout << topConfig()->systematicName(event.m_hashValue) << endl;
	  continue;
	}

	const TrackParticle* tp = (TrackParticle*) *el;

	// reject non-tight PV tracks
	if (!m_tracktool.accept(*tp, vertexPtr))
	  continue;

	// remove tracks that are in the sv1 collection
	if (find(sv1trks.begin(), sv1trks.end(), tp) != sv1trks.end())
	  continue;

	float d0sig = xAOD::TrackingHelpers::d0significance( tp, eventinfo->beamPosSigmaX(), eventinfo->beamPosSigmaY(), eventinfo->beamPosSigmaXY() );
	float delta_z0 = -999;
	float dzsintheta = -999;
	if(vertexPtr != NULL ){
	  delta_z0 = fabs(tp->z0() + tp->vz() - vertexPtr->z());
	  dzsintheta = delta_z0*sin(tp->theta());
	}
	m_jet_pv_track_pt[ij].push_back(tp->pt());
	m_jet_pv_track_eta[ij].push_back(tp->eta());
	m_jet_pv_track_phi[ij].push_back(tp->phi());
	m_jet_pv_track_e[ij].push_back(tp->e());
	m_jet_pv_track_d0[ij].push_back(tp->d0());
	m_jet_pv_track_z0[ij].push_back(tp->z0());
	m_jet_pv_track_d0sig[ij].push_back(d0sig);
	m_jet_pv_track_z0sintheta[ij].push_back(dzsintheta);


	pvtracks.push_back(tp);

      }


      /* ################################################
	 #    Particle Flow part accesing tracks        #
	 #  Commented out until a derivation contains   #
	 #   neutral and charged PFlow components       #
	 ################################################
      
      JetConstituentVector pflowConsts = jp->getConstituents();
      
      m_jet_pfo_track_pt[ij].clear();
      m_jet_pfo_track_eta[ij].clear();
      m_jet_pfo_track_phi[ij].clear();
      m_jet_pfo_track_e[ij].clear();
      m_jet_pfo_track_d0[ij].clear();
      m_jet_pfo_track_z0[ij].clear();
      m_jet_pfo_track_d0sig[ij].clear();
      m_jet_pfo_track_z0sintheta[ij].clear();
      m_jet_pfo_track_isSV1[ij].clear();
      m_jet_pfo_track_isPV[ij].clear();

      std::cout << "Constituent  size : " << pflowConsts.size()  << std::endl;
      for (const auto& constit : pflowConsts ) {
	if (!constit || constit == nullptr )
	  continue;

	std::cout << "Constituent : " << constit->type() << std::endl;
	const xAOD::PFO* pfo = (xAOD::PFO*) constit->rawConstituent();


	std::vector<const xAOD::IParticle*> tracks;
	if( !pfo->associatedParticles(PFODetails::PFOParticleType::Track,tracks)) 
	  continue;
	for (const xAOD::IParticle* part : tracks){
	  const xAOD::TrackParticle* tp = (xAOD::TrackParticle*) part;
	  if(tp == nullptr )
	    continue;

	  float d0sig = xAOD::TrackingHelpers::d0significance( tp, eventinfo->beamPosSigmaX(), eventinfo->beamPosSigmaY(), eventinfo->beamPosSigmaXY() );
	  float delta_z0 = -999;
	  float dzsintheta = -999;
	  if(vertexPtr != NULL ){
	    delta_z0 = fabs(tp->z0() + tp->vz() - vertexPtr->z());
	    dzsintheta = delta_z0*sin(tp->theta());
	  }
	  // if( fabs(pfo->charge()) > 0.1 )
	  //   tp = pfo->track();
	  m_jet_pfo_track_pt[ij].push_back(tp->pt());
	  m_jet_pfo_track_eta[ij].push_back(tp->eta());
	  m_jet_pfo_track_phi[ij].push_back(tp->phi());
	  m_jet_pfo_track_e[ij].push_back(tp->e());
	  m_jet_pfo_track_d0[ij].push_back(tp->d0());
	  m_jet_pfo_track_z0[ij].push_back(tp->z0());
	  m_jet_pfo_track_d0sig[ij].push_back(d0sig);
	  m_jet_pfo_track_z0sintheta[ij].push_back(dzsintheta);

	  if (find(sv1trks.begin(), sv1trks.end(), tp) != sv1trks.end()){
	    m_jet_pfo_track_isSV1[ij].push_back(1);
	  }else{
	    m_jet_pfo_track_isSV1[ij].push_back(0);
	    if (find(pvtracks.begin(), pvtracks.end(), tp) != pvtracks.end())
	      m_jet_pfo_track_isPV[ij].push_back(1);
	    else
	      m_jet_pfo_track_isPV[ij].push_back(0);
	  }    



	}
      }


      */
      ij++;
    }


    for ( int i : jet_passTightCleaning){
      if(i == 0 )
	passTightCleaning=0;  
    }





  
    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  }

  BFragEventSaver::~BFragEventSaver()
  {

  }

void BFragEventSaver::setupParticleLevelTreeManager(){

  std::cout <<"HEY I'M settingUpParticleLevelTreeManager!" <<std::endl;

  if ( topConfig()->doTopParticleLevel() ){

    if ( not particleLevelTreeManager() )
      top::EventSaverFlatNtuple::setupParticleLevelTreeManager();

    //particleLevelTreeManager()->makeOutputVariable(m_averageIntPerXing    , "averageIntPerXing");
    //particleLevelTreeManager()->makeOutputVariable(m_nPV                  , "nPV");
    particleLevelTreeManager()->makeOutputVariable(m_HadronConeExclExtendedTruthLabelID   , "HadronConeExclExtendedTruthLabelID");

    particleLevelTreeManager()->makeOutputVariable(m_HadronConeExclTruthLabelID,"jet_truthflav");
    particleLevelTreeManager()->makeOutputVariable(m_jet_TruthLabelDeltaR_B,"jet_TruthLabelDeltaR_B");
    particleLevelTreeManager()->makeOutputVariable(m_jet_TruthLabelDeltaR_C,"jet_TruthLabelDeltaR_C");
    particleLevelTreeManager()->makeOutputVariable(m_jet_TruthLabelDeltaR_T,"jet_TruthLabelDeltaR_T");

    //particleLevelTreeManager()->makeOutputVariable(m_jet_isTrueHardScat,"jet_isTrueHS");

  }
}

void BFragEventSaver::saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent) {

  //    std::cout <<"HEY I'M HEREEEEEE" <<std::endl;

  if ( not particleLevelTreeManager())
    setupParticleLevelTreeManager();

  ///-- set our variables to zero --///
  unsigned int njet = plEvent.m_jets->size();

  m_jet_TruthLabelDeltaR_T.clear();
  m_jet_TruthLabelDeltaR_B.clear();
  m_jet_TruthLabelDeltaR_C.clear();

  m_averageIntPerXing = 0;
  m_nPV=0;
  m_HadronConeExclExtendedTruthLabelID.clear();
  m_HadronConeExclTruthLabelID.clear();
  //    m_jet_isTrueHardScat.clear();

  ///-- Fill them - you should probably do something more complicated --///
  int HadronConeExclExtendedTruthLabelID;
  int HadronConeExclTruthLabelID;

  for (auto jp : *plEvent.m_jets){
    HadronConeExclExtendedTruthLabelID = -999;
    // Getting truth information
    if ( jp->isAvailable<int>("HadronConeExclExtendedTruthLabelID") )
      jp->getAttribute("HadronConeExclExtendedTruthLabelID", HadronConeExclExtendedTruthLabelID);
    else
      std::cout <<"HadronConeExclExtendedTruthLabelID is not availale!" <<std::endl;

    m_HadronConeExclExtendedTruthLabelID.push_back(HadronConeExclExtendedTruthLabelID);

    if ( jp->isAvailable<int>("HadronConeExclTruthLabelID") )
      jp->getAttribute("HadronConeExclTruthLabelID", HadronConeExclTruthLabelID);
    else
      std::cout <<"HadronConeExclTruthLabelID is not availale!" <<std::endl;
    m_HadronConeExclTruthLabelID.push_back(HadronConeExclTruthLabelID);

    if(jp->isAvailable<float>("TruthLabelDeltaR_B") ){
      m_jet_TruthLabelDeltaR_T.push_back(jp->auxdata<float>("TruthLabelDeltaR_T"));
      m_jet_TruthLabelDeltaR_B.push_back(jp->auxdata<float>("TruthLabelDeltaR_B"));
      m_jet_TruthLabelDeltaR_C.push_back(jp->auxdata<float>("TruthLabelDeltaR_C"));
    }
    else{
      std::cout <<"TruthLabelDeltaR_B is not availale!" <<std::endl;
    }

    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveParticleLevelEvent(plEvent);
  }
}

}
