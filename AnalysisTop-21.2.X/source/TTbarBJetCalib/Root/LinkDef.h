#include "TTbarBJetCalib/CustomEventSaver.h"
#include "TTbarBJetCalib/PFlowEventSaver.h"
#include "TTbarBJetCalib/ObjectManager.h"
#include "TTbarBJetCalib/BFragEventSaver.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//for loading the object selection at run time
#pragma link C++ class top::CustomEventSaver+;
#pragma link C++ class top::PFlowEventSaver+;
#pragma link C++ class top::ObjectManager+;
#pragma link C++ class top::TriggerManager+;
#pragma link C++ class top::LowTaggerManager+;
#pragma link C++ class top::HighTaggerManager+;
#pragma link C++ class top::BFragEventSaver+;

#endif
