#ifndef TTBARBJETCALIB_CUSTOMEVENTSAVER_H
#define TTBARBJETCALIB_CUSTOMEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"


/**
 * This class shows you how to extend the flat ntuple to include your own variables
 *
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work
 *
 */

namespace top {
class CustomEventSaver : public top::EventSaverFlatNtuple {
public:
///-- Default constrcutor with no arguments - needed for ROOT --///
CustomEventSaver();
///-- Destructor does nothing --///
virtual ~CustomEventSaver();

///-- initialize function for top::EventSaverFlatNtuple --///
///-- We will be setting up out custom variables here --///
virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;

///-- Keep the asg::AsgTool happy --///
virtual StatusCode initialize(){
        return StatusCode::SUCCESS;
}

///-- saveEvent function for top::EventSaverFlatNtuple --///
///-- We will be setting our custom variables on a per-event basis --///
virtual void saveEvent(const top::Event& event) override;

private:
///-- Some additional custom variables for the output --///

///---All the output vectors
//std::vector<xAOD::BTagging::TPELVec_t> * m_IP3D_TPELVec;
float m_averageIntPerXing;
int   m_nPV;

///Configuration
std::shared_ptr<top::TopConfig> m_config;

std::vector<int>     m_IP2D_ntrk;
std::vector<double>  m_IP2D_pb;
std::vector<double>  m_IP2D_pc;
std::vector<double>  m_IP2D_pu;


std::vector<double>  m_IP3D_pb;
std::vector<double>  m_IP3D_pc;
std::vector<double>  m_IP3D_pu;
std::vector<int>     m_IP3D_ntrk;

//std::vector<xAOD::BTagging::TPELVec_t> * m_IP2DNeg_TPELVec;
//std::vector<xAOD::BTagging::TPELVec_t> * m_IP3DNeg_TPELVec;
std::vector<double>  m_IP2DNeg_pb;
std::vector<double>  m_IP2DNeg_pc;
std::vector<double>  m_IP2DNeg_pu;
std::vector<int>     m_IP2DNeg_ntrk;

std::vector<double>  m_IP3DNeg_pb;
std::vector<double>  m_IP3DNeg_pc;
std::vector<double>  m_IP3DNeg_pu;
std::vector<int>     m_IP3DNeg_ntrk;

std::vector<float>  m_SMT;


//std::vector<xAOD::BTagging::TPELVec_t> * m_SV1_TPELVec;                                                                                                                                                   
 std::vector<float>  m_SV1_masssvx;
 std::vector<float>  m_SV1_efracsvx;
 std::vector<int>    m_SV1_N2Tpair;
 std::vector<int>    m_SV1_NGTinSvx;
 std::vector<float>  m_SV1_normdist;
 std::vector<float>  m_SV1_deltaR;
 std::vector<float>  m_SV1_Lxy;
 std::vector<float>  m_SV1_L3d;
 std::vector<int>    m_SV1_ntrk;

//std::vector<xAOD::BTagging::TPELVec_t> * m_SV1Flip_TPELVec;
std::vector<float>  m_SV1Flip_masssvx;
std::vector<float>  m_SV1Flip_efracsvx;
std::vector<int>    m_SV1Flip_N2Tpair;
std::vector<int>    m_SV1Flip_NGTinSvx;
std::vector<float>  m_SV1Flip_normdist;
std::vector<float>  m_SV1Flip_deltaR;
std::vector<float>  m_SV1Flip_Lxy;
std::vector<float>  m_SV1Flip_L3d;
std::vector<int>    m_SV1Flip_ntrk;

//std::vector<xAOD::BTagging::TPELVec_t> * m_JF_TPELVec;
std::vector<int>    m_JF_nVTX;
std::vector<int>    m_JF_nSingleTracks;
std::vector<int>    m_JF_nTracksAtVtx;
std::vector<int>    m_JF_N2Tpair;
std::vector<float>  m_JF_energyFraction;
std::vector<float>  m_JF_mass;
std::vector<float>  m_JF_significance3d;
std::vector<float>  m_JF_deltaphi;
std::vector<float>  m_JF_deltaeta;
std::vector<float>  m_JF_dRFlightDir;
std::vector<int>    m_JF_ntrk;

//std::vector<xAOD::BTagging::TPELVec_t> * m_JFFlip_TPELVec;
std::vector<int>     m_JFFlip_nVTX;
std::vector<int>     m_JFFlip_nSingleTracks;
std::vector<int>     m_JFFlip_nTracksAtVtx;
std::vector<int>     m_JFFlip_N2Tpair;
std::vector<float>   m_JFFlip_energyFraction;
std::vector<float>   m_JFFlip_mass;
std::vector<float>   m_JFFlip_significance3d;
std::vector<float>   m_JFFlip_deltaphi;
std::vector<float>   m_JFFlip_deltaeta;
std::vector<float>   m_JFFlip_dRFlightDir;
std::vector<int>     m_JFFlip_ntrk;

std::vector<double>  m_MV2c10Flip;
std::vector<double>  m_DL1Flip_pb;
std::vector<double>  m_DL1Flip_pc;
std::vector<double>  m_DL1Flip_pu;
std::vector<int>     m_HadronConeExclExtendedTruthLabelID;

///---All the Accesors

SG::AuxElement::Accessor<char>* m_eventClean;
SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_IP2D_TPELVecACC;
SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_IP3D_TPELVecACC;
SG::AuxElement::Accessor<double> * m_IP2D_pbACC;
SG::AuxElement::Accessor<double> * m_IP2D_pcACC;
SG::AuxElement::Accessor<double> * m_IP2D_puACC;
SG::AuxElement::Accessor<double> * m_IP3D_pbACC;
SG::AuxElement::Accessor<double> * m_IP3D_pcACC;
SG::AuxElement::Accessor<double> * m_IP3D_puACC;

SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_IP2DNeg_TPELVecACC;
SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_IP3DNeg_TPELVecACC;
SG::AuxElement::Accessor<double> * m_IP2DNeg_pbACC;
SG::AuxElement::Accessor<double> * m_IP2DNeg_pcACC;
SG::AuxElement::Accessor<double> * m_IP2DNeg_puACC;
SG::AuxElement::Accessor<double> * m_IP3DNeg_pbACC;
SG::AuxElement::Accessor<double> * m_IP3DNeg_pcACC;
SG::AuxElement::Accessor<double> * m_IP3DNeg_puACC;

SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_SV1_TPELVecACC;
SG::AuxElement::Accessor<float>  * m_SV1_masssvxACC;
SG::AuxElement::Accessor<float>  * m_SV1_efracsvxACC;
SG::AuxElement::Accessor<int>    * m_SV1_N2TpairACC;
SG::AuxElement::Accessor<int>    * m_SV1_NGTinSvxACC;
SG::AuxElement::Accessor<float>  * m_SV1_normdistACC;
SG::AuxElement::Accessor<float>  * m_SV1_deltaRACC;
SG::AuxElement::Accessor<float>  * m_SV1_LxyACC;
SG::AuxElement::Accessor<float>  * m_SV1_L3dACC;

SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_SV1Flip_TPELVecACC;
SG::AuxElement::Accessor<float>  * m_SV1Flip_masssvxACC;
SG::AuxElement::Accessor<float>  * m_SV1Flip_efracsvxACC;
SG::AuxElement::Accessor<int>    * m_SV1Flip_N2TpairACC;
SG::AuxElement::Accessor<int>    * m_SV1Flip_NGTinSvxACC;
SG::AuxElement::Accessor<float>  * m_SV1Flip_normdistACC;
SG::AuxElement::Accessor<float>  * m_SV1Flip_deltaRACC;
SG::AuxElement::Accessor<float>  * m_SV1Flip_LxyACC;
SG::AuxElement::Accessor<float>  * m_SV1Flip_L3dACC;

SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_JF_TPELVecACC;
SG::AuxElement::Accessor<int>    * m_JF_nVTXACC;
SG::AuxElement::Accessor<int>    * m_JF_nSingleTracksACC;
SG::AuxElement::Accessor<int>    * m_JF_nTracksAtVtxACC;
SG::AuxElement::Accessor<int>    * m_JF_N2TpairACC;
SG::AuxElement::Accessor<float>  * m_JF_energyFractionACC;
SG::AuxElement::Accessor<float>  * m_JF_massACC;
SG::AuxElement::Accessor<float>  * m_JF_significance3dACC;
SG::AuxElement::Accessor<float>  * m_JF_deltaphiACC;
SG::AuxElement::Accessor<float>  * m_JF_deltaetaACC;
SG::AuxElement::Accessor<float>  * m_JF_dRFlightDirACC;

SG::AuxElement::Accessor<xAOD::BTagging::TPELVec_t> * m_JFFlip_TPELVecACC;
SG::AuxElement::Accessor<int>    * m_JFFlip_nVTXACC;
SG::AuxElement::Accessor<int>    * m_JFFlip_nSingleTracksACC;
SG::AuxElement::Accessor<int>    * m_JFFlip_nTracksAtVtxACC;
SG::AuxElement::Accessor<int>    * m_JFFlip_N2TpairACC;
SG::AuxElement::Accessor<float>  * m_JFFlip_energyFractionACC;
SG::AuxElement::Accessor<float>  * m_JFFlip_massACC;
SG::AuxElement::Accessor<float>  * m_JFFlip_significance3dACC;
SG::AuxElement::Accessor<float>  * m_JFFlip_deltaphiACC;
SG::AuxElement::Accessor<float>  * m_JFFlip_deltaetaACC;
SG::AuxElement::Accessor<float>  * m_JFFlip_dRFlightDirACC;

SG::AuxElement::Accessor<double> * m_MV2c10FlipACC;
SG::AuxElement::Accessor<double> * m_DL1Flip_pbACC;
SG::AuxElement::Accessor<double> * m_DL1Flip_pcACC;
SG::AuxElement::Accessor<double> * m_DL1Flip_puACC;
SG::AuxElement::Accessor<int>    * m_HadronConeExclExtendedTruthLabelIDACC;

///-- Tell RootCore to build a dictionary (we need this) --///
ClassDef(top::CustomEventSaver, 0);


//---Trigger variables---
  TrigConf::xAODConfigTool* m_TrigConfigTool;
  Trig::TrigDecisionTool* m_TrigDecisionTool;
//trigger lists
  std::vector<std::string> m_SplitJetTriggerList;
  std::vector<std::string> m_GSCJetTriggerList;
  std::vector<int> m_JetTriggerDecisions;
//LV1
  const xAOD::JetRoIContainer* m_L1Jets;
  std::vector<float> m_L1Jet_et8x8;
  std::vector<float> m_L1Jet_eta;
  std::vector<float> m_L1Jet_phi;
//HLT Split
  std::vector<float> m_HLT_SplitJet_pt;
  std::vector<float> m_HLT_SplitJet_et;
  std::vector<float> m_HLT_SplitJet_eta;
  std::vector<float> m_HLT_SplitJet_phi;
  std::vector<float> m_HLT_SplitJet_m;
  std::vector<float> m_HLT_SplitJet_mv2c00;
  std::vector<float> m_HLT_SplitJet_mv2c10;
  std::vector<float> m_HLT_SplitJet_mv2c20;
//HLT GSC
  std::vector<float> m_HLT_GSCJet_pt;
  std::vector<float> m_HLT_GSCJet_et;
  std::vector<float> m_HLT_GSCJet_eta;
  std::vector<float> m_HLT_GSCJet_phi;
  std::vector<float> m_HLT_GSCJet_m;
  std::vector<float> m_HLT_GSCJet_mv2c00;
  std::vector<float> m_HLT_GSCJet_mv2c10;
  std::vector<float> m_HLT_GSCJet_mv2c20;
//online mv2
  std::vector<float> m_jet_onlinemv2c20_split;
  std::vector<float> m_jet_onlinemv2c10_split;
  std::vector<float> m_jet_onlinemv2c20_gsc;
  std::vector<float> m_jet_onlinemv2c10_gsc;
  //track jets
  std::vector<int> m_tjet_numConstituents;
  std::vector<float> m_tjet_MV2r;
  std::vector<float> m_tjet_MV2rmu;
  std::vector<float> m_tjet_DL1_pu;
  std::vector<float> m_tjet_DL1_pb;
  std::vector<float> m_tjet_DL1_pc;
  std::vector<float> m_tjet_DL1r_pu;
  std::vector<float> m_tjet_DL1r_pb;
  std::vector<float> m_tjet_DL1r_pc;
  std::vector<float> m_tjet_DL1rmu_pu;
  std::vector<float> m_tjet_DL1rmu_pb;
  std::vector<float> m_tjet_DL1rmu_pc;
  std::vector<std::vector<float> > m_tjet_BHadron_eta; //b-hadrons within dR=0.3 wrt track jet
  std::vector<std::vector<float> > m_tjet_BHadron_phi;
  std::vector<std::vector<float> > m_tjet_BHadron_pt;
  std::vector<std::vector<float> > m_tjet_BHadron_e;
  std::vector<int> m_tjet_Ghosts_BHadron_Final_Count;
  std::vector<int> m_tjet_Ghosts_CHadron_Final_Count;
  std::vector<int> m_tjet_Ghosts_Taus_Final_Count;
  std::vector<std::vector<float> > m_tjet_track_eta; //constituent tracks
  std::vector<std::vector<float> > m_tjet_track_pt;
  std::vector<std::vector<float> > m_tjet_track_phi;
  std::vector<std::vector<float> > m_tjet_track_e;
  std::vector<int> m_tjet_HadronConeExclTruthLabelID;
  std::vector<int> m_tjet_HadronConeExclExtendedTruthLabelID;

//helper function for trigger navigation
  template<class Object, class Collection>
    const Object* getTrigObject(Trig::Feature<Collection>& feature){
    
    const Collection* trigCol = feature.cptr();
    if ( !trigCol ) {
      std::cout << "ERROR: No Trig Collection pointer" << std::endl;
      return 0;
    }
    if(trigCol->size() != 1){
      std::cout << "ERROR Trig Collection size " << trigCol->size() << std::endl;
      return 0;
    }
    return trigCol->at(0);
  }


};
}

#endif
