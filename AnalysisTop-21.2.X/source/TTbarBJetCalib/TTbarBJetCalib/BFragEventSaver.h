#ifndef TTBARBJETCALIB_BFRAGEVENTSAVER_H
#define TTBARBJETCALIB_BFRAGEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TopParticleLevel/ParticleLevelEvent.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODPFlow/PFO.h"
#include "xAODPFlow/PFOContainer.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 *
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work
 *
 */

namespace top {
  class BFragEventSaver : public top::EventSaverFlatNtuple {
  public:
    ///-- Default constrcutor with no arguments - needed for ROOT --///
    BFragEventSaver();
    ///-- Destructor does nothing --///
    virtual ~BFragEventSaver();

    ///-- initialize function for top::EventSaverFlatNtuple --///
    ///-- We will be setting up out custom variables here --///
    virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;

    ///-- Keep the asg::AsgTool happy --///
    virtual StatusCode initialize(){
      return StatusCode::SUCCESS;
    }

    ///-- saveEvent function for top::EventSaverFlatNtuple --///
    ///-- We will be setting our custom variables on a per-event basis --///
    virtual void saveEvent(const top::Event& event) override;
    virtual void setupParticleLevelTreeManager() ;
    virtual void saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent) override;
  private:
    ///-- Some additional custom variables for the output --///

    ///---All the output vectors
    //std::vector<xAOD::BTagging::TPELVec_t> * m_IP3D_TPELVec;
    float m_averageIntPerXing;
    int   m_nPV;

    InDet::InDetTrackSelectionTool m_tracktool;

    // Jet cleaning tools
    SG::AuxElement::Accessor<char>* m_eventClean;
    int passTightCleaning;
    std::vector<int> jet_passTightCleaning;


    std::vector<float>  m_SMT;
    std::vector<int> m_HadronConeExclExtendedTruthLabelID;
    std::vector<int> m_HadronConeExclTruthLabelID;
    std::vector<std::vector<int> > m_jet_pv_track_origin;
    std::vector<float> m_jet_fcharge;
    std::vector<float> m_jet_fTile0;
    std::vector<float> m_jet_fEM3;
    std::vector<int> m_jet_Ntrk;
    std::vector<float> m_jet_Wtrk;
    std::vector<int> m_jet_NmuSeg;
    std::vector<float> m_jet_EMFrac;
    std::vector<float> m_jet_charge;
    std::vector<float> m_jet_TruthLabelDeltaR_T;
    std::vector<float> m_jet_TruthLabelDeltaR_B;
    std::vector<float> m_jet_TruthLabelDeltaR_C;
    std::vector<std::vector<float> > m_jet_pv_track_pt;
    std::vector<std::vector<float> > m_jet_pv_track_eta;
    std::vector<std::vector<float> > m_jet_pv_track_phi;
    std::vector<std::vector<float> > m_jet_pv_track_e;
    std::vector<std::vector<float> > m_jet_pv_track_d0;
    std::vector<std::vector<float> > m_jet_pv_track_z0;
    std::vector<std::vector<float> > m_jet_pv_track_d0sig;
    std::vector<std::vector<float> > m_jet_pv_track_z0sintheta;
    std::vector<std::vector<float> > m_jet_pfo_track_pt;
    std::vector<std::vector<float> > m_jet_pfo_track_eta;
    std::vector<std::vector<float> > m_jet_pfo_track_phi;
    std::vector<std::vector<float> > m_jet_pfo_track_e;
    std::vector<std::vector<float> > m_jet_pfo_track_d0;
    std::vector<std::vector<float> > m_jet_pfo_track_z0;
    std::vector<std::vector<float> > m_jet_pfo_track_d0sig;
    std::vector<std::vector<float> > m_jet_pfo_track_z0sintheta;
    std::vector<std::vector<int> > m_jet_pfo_track_isSV1;
    std::vector<std::vector<int> > m_jet_pfo_track_isPV;
    std::vector<std::vector<int> > m_jet_sv1_track_origin;
    std::vector<std::vector<float> > m_jet_sv1_track_pt;
    std::vector<std::vector<float> > m_jet_sv1_track_eta;
    std::vector<std::vector<float> > m_jet_sv1_track_phi;
    std::vector<std::vector<float> > m_jet_sv1_track_e;
    std::vector<std::vector<float> > m_jet_sv1_track_d0;
    std::vector<std::vector<float> > m_jet_sv1_track_z0;
    std::vector<std::vector<float> > m_jet_sv1_track_d0sig;
    std::vector<std::vector<float> > m_jet_sv1_track_z0sintheta;


    ///-- Tell RootCore to build a dictionary (we need this) --///
    ClassDef(top::BFragEventSaver, 0);



  };
}

#endif
