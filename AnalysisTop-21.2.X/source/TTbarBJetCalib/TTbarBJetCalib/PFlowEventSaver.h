#ifndef TTBARBJETCALIB_PFLOWEVENTSAVER_H
#define TTBARBJETCALIB_PFLOWEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"

#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

#include "TTbarBJetCalib/ObjectManager.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 *
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work
 *
 */

namespace top {
  class PFlowEventSaver : public top::EventSaverFlatNtuple {
  public:
    ///-- Default constrcutor with no arguments - needed for ROOT --///
    PFlowEventSaver();
    ///-- Destructor does nothing --///
    virtual ~PFlowEventSaver();

    ///-- initialize function for top::EventSaverFlatNtuple --///
    ///-- We will be setting up out custom variables here --///
    virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;

    ///-- Keep the asg::AsgTool happy --///
    virtual StatusCode initialize(){
      return StatusCode::SUCCESS;
    }

    ///-- saveEvent function for top::EventSaverFlatNtuple --///
    ///-- We will be setting our custom variables on a per-event basis --///
    virtual void saveEvent(const top::Event& event) override;

  private:

    void readConfigSettings();


    ///-- Some additional custom variables for the output --///

    float m_averageIntPerXing;
    int   m_nPV;
    std::vector<int> m_HadronConeExclExtendedTruthLabelID;
    std::vector<int> m_passTightCleaning;

    bool m_saveAllInfo;
    bool m_saveLowTaggers;
    bool m_saveHighTaggers;
    bool m_saveTriggerInfo;

    // Objects to use a more schematic code.
 
    TriggerManager* m_triggerManager;
    LowTaggerManager* m_lowTaggerManager;
    HighTaggerManager* m_highTaggerManager;

    ///Configuration
    std::shared_ptr<top::TopConfig> m_config;
    top::ConfigurationSettings* m_configSettings;

    //---All the Accesors

    SG::AuxElement::Accessor<char>   * m_eventClean;
    SG::AuxElement::Accessor<int>    * m_HadronConeExclExtendedTruthLabelIDACC;

    ///-- Tell RootCore to build a dictionary (we need this) --///
    ClassDef(top::PFlowEventSaver, 5);
  };
}

#endif
