#!/usr/bin/env python
import TopExamples.grid
import DerivationTags
import Data_rel21
import MC16_FTAG2_rel21

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'dil-cuts-b-calib_1516.txt'
config.gridUsername  = 'alopezso'			#Your username here!
config.suffix        = 'AT21.2.46.0_24.10.2018_v2'	#the last number is meant to be the date of submitting
config.excludedSites = ''				#e.g., ANALY_ABC,ANALY_XYZ -> ANALY_GLASGOW_SL6
config.noSubmit      = False
config.mergeType     = 'Default'			#'None', 'Default' or 'xAOD'
config.destSE        = 'UKI-NORTHGRID-SHEF-HEP_LOCALGROUPDISK'		#copy the results to our Groupdisk. Makes it easy accesable from the NAF
config.CMake	     = True
#config.memory	     = '2000'
config.memory	     = ''
config.otherOptions  = '--extFile=./FTAG2_PRW_mc16a_18-09-06_AF.root,./FTAG2_PRW_mc16a_18-09-06_FS.root,./FTAG2_PRW_mc16d_18-09-06_AF.root,./FTAG2_PRW_mc16d_18-09-06_FS.root,./libTTbarBJetCalib.so --maxFileSize=1300000 --nFilesPerJob=3'
### DECLARE WHAT YOU WANT TO SUBMIT!
Submit1516 = True
Submit17 = False
### If your job fails/crashes because of memory issues, instead of resubmitting a new job, consider using pbook -c "retry(id,newOpts={'maxNFilesPerJob':'1'})"
###############################################################################
###
# MC Simulation - look in MC16_FTAG2_rel21.py
# Data - look in Data_rel21.py
###Using list of FTAG2 25ns MC samples, consistent mixture of p-tags
###############################################################################
if Submit1516:
	config.settingsFile  = 'bfrag-cuts_1516.txt'
	names = [
		#'FTAG2_Resubmit_mc16a_v2',
		'FTAG2_ttbar_PhPy8_mc16a',
		#'FTAG2_ttbar_PhPy8_hdamp3mtop_mc16a',
		'FTAG2_singletop_PowPy8_mc16a',
		'FTAG2_Diboson_Sherpa_mc16a',
		'FTAG2_Zjets_Sherpa221_mc16a',
		'FTAG2_Wjets_Sherpa221_mc16a',
		#'Data15_FTAG2',
		#'Data16_FTAG2',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit17:
	config.settingsFile  = 'bfrag-cuts_17.txt'
	names = [
		#'FTAG2_Resubmit_mc16d_sys_v2'
		#'FTAG2_ttbar_PhPy8_AF2_mc16d',
		#'FTAG2_ttbar_aMcAtNloPy8_mc16d',
		#'FTAG2_ttbar_PowHW7_mc16d',
		#'FTAG2_ttbar_Sherpa221_mc16d',
		#'FTAG2_singletop_PowPy8_syst_mc16d',
		'FTAG2_singletop_aMcAtNloPy8_mc16d',
		#'FTAG2_singletop_PowHW7_mc16d',
		#'FTAG2_Diboson_PowPy8_mc16d',
		#'FTAG2_Zjets_MadGraphPy8_mc16d',
		#'FTAG2_Zjets_PowPy8_mc16d',
		#'FTAG2_Wjets_PowPy8_mc16d',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
