setupATLAS
cd build
if [[ ! -e CMakeLists.txt ]];
then
    acmSetup AnalysisBase,21.2.115,here
else
    acmSetup
fi


if [[ ! -e /tmp/x509* ]]
then
    voms-proxy-init -voms atlas
fi

lsetup panda
lsetup rucio
lsetup pyami


cd ../
