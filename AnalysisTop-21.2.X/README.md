In this Folder are configuration files for the TTBar analysis with AnalysisTop for the b-jet efficiency calibration.

In grid/ are the tools for a run on FTAG2 derivations with AnalysisTop 21.2.X to produce Ntuples on the Grid.

In source/TTBarBJetCalib/ is the Rootcore Package for a Customised Event Saver to save all the Data needed for calibrating the b-jet efficiency.

If you want to run this Analysis **locally**:
1. source setup_local.sh
2. mkdir run && cd run
3. create a text file (called e.g. "input.txt") that contains the full path to your input file(s)
4. get a cutfile for your analysis, e.g. dil-cuts-b-calib_1516.txt (for mc16a and 2015+2016 data)
5. top-xaod dil-cuts-b-calib_1516.txt input.txt

If you want to run this Analysis on the **Grid** (Panda):
1. source setup_grid.sh
2. cd grid
3. modify 01SubmitToGrid.py according to your needs
4. maybe you need to modify Data_rel21.py and MC16_FTAG2_rel21.py as well
5. don't forget: if you change anything in the code, rebuild and copy ../build/${CMTCONFIG}/lib/lib*so to the grid/ directory
6. also: copy PRW files from /afs/cern.ch/work/j/jgeisen/public/ to the grid/ directory
7. ./01SubmitToGrid.py

If you want to use **VR track jets**:
1. change the TrackJetCollectionName is your cut file, like this: TrackJetCollectionName AntiKtVR30Rmax4Rmin02TrackJets
2. if you want to cut on the number of track jets (instead of calo jet): add the library libTrackJetAnalysis to the LibraryNames at the top of your cut file and replace "JET_N" with "TRACKJET_N"

Installing AnalysisTop,21.2.X (based on https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisTop21 ):
1. Go to https://gitlab.cern.ch/atlas/athena and "Fork" the project to your gitlab account
2. setupATLAS
3. lsetup git
4. git atlas init-workdir https://yourusername@gitlab.cern.ch:8443/atlas/athena.git
# If you want to customise code:
# cd athena
# git atlas addpkg <list_of_packages_to_modify_locally> or checkout your own package
5. mkdir build && cd build
6. asetup AnalysisTop,21.2.X,here
7. cmake ../athena/Projects/WorkDir
8. cmake --build ./
9. source ${CMTCONFIG}/setup.sh
