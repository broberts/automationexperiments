#/usr/bin/python
import os
import argparse

#Parse arguments from the command line                                                                                                                                                                              
parser = argparse.ArgumentParser(description='Identify from pandamon output the input files that failed.')
parser.add_argument('-o', action='store', dest='output',
                    default='resubmit.txt',
                    help='Output file where input datasets that failed are stored.')
parser.add_argument('-i', action='store', dest='filename',
                    default=None,
                    help='File from pandamon user.<username> -i finished/error/broken/exhausted ',
                    )
parser.add_argument('-f', action='store', dest='ftagfiles',
                    default=None,
                    help='List of files on which the production is run.',
                    )
args=parser.parse_args()

listOfFiles=[]
outputFile=open(args.output,'w')

for infile in open(args.ftagfiles,'r'):
    listOfFiles.append(infile.split('\n')[0])
for infile in open(args.filename,'r'):
    output=infile.split('\n')[0].split(' ')[-2]
    if not 'user.alopezso.' in output:
        continue
    names=[output.split('.')[2],output.split('.')[5]]
    
    isInFile=False
    for outfile in listOfFiles:
        if names[0] in outfile and names[1] in outfile:
            outputFile.write(outfile+'\n')
            isInFile=True


    if not isInFile:
        print("File %s could not be found"%output)
    


outputFile.close()
            



