rm -rf build
mkdir build
cd build
acmSetup AnalysisBase,21.2.110,here
cmake ../source
cmake --build ./
source ${CMTCONFIG}/setup.sh
cd ..
