#!/usr/bin/env python
import TopExamples.grid
import DerivationTags
import Data_rel21
import MC16_FTAG2_rel21

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'dil-cuts-b-calib_1516.txt'
config.gridUsername  = 'jgeisen'			#Your username here!
#config.gridUsername  = 'perf-flavtag'			#For group production
config.suffix        = 'AT21.2.53.0_v1'			#the last number is meant to be the analysis version
config.excludedSites = ''				#e.g., ANALY_ABC,ANALY_XYZ -> ANALY_GLASGOW_SL6
config.noSubmit      = False
config.mergeType     = 'Default'			#'None', 'Default' or 'xAOD'
config.destSE        = 'DESY-HH_LOCALGROUPDISK'		#copy the results to our Groupdisk. Makes it easy accesable from the NAF
config.CMake	     = True
#config.memory	     = '2000'
config.memory	     = ''
config.otherOptions  = '--extFile=./FTAG2_PRW_mc16a_18-11-12_AF.root,./FTAG2_PRW_mc16a_18-12-19_FS.root,./libTTbarBJetCalib.so'
### DECLARE WHAT YOU WANT TO SUBMIT!
Submit1516_sys = False
Submit1516 = False
Submit1516_trig = False
Submit17_sys = False
Submit17 = False
Submit17_trig = False
Submit18_sys = False
Submit18 = True
Submit18_trig = False
### If your job fails/crashes because of memory issues, instead of resubmitting a new job, consider using pbook -c "retry(id,newOpts={'maxNFilesPerJob':'1'})"
###############################################################################
###
# MC Simulation - look in MC16_FTAG2_rel21.py
# Data - look in Data_rel21.py
###Using list of FTAG2 25ns MC samples, consistent mixture of p-tags
###############################################################################
if Submit1516_sys:
	config.settingsFile  = 'dil-cuts-b-calib_1516_sys.txt'
	names = [
		#'FTAG2_Resubmit_mc16a_sys_v2',
		#'FTAG2_ttbar_PhPy8_mc16a',
		#'FTAG2_ttbar_PhPy8_hdamp3mtop_mc16a',
		#'FTAG2_singletop_PowPy8_mc16a',
		#'FTAG2_Diboson_Sherpa_mc16a',
		#'FTAG2_Zjets_Sherpa221_mc16a',
		#'FTAG2_Wjets_Sherpa221_mc16a',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit1516:
	config.settingsFile  = 'dil-cuts-b-calib_1516.txt'
	names = [
		#'FTAG2_Resubmit_mc16a_v2',
		#'FTAG2_ttbar_PhPy8_AF2_mc16a',
		#'FTAG2_ttbar_aMcAtNloPy8_mc16a',
		#'FTAG2_ttbar_PowHW7_mc16a',
		#'FTAG2_ttbar_Sherpa221_mc16a',
		#'FTAG2_singletop_PowPy8_syst_mc16a',
		#'FTAG2_singletop_aMcAtNloPy8_mc16a',
		#'FTAG2_singletop_PowHW7_mc16a',
		#'FTAG2_Diboson_PowPy8_mc16a',
		#'FTAG2_Zjets_MadGraphPy8_mc16a',
		#'FTAG2_Zjets_PowPy8_mc16a',
		#'FTAG2_Wjets_PowPy8_mc16a',
		#'FTAG2_ttbar_PhPy8_nonallhad_mc16a',
		#'FTAG2_singletop_PowPy8_t+s_mc16a',
		#'Data15_FTAG2',
		#'Data16_FTAG2',
		#'Data15_FTAG2_indiv',
		#'Data16_FTAG2_indiv',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit1516_trig:
	config.settingsFile  = 'dil-cuts-b-calib_1516_trig.txt'
	names = [
		'Data16_FTAG2',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
config.otherOptions  = '--extFile=./FTAG2_PRW_mc16d_18-11-12_AF.root,./FTAG2_PRW_mc16d_18-11-12_FS.root,./libTTbarBJetCalib.so --expertOnly_skipScout' #--nFilesPerJob=1 for data17 jobs!
if Submit17_sys:
	config.settingsFile  = 'dil-cuts-b-calib_17_sys.txt'
	names = [
		#'FTAG2_Resubmit_mc16d_sys_v2',
		#'FTAG2_ttbar_PhPy8_mc16d',
		#'FTAG2_ttbar_PhPy8_hdamp3mtop_mc16d',
		#'FTAG2_singletop_PowPy8_mc16d',
		#'FTAG2_Diboson_Sherpa_mc16d',
		#'FTAG2_Zjets_Sherpa221_mc16d',
		#'FTAG2_Wjets_Sherpa221_mc16d',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit17:
	config.settingsFile  = 'dil-cuts-b-calib_17.txt'
	names = [
		#'FTAG2_Resubmit_mc16d_v2',
		#'FTAG2_ttbar_PhPy8_AF2_mc16d',
		#'FTAG2_ttbar_aMcAtNloPy8_mc16d',
		#'FTAG2_ttbar_PowHW7_mc16d',
		#'FTAG2_ttbar_Sherpa221_mc16d',
		#'FTAG2_singletop_PowPy8_syst_mc16d',
		#'FTAG2_singletop_aMcAtNloPy8_mc16d',
		#'FTAG2_singletop_PowHW7_mc16d',
		#'FTAG2_Diboson_PowPy8_mc16d',
		#'FTAG2_Zjets_MadGraphPy8_mc16d',
		#'FTAG2_Zjets_PowPy8_mc16d',
		#'FTAG2_Wjets_PowPy8_mc16d',
		#"FTAG2_ttbar_PhPy8_nonallhad_mc16d",
		#"FTAG2_singletop_PowPy8_t+s_mc16d",
		#'Data17_FTAG2',
		#'Data17_FTAG2_indiv',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit17_trig:
	config.settingsFile  = 'dil-cuts-b-calib_17_trig.txt'
	names = [
		'Data17_FTAG2',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
config.otherOptions  = '--extFile=./FTAG2_PRW_mc16e_18-11-12_AF.root,./FTAG2_PRW_mc16e_18-11-30_FS.root,./libTTbarBJetCalib.so --expertOnly_skipScout' 
if Submit18_sys:
	config.settingsFile  = 'dil-cuts-b-calib_18_sys.txt'
	names = [
		#'FTAG2_Resubmit_mc16e_sys_v3',
		#'FTAG2_ttbar_PhPy8_mc16e',
		#'FTAG2_ttbar_PhPy8_hdamp3mtop_mc16e',
#		'FTAG2_singletop_PowPy8_mc16e',
#		'FTAG2_Diboson_Sherpa_mc16e',
		#'FTAG2_Zjets_Sherpa221_mc16e',
#		'FTAG2_Wjets_Sherpa221_mc16e',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit18:
	config.settingsFile  = 'dil-cuts-b-calib_18.txt'
	names = [
		'FTAG2_ttbar_PhPy8_AF2_mc16e',
		#'FTAG2_ttbar_aMcAtNloPy8_mc16e',
		#'FTAG2_ttbar_PowHW7_mc16e',
		#'FTAG2_singletop_PowPy8_syst_mc16e',
		#'FTAG2_singletop_aMcAtNloPy8_mc16e',
		#'FTAG2_singletop_PowHW7_mc16e',
		#'FTAG2_Diboson_PowPy8_mc16e',
#		'FTAG2_Zjets_MadGraphPy8_mc16e',
		#'FTAG2_Zjets_PowPy8_mc16e',
		#'FTAG2_Wjets_PowPy8_mc16e',
		#"FTAG2_ttbar_PhPy8_nonallhad_mc16e",
		#"FTAG2_singletop_PowPy8_t+s_mc16e",
		#'Data18_Resubmit_v2',
		#'Data18_FTAG2',
		#'Data18_FTAG2_indiv',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit18_trig:
	config.settingsFile  = 'dil-cuts-b-calib_18_trig.txt'
	names = [
		#'Data18_FTAG2',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
