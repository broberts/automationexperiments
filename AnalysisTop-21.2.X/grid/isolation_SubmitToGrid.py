#!/usr/bin/env python
import TopExamples.grid
import DerivationTags
import Data_rel21
import MC16_FTAG2_rel21

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
#config.settingsFile  = "Isolation/pflow-dil-cuts-b-calib_18.txt"
config.settingsFile  = "Isolation/pflow-dil-cuts-b-calib_18.txt,Isolation/pflow-dil-cuts-b-calib_18_Loose.txt,Isolation/pflow-dil-cuts-b-calib_18_Tight.txt,Isolation/pflow-dil-cuts-b-calib_18_TrackOnly.txt"
#config.combined_prefixes = "Gradient,FCLoose,FCTight,TrackOnly"
#config.combined_outputFile = "output.root"
config.gridUsername  = 'alopezso'			#Your username here!
config.suffix        = 'AT21.2.80.0_090919_test'	#the last number is meant to be the date of submitting
#config.excludedSites = ''		       	#e.g., ANALY_ABC,ANALY_XYZ -> ANALY_GLASGOW_SL6
config.noSubmit      = False
config.mergeType     = 'Default'		#'None', 'Default' or 'xAOD'
config.destSE        = 'CERN-PROD_PERF_FLAVTAG'		#copy the results to our Groupdisk. Makes it easy accesable from the NAF
config.CMake	     = True
#config.memory	     = '2000'
config.memory	     = '2000'
config.otherOptions  = '--extFile=./FTAG2_PRW_mc16a_18-11-12_AF.root,./FTAG2_PRW_mc16a_18-12-19_FS.root,./FTAG2_PRW_mc16d_18-11-12_AF.root,./FTAG2_PRW_mc16d_18-11-12_FS.root,./FTAG2_PRW_mc16e_18-11-12_AF.root,./FTAG2_PRW_mc16e_18-11-30_FS.root,testPRW.pool.root,./libTTbarBJetCalib.so --nGBPerJob=8'
### DECLARE WHAT YOU WANT TO SUBMIT!
Submit1516 = True
Submit1516_sys = False
Submit1516_fake = False
Submit1516_trig = False
Submit17 = False
Submit17_sys = False
Submit17_fake = False
Submit17_trig = False
Submit18 = True
### If your job fails/crashes because of memory issues, instead of resubmitting a new job, consider using pbook -c "retry(id,newOpts={'maxNFilesPerJob':'1'})"
###############################################################################
###
# MC Simulation - look in MC16_FTAG2_rel21.py
# Data - look in Data_rel21.py
###Using list of FTAG2 25ns MC samples, consistent mixture of p-tags
###############################################################################
if Submit1516:
	config.settingsFile  = 'Isolation/pflow-dil-cuts-b-calib_1516.txt,Isolation/pflow-dil-cuts-b-calib_1516_Loose.txt,Isolation/pflow-dil-cuts-b-calib_1516_Tight.txt,Isolation/pflow-dil-cuts-b-calib_1516_TrackOnly.txt'
	names = [
		'FTAG2_test_mc16a',
		]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit17:
	config.settingsFile  = 'Isolation/pflow-dil-cuts-b-calib_17.txt,Isolation/pflow-dil-cuts-b-calib_17_Loose.txt,Isolation/pflow-dil-cuts-b-calib_17_Tight.txt,Isolation/pflow-dil-cuts-b-calib_17_TrackOnly.txt'
	names = [
		'FTAG2_test_mc16d',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
if Submit18:
	config.settingsFile  = 'Isolation/pflow-dil-cuts-b-calib_18.txt,Isolation/pflow-dil-cuts-b-calib_18_Loose.txt,Isolation/pflow-dil-cuts-b-calib_18_Tight.txt,Isolation/pflow-dil-cuts-b-calib_18_TrackOnly.txt'
	names = [
		'FTAG2_test_mc16e',
	]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
###############################################################################
