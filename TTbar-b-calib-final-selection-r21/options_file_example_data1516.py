import ROOT
import os


#sample with plot options
class sample:
    _lastFillColor =0
    _fillColors=[ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1]
    _fillColors=_fillColors+range(1,50)
    def __init__(self, name, boot_strap_available = False, systematic= "nominal", name_short=""):
        self.name = name
        self.markerStyle = 1
        self.fillStyle = 1001
        self.lineColor = 1
        self.fillColor = sample._fillColors[sample._lastFillColor]
        sample._lastFillColor=sample._lastFillColor+1
        self.lineStyle = 1
        self.root_file_path = ""
        self.boot_strap_available = boot_strap_available
        self.systematic = systematic
        if name_short=="":
            name_short=name
        self.name_short=name_short
        if systematic=="nominal":
            self.systematic_name =name_short   #unique name of systamtic in case sample is only used to calcualte it.
        else:
            self.systematic_name =name_short+"_"+systematic
        self.do_bootstrap=boot_strap_available  
        if "Py8" in name:
            self.hadronization=410470
        if "Sherpa22" in name:
            self.hadronization=410250
        if "HW7" in name:
            self.hadronization=410558

    def setHistOptions(self,hist):
        hist.SetMarkerStyle(self.markerStyle)
        hist.SetFillStyle(self.fillStyle);
        hist.SetLineColor(self.lineColor);
        hist.SetFillColor(self.fillColor);
        hist.SetLineStyle(self.lineStyle);
    def addToLegend(self,hist,legend):
        legend.AddEntry(hist,self.name[6:],"F")


class channel:
    def __init__(self,name="ee_OS_J2"):
        self.name = name

class systematic:
    def __init__(self,systematic_name="nominal",do_bootstrap=False, systematic_command=""):
        self.systematic_name = systematic_name  #unique name of systamtic
        self.do_bootstrap=do_bootstrap       #if systematic appears bumpy- you can calc mc stat unc of systematic here and smooth it later
        if systematic_command == "":
            self.systematic_command = systematic_name
        else:
            self.systematic_command = systematic_command
#defining options for dircetories
class options_container:
    def __init__(self):
        self.name="Summary of options"
        self.version_tag="rl21-2-53_19-02-11_mc16a/"#""
        self.dust_dir= "/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/"+self.version_tag
        self.output_dir= self.dust_dir+"out/"
        self.condor_dir= self.dust_dir+"htc_submit/"
        self.input_selection_dir= "ntuple_input_files/"+"rl21-2-53_mc16a/"+"input_selection/"
        self.plot_dir= self.dust_dir+"plots-ttbar-PhPy8/"#"plots-PowPy8/"
        self.www_dir="/afs/desy.de/user/s/schmoecj/www/b-jet-eff-ttbarPDF/"
        #the sample list. Change names here!!!
        self.data_name="d1516"
        self.data_lumi = (32988.1 + 3219.56)      #from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2 2016+2015
        self.singleTop_sample=sample('FTAG2_singletop_PowPy8', True,"nominal","singletop");
        self.ZJets_sample=sample('FTAG2_Zjets_Sherpa221', True, "nominal", "Zjets");
        self.Diboson_sample=sample('FTAG2_Diboson_Sherpa222', True, "nominal", "Diboson");
        self.Wjets_sample=sample('FTAG2_Wjets_Sherpa221', True, "nominal", "Wjets")
        other_s=[self.Diboson_sample,self.ZJets_sample,self.Wjets_sample]
        self.other_samples=list(other_s)
        
        self.ttb_sample=sample('FTAG2_ttbar_PhPy8',True,"nominal","ttbar")
        self.ttb_sample.fillColor=0
        other_s.append(self.singleTop_sample)
        other_s.append(self.ttb_sample)
        self.nominal_samples=list(other_s)
        for s in self.nominal_samples:
             s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

    
        #options for the combine plots makro (obsolete?)
        #the channel names. change names here!!
        self.channels=[channel('emu_OS_J2'),channel('emu_OS_J3'),channel('ee_OS_J2_aCuts'),channel('ee_OS_J3_aCuts'),channel('mumu_OS_J2_aCuts'),channel('mumu_OS_J3_aCuts')]
        self.try_cut_ratio_in_xxcon_plot=0  #=0 to have now cutraio label

        ##set plot limits
        self.plot_lim_standrad=0#[2,20e4]
        self.plot_lim_pt=0#[2,20e3]
        self.plot_lim_m=0#[2,20e3]
        self.plot_lim_mT=0#[2,20e3]

    #settings for the systematic uncertainties
    ##ttbar:
        self.ttbar_syst_samples = [
            sample('FTAG2_ttbar_aMcPy8',True,"nominal", "ttbar_aMcPy8"),
            sample('FTAG2_ttbar_PowHW7',True,"nominal", "ttbar_PowHW7"),
            sample("FTAG2_ttbar_Sherpa221",True,"nominal", "ttbar_Sherpa221"),
            sample('FTAG2_ttbar_PhPy8_AF2',True,"nominal", "ttbar_PhPy8_AF2"),
            # sample('FTAG2_ttbar_PhPy8_hdamp3mtop'),
            sample('FTAG2_ttbar_PhPy8_nonallhad',False,"nominal", "ttbar_PhPy8_nonallhad"),
        ]

        for s in self.ttbar_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        self.ttbar_rad_up_sample=sample('FTAG2_ttbar_PhPy8_hdamp3mtop', True, "weight_mc_rad_UP", "ttbar_PhPy8_hdamp")
        self.ttbar_rad_down_sample=sample('FTAG2_ttbar_PhPy8', True, "weight_mc_rad_DOWN", "ttbar_PhPy8")
        self.ttbar_rad_samples=[
            self.ttbar_rad_down_sample,
            self.ttbar_rad_up_sample,
        ]
        self.ttbar_fsr_up_sample=sample('FTAG2_ttbar_PhPy8', True, "weight_mc_fsr_UP", "ttbar_PhPy8")
        self.ttbar_fsr_down_sample=sample('FTAG2_ttbar_PhPy8', True, "weight_mc_fsr_DOWN", "ttbar_PhPy8")
        self.ttbar_fsr_samples=[
            self.ttbar_fsr_down_sample,
            self.ttbar_fsr_up_sample,
        ]
        self.ttbar_pdf_systematics=[systematic(self.ttb_sample.name_short + "_" +"weight_mc_shower_np_11",False, "weight_mc_shower_np_11")]
        for np in xrange(112,142):
            self.ttbar_pdf_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_np_"+str(np), False, "weight_mc_shower_np_"+str(np)) )

    #   ZJets:
        self.ZJets_syst_samples = [
            sample("FTAG2_Zjets_PowPy8",True),
            sample("FTAG2_Zjets_MGPy8",True),
        ]
        for s in self.ZJets_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"


        bootstrap_high_systs=[4,5]  #-there are a few which had crazy high uncertainty, better check if they are statistical significant
        self.ZJets_weight_mc_systematics=[]
        self.ZJets_weight_mc_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_crossec_5per_UP", False , "weight_crossec_5per_UP"))
        self.ZJets_weight_mc_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_crossec_5per_DOWN", False , "weight_crossec_5per_DOWN"))
        for np in xrange(4,113):
            self.ZJets_weight_mc_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_mc_shower_np_"+str(np), np in bootstrap_high_systs , "weight_mc_shower_np_"+str(np)) )
   

    #   Diboson:
        self.Diboson_syst_samples = [
            sample("FTAG2_Diboson_PowPy8",True),
        ]
        for s in self.Diboson_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        #weight systematics  
        bootstrap_high_systs=[]  #-there are a few which had crazy high uncertainty, better check if they are statistical significant
        self.Diboson_weight_mc_systematics=[]
        self.Diboson_weight_mc_systematics.append(systematic(self.Diboson_sample.name_short + "_" + "weight_crossec_6per_UP", False , "weight_crossec_6per_UP"))
        self.Diboson_weight_mc_systematics.append(systematic(self.Diboson_sample.name_short + "_" + "weight_crossec_6per_DOWN", False , "weight_crossec_6per_DOWN"))
        for np in xrange(4,113):
            self.Diboson_weight_mc_systematics.append(systematic(self.Diboson_sample.name_short + "_" + "weight_mc_shower_np_"+str(np), np in bootstrap_high_systs , "weight_mc_shower_np_"+str(np)) )
   


    #   singletop:
        self.singletop_syst_samples = [
            sample('FTAG2_singletop_PowPy8_DS', True, "nominal", "singletop_PowPy8_DS"),
            sample('FTAG2_singletop_PowPy8_DS_AF2', True, "nominal", "singletop_PowPy8_DS_AF2"),
            sample('FTAG2_singletop_PowPy8_AF2', True, "nominal", "singletop_PowPy8_AF2"),
            sample('FTAG2_singletop_PowHW7', True, "nominal", "singletop_PowHW7"),
            sample('FTAG2_singletop_aMcPy8', True, "nominal", "singletop_aMcPy8"),
            sample('FTAG2_singletop_PowPy8_Full', False, "nominal", "singletop_PowPy8_Full"),
        ]
        for s in self.singletop_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        self.singletop_rad_up_sample=sample('FTAG2_singletop_PowPy8', True, "weight_mc_rad_UP", "singletop_PhPy8")
        self.singletop_rad_down_sample=sample('FTAG2_singletop_PowPy8', True, "weight_mc_rad_DOWN", "singletop_PhPy8")

        self.singletop_rad_samples=[
            self.singletop_rad_down_sample,
            self.singletop_rad_up_sample,
        ]
        self.singletop_fsr_up_sample=sample('FTAG2_singletop_PowPy8', True, "weight_mc_fsr_UP", "singletop_PhPy8")
        self.singletop_fsr_down_sample=sample('FTAG2_singletop_PowPy8', True, "weight_mc_fsr_DOWN", "singletop_PhPy8")
        self.singletop_fsr_samples=[
            self.singletop_fsr_down_sample,
            self.singletop_fsr_up_sample,
        ]

        bootstrap_high_systs=[]
        self.singletop_pdf_systematics=[systematic(self.singleTop_sample.name_short +"_"+ "weight_mc_shower_np_11",False, "weight_mc_shower_np_11") ]
        for np in xrange(112,142): #142
            self.singletop_pdf_systematics.append(systematic(self.singleTop_sample.name_short +"_"+"weight_mc_shower_np_"+str(np), np in bootstrap_high_systs, "weight_mc_shower_np_"+str(np)) )
   
   
    #   detector, pileup, leptons
        self.fake_estimation = 0 #systematic("correctFakes")
        #systematics with weights in the nominal tree:
        self.inner_systematics_in_nominal_tree = [
             systematic("weight_pileup_UP"),
             systematic("weight_pileup_DOWN"),
             systematic("weight_leptonSF_EL_SF_Trigger_UP"),
             systematic("weight_leptonSF_EL_SF_Trigger_DOWN"),
             systematic("weight_leptonSF_EL_SF_Reco_UP"),
             systematic("weight_leptonSF_EL_SF_Reco_DOWN"),
             systematic("weight_leptonSF_EL_SF_ID_UP"),
             systematic("weight_leptonSF_EL_SF_ID_DOWN"),
             systematic("weight_leptonSF_EL_SF_Isol_UP"),
             systematic("weight_leptonSF_EL_SF_Isol_DOWN"),
             systematic("weight_leptonSF_MU_SF_Trigger_STAT_UP"),
             systematic("weight_leptonSF_MU_SF_Trigger_STAT_DOWN"),
             systematic("weight_leptonSF_MU_SF_Trigger_SYST_UP"),
             systematic("weight_leptonSF_MU_SF_Trigger_SYST_DOWN"),
             systematic("weight_leptonSF_MU_SF_ID_STAT_UP"),
             systematic("weight_leptonSF_MU_SF_ID_STAT_DOWN"),
             systematic("weight_leptonSF_MU_SF_ID_SYST_UP"),
             systematic("weight_leptonSF_MU_SF_ID_SYST_DOWN"),
             systematic("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP"),
             systematic("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN"),
             systematic("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP"),
             systematic("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN"),
             systematic("weight_leptonSF_MU_SF_Isol_STAT_UP"),
             systematic("weight_leptonSF_MU_SF_Isol_STAT_DOWN"),
             systematic("weight_leptonSF_MU_SF_Isol_SYST_UP"),
             systematic("weight_leptonSF_MU_SF_Isol_SYST_DOWN"),
             systematic("weight_leptonSF_MU_SF_TTVA_STAT_UP"),
             systematic("weight_leptonSF_MU_SF_TTVA_STAT_DOWN"),
             systematic("weight_leptonSF_MU_SF_TTVA_SYST_UP"),
             systematic("weight_leptonSF_MU_SF_TTVA_SYST_DOWN"),
             systematic("weight_jvt_UP"),
             systematic("weight_jvt_DOWN"),
             systematic("FT_EFF_Eigen_C_0__1down"),
             systematic("FT_EFF_Eigen_C_0__1up"),
             systematic("FT_EFF_Eigen_C_1__1down"),
             systematic("FT_EFF_Eigen_C_1__1up"),
             systematic("FT_EFF_Eigen_C_2__1down"),
             systematic("FT_EFF_Eigen_C_2__1up"),
             systematic("FT_EFF_Eigen_C_3__1down"),
             systematic("FT_EFF_Eigen_C_3__1up"),
             systematic("FT_EFF_Eigen_C_4__1down"),
             systematic("FT_EFF_Eigen_C_4__1up"),
             systematic("FT_EFF_Eigen_C_5__1down"),
             systematic("FT_EFF_Eigen_C_5__1up"),
             systematic("FT_EFF_Eigen_C_6__1down"),
             systematic("FT_EFF_Eigen_C_6__1up"),
             systematic("FT_EFF_Eigen_C_7__1down"),
             systematic("FT_EFF_Eigen_C_7__1up"),
             systematic("FT_EFF_Eigen_C_8__1down"),
             systematic("FT_EFF_Eigen_C_8__1up"),
             systematic("FT_EFF_Eigen_C_9__1down"),
             systematic("FT_EFF_Eigen_C_9__1up"),
             systematic("FT_EFF_Eigen_C_10__1down"),
             systematic("FT_EFF_Eigen_C_10__1up"),
             systematic("FT_EFF_Eigen_C_11__1down"),
             systematic("FT_EFF_Eigen_C_11__1up"),
             systematic("FT_EFF_Eigen_C_12__1down"),
             systematic("FT_EFF_Eigen_C_12__1up"),
             systematic("FT_EFF_Eigen_C_13__1down"),
             systematic("FT_EFF_Eigen_C_13__1up"),
             systematic("FT_EFF_Eigen_C_14__1down"),
             systematic("FT_EFF_Eigen_C_14__1up"),
             systematic("FT_EFF_Eigen_C_15__1down"),
             systematic("FT_EFF_Eigen_C_15__1up"),
             systematic("FT_EFF_Eigen_C_16__1down"),
             systematic("FT_EFF_Eigen_C_16__1up"),
             systematic("FT_EFF_Eigen_C_17__1down"),
             systematic("FT_EFF_Eigen_C_17__1up"),
             systematic("FT_EFF_Eigen_C_18__1down"),
             systematic("FT_EFF_Eigen_C_18__1up"),
             systematic("FT_EFF_Eigen_C_19__1down"),
             systematic("FT_EFF_Eigen_C_19__1up"),
             systematic("FT_EFF_Eigen_Light_0__1down"),
             systematic("FT_EFF_Eigen_Light_0__1up"),
             systematic("FT_EFF_Eigen_Light_1__1down"),
             systematic("FT_EFF_Eigen_Light_1__1up"),
             systematic("FT_EFF_Eigen_Light_2__1down"),
             systematic("FT_EFF_Eigen_Light_2__1up"),
             systematic("FT_EFF_Eigen_Light_3__1down"),
             systematic("FT_EFF_Eigen_Light_3__1up"),
             systematic("FT_EFF_Eigen_Light_4__1down"),
             systematic("FT_EFF_Eigen_Light_4__1up"),
             systematic("FT_EFF_Eigen_Light_5__1down"),
             systematic("FT_EFF_Eigen_Light_5__1up"),
             systematic("FT_EFF_Eigen_Light_6__1down"),
             systematic("FT_EFF_Eigen_Light_6__1up"),
             systematic("FT_EFF_Eigen_Light_7__1down"),
             systematic("FT_EFF_Eigen_Light_7__1up"),
             systematic("FT_EFF_Eigen_Light_8__1down"),
             systematic("FT_EFF_Eigen_Light_8__1up"),
             systematic("FT_EFF_Eigen_Light_9__1down"),
             systematic("FT_EFF_Eigen_Light_9__1up"),
             systematic("FT_EFF_Eigen_Light_10__1down"),
             systematic("FT_EFF_Eigen_Light_10__1up"),
             systematic("FT_EFF_Eigen_Light_11__1down"),
             systematic("FT_EFF_Eigen_Light_11__1up"),
             systematic("FT_EFF_Eigen_Light_12__1down"),
             systematic("FT_EFF_Eigen_Light_12__1up"),
             systematic("FT_EFF_Eigen_Light_13__1down"),
             systematic("FT_EFF_Eigen_Light_13__1up"),
             systematic("FT_EFF_Eigen_Light_14__1down"),
             systematic("FT_EFF_Eigen_Light_14__1up"),
             systematic("FT_EFF_Eigen_Light_15__1down"),
             systematic("FT_EFF_Eigen_Light_15__1up"),
             systematic("FT_EFF_Eigen_Light_16__1down"),
             systematic("FT_EFF_Eigen_Light_16__1up"),
             systematic("FT_EFF_Eigen_Light_17__1down"),
             systematic("FT_EFF_Eigen_Light_17__1up"),
             systematic("FT_EFF_Eigen_Light_18__1down"),
             systematic("FT_EFF_Eigen_Light_18__1up"),
             systematic("FT_EFF_Eigen_Light_19__1down"),
             systematic("FT_EFF_Eigen_Light_19__1up"),
             ]
        #systematics with a own tree:
        self.tree_systematics = [
            systematic("EG_RESOLUTION_ALL__1down"),
            systematic("EG_RESOLUTION_ALL__1up"),
            systematic("EG_SCALE_AF2__1down"),
            systematic("EG_SCALE_AF2__1up"),
            systematic("EG_SCALE_ALL__1down"),
            systematic("EG_SCALE_ALL__1up"),
            systematic("JET_CategoryReduction_JET_BJES_Response__1down"),
            systematic("JET_CategoryReduction_JET_BJES_Response__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Detector1__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Detector1__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Detector2__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Detector2__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Mixed2__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling1__1down",True),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling1__1up",True),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling2__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling2__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling3__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling3__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling4__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Modelling4__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down"),
            systematic("JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1down", True),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1up", True ),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down"),
            systematic("JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up"),
            systematic("JET_CategoryReduction_JET_Flavor_Composition__1down", True),
            systematic("JET_CategoryReduction_JET_Flavor_Composition__1up", True),
            systematic("JET_CategoryReduction_JET_Flavor_Response__1down",True),
            systematic("JET_CategoryReduction_JET_Flavor_Response__1up",True),
            systematic("JET_CategoryReduction_JET_JER_DataVsMC__1down", True),
            systematic("JET_CategoryReduction_JET_JER_DataVsMC__1up", True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_1__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_1__1up",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_2__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_2__1up",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_3__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_3__1up",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_4__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_4__1up",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_5__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_5__1up",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_6__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_6__1up",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down",True),
            systematic("JET_CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up",True),
            systematic("JET_CategoryReduction_JET_Pileup_OffsetMu__1down"),
            systematic("JET_CategoryReduction_JET_Pileup_OffsetMu__1up"),
            systematic("JET_CategoryReduction_JET_Pileup_OffsetNPV__1down"),
            systematic("JET_CategoryReduction_JET_Pileup_OffsetNPV__1up"),
            systematic("JET_CategoryReduction_JET_Pileup_PtTerm__1down"),
            systematic("JET_CategoryReduction_JET_Pileup_PtTerm__1up"),
            systematic("JET_CategoryReduction_JET_Pileup_RhoTopology__1down", True),
            systematic("JET_CategoryReduction_JET_Pileup_RhoTopology__1up", True),
            systematic("JET_CategoryReduction_JET_PunchThrough_MC16__1down"),
            systematic("JET_CategoryReduction_JET_PunchThrough_MC16__1up"),
            systematic("JET_CategoryReduction_JET_SingleParticle_HighPt__1down"),
            systematic("MET_SoftTrk_ResoPara"),
            systematic("MET_SoftTrk_ResoPerp"),
            systematic("MET_SoftTrk_ScaleDown"),
            systematic("MET_SoftTrk_ScaleUp"),
            systematic("MUON_ID__1down"),
            systematic("MUON_ID__1up"),
            systematic("MUON_MS__1down"),
            systematic("MUON_MS__1up"),
            systematic("MUON_SAGITTA_RESBIAS__1down"),
            systematic("MUON_SAGITTA_RESBIAS__1up"),
            systematic("MUON_SAGITTA_RHO__1down"),
            systematic("MUON_SAGITTA_RHO__1up"),
            systematic("MUON_SCALE__1down"),
            systematic("MUON_SCALE__1up"),
            ]

options=options_container()
