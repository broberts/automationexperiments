When your grid jobs are finished you can run the final selection locally. This is done in order to be able to do small changes fast without rerunning the full event selection.


#### sample collection
Your first steps should be to gather all the files you ran over on the grid per sample. Compare for example:

[r21-2-29_18-05-14_mc16a/input_selection/FTAG2_singletop_PowPy8_DS.txt](r21-2-29_18-05-14_mc16a/input_selection/FTAG2_singletop_PowPy8_DS.txt)

each line in one of those input files references to a *txt* file in the corresponding folder:
[r21-2-29_18-05-14_mc16a/input_selection/FTAG2_singletop_PowPy8_DS/](r21-2-29_18-05-14_mc16a/input_selection/FTAG2_singletop_PowPy8_DS/)

the folder can be created by *RucioListBuilder.py* , so the only thing you have to do manually is to find all the files you ran over on the grid and create on of the "high-level-txt" files per sample.

To find the files commands like:
```bash
source setup.sh
rucio list-dids "user.jgeisen:user.jgeisen.3616*.PowhegPy8EG.DAOD_FTAG2.*.17-10-09_v1_output.root"
```
can be used.

The corresponding folder with the corresponding files on your lokal groupdisk is done afterwards automatically by *RucioListBuilder.py*.

now run *RucioListBuilder.py* in order to get all files of a sample on your lokal-group-disk.:

```bash
python RucioListBuilder.py -r DESY-HH_LOCALGROUPDISK rl21-21-2-4-171009/input_selection/*.txt
```

now insert your sample file names and other options into (you can look how this is done in the example option files.)
*options_file.py*. The names in the option file have to be the same as the names of the "high-level-txt" files!

#### event selection
afterward you can run final selection over the found files:
```bash
python finalSelection.py --rrS rl21-21-2-4-171009/input_selection/FTAG2_Diboson_PowPy8_21-2-4_old.txt
```

*finalSelection.py* is a python script which calculates the lumiweight and runs the */TSelectors/source/Selectors/macros/runTSelectorMC.cxx* on it. The input files are gatherd in txt files by *RucioListBuilder.py* for each sample separately. Running with root 6.
don't forget the flag  *--data* when you run over data files!
```
positional arguments:

  input_file            input file for example: ../selecting_inputs/user.jschm
                        oec.410000.PowhegPythiaEvtGen.DAOD_FTAG2.e3698_s2608_s
                        2183_r7725_r7676_p2669.anaTopStyle-17-06-06_output.roo
                        t.txt



optional arguments:

  -h, --help            show this help message and exit

  --rrS                 rerun the Event Selection (default: false)

  --data                the input is data (default: false)

  --test                only run on two files for testing (default: false)

  --dPlots              draw Comparison Plots, takes a while. (default: false)

  --sFile               The input is a single File and not a file with Folders
                        as usual (default: false)

  -t TSYST, --tsyst TSYST

                        name of systematic tree to run over. default=nominal.

  -w ISYST, --isyst ISYST
                        only valid if you run on nominal. You can specify
                        inner systematics here like
                        weight_leptonSF_EL_SF_Trigger_UP(weights)

  --log                 Create log files, nice option for batch submission!
                        (default: false)

  --runParallel         if you want to run all jubs in parallel. (default:
                        false)

  --runFakes            Includes SS channels relevant for fake estimation
                        (default: false)

  --correctFakes        Scale factors are applied to correct weights of OS events
                        with a non-prompt electron - have to be generated first!
                        (default: false)
			To generate them, first run with option --run_fakes
			Then, use Fakefactor_scripts/compare_1PL_2PL_Data.py

```

The real selection is done by the Tselector *finalSelectionMC.C*.
*TTbar-b-calib-final-selection/TSelectors/source/Selectors/libSelectorsMC/finalSelectionMC.cxx*

Is the place to look. The selection happens in the lines 750 until 1050 and should be more or less understandable. The Containers which are defined before are for keeping all the histograms for the plots and the 4d histograms for the likelihoodfits later on.

You also need to make sure you run the nominal with save_bin_mean option turned on in the TSelectors (HistForFitContainer.h with variable name save_bin_something_sr_hists). Then do the same thing again when you use combinehists_for_fit_systs in the likelihood section. Its used for the final calculate_all_fits so you'll be mad if you dont run it as you'll have to redo it all again anyway so make sure its turned on for both stages for the nominal SM samples.

and if you processed all samples you can run:
*python combinePlots.py*
which will produce a bunch of control plots.


then go to [likelihood-fit](../likelihood-fit/).

If you want to run over all systematic samples you should use a batch system. Therefor we created the folder htcondor_submit. You can find a python script which submits all needed jobs to condor here.
When all of your batch jobs are finished you can run: combine_histos_for_fit_with_syst.py.

#### Run with Bootstrap Weights:
- You'll need use *pflow_submit_to_condor.py*
- Turn on all options except *run_Pdf_syst*
- Turn on /uncomment a makeshift flag on the first lines of the submitfinalselction method something relating to:
    ```
    if not "data15161718FTAG2_ttbar_PhPy8" in job_name:
       return
       ```
- Then go into your respective options file year and turn off all inner tree systematics (by commenting them out) as well as all tree systematics except those that have the do_boostrap attribute set to True (they are the ones we'll be running on).
- You can now run *pflow_submit_to_condor.py*. This should give you give a high purity list of jobs with a dozen or so that aren't run on bootstrap weights. It's not perfect but if you can be bothered to think of an idea to implement, be our guest!
- Next you need to merge these files together you have created. To do this use *htcondor_submit/merger_boostrap.py* (to use on condor). You can also do this locally but it uses lots of RAM so if you're doing full Run 2 data, use condor. Beware though, it takes a lot of RAM so your system admin will not be happy so do this out-of-hours so they don't notice.
- If you have run on condor, your files will be in your output directory (check options file if you can't remember) + "/bootstrap_merged/". We now need to replace these files with our existing ones created without bootstrap weights. For this *ReplaceNominalWBootStrap.py* was created and you can just run this but first follow the instructions at the top of file and secondly, if you're including or missing an files you can uncomment the section it loops over, for example, FTAG2_ttbar_Sherpa221.

#### ToDo:
 - clean up the finalSelection.py




##Particle-flow specific files

I found some issues replicating the ntuples in my RSE, so I created few files to substitute the RucioListBuilder.py functionality. 
NTuples are downloaded locally (huge amount of space, need to clear it up) and the rl21-21-2-4-171009/input_selection/*.txt files point 
to local files.

You should check the file LocalListBuilder.py and change the inputContainer, outputContainer and the dictionary to agree with your ntuples 
name, location and the output folder for lists that you desire. Then, just do:
``` 
python LocalListBuilder.py
```

Change accordingly the local options_file.py and you can run finalSelection.py as explained previously. 
Due to the differences in the ntuple location, the names in the lists that you just created are different than the ones used for emtopo jets.
finalSelection.py has then been be copied into finalSelection_pflow.py and updated to run on local files. 


New options have been added to finalSelection_pflow.py w.r.t to finalSelection.py, just to test the effect of reweighting in the final distributions.
In this context, one has the *weight.root* files inside the folder:

```

	--reweight_pT
                    Reweight the pT of the first leading jet
	--reweight_ip3d
                    Reweight the IP3D tracks of the first leading jet
	--reweight_jf
                    Reweight the JetFitter tracks of the first leading jet
	--reweight_sv1
                    Reweight the SV1 tracks of the first leading jet

```

