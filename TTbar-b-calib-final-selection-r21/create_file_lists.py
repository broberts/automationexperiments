

import glob
import os
import sys


workdir = os.getcwd()


for ade in ['a','d','e']:
	inputdir = '/afs/cern.ch/user/b/broberts/bjets_ttbardilepton_PDF-TMod/'+\
					'TTbar-b-calib-final-selection-r21/ntuple_input_files/rl21-2-110_mc16'+ade+'/input_selection/'
	datasetdir = '/eos/home-b/broberts/FTagTSel110/input_files_'+ade+'/'

	os.chdir(inputdir)
	datasets = glob.glob('*.txt')


	for ds in datasets:
		print ds
		ds_folder_name = ds[:-4] 

		
		if not os.path.exists(ds_folder_name):
			os.mkdir(ds_folder_name)
		

		f = open(ds)
		sub_datasets = f.readlines()
		f.close()

		sub_datasets = [x.rstrip() for x in sub_datasets]

		os.chdir(ds_folder_name)
		for sub_ds in sub_datasets:
			ds_files = glob.glob(datasetdir + ds_folder_name +'/' + sub_ds+'/*.root')

			f = open(sub_ds+'.txt','w')
			for l in ds_files:
				f.write(l+'\n')
			f.close()

		os.chdir('..')
	os.chdir(workdir)


