import ROOT
import numpy as np
import os,sys,math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from options_file import *
import argparse

def SaveObjs(inputfile,outfile,isnominal=False):
	print("Reading file %s "%inputfile.GetName())
	# Loop over all the objects in the file in the form of TKeys
	for Obj in inputfile.GetListOfKeys():
		print(Obj, Obj.GetName())
		if "emu" in Obj.GetName():
			outfile.cd()
			directory=outfile.Get(Obj.GetName())
			if not directory:
				# The dir doesnt exist in the outfile, make it
				outfile.mkdir(Obj.GetName())
				directory=outfile.Get(Obj.GetName())
			directory.cd()
			for key1 in inputfile.Get(Obj.GetName()).GetListOfKeys():
				# Loop over the dir inside
				if not "boot_strap" in key1.GetName() and not isnominal:
					continue
				if "bin_mean" in key1.GetName():
					# Have more folders to loop over
					directory=outfile.Get(Obj.GetName()+"/"+key1.GetName())
					if not directory:
						# The dir doesnt exist in the outfile, make it
						outfile.mkdir(Obj.GetName()+"/"+key1.GetName())
						directory=outfile.Get(Obj.GetName()+"/"+key1.GetName())
					directory.cd()
					for key2 in inputfile.Get(Obj.GetName()+"/"+key1.GetName()).GetListOfKeys():
						obj=inputfile.Get(Obj.GetName()+"/"+key1.GetName()+"/"+key2.GetName())
						obj.Write()
				elif not outfile.Get(Obj.GetName()+"/"+key1.GetName()):
					outfile.Get(Obj.GetName()).cd()
					obj=inputfile.Get(Obj.GetName()+"/"+key1.GetName())
					obj.Write()
		else:
			outfile.cd()
			if not isnominal:
				continue
			if not outfile.Get(Obj.GetName()):
				# If the file doesnt exist in outfile, write to it
				obj=inputfile.Get(Obj.GetName())
				obj.Write()

def saveBoot(infile,outfile,process):
    for bin in range(0,10):
        sfile=infile.replace("combination","bootStrapBin%d_combination"%(bin+1))
        sfile=sfile.replace(process+"/","%s_bootStrap_bin%d/"%(process,bin+1))
        in_file=ROOT.TFile(sfile, "read")
        SaveObjs(in_file,outfile)
        in_file.Close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Processes the final selection and plots some cuts.')
    parser.add_argument('-o',dest="output",
                        help='Output file')
    parser.add_argument('-f',dest="ftm",
                        help='Files to merge')
    parser.add_argument('-p',dest="process",default="process",
                        help='Systematic name (default: nominal)')

    # Parse arguments
    args = parser.parse_args()
    outputfile=ROOT.TFile(args.output, "RECREATE")
    print("Outputfile: " + str(outputfile))
    nominal_file=ROOT.TFile(args.ftm, "READ")
    print("Nominal file representing the files to merge: " + str(nominal_file))
    outputfile.cd()
    print("Going into SaveObjs method ... ")

    # Done the merging
    SaveObjs(nominal_file,outputfile,True)
    nominal_file.Close()
    saveBoot(args.ftm,outputfile,args.process)
    outputfile.Close()
