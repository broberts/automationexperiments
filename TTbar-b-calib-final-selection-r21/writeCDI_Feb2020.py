import ROOT
import os
import math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse
from options_file import *
m_lj_cut=False

taggers=["DL1r", "DL1"]#, "MV2c10mu", "MV2c10rnn","DL1mu","DL1rnn"]
cuts=["FixedCutBEff"]
workingpoints=["60", "70", "77", "85"]
#be carefull if you change this! do you really use the new one? Change it in the TSelectors!!
cdi_input_name="2019-21-13TeV-MC16-CDI-2019-10-07_v1.root"
cdi_input_path="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/"
workdir=options.plot_dir+"FitPlots_3SB/"
#outdir=options.output_dir
outdir="./"+"cdi-input-files/d15161718/cdi-input-files-combined_fit/"
version="mc16ade_v1.0"
if m_lj_cut:
    outdir="./"+"cdi-input-files/d1516/cdi-input-files-m_lj_cut/"
    workdir=options.plot_dir+"FitPlots/"
    version="mc16ad_v2.0"
pt_binedges=["20","30","40","60","85","110","140","175","250","600"]
nbins = len(pt_binedges)
dataName=options.data_name

#parser = argparse.ArgumentParser(
#    description='Writes the scale factors and relative uncetainties into .txt files for the CDI file')
#parser.add_argument('-t',"--threshold",default="0.0",
#        help='Sets the threshold at which relative systematic uncertainties are dropped.')
#args = parser.parse_args()
#this does not work!
#if args.threshold:
#    threshold = args.threshold
anna_top_names_to_replace={
    "pileup" : "PRW_DATASF",
    "jvt" : "JET_JvtEfficiency",
    "leptonSF_MU_SF_Trigger_STAT" : "MUON_EFF_TrigStatUncertainty",
    "leptonSF_MU_SF_Trigger_SYST" : "MUON_EFF_TrigSystUncertainty",
    "leptonSF_MU_SF_ID_STAT" : "MUON_EFF_RECO_STAT",
    "leptonSF_MU_SF_ID_SYST" : "MUON_EFF_RECO_SYS",
    "leptonSF_MU_SF_ID_STAT_LOWPT" : "MUON_EFF_RECO_STAT_LOWPT",
    "leptonSF_MU_SF_ID_SYST_LOWPT" : "MUON_EFF_RECO_SYS_LOWPT",
    "leptonSF_MU_SF_TTVA_STAT" : "MUON_EFF_TTVA_STAT",
    "leptonSF_MU_SF_TTVA_SYST" : "MUON_EFF_TTVA_SYS",
    "leptonSF_MU_SF_Isol_STAT" : "MUON_EFF_ISO_STAT",
    "leptonSF_MU_SF_Isol_SYST" : "MUON_EFF_ISO_SYS",
    "leptonSF_EL_SF_Trigger" : "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
    "leptonSF_EL_SF_Reco" : "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
    "leptonSF_EL_SF_ID" : "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
    "leptonSF_EL_SF_Isol" : "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
    "hdamp_mc_rad" : "FT_EFF_ttbar_PhPy8Rad",
    "MC_stat_nominal" : "FT_EFF_MC_stat_nominal",
    "mc_fsr" : "FT_EFF_ttbar_FSR",
    "misstagLight_up" : "FT_EFF_misstagLight_up",
    "clTestMoMc_nstat" : "FT_EFF_closure",
    "correctFakes" : "FT_EFF_correctFakes",
}
threshold = -1 # 0.0005
#  KEY: TDirectoryFile	stat;1	stat
#   KEY: TDirectoryFile	unc_summary_plots;1	unc_summary_plots
#   KEY: TDirectoryFile	results_comulative;1	results_comulative
#   KEY: TDirectoryFile	results_continuous;1	results_continuous
#   KEY: TDirectoryFile	bin_mean_estimation;1	bin_mean_estimation
#   KEY: TDirectoryFile	other_systematics;1	other_systematics
#   KEY: TDirectoryFile	FT_EFF_Eigen_C_systematics;1	FT_EFF_Eigen_C_systematics
#   KEY: TDirectoryFile	FT_EFF_Eigen_Light_systematics;1	FT_EFF_Eigen_Light_systematics
#   KEY: TDirectoryFile	JET_JES_systematics;1	JET_JES_systematics
#   KEY: TDirectoryFile	JET_JER_systematics;1	JET_JER_systematics
#   KEY: TDirectoryFile	ttbar_pdf_systematics;1	ttbar_pdf_systematics
#   KEY: TDirectoryFile	singletop_pdf_systematics;1	singletop_pdf_systematics
#   KEY: TDirectoryFile	Zjets_systematics;1	Zjets_systematics
#   KEY: TDirectoryFile	Diboson_systematics;1	Diboson_systematics
#   KEY: TDirectoryFile	ttbar_systematics;1	ttbar_systematics
#   KEY: TDirectoryFile	singletop_systematics;1	singletop_systematics
unc_directorys=[
    "other_systematics",
    "FT_EFF_Eigen_C_systematics",
    "FT_EFF_Eigen_Light_systematics",
    "JET_JES_systematics",
    "JET_JER_systematics",
    # "Zjets_systematics",
    "Diboson_systematics",
    "ttbar_systematics",
    "singletop_systematics",
]

for t in taggers:
    for c in cuts:
        inputFilename = workdir+"FinalFitPlots_r21_"+dataName+"_"+t+"_"+c+"_emu_OS_J2.root"
        if m_lj_cut:
            inputFilename = workdir+"FinalFitPlots_r21_"+dataName+"_"+t+"_"+c+"_emu_OS_J2_m_lj_cut_fconst.root"
        if not os.path.isfile(inputFilename):
            print "File "+inputFilename+" not found!"
            break
        inputFile=ROOT.TFile(inputFilename,"read")
        keyList=inputFile.GetListOfKeys()
        for wp in workingpoints:
            outputFilename = "btag_ttbarPDF_"+version+"_21-2-93_"+t+"_"+c+"_"+wp+".txt"
            outputFile = open(outdir + outputFilename, "w")
            cutvalue = c+"_"+wp+"_"+cdi_input_name.replace(".root","")
            if c== "FixedCutBEff":
                cdiFile=ROOT.TFile(cdi_input_path+cdi_input_name,"read")
                cutval=cdiFile.Get(t+"/AntiKt4EMPFlowJets_BTagging201903/FixedCutBEff_"+wp+"/cutvalue")
                cutvalue=cutval.Max()
                #if (wp == "85"):
                #    cutvalue = 0.175847
                #elif (wp == "77"):
                #    cutvalue = 0.4
                #elif (wp == "70"):
                #    cutvalue = 0.6
                #else:
                #    cutvalue = 0.8
            CDIstring = "Analysis(ttbar_PDF,bottom,"+t+","+c+"_"+wp+",AntiKt4EMPFlowJets_BTagging201903){\n\n"
            CDIstring += "\tmeta_data_s (Hadronization, Pythia8EvtGen)\n"
            CDIstring += "\tmeta_data_s (OperatingPoint, "+str(cutvalue)+")\n\n"
            for ibin in range(0, nbins-1):
                CDIstring += "\tbin("+pt_binedges[ibin]+"<pt<"+pt_binedges[ibin+1]+",0<abseta<2.5)\n"
                CDIstring += "\t{\n"
                TotalRelativeSysError = 0.
                TotalRelativeStatError = 0.
                stat_string = ""
                sys_string = ""
                for unc_folder in unc_directorys:
                    sys_dir=inputFile.Get(unc_folder+"/e_b_wp_"+wp)
                    sys_keyList=sys_dir.GetListOfKeys()
                    for ikey in xrange(1, sys_keyList.GetSize()):
                        obj=sys_dir.Get(sys_keyList.At(ikey).GetName())
                        className=obj.ClassName()
                        if className[:2]=="TH":
                            hist_name=obj.GetName();
                            if hist_name.find("syst_Error_rel")>0 and hist_name.find(wp)>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
                                #print "Reading syst histogam:\t"+hist_name
                                hist_sys = sys_dir.Get(hist_name)
                                sys_value = hist_sys.GetBinContent(ibin+1)
                                if (abs(sys_value) < threshold):
                                    print "Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold)
                                    continue
                                TotalRelativeSysError += math.pow(sys_value,2)
                                print "Added:\t"+hist_name, str(round(100*sys_value,2))+"%)" , "current value: " ,str(round(100*math.sqrt(TotalRelativeSysError),2))+"%)"
                                syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("e_b_"+wp+"_","")
                                syst_name_anaTop=syst_name_anaTop.replace("JET_21NP_","").replace("CategoryReduction_JET_","").replace("FTAG2_","FT_EFF_")
                                syst_name_cp=syst_name_anaTop.replace("weight_","")
                                if syst_name_cp in anna_top_names_to_replace:
                                    syst_name_cp=anna_top_names_to_replace[syst_name_cp]
                                sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,2))+"%)\n"

                pdf_sys_dir=inputFile.Get("ttbar_pdf_systematics/e_b_wp_"+wp)
                pdf_sys_keyList=pdf_sys_dir.GetListOfKeys()
                for ikey in xrange(1, pdf_sys_keyList.GetSize()):
                    obj=pdf_sys_dir.Get(pdf_sys_keyList.At(ikey).GetName())
                    className=obj.ClassName()
                    if className[:2]=="TH":
                        hist_name=obj.GetName();
                        if hist_name.find("syst_Error_rel")>0 and hist_name.find(wp)>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
                            print "Reading pdf syst histogam:\t"+hist_name
                            hist_sys = pdf_sys_dir.Get(hist_name)
                            sys_value = hist_sys.GetBinContent(ibin+1)
                            if (abs(sys_value) < threshold):
                                print "Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold)
                                continue
                            TotalRelativeSysError += math.pow(sys_value,2)
                            syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("e_b_"+wp+"_","")
                            syst_name_anaTop=syst_name_anaTop.replace("mc_shower","PDF4LHC")
                            syst_name_cp=syst_name_anaTop.replace("ttbar_weight_","")
                            np=int(syst_name_cp.replace("weight_PDF4LHC_np_",""))
                            if np == 111:
                                continue
                            syst_name_cp="FT_EFF_ttbar_PDF4LHC_np_"+str(np-111)
                            sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,2))+"%)\n"


#                pdf_sys_dir=inputFile.Get("singletop_pdf_systematics/e_b_wp_"+wp)
#                pdf_sys_keyList=pdf_sys_dir.GetListOfKeys()
#                for ikey in xrange(1, pdf_sys_keyList.GetSize()):
#                    obj=pdf_sys_dir.Get(pdf_sys_keyList.At(ikey).GetName())
#                    className=obj.ClassName()
#                    if className[:2]=="TH":
#                        hist_name=obj.GetName();
#                        if hist_name.find("syst_Error_rel")>0 and hist_name.find(wp)>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
#                            print "Reading pdf syst histogam:\t"+hist_name
#                            hist_sys = pdf_sys_dir.Get(hist_name)
#                            sys_value = hist_sys.GetBinContent(ibin+1)
#                            if (abs(sys_value) < threshold):
#                                print "Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold)
#                                continue
#                            TotalRelativeSysError += math.pow(sys_value,2)
#                            syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("e_b_"+wp+"_","")
#                            syst_name_anaTop=syst_name_anaTop.replace("mc_shower","PDF4LHC")
#                            syst_name_cp=syst_name_anaTop.replace("singletop_weight_","")
#                            np=int(syst_name_cp.replace("PDF4LHC_np_",""))
#                            syst_name_cp="FT_EFF_singletop_PDF4LHC_np_"+str(np-111)
#                            sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,2))+"%)\n"

                stat_dir=inputFile.Get("stat/stats_wp_"+wp)
                stat_keyList=stat_dir.GetListOfKeys()
                for ikey in xrange(1, stat_keyList.GetSize()):
                    obj=stat_dir.Get(stat_keyList.At(ikey).GetName())
                    className=obj.ClassName()
                    if className[:2]=="TH":
                        hist_name=obj.GetName();
                        if hist_name.find("stat_np")>0 and hist_name.find("Error_rel")>0 and hist_name.find(wp)>0:
                            print "Reading stat histogam:\t"+hist_name
                            hist_stat = stat_dir.Get(hist_name)
                            stat_value = hist_stat.GetBinContent(ibin+1)
                            if (abs(stat_value) < threshold):
                                print "Dropping this statistical error, because its absolute value "+str(abs(stat_value))+" is smaller than threshold "+str(threshold)
                                continue
                            TotalRelativeStatError += math.pow(stat_value,2)
                            sys_string += "\t\tsys("+(hist_name.replace("_Error_rel","")).replace("e_b_"+wp+"_","FT_EFF_"+"e_b_"+wp+"_")+","+str(round(100*stat_value,2))+"%)\n"
                #Get SF here
                string_nominal = "sf_b_"+wp+"_Postfit"
                hist_nominal = inputFile.Get("results_comulative/"+string_nominal)
                central_value = hist_nominal.GetBinContent(ibin+1)
                totalstat_value = hist_nominal.GetBinError(ibin+1)
                totalstat_value /= central_value
                string_totalsys = "sf_b_"+wp+"_syst_Error_combined_rel"
                hist_totalsys = inputFile.Get("results_comulative/"+string_totalsys)
                totalsys_value = hist_totalsys.GetBinContent(ibin+1)
                totalerr_value = math.sqrt(math.pow(totalsys_value,2) + math.pow(totalstat_value,2))
                TotalRelativeError = math.sqrt(TotalRelativeSysError + TotalRelativeStatError)
                TotalRelativeSysError = math.sqrt(TotalRelativeSysError)
                TotalRelativeStatError = math.sqrt(TotalRelativeStatError)
                print "Summed up total relative syst. error for bin "+str(ibin)+":\t"+str(100.*TotalRelativeSysError)+"%"
                print "Summed up total relative stat. error for bin "+str(ibin)+":\t"+str(100.*TotalRelativeStatError)+"%"
                print "Total error for bin "+str(ibin)+":\t"+str(100.*TotalRelativeError)+"%"
                print "Cross check: reading total relative syst. error for bin "+str(ibin)+" from file: "+str(100.*totalsys_value)+"%"
                print "Cross check: reading total relative stat. error for bin "+str(ibin)+" from file: "+str(100.*totalstat_value)+"%"
                print "Cross check: reading total relative error for bin "+str(ibin)+" from file: "+str(100.*totalerr_value)+"%"
                #CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(round(100.*TotalRelativeStatError,2))+"%)\n"
                #CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(-999)+")\n"
                CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(0)+")\n"
                # CDIstring += "\t\tmeta_data(N_jets total,-999,-999)\n"
                # CDIstring += "\t\tmeta_data(N_jets tagged,-999,-999)\n"
                CDIstring += sys_string
                CDIstring += "\t}\n"
            CDIstring += "}"
            print "Writing to file "+outputFilename
            outputFile.write(CDIstring)
            outputFile.close()
        inputFile.Close()
        print "File "+inputFilename+" closed."
print "Done"
