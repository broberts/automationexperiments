import ROOT
import os
import glob
from options_file import *
import argparse

# Write the folder you want to create here
folderToCreate = options.version_tag

parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts.')
parser.add_argument('--combine_datasets',action='store_true',
                    help='Want to create a dir to run over all your data not just one year!')
parser.add_argument('--campaign',action='store',dest="campaign",default="mc16a",
                    help='Campaign to create')

args = parser.parse_args()

if args.combine_datasets:
    args.campaign="all"

folderToCreate=folderToCreate
#+"_"+args.campaign

print(folderToCreate)

nominal={
    'FTAG2_Zjets_Sherpa221' : ["*36410*Sherpa*","*36411*Sherpa*","*36412*Sherpa*","*36413*Sherpa*","*364140*Sherpa*","*364141*Sherpa*","*364198*Sherpa*","*364199*Sherpa*","*36420*Sherpa*","*36421*Sherpa*"],
    'FTAG2_Zjets_PowPy8' : ["*361106*","*361107*","*361108*"],
    'FTAG2_Zjets_MGPy8' : [".*3631*","*.36151*"],
    'FTAG2_Wjets_Sherpa221' : ["*36415*Sherpa*","*36416*Sherpa*","*36417*Sherpa*","*36418*Sherpa*","*36419*Sherpa*"],
    'FTAG2_singletop_PowPy8' : ["*410646*","*410647*","*410644*","*410645*","*410658*","*410659*"],
    'FTAG2_singletop_aMcPy8' : ["*412005*","*412002*"],
    'FTAG2_singletop_PowHW7' : ["*411034*","*411035*","*411036*","*411037*"],
    'FTAG2_singletop_PowPy8_DS' : ["*410654*","*410655*"],
    'FTAG2_Diboson_Sherpa222' : ["*364250*","*364253*","*364254*","*36335*","*363360*","*363489*","*364255*","*36428*","*364290*","*345705*","*345706*","*345715*","*345723*","*364302*","*364304*","*364305*"],
    'FTAG2_Diboson_PowPy8' : ["*.3616*"],
    'FTAG2_ttbar_PhPy8' : ["*410472.PhPy8EG.*_s3126*"],
    'FTAG2_ttbar_PhPy8_AF2' : ["*410472.PhPy8EG.*_a875*"],
    'FTAG2_ttbar_PhPy8_hdamp3mtop' : ["*410482.*"],
    'FTAG2_ttbar_Sherpa221' : ["*410250.*","*410251.*","*410252.*"],
    'FTAG2_ttbar_aMcPy8' : ["*410465.*"],
    'FTAG2_ttbar_PowHW7' : ["*410558.*"],
    'data1516' : ["*grp15*","*grp16*"],
    'data17' : ["*grp17*"],
    'data18' : ["*grp18*"]
}

if not args.combine_datasets:
    if options.data_year == "1516":
    	input_container="/eos/home-j/jhall/BTagging/Productions/October19_ntuples/mc16a/"
        mctag="r9364"
    elif options.data_year == "17":
        input_container="/eos/home-j/jhall/BTagging/Productions/October19_ntuples/mc16d/"
        mctag="r10201"
    elif options.data_year == "18":
        input_container="/eos/home-j/jhall/BTagging/Productions/October19_ntuples/mc16e/"
        mctag="r10724"
else:
    for name, folders in nominal.items():
        if name == "data1516":
            datat1516_folders = nominal.pop("data1516", None)
        elif name == "data17":
            datat17_folders = nominal.pop("data17", None)
        elif name == "data18":
            datat18_folders = nominal.pop("data18", None)
    combined_data = datat1516_folders + datat17_folders + datat18_folders
    nominal[options.data_name] = combined_data
    input_container_1516="/eos/home-j/jhall/BTagging/Productions/October19_ntuples/mc16a/"
    input_container_17="/eos/home-j/jhall/BTagging/Productions/October19_ntuples/mc16d/"
    input_container_18="/eos/home-j/jhall/BTagging/Productions/October19_ntuples/mc16e/"


os.system("mkdir -p %s "%(folderToCreate))

for name,folders in nominal.items():
    os.system("mkdir -p %s "%(folderToCreate+"/"+name))
    globbedFolders=[]
    for folder in folders:
        if not args.combine_datasets:
            globbedFolders.extend(glob.glob(input_container+"/"+folder))
        else:
            globbedFolders.extend(glob.glob(input_container_1516+"/"+folder))
            globbedFolders.extend(glob.glob(input_container_17+"/"+folder))
            globbedFolders.extend(glob.glob(input_container_18+"/"+folder))
    print("Sample %s has the following files "%name)
    fileIn=open(os.getcwd()+"/"+folderToCreate+"/"+name+".txt",'w')
    for myfile in globbedFolders:
        if not "physics_Main" in myfile:
            if args.combine_datasets:
                print("Passes here")
                # continue
            else:
                if not args.campaign in myfile:
#                    print("Passes here 3")
                    continue
        print("     %s"%myfile)
        fileIn.write(myfile+"\n")
        fileIn2=open(os.getcwd()+"/"+folderToCreate+"/"+name+"/"+myfile.split('/')[-1]+".txt",'w')
        listofFiles=glob.glob(myfile+"/*")
        for myfile2 in listofFiles:
            fileIn2.write(myfile2+"\n")
        fileIn2.close()
    fileIn.close()
