#! /usr/bin/env python
#from XAMPPbase.ListGroupDisk import *

import os, commands, sys, argparse, re
import errno, subprocess

RSE = 'CERN-PROD_PERF-FLAVTAG'
if os.getenv("RUCIO_RSE"):
    RSE = os.getenv("RUCIO_RSE")

def prettyPrint(preamble, data, width=30, separator=":"):
    """Prints uniformly-formatted lines of the type "preamble : data".
    """
    preamble = preamble.ljust(width)
    print '%s%s %s' % (preamble, separator, data)

def GetDataSetFiles (dsname , options):
    RSE = options.RSE
    prettyPrint("Get the Dataset Files for", dsname)
    prettyPrint("on RSE", RSE)
    print "rucio list-file-replicas --rse %s %s " % (RSE, dsname)
    DSReplicas = commands.getoutput("rucio list-file-replicas --rse %s %s " % (RSE, dsname))
    prettyPrint("commandOut: ", DSReplicas)
    DS = []
    for d in DSReplicas.split():
        if "srm://grid-srm.rzg.mpg.de" in d:
            Entry = d.replace(":8443/srm/managerv2?SFN=", "")
            Entry = Entry.replace("srm://" , "dcap://")
            #print Entry
            DS.append(Entry)
        elif "https://grid-dav.rzg.mpg.de:2880/" in d:
            Entry = d.replace("https://grid-dav.rzg.mpg.de:2880/", "dcap://grid-srm.rzg.mpg.de/pnfs/rzg.mpg.de/data/")
            #print Entry
            DS.append(Entry)
        elif "srm://dcache-se-atlas.desy.de" in d:
            Entry = d.replace("srm://dcache-se-atlas.desy.de:8443/srm/managerv2?SFN=", "")
            #print Entry
            DS.append(Entry)
        elif "davs://dcache-atlas-webdav.desy.de:2880/" in d:
            Entry = d.replace("davs://dcache-atlas-webdav.desy.de:2880/", "/pnfs/desy.de/atlas/")
            if (options.AR):
                Entry = "root://dcache-atlas-xrootd.desy.de:1094" + Entry
            #print Entry
            DS.append(Entry)
        elif "root://eosatlas.cern.ch:1094/" in d:
            Entry = d.replace("root://eosatlas.cern.ch:1094/","")
            DS.append(Entry)
    return DS

def GetScopes():
    print "Reading in the scopes:"
    Scopes = commands.getoutput("rucio list-scopes")
    ScopeList = []
    for Entry in Scopes.split():
        if ("user." not in Entry and "group" not in Entry):
            ScopeList.append(Entry + ":")
    ScopeList.append ("user." + RUCIO_ACC)
    print "Done found %d scopes" % (len (ScopeList))
    return ScopeList

def createFileList(dsname, options):
    prettyPrint('Creating file list for', dsname)
    DS = GetDataSetFiles(dsname, options)
    if len(DS) == 0:
        print "No datasets found"
        return
    if dsname.find(":") > -1:
       dsname = dsname [ dsname.find(":") + 1 : len(dsname)]
    if os.path.exists(OutDir) == False:
        print "mkdir -p " + OutDir
        os.system ("mkdir -p " + OutDir)
    filelistname = OutDir + "/" + dsname.rstrip('/') + ".txt"
    if os.path.exists(filelistname) == True:
        print "Remove the old FileList"
        os.system ("rm " + filelistname)
    for fname in DS:
        os.system("echo \"%s\" >> %s" % (fname, filelistname))

if __name__ == '__main__':


    parser = argparse.ArgumentParser(description='This script lists datasets located at a RSE location. Futher patterns to find or exclude can be specified.', prog='ListGroupDisk', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '-S', '--FLsuffix', help='File list suffix', default='')
    parser.add_argument('-d', '-l', '-D', '-L', '--dataset', help='Dataset to collect data for, or filename containing list of datasets', default='')
    parser.add_argument('-o', '-O', '--OutDir', help='specify output directory to put file list(s) into')
    parser.add_argument('-r', '-R', '--RSE', help='specify RSE storage element which should be read (default: %s)' % RSE, default=RSE)
    parser.add_argument('-a', '-A', '--AR', action='store_true', help='option for accessing files on the DESY-HH_LOCALGROUPDISK from a remote workplace. (default: false)')
    parser.add_argument('input_files',nargs='*',
                        help='input files')
    RunOptions = parser.parse_args()

    incompleteJobs = []
    transferIssues = []
    if RunOptions.input_files:
        for input_file in RunOptions.input_files:
            inpath, inFileName = os.path.split(input_file)
            sample_name=inFileName[:-4]
            if RunOptions.OutDir:
                OutDir=RunOptions.OutDir
            else:
                OutDir=inpath
            OutDir=OutDir+"/"+sample_name
            RunOptions.dataset=input_file
            dsfile = open(input_file)
            for line in dsfile:
                # Ignore comment lines and empty lines
                if line.startswith('#'): continue
                realline = line.strip()
                if realline.find ("_tid") > -1: realline = realline [0 :realline.find ("_tid") ]
                if not realline: continue # Ignore whitespace
                createFileList(realline, RunOptions)


        # Do we have one dataset, or a file with a list of them?
    elif os.path.exists(RunOptions.dataset):
        dsfile = open(RunOptions.dataset)
        inpath, inFileName = os.path.split(RunOptions.dataset)
        if RunOptions.OutDir:
            OutDir=RunOptions.OutDir
        else:
            OutDir=inpath
        OutDir=OutDir+"/"+inFileName[:-4]
        for line in dsfile:
            # Ignore comment lines and empty lines
            if line.startswith('#'): continue
            realline = line.strip()
            if realline.find ("_tid") > -1: realline = realline [0 :realline.find ("_tid") ]
            if not realline: continue # Ignore whitespace
            createFileList(realline, RunOptions)
    else:
        createFileList(RunOptions.dataset, RunOptions)

    # Summary output if there are any problems
    print
    print 'Finished making file lists'
    if incompleteJobs:
        print 'The following datasets are not yet complete'
        for ds in incompleteJobs:
            print ds
    else:
        print 'All jobs are complete. Well done!'
        print "Please: "
        print "1. append this dataset to your sample list as follows: "
        print "echo \"%s\" >> XAMPPbase/data/SampleLists/<path/file>"%(RunOptions.dataset)
        print "2. put your samples on SVN"

    if transferIssues:
        print
        print 'The following datasets are not transferring.'
        print 'Check that subscriptions are coming, you may have to manually request the transfer.'
        print 'Check the status here: http://panda.cern.ch/server/pandamon/query?mode=ddm_pathenareq&action=List'
        print 'Request (if necessary) here: http://panda.cern.ch/server/pandamon/query?mode=ddm_req'
        for ds in transferIssues:
            print ds
