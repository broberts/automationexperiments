import ROOT
import os
import math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
from options_file import *

flavors=['bb','bl','lb','ll']
c_emu_OS_J2=channel('emu_OS_J2')
channels=[c_emu_OS_J2,channel('emu_OS_J2_m_lj_cut')]
taggers=["MV2c10","DL1",]#"DL1mu","DL1rnn","MV2c10rnn","MV2c10mu","MV2c10Flip_MV2c10"]
workingpoints=["FixedCutBEff"]
ttbar_file_to_create_model="ttbar_Sherpa221"
ttbar_samples=options.ttbar_syst_samples[:]+options.ttbar_rad_samples[:]+options.ttbar_fsr_samples[:]
ttbar_samples.append(options.ttb_sample)
for this_ttbar_sample in ttbar_samples:
    if this_ttbar_sample.systematic_name== "ttbar":
        this_ttbar_sample.systematic_name="nominal"
    inputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+this_ttbar_sample.systematic_name+".root"
    inputfile=ROOT.TFile(inputfile_name, "read")
    if not inputfile:
        print "Inputfile: ",inputfile_name, "not found!"
    else:
        print "Inputfile opend! : ",inputfile_name
    inputfile_name_c=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+ttbar_file_to_create_model+".root"
    inputfile_model=ROOT.TFile(inputfile_name_c, "read")
    print "Inputfile Model: ",inputfile_name_c
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+this_ttbar_sample.systematic_name+"_stressTestMoMc_"+ttbar_file_to_create_model+".root"
    outputfile=ROOT.TFile(ouputfile_name,"recreate")
    print "Outputfile: ",ouputfile_name,
    hf4_nominal={}
    hf4_inputfile_model={}
    hf4_Ncr_nominal={}
    hf4_Ncr_inputfile_model={}

    for c in channels:
      for tagger in taggers:
        for workingpoint in workingpoints:
            combined_tagger_name=c.name+"_hist_for_fit_"+tagger+"_"+workingpoint
            hff_l=inputfile.Get("hff_"+"MC_combined_" + combined_tagger_name + "_light")
            print "loaded:", "hff_"+"MC_combined_" + combined_tagger_name + "_light"
            hff_b=inputfile.Get("hff_"+"MC_combined_" + combined_tagger_name + "_b")
            outputfile.cd()
            hff_l.Write()
            hff_b.Write()
            for flav in flavors:
                hf4_nominal[flav] =inputfile.Get("hf4_MC_combined_" + combined_tagger_name + "_" + flav)
                hf4_nominal[flav].Write()
                hf4_inputfile_model[flav] =inputfile_model.Get("hf4_MC_combined_" + combined_tagger_name + "_" + flav)
                if c==c_emu_OS_J2:
                    hf4_Ncr_nominal[flav] =inputfile.Get("hf4_Ncr_MC_combined_" + combined_tagger_name + "_" + flav)
                    hf4_Ncr_nominal[flav].Write()
                    hf4_Ncr_inputfile_model[flav] =inputfile_model.Get("hf4_Ncr_MC_combined_" + combined_tagger_name + "_" + flav)



            #lets take ttb values from inputfile_model -> so sf should be 1 afterwards.
            hff_l_ttb=inputfile_model.Get("hff_"+"MC_ttb_" + combined_tagger_name + "_light")
            hff_b_ttb=inputfile_model.Get("hff_"+"MC_ttb_" + combined_tagger_name + "_b")
            hff_l_ttb.Write()
            hff_b_ttb.Write()
            hff_l_ttb=inputfile_model.Get("hff_"+"MC_ttb_nominal_" + combined_tagger_name + "_light")
            hff_b_ttb=inputfile_model.Get("hff_"+"MC_ttb_nominal_" + combined_tagger_name + "_b")
            hff_l_ttb.Write()
            hff_b_ttb.Write()

            data=hf4_inputfile_model["bb"].Clone("hf4_"+"data_" + combined_tagger_name)
            data.Add(hf4_inputfile_model["bl"])
            data.Add(hf4_inputfile_model["lb"])
            data.Add(hf4_inputfile_model["ll"])
            data.Write()
            if c==c_emu_OS_J2:
                data_Ncr=hf4_Ncr_inputfile_model["bb"].Clone("hf4_Ncr_"+"data_" + combined_tagger_name)
                data_Ncr.Add(hf4_Ncr_inputfile_model["bl"])
                data_Ncr.Add(hf4_Ncr_inputfile_model["lb"])
                data_Ncr.Add(hf4_Ncr_inputfile_model["ll"])
                data_Ncr.Write()


#now  lets crate momo data:
inputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"
inputfile=ROOT.TFile(inputfile_name, "read")
if not inputfile:
    print "Inputfile: ",inputfile_name, "not found!"
else:
    print "Inputfile opend! : ",inputfile_name

ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"clTestMoMo"+".root"
outputfile=ROOT.TFile(ouputfile_name,"recreate")
print "Outputfile: ",ouputfile_name,
for c in channels:
  for tagger in taggers:
    for workingpoint in workingpoints:
        combined_tagger_name=c.name+"_hist_for_fit_"+tagger+"_"+workingpoint
        hff_l=inputfile.Get("hff_"+"MC_combined_" + combined_tagger_name + "_light")
        hff_b=inputfile.Get("hff_"+"MC_combined_" + combined_tagger_name + "_b")
        hff_l_ttb=hff_l.Clone("hff_"+"MC_ttb_" + combined_tagger_name + "_light")
        hff_b_ttb=hff_b.Clone("hff_"+"MC_ttb_" + combined_tagger_name + "_b") #let the sf be 1!
        hf4_nominal={}
        hf4_Ncr_nominal={}
        P_xx={}
        n_tot=0
        for flav in flavors:
            hf4_nominal[flav] =inputfile.Get("hf4_MC_combined_" + combined_tagger_name + "_" + flav)
            if c==c_emu_OS_J2:
                hf4_Ncr_nominal[flav] =inputfile.Get("hf4_Ncr_MC_combined_" + combined_tagger_name + "_" + flav)
                hf4_Ncr_nominal[flav].Write()
            hf4_xx_32=hf4_nominal[flav].Projection(3,2)
            P_xx[flav]=hf4_xx_32.Clone("h_P_"+flav)
            n_tot=n_tot+hf4_xx_32.Integral()



        outputfile.cd()
        hff_l.Write()
        hff_b.Write()
        hff_l_ttb.Write()
        hff_b_ttb.Write()
        p_b=hff_b.Clone("h_p_b")
        p_l=hff_l.Clone("h_p_l")


        for flav in flavors:
            n_xx=P_xx[flav].Integral()
            print "n_",flav,n_xx
            f_xx=n_xx/n_tot
            n_pt_bins=P_xx[flav].GetXaxis().GetNbins()
            n_w_bins=hf4_nominal[flav].GetAxis(0).GetNbins()
            print "n_w_bins: ",n_w_bins
            for p1 in xrange(1,n_pt_bins+1):
                for p2 in xrange(1,n_pt_bins+1):
                    print "P_xx (",p1, " , ", p2," ): ", P_xx[flav].GetBinContent(p1,p2)
                    for w1 in xrange (1,n_w_bins+1):
                        for w2 in xrange (1,n_w_bins+1):
                            if flav=="bb":
                                p_xx_p1_p2=p_b.GetBinContent(w1,p1)*p_b.GetBinContent(w2,p2)/p_b.Integral(1,n_w_bins+1,p1,p1)/p_b.Integral(1,n_w_bins+1,p2,p2)
                            elif flav=="bl":
                                p_xx_p1_p2=p_b.GetBinContent(w1,p1)*p_l.GetBinContent(w2,p2)/p_b.Integral(1,n_w_bins+1,p1,p1)/p_l.Integral(1,n_w_bins+1,p2,p2)
                            elif flav=="lb":
                                p_xx_p1_p2=p_l.GetBinContent(w1,p1)*p_b.GetBinContent(w2,p2)/p_l.Integral(1,n_w_bins+1,p1,p1)/p_b.Integral(1,n_w_bins+1,p2,p2)
                            elif flav=="ll":
                                p_xx_p1_p2=p_l.GetBinContent(w1,p1)*p_l.GetBinContent(w2,p2)/p_l.Integral(1,n_w_bins+1,p1,p1)/p_l.Integral(1,n_w_bins+1,p2,p2)
                            val=P_xx[flav].GetBinContent(p1,p2)*p_xx_p1_p2
                            #print "old:, ",hf4_bb.GetBinContent(array("i",[w1,w2,p1,p2]))
                            hf4_nominal[flav].SetBinContent(array("i",[w1,w2,p1,p2]),val)
                            hf4_nominal[flav].SetBinError(array("i",[w1,w2,p1,p2]),math.sqrt(val))
            hf4_nominal[flav].Write()
            if c==c_emu_OS_J2:
                hf4_Ncr_nominal[flav].Write()





        data=hf4_nominal["bb"].Clone("hf4_"+"data_" + combined_tagger_name)
        data.Add(hf4_nominal["bl"])
        data.Add(hf4_nominal["lb"])
        data.Add(hf4_nominal["ll"])
        data.Write()
        if c==c_emu_OS_J2:
            data_Ncr=hf4_Ncr_nominal["bb"].Clone("hf4_Ncr_"+"data_" + combined_tagger_name)
            data_Ncr.Add(hf4_Ncr_nominal["bl"])
            data_Ncr.Add(hf4_Ncr_nominal["lb"])
            data_Ncr.Add(hf4_Ncr_nominal["ll"])
            data_Ncr.Write()
