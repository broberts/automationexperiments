"""
Module with constnants and functions for plotting ATLAS things
"""

###-- Standard text for plots (configure per-analysis) --###

#LUMI_STRING = "#sqrt{s} = 13 TeV, 3.2 fb^{-1}"
LUMI_STRING = "#sqrt{s} = 13 TeV, 36.2 fb^{-1}"
ATLAS_PRELIM_SIM = "Simulation Preliminary"
ATLAS_PRELIM     = "Preliminary"

###-- Top group standard colour scheme --###
DY_COLOUR       = 95
ZEE_COLOUR      = 95
ZMUMU_COLOUR    = 95
ZTAUTAU_COLOUR  = 97
DIBOSON_COLOUR  = 5
ST_WT_COLOUR    = 62
ST_SCHAN_COLOUR = 61
ST_TCHAN_COLOUR = 60
TTBAR_COLOUR    = 0
TTH_COLOUR      = 2

LINE_WIDTH       = 3
LINE_STYLE       = 1

###--  TCanvas defaults --###
CANVAS_WIDTH                 = 840
CANVAS_HEIGHT_NORATIO        = 660
CANVAS_HEIGHT_WITHRATIO      = 825

MAINPAD_MARGIN_LEFT          = 0.16
MAINPAD_MARGIN_RIGHT         = 0.05
MAINPAD_MARGIN_TOP           = 0.05
MAINPAD_MARGIN_BOTTOM_RATIO  = 0.16
MAINPAD_MARGIN_BOTTOM        = 0.0
RATIOPAD_MARGIN_LEFT         = 0.16
RATIOPAD_MARGIN_RIGHT        = 0.05
RATIOPAD_MARGIN_TOP          = 0.0
RATIOPAD_MARGIN_BOTTOM       = 0.34
MAINPAD_XLOW                 = 0.0 
MAINPAD_XHIGH                = 1.0
MAINPAD_YLOW                 = 0.26
MAINPAD_YHIGH                = 1.0 
RATIOPAD_XLOW                = 0.0 
RATIOPAD_XHIGH               = 1.0
RATIOPAD_YLOW                = 0.0
RATIOPAD_YHIGH               = 0.24 

# LABELS
ATLASLABEL_TOPLEFT_X         = 0
ATLASLABEL_TOPLEFT_Y         = 0
ATLASLABEL_TOPLEFT_WIDTH     = 0
ATLASLABEL_TOPLEFT_HEGITH    = 0

ATLASLABEL_TOPRIGHT_X        = 0
ATLASLABEL_TOPRIGHT_Y        = 0
ATLASLABEL_TOPRIGHT_WIDTH    = 0
ATLASLABEL_TOPRIGHT_HEGITH   = 0

# LEGENDS
LEGEND_TOPLEFT_X             = 0.2
LEGEND_TOPLEFT_Y             = 0.70
LEGEND_TOPRIGHT_X            = 0.7
LEGEND_TOPRIGHT_Y            = 0.45
LEGEND_WIDTH                 = 0.30
LEGEND_HEIGTH                = 0.30

# TEXT SIZES
TEXT_SIZE                    = 0.036
XLABEL_SIZE                  = 0.045
YLABEL_SIZE                  = 0.045
ZLABEL_SIZE                  = 0.045

YTITLE_SIZE                  = 0.05
YTITLE_OFFSET                = 1.50
XTITLE_SIZE                  = 0.15
XTITLE_OFFSET                = 0.90

BLANK = 0
FONT = 42 

def atlasStyle():

    """ Function to set ATLAS style for ROOT plots """

    from ROOT import TStyle

    atlasStyle = TStyle("ATLAS","Atlas style")

    atlasStyle.SetFrameBorderMode(BLANK)
    atlasStyle.SetFrameFillColor(BLANK)
    atlasStyle.SetCanvasBorderMode(BLANK)
    atlasStyle.SetCanvasColor(BLANK)

    atlasStyle.SetCanvasDefW(CANVAS_WIDTH)
    atlasStyle.SetCanvasDefH(CANVAS_HEIGHT_WITHRATIO)
    atlasStyle.SetPadBorderMode(BLANK)
    atlasStyle.SetPadColor(BLANK)
    atlasStyle.SetStatColor(BLANK)
    atlasStyle.SetPaperSize(20, 26)

    #FIT IN Z AXIS LABEL
    atlasStyle.SetPadTopMargin(MAINPAD_MARGIN_TOP)
    atlasStyle.SetPadRightMargin(MAINPAD_MARGIN_RIGHT)
    atlasStyle.SetPadBottomMargin(MAINPAD_MARGIN_BOTTOM)
    atlasStyle.SetPadLeftMargin(MAINPAD_MARGIN_LEFT)
    atlasStyle.SetTitleXOffset(YTITLE_OFFSET )
    atlasStyle.SetTitleYOffset(YTITLE_OFFSET )
    atlasStyle.SetTextFont(FONT)
    atlasStyle.SetTextSize(TEXT_SIZE)
    atlasStyle.SetLabelFont(FONT,"x")
    atlasStyle.SetTitleFont(FONT,"x")
    atlasStyle.SetLabelFont(FONT,"y")
    atlasStyle.SetTitleFont(FONT,"y")
    atlasStyle.SetLabelFont(FONT,"z")
    atlasStyle.SetTitleFont(FONT,"z")
    
    atlasStyle.SetLabelSize(XLABEL_SIZE,"x")
    atlasStyle.SetTitleSize(YTITLE_SIZE,"x")

    atlasStyle.SetLabelSize(YLABEL_SIZE,"y")
    atlasStyle.SetTitleSize(YTITLE_SIZE,"y")

    atlasStyle.SetLabelSize(ZLABEL_SIZE,"z")
    atlasStyle.SetTitleSize(YTITLE_SIZE,"z")

    atlasStyle.SetMarkerStyle(20)
    atlasStyle.SetMarkerSize(1.2)
    atlasStyle.SetHistLineWidth(LINE_WIDTH)
    atlasStyle.SetLineStyleString(2,"[12 12]")
    atlasStyle.SetEndErrorSize(0.)
    atlasStyle.SetOptTitle(0)
    atlasStyle.SetOptStat(0)
    atlasStyle.SetOptFit(0)
    atlasStyle.SetPadTickX(1)
    atlasStyle.SetPadTickY(1)
    atlasStyle.SetErrorX(0.5)

    atlasStyle.SetPaintTextFormat("4.2f");
    atlasStyle.SetPalette(1)
    #atlasStyle.SetEndErrorSize( 4 )
    
    return atlasStyle


def make_ATLAS_string(x, y, string, size):

    """ Function to make an ATLAS luminosity TLatex Box """

    from ROOT import TLatex

    text = TLatex()
    text.SetNDC()
    text.SetTextColor(1)
    text.SetTextSize(size) 
    text.DrawLatex(x, y, string)


def make_ATLAS_label(x, y, color, pad, size, word):

    """ Function to return an ATLAS style TLatex Object """

    from ROOT import TLatex, TPad

    l = TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(color)
    l.SetTextSize(size)

    l.DrawLatex(x,y,"ATLAS");

    p = TLatex()
    p.SetNDC()
    p.SetTextFont(42)
    p.SetTextSize(size)
    diff = size*2.8
    p.DrawLatex(x+diff,y,word)        


def set_palette(name="palette", ncontours=999):

    from ROOT import TColor, gROOT, gStyle
    from array import array
 
    """
    Set a color palette from a given RGB list
    - stops, red, green and blue should all be lists of the same length
    - see set_decent_colors for an example
    """
 
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    elif name == "green":
        stops = [0.000, 0.100, 0.700, 1.000]        
        red   = [1.000, 1.000, 0.476, 0.476]
        green = [1.000, 1.000, 0.760, 0.760]
        blue  = [1.000, 1.000, 0.476, 0.476]
    elif name == "blue":
        stops = [0.000, 0.100, 0.700, 1.000]
        red   = [1.000, 1.000, 0.476, 0.476]
        green = [1.000, 1.000, 0.476, 0.476]
        blue  = [1.000, 1.000, 0.760, 0.760]
    else:
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
 
    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)
 
    npoints = len(s)
    TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)
