import ROOT
import numpy as np
import os,sys,math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from options_file import *

#import pandas as pd

dataName=options.data_name
inputdir=options.output_dir
outputdir=options.plot_dir
ttb=options.ttb_sample
systematic_name="nominal" #"weight_pileup_DOWN"

option="INT"
lumi=options.data_lumi

doData1516=False
doData17=False
doData18=False
if "1516" in dataName:
    doData1516=True
elif "17" in dataName:
    doData17=True
elif "18" in dataName:
    doData18=True

legendLabels={
    'FTAG2_ttV_aMCnlo' : 'ttV',
    'FTAG2_Zjets_Sherpa221' : 'Z+jets',
    'FTAG2_Wjets_Sherpa221' : 'W+jets',
    'FTAG2_singletop_PowPy8' : 'Single Top',
    'FTAG2_Diboson_Sherpa222' : "Diboson",
    'FTAG2_ttbar_PhPy8' : "t#bar{t}",
    'data1516' : "Data 2015 + 2016",
    'data17' : "Data 2017",
    'data18' : "Data 2018"
}


flavours={'bb' : ROOT.kBlue, 'bc':ROOT.kRed-3, 'cb':ROOT.kRed+2,'cc':ROOT.kRed,'bl':ROOT.kOrange+3,'lb':ROOT.kOrange-3,'ll':ROOT.kOrange,'cl':ROOT.kGreen-3,'lc':ROOT.kGreen+3 }
syst_global=[y.name for y in options.inner_systematics_in_nominal_tree]
syst_global.extend([y.name for y in options.tree_systematics])
syst_samples={
    'ttbar' : [y.name for y in options.syst_samples],
    'ttbar_Sherpa' : [y.name for y in options.syst_samples],
    'ttbar_AF2' : [y.name for y in options.syst_samples],
    'ttbar_hdamp3mtop' : [y.name for y in options.syst_samples],
    'Zjets' : [y.name for y in options.syst_samples_ZJets],
    'Wjets' : [],#y.name for y in options.syst_samples_WJets],
    'singletop' : [y.name for y in options.syst_samples_singletop],
    'diboson' : [y.name for y in options.syst_samples_Diboson],
}

syst_samples['ttbar'].remove('FTAG2_ttbar_aMcPy8')
syst_samples['ttbar'].remove('FTAG2_ttbar_PowHW7')
syst_samples['ttbar'].remove("FTAG2_ttbar_Sherpa221")
syst_samples['ttbar'].remove('FTAG2_ttbar_PhPy8_AF2')
syst_samples['ttbar'].remove('FTAG2_ttbar_PhPy8_hdamp3mtop')


other_ttbar_systs=options.pdf_systematics
other_ttbar_systs.extend(["weight_mc_fsr_DOWN","weight_mc_fsr_UP","weight_mc_rad_UP","weight_mc_rad_DOWN"])

samples={ #'ttbar' : ['FTAG2_ttbar_PhPy8',"t#bar{t}", ROOT.kBlue],
          #'ttbar_Sherpa' : ['FTAG2_ttbar_PhPy8_Sherpa221',"t#bar{t}", ROOT.kBlue],
    'ttbar_PowHW7' : ['FTAG2_ttbar_PowHW7',"t#bar{t}", ROOT.kBlue],
    #          'ttbar_hdamp3mtop' : ['FTAG2_ttbar_PhPy8_hdamp3mtop',"t#bar{t}", ROOT.kBlue],
#    'ttbar_AF2' : ['FTAG2_ttbar_PhPy8_AF2',"t#bar{t}", ROOT.kBlue],
          # 'Zjets' : ['FTAG2_Zjets_Sherpa221',"Z+jets", ROOT.kRed],
          # 'Wjets' : ['FTAG2_Wjets_Sherpa221',"W+jets", ROOT.kGreen+2],
          # 'singletop' : ['FTAG2_singletop_PowPy8',"Single Top", ROOT.kOrange-3],
          # 'diboson' : ['FTAG2_Diboson_Sherpa222',"Diboson", ROOT.kViolet],
          # 'data1516' : ['data1516',"Data 2015 + 2016", ROOT.kBlack],
          # 'data17' : ['data17',"Data 2017", ROOT.kBlack],
          # 'data18' : ['data18',"Data 2018", ROOT.kBlack]
          }

#folders=['emu_OS_J2']
folders=['emu_OS_J2','emu_OS_J2_m_lj_cut']#,'emu_OS_J2_CR_lb','emu_OS_J2_CR_bl','emu_OS_J2_CR_ll']


def saveKeys(inputfile,outfile,isnominal=False):
    
    print("Reading file %s "%inputfile.GetName())
    for key in inputfile.GetListOfKeys():
#    for key in nominal_file.GetListOfKeys():
        if "emu" in key.GetName():
            
            outfile.cd()
            directory=outfile.Get(key.GetName())
            if not directory:
                outfile.mkdir(key.GetName())
                directory=outfile.Get(key.GetName())
            for key1 in inputfile.Get(key.GetName()).GetListOfKeys():
                if not "boot_strap" in key1.GetName() and not isnominal:
                    continue
#                print(key.GetName()+"/"+key1.GetName())
                directory.cd()
                if not outfile.Get(key.GetName()+"/"+key1.GetName()):
                    obj=inputfile.Get(key.GetName()+"/"+key1.GetName())
                    obj.Write()
        else:
            outfile.cd()
            if not isnominal:
                continue
            if not outfile.Get(key.GetName()):
                obj=inputfile.Get(key.GetName())
                obj.Write()
                

def saveBoot(infile,outfile,process):
    for bin in range(0,10):
        sfile=infile.replace("combination","bootStrapBin%d_combination"%(bin+1))
        sfile=sfile.replace(process+"/","%s_bootStrap_bin%d/"%(process,bin+1))
        in_file=ROOT.TFile(sfile, "read")
        saveKeys(in_file,outfile)
        in_file.Close()
    


if __name__ == '__main__':
    
    os.system("mkdir -p BootStrap/")
    
    args = parser.parse_args()

    for sam,values in samples.items():

        outputfile=ROOT.TFile('BootStrap/'+values[0]+"_nominal_combination.root", "RECREATE")
        nominal_file=ROOT.TFile(inputdir+'/'+values[0]+"/"+values[0]+"_nominal_combination.root", "READ")
        outputfile.cd()
        saveKeys(nominal_file,outputfile,True)
        nominal_file.Close()
        saveBoot(inputdir+'/'+values[0]+"/"+values[0]+"_nominal_combination.root",outputfile,values[0])
        outputfile.Close()
        # for bin in range(0,10):
        #     nominal_file=ROOT.TFile(inputdir+'/'+values[0]+"/"+values[0]+"_nominal_combination_bootStrap_bin%d.root"%(bin+1), "read")

        for syst in syst_global:
            
            # if 'weight' in syst:                                                                                                                                                                      
            #     syst=syst.split('weight_')[-1]
            outputfile=ROOT.TFile('BootStrap/'+values[0]+"_"+syst+"_combination.root", "RECREATE")
            if not os.path.isfile(inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root"):
                print("  WARNING :  "+inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root")
                continue
            syst_file=ROOT.TFile(inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root", "read")
            if syst_file.IsZombie():
                print("   ERROR :   File "+inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root"+" but it could not be opened")
                syst_file.Close()
                continue
            saveKeys(syst_file,outputfile,True)
            syst_file.Close()
            saveBoot(inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root",outputfile,values[0])
            outputfile.Close()

            # for syst in syst_samples[sam]:
            #     if 'weight' in syst:
            #         syst=syst.split('weight_')[-1]
            #     if not os.path.isfile(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"):
            #         print("  WARNING :  "+inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"+"_combination.root")
            #         continue
            #     syst_file=ROOT.TFile(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root", "read")
            #     if syst_file.IsZombie():
            #         print("   ERROR :   File "+inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"+" but it could not be opened")
            #         syst_file.Close()
            #         continue
            #     saveKeys(syst_file,outputfile,True)
            #     syst_file.Close()
            #     saveBoot(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root",outputfile,syst)

        if sam == "ttbar":
            for syst in other_ttbar_systs:
                outputfile=ROOT.TFile('BootStrap/'+values[0]+"_"+syst+"_combination.root", "RECREATE")
                if 'weight' in syst:
                    syst=syst.split('weight_')[-1]
                if not os.path.isfile(inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root"):
                    print("  WARNING :  "+inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root")
                    continue
                syst_file=ROOT.TFile(inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root", "read")
                if syst_file.IsZombie():
                    print("   ERROR :   File "+inputdir+'/'+values[0]+"/"+values[0]+"_"+syst+"_combination.root"+" but it could not be opened")
                    syst_file.Close()
                    continue

                saveKeys(syst_file,outputfile,True)
                syst_file.Close()
                saveBoot(inputdir+'/'+values[0]+"/"+values[0]+"_nominal_combination.root",outputfile,values[0])
                outputfile.Close()
