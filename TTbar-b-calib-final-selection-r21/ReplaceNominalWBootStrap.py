import os
import time
import sys
import subprocess
import shutil
from options_file import *

# Before running this, I have commented out ~L20 which remove files!
# Please run this and test the print statements are consistent with your directories and file locations

def ReplaceOddWNew(JobFolder, ROOTFileName, NewFile):
	OldPath = options.output_dir + JobFolder + "/" + ROOTFileName
	print("Path to the existing root file: ")
	print(OldPath)
	CopyOfOldPath = OldPath[:-5] + "2.root"
	print("The second version of this OG file will be called: ")
	print(CopyOfOldPath)
	CopyCMD = "cp " + OldPath + " " + CopyOfOldPath
	os.system(CopyCMD)
	print("Copied a second version of the OG file ... ") 
	# RMcmd = "rm " + OldPath
	# os.system(RMcmd)
	# print("Removed the previous version to make way for the new .. ")
	CopyOverNewFileCMD = "cp " + NewFile + " " + options.output_dir + JobFolder + "/"
	os.system(CopyOverNewFileCMD)
	print("New version has been copied from bootstrap_merged folder!")

# 
# Main
# 

bootstrap_folder = options.output_dir + "bootstrap_merged/"
list_of_job_dirs = os.listdir(bootstrap_folder)

arr_of_job_dirs_w_full_paths = []
for job_dir in list_of_job_dirs:
	arr_of_job_dirs_w_full_paths.append(bootstrap_folder + job_dir + "/")

for i in range (0,len(arr_of_job_dirs_w_full_paths)):
	print("\nLooping over the next bootstrap_merged folders ... ")
	file = os.listdir(arr_of_job_dirs_w_full_paths[i])
	full_path = arr_of_job_dirs_w_full_paths[i] + file[0]
	print("Bootstrap merged folder: ")
	print(full_path)
	if "FTAG2_ttbar_aMcPy8" in list_of_job_dirs[i]:
		job_folder = "FTAG2_ttbar_aMcPy8"
		ReplaceOddWNew(job_folder, file[0], full_path)
	elif "FTAG2_ttbar_PowHW7" in list_of_job_dirs[i]:
		job_folder = "FTAG2_ttbar_PowHW7"
		ReplaceOddWNew(job_folder, file[0], full_path)
	# elif "FTAG2_ttbar_Sherpa221" in list_of_job_dirs[i]:
	# 	job_folder = "FTAG2_ttbar_Sherpa221"
	# 	ReplaceOddWNew(job_folder, file[0])
	elif "FTAG2_ttbar_PhPy8_hdamp3mtop" in list_of_job_dirs[i]:
		job_folder = "FTAG2_ttbar_PhPy8_hdamp3mtop"
		ReplaceOddWNew(job_folder, file[0], full_path)
	elif "FTAG2_ttbar_PhPy8_AF2" in list_of_job_dirs[i]:
		job_folder = "FTAG2_ttbar_PhPy8_AF2"
		ReplaceOddWNew(job_folder, file[0], full_path)
	elif "FTAG2_ttbar_PhPy8" in list_of_job_dirs[i]:
		job_folder = "FTAG2_ttbar_PhPy8"
		ReplaceOddWNew(job_folder, file[0], full_path)
