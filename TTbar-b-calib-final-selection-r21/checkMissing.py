import ROOT
import numpy as np
import os,sys,math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from options_file import *

import pandas as pd

dataName=options.data_name
inputdir=options.output_dir
outputdir=options.plot_dir
ttb=options.ttb_sample
systematic_name="nominal" #"weight_pileup_DOWN"

option="INT"
lumi=options.data_lumi

    
doData1516=False
doData17=False
doData18=False
if "1516" in dataName:
    doData1516=True
elif "17" in dataName:
    doData17=True
elif "18" in dataName:
    doData18=True

legendLabels={
    'FTAG2_ttV_aMCnlo' : 'ttV',
    'FTAG2_Zjets_Sherpa221' : 'Z+jets',
    'FTAG2_Wjets_Sherpa221' : 'W+jets',
    'FTAG2_singletop_PowPy8' : 'Single Top',
    'FTAG2_Diboson_Sherpa222' : "Diboson",
    'FTAG2_ttbar_PhPy8' : "t#bar{t}",
    'data1516' : "Data 2015 + 2016",
    'data17' : "Data 2017",
    'data18' : "Data 2018"
}


flavours={'bb' : ROOT.kBlue, 'bc':ROOT.kRed-3, 'cb':ROOT.kRed+2,'cc':ROOT.kRed,'bl':ROOT.kOrange+3,'lb':ROOT.kOrange-3,'ll':ROOT.kOrange,'cl':ROOT.kGreen-3,'lc':ROOT.kGreen+3 }
syst_global=[y.name for y in options.inner_systematics_in_nominal_tree]
syst_global.extend([y.name for y in options.tree_systematics])
syst_global.append("nominal")
syst_samples={
    'ttbar' : [y.name for y in options.syst_samples],
    'Zjets' : [y.name for y in options.syst_samples_ZJets],
    'Wjets' : [],#y.name for y in options.syst_samples_WJets],
    'singletop' : [y.name for y in options.syst_samples_singletop],
    'diboson' : [y.name for y in options.syst_samples_Diboson],
}

other_ttbar_systs=[]
for syst in options.pdf_systematics:
    other_ttbar_systs.append(syst.name)
other_ttbar_systs.extend(["_weight_mc_fsr_DOWN","_weight_mc_fsr_UP","_weight_mc_rad_UP","_weight_mc_rad_DOWN"])

samples={'ttbar' : ['FTAG2_ttbar_PhPy8',"t#bar{t}", ROOT.kBlue],
         'Zjets' : ['FTAG2_Zjets_Sherpa221',"Z+jets", ROOT.kRed],
         'Wjets' : ['FTAG2_Wjets_Sherpa221',"W+jets", ROOT.kGreen+2],
         'singletop' : ['FTAG2_singletop_PowPy8',"Single Top", ROOT.kOrange-3],
         'diboson' : ['FTAG2_Diboson_Sherpa222',"Diboson", ROOT.kViolet],
         'data1516' : ['data1516',"Data 2015 + 2016", ROOT.kBlack],
         'data17' : ['data17',"Data 2017", ROOT.kBlack],
         'data18' : ['data18',"Data 2018", ROOT.kBlack]
         }



resubmitFile="resubmit.sh"
os.system("echo '' > "+resubmitFile)
for sample,properties in samples.items():

    if 'data' in sample:
        continue
    for syst in syst_global:
        if not os.path.isfile(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root"):
            print(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root")
            os.system("echo condor_submit htcondor_submit/"+dataName+properties[0]+syst+"/"+dataName+properties[0]+syst+".submit >> "+resubmitFile)
            # syst_file.Close() #syst_file is not defined if it goes through this loop
            continue

        syst_file=ROOT.TFile(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root", "read")
        if syst_file.IsZombie():
            print(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root")
            os.system("echo condor_submit htcondor_submit/"+dataName+properties[0]+syst+"/"+dataName+properties[0]+syst+".submit >> "+resubmitFile)
        syst_file.Close()


    sys_sample = syst_samples[sample]
    for syst in sys_sample:
        if not os.path.isfile(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"):
            print(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"+"_combination.root")
            os.system("echo condor_submit htcondor_submit/"+dataName+syst+"/"+dataName+syst+".submit >> "+resubmitFile)
            syst_file.Close()
            continue

        syst_file=ROOT.TFile(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root", "read")
        if syst_file.IsZombie():
            print(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root")
            os.system("echo condor_qsub htcondor_submit/"+dataName+syst+"/"+dataName+syst+".submit >> "+resubmitFile)
            syst_file.Close()
    
    if sample == "ttbar":
        for syst in other_ttbar_systs:
            if 'mc_rad_UP' in syst:
                syst="weight_mc_rad_UP"
                properties[0]='FTAG2_ttbar_PhPy8_hdamp3mtop'
            else:
                syst=syst
                properties[0]='FTAG2_ttbar_PhPy8'
            if not os.path.isfile(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root"):
                print(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root")
                os.system("echo condor_submit htcondor_submit/"+dataName+properties[0]+syst+"/"+dataName+properties[0]+syst+".submit >> "+resubmitFile)
                syst_file.Close()
                continue
            syst_file=ROOT.TFile(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root", "read")
            if syst_file.IsZombie():
                print(inputdir+'/'+properties[0]+"/"+properties[0]+"_"+syst+"_combination.root")
                os.system("echo condor_submit htcondor_submit/"+dataName+properties[0]+syst+"/"+dataName+properties[0]+syst+".submit >> "+resubmitFile)
                syst_file.Close()
            

