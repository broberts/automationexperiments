import ROOT
import os
from os import listdir
from os.path import isfile, join, isdir, split

ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../')
from options_file import *


def combine_files(file1_name,file2_name,out_name):
    out_path, out_fname = split(out_name)
    if not os.path.exists(out_path):
        os.makedirs(out_path) 
    command="hadd -f "+out_name+" "+file1_name+" "+file2_name
    #print command
    fsh.write(out_name+' '+file1_name+' '+file2_name+'\n')
    
    # os.system(command)


combi_dir_1="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/rl21-2-53_05-02-19_mc16a/out/"
combi_dir_2="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/rl21-2-53_05-02-19_mc16d/out/"
merged_dir=options.output_dir

if not os.path.exists(merged_dir):
    os.makedirs(merged_dir)


fsh = open('job_parameters.txt', 'w')
    
content_list=listdir(combi_dir_1)
for content_name in content_list:
    print "content name:", content_name
    c1_path=join(combi_dir_1, content_name)
    c2_path=join(combi_dir_2, content_name)
    if isdir(c1_path):
        print "we should combine files in folder: ", c1_path
        if content_name == "d1516":
            comb1_path=join(combi_dir_1,"d1516" ,"d1516_data_combination.root")
            comb2_path=join(combi_dir_2, "d17","d17_data_combination.root")
            combi_path=join(merged_dir, "d151617", "d151617_data_combination.root")
            combine_files(comb1_path,comb2_path,combi_path)
        else:
            dir_cont_list=listdir(c1_path)
            for combi_file in dir_cont_list:
                if "_combination.root" in combi_file:
                    comb1_path=join(c1_path, combi_file)
                    comb2_path=join(c2_path, combi_file)
                    if isfile(comb2_path):
                        combi_path=join(merged_dir, content_name, combi_file)
                        #print 'combining ',combi_file
                        combine_files(comb1_path,comb2_path,combi_path)
                    else:
                        print "we are somehowe missing:", comb2_path


print "Ende gut alles gut! "
