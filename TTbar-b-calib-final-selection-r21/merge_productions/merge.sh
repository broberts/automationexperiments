 #!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK;
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh;
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

for i; do 
    echo argument i = $i 
done
hadd -f $1 $2 $3