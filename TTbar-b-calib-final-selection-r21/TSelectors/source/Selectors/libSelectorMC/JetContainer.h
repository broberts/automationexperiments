// JetContainer.h
#pragma once

#include "VariableContainer.h"
#include "VariablePtBinsContainer.h"
#include "VariablePtBinsCorrelationContainer.h"
#include "TreeReader.h"
#include <string>

class jet_Container : public TObject
{
 public:
  std::string name;
  int n_jets;
  bool m_data;
  Variable_Container *jet_pt;
  Variable_Container *jet_eta;
  Variable_Container *jet_eta_pt_gt_60;
  Variable_Container *jet_phi;
  Variable_Container *jet_mv2c10;
  Variable_ptBins_Container *jet_mv2c10_pt;
  //Variable_Container *jet_MV2r;
  //Variable_Container *jet_MV2rmu;
  Variable_Container *jet_DL1;
  Variable_Container *jet_DL1r;
  Variable_Container *jet_DL1rmu;
  Variable_Container *jet_m_jl;
  Variable_ptBins_Correlation_Container *jet_mv2c10_m_jl_cor;
  Variable_Container *HadronConeExclExtendedTruthLabelID;
  Variable_ptBins_Container *jet_HadronConeExclExtendedTruthLabelID_pt;
  
 public:
  // constructor
  jet_Container(std::string name, int n_jets, bool data, std::vector<std::string> eflavours);
  
  // Destructor
  ~jet_Container();
  
  // Write something
  void Write();
  
  // Add Jet to something
  void addJet(int jet_n,TreeReader *selector, double weight);
  
  // Gets DArray
  double* getDArray(int bins, double lower, double upper);


  // Calculate DL1 by Hand
  std::vector<float> DL1Hand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction);
  ClassDef(jet_Container,1);
};
