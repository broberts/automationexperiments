// finalSelectionMC.cxx

#include "finalSelectionMC.h"

using namespace std;
using CP::SystematicCode;

// Constructor
finalSelectionMC::finalSelectionMC() : fChain(0)
{
  m_treeReader = new TreeReader();
}

// Destrcutor
finalSelectionMC::~finalSelectionMC()
{
}

std::string finalSelectionMC::getEventFlav(std::vector<int> *jet_truthflav)
{
	std::string eventFlav = "";
	if (data)
	{
		eventFlav = "data";
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			if (jet_truthflav->at(i) == 4)
				eventFlav = eventFlav + "c";
			else if (jet_truthflav->at(i) == 5)
				eventFlav = eventFlav + "b";
			else
				eventFlav = eventFlav + "l"; //this is also tau jets!
		}
	}
	return eventFlav;
}

void finalSelectionMC::Begin(std::string soption)
{
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.

  f_out = gDirectory;
  std::string blah = f_out->GetPath();
  std::cout << "Printing f_out: " << std::endl;
  // std::cout << blah << std::endl;
  // TString option = this->GetOption();
  // std::string soption = string(option.Data());
  nE = 0;
  // cout << "Starting h1analysis with process option: " << option.Data() << endl;
  cout << "soption: " << soption << endl;
  istringstream iss(soption);
  vector<char *> vec_options;

  histo_reweight=NULL;
  do {
      string subs;
      iss >> subs;
      char *cstr = new char[subs.length() + 1];
      strcpy(cstr, subs.c_str());
      vec_options.push_back(cstr);
  } while (iss);
  int argc = vec_options.size();
  char **argv = vec_options.data();
  string input_file, output_file, tChain_name, inner_Systematic, NPLeptons_file, cdi_file_path;
	try
	{
		cxxopts::Options options(argv[0], " - example command line options");
		options.positional_help("");
		options.add_options()
		  ("i,input", "Input", cxxopts::value<std::string>(), "input_file")
		  ("o,output", "outputFilename", cxxopts::value<std::string>()->default_value("test.root"), "output_file") //name
		  ("c, cdiPath", "Path to CDI file to be used", cxxopts::value<std::string>(), "cdi_file_path")
		  ("save_fit_input", "run over fakes", cxxopts::value<bool>(save_fits))
		  ("t,tsyst", "name of tChain Systematic", cxxopts::value<std::string>()->default_value("nominal"), "systName")
		  ("w,isyst", "name of inner Systematic(weights)", cxxopts::value<std::string>()->default_value("nominal"), "isystName")
		  ("f,NPLeptons_file", "NPLeptons_file", cxxopts::value<std::string>()->default_value(""), "NPLeptons_file")
		  ("l,lumiWeight", "lumiWeight", cxxopts::value<double>(w_lumi), "lumiWeight")
		  ("data", "run over data", cxxopts::value<bool>(data))
		  ("storeTracks", "Store histograms from tracks", cxxopts::value<bool>(storeTracks))
		  ("reweight_pT", "Reweight the histograms", cxxopts::value<bool>(reweight_pT))
		  ("reweight_ip3d", "Reweight the histograms", cxxopts::value<bool>(reweight_ip3d))
		  ("reweight_jf", "Reweight the histograms", cxxopts::value<bool>(reweight_jf))
		  ("reweight_sv1", "Reweight the histograms", cxxopts::value<bool>(reweight_sv1))
		  ("boot_strap", "save boot strap weights. takes sapce and time!", cxxopts::value<bool>(boot_strap))
		  ("bsbin", "Choose which boot strap weights to run on", cxxopts::value<int>(bsbin))
		  ("run_fakes", "run over fakes", cxxopts::value<bool>(run_fakes))
		  ("apply_lf_calib", "apply_lf_calib", cxxopts::value<bool>(apply_lf_calib))
		  ("jet_collection", "jet_collection", cxxopts::value<string>(jet_collection))
		  ("jet_collection_lf", "jet_collection_lf", cxxopts::value<string>(jet_collection_lightjets))
		  ("hadronization", "hadronization use for mc/mc sf", cxxopts::value<int>(hadronization), "int")
		  ("use_pt_bins_as_eta", "run calibration with eta bins instead of pT.", cxxopts::value<bool>(use_pt_bins_as_eta) )
		  ("h, help", "Print help")
		  ;

		options.parse(argc, argv);

		if (options.count("help"))
		{
			cout << "Where input_file is a text file with the filenames to be added to the Chain for the TSelectors" << endl;
			cout << "TChain_name = syst name or nominal!" << endl;
			cout << "if TChain_name ==  nominal you can specify here an inner systematic like weight_leptonSF_EL_SF_Trigger_UP" << endl;
			std::cout << options.help({""}) << std::endl;
			exit(0);
		}
		if (options.count("input"))
		  {
			std::cout << "Input = " << options["input"].as<std::string>() << std::endl;
			input_file = options["input"].as<std::string>();
		}
		else
		{
			std::cout << "please give input file!" << std::endl;
			std::cout << options.help({""}) << std::endl;
			exit(0);
		}

		if (options.count("cdiPath"))
		{
		  std::cout << "CDI Path = " << options["cdiPath"].as<std::string>() << std::endl;
		  cdi_file_path = options["cdiPath"].as<std::string>();
		}
		else
	        {
		  std::cout << "please give CDI path!" << std::endl;
		  std::cout << options.help({""}) << std::endl;
		  exit(0);
		}
		

		std::cout << "Output = " << options["output"].as<std::string>() << std::endl;
		output_file = options["output"].as<std::string>();
		std::cout << "tChain_name = " << options["tsyst"].as<std::string>() << std::endl;
		tChain_name = options["tsyst"].as<std::string>();
		std::cout << "inner_Systematic = " << options["isyst"].as<std::string>() << std::endl;
		inner_Systematic = options["isyst"].as<std::string>();
		std::cout << "NPLeptons_file = " << options["NPLeptons_file"].as<std::string>() << std::endl;
		NPLeptons_file = options["NPLeptons_file"].as<std::string>();
		std::cout << "w_lumi = " << w_lumi << std::endl;
		if (!boot_strap)
		{
			m_n_boot_strap_max = 0;
		}
		else
		{
		  m_n_boot_strap_min=bsbin > 0 ? 10*(bsbin-1):0;
		  m_n_boot_strap_max=10*bsbin;
		  std::cout << "m_n_boot_strap_max = " << m_n_boot_strap_max << std::endl;
		}
	}
	catch (const cxxopts::OptionException &e)
	{
		std::cout << "error parsing options: " << e.what() << std::endl;
		exit(1);
	}
	if (run_fakes)
	{
		std::cout << "running fake estimation!. " << std::endl;
	}
	if (save_fits)
	{
		std::cout << "saving the input histogramms for the fits. " << std::endl;
	}


	// Initialize the tree
	std::cout << "Beginning to initialise TreeReader" << std::endl;
	m_treeReader->m_storeTracks=storeTracks;
	std::cout << "Set store tracks attribute" << std::endl;
	m_treeReader->Begin();
	std::cout << "Finished begin method" << std::endl;

	int changed = 0;
	string syst_weightname = inner_Systematic;
	if ((syst_weightname != "nominal") && data)
	{
		cout << "Error with systematic " << syst_weightname << "!!!"
			 << " but data: " << data << endl;
		exit(1);
	}
	if (syst_weightname != "nominal")
	{
		string w_mc_str = "weight_mc";
		if ("FT_" == syst_weightname.substr(0, 3))
		{
			bTagSystName = syst_weightname;
		}
		else if (w_mc_str == syst_weightname.substr(0, w_mc_str.size()))
		{
			//we have one of these strange mc weights
			string w_mc_str_up = "weight_mc_rad_UP";
			string w_mc_str_down = "weight_mc_rad_DOWN";
			string w_mc_str_shower_np = "weight_mc_shower_np_";
			string w_mc_fsr_up = "weight_mc_fsr_UP";
			string w_mc_fsr_down = "weight_mc_fsr_DOWN";
			if (syst_weightname == w_mc_str_up && input_file.find("ttbar") != std::string::npos)
			{
				n_weight_shower = 193;
				n_weight_muRmuF = 5;
			}
			else if (syst_weightname == w_mc_str_down && input_file.find("ttbar") != std::string::npos)
			{
				n_weight_shower = 194;
				n_weight_muRmuF = 6;
			}
			else if (syst_weightname == w_mc_str_up && input_file.find("singletop") != std::string::npos)
			{
				n_weight_shower = 142;
				n_weight_muRmuF = 5;
			}
			else if (syst_weightname == w_mc_str_down && input_file.find("singletop") != std::string::npos)
			{
				n_weight_shower = 143;
				n_weight_muRmuF = 6;
			}
			else if (syst_weightname == w_mc_fsr_up  && input_file.find("ttbar") != std::string::npos)
			{
				n_weight_shower = 198;
			}
			else if (syst_weightname == w_mc_fsr_down && input_file.find("ttbar") != std::string::npos)
			{
				n_weight_shower = 199;
			}
			else if (syst_weightname == w_mc_fsr_up  && input_file.find("singletop") != std::string::npos)
			{
				n_weight_shower = 147;
			}
			else if (syst_weightname == w_mc_fsr_down && input_file.find("singletop") != std::string::npos)
			{
				n_weight_shower = 148;
			}
			else if (w_mc_str_shower_np == syst_weightname.substr(0, w_mc_str_shower_np.size()))
			{
				string np_number = syst_weightname.substr(w_mc_str_shower_np.size());
				cout << "trying to find to get np from string: " << np_number << endl;
				int np = stoi(np_number);
				n_weight_shower = np;
				cout << "Found np " << np << " in " << syst_weightname << ". Setted n_weight_shower = " << n_weight_shower << endl;
			}
			if (n_weight_shower == 0)
			{
				cout << "Error with systematic " << syst_weightname << "!!!"
					 << " but found: " << w_mc_str << endl;
			}
			//earese weight mc from weight to use!
			else{
				weight_names_to_use.erase(weight_names_to_use.begin());
				cout << "erased weight_mc doue to: " << syst_weightname << "!!!" << endl;
			}

		}
		else
		{
			for (auto &w : weight_names_to_use)
			{
				if (w == syst_weightname.substr(0, w.size()))
				{
					cout << w;
					w = syst_weightname;
					cout << " changed to w = " << w << endl;
					changed++;
				}
			}
			if (changed != 1)
			{
				cout << "Error with systematic " << syst_weightname << "!!!" << endl;
				cout << "Found " << changed << " times!!" << endl;
				exit(1);
			}
		}
	}
	if ((NPLeptons_file != "") && (!data))
	{
		TFile *npleptonsfile = TFile::Open(NPLeptons_file.c_str());
		if (!npleptonsfile)
		{
			cout << "File " << NPLeptons_file.c_str() << " not found!" << endl;
			return;
		}
		TH1D *npleptonshist = (TH1D *)npleptonsfile->Get("hist_SS_data_minus_MC2PL_over_MC1PL");
		if (!npleptonshist)
		{
			cout << "Histogram 'hist_SS_data_minus_MC2PL_over_MC1PL' could not be found in " << NPLeptons_file.c_str() << endl;
			return;
		}
		cout << "Data/MC correction factors for OS emu events with a non-prompt electron will be applied from file " << NPLeptons_file.c_str() << endl;
		NPel_bin1 = npleptonshist->GetBinContent(1);
		NPel_bin2 = npleptonshist->GetBinContent(2);
		NPel_bin3 = npleptonshist->GetBinContent(3);
		correctFakes = true;
		npleptonsfile->Close();
	}

	if (reweight_pT){
	  TString filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/data1516_pT_weights.root";
	  if ( input_file.find("r10201") != std::string::npos)
	    filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/data17_pT_weights.root";
	  else if ( input_file.find("r10724") != std::string::npos)
	    filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/data18_pT_weights.root";

	  // if( apply_lf_calib )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_lightJet.root";
	  // else if( correctFakes )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_fakeRate.root";
	  TFile* file_reweight = TFile::Open(filename,"READ");
	  histo_reweight = (TH1F*) file_reweight->Get("Weights");
	  histo_reweight->SetDirectory(0);
	  file_reweight->Close();
	}

	if (reweight_jf){
	  TString filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_jf_lead.root";
	  // if( apply_lf_calib )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_lightJet.root";
	  // else if( correctFakes )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_fakeRate.root";
	  TFile* file_reweight = TFile::Open(filename,"READ");
	  histo_reweight = (TH1F*) file_reweight->Get("Weights");
	  histo_reweight->SetDirectory(0);
	  file_reweight->Close();
	}
	
	if (reweight_sv1){
	  TString filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_sv1_lead.root";
	  // if( apply_lf_calib )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_lightJet.root";
	  // else if( correctFakes )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_fakeRate.root";
	  TFile* file_reweight = TFile::Open(filename,"READ");
	  histo_reweight = (TH1F*) file_reweight->Get("Weights");
	  histo_reweight->SetDirectory(0);
	  file_reweight->Close();
	}
	
	if (reweight_ip3d){
	  TString filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_ip3dtrk_lead.root";
	  // if( apply_lf_calib )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_lightJet.root";
	  // else if( correctFakes )
	  //   filename="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/pT_weights_fakeRate.root";
	  TFile* file_reweight = TFile::Open(filename,"READ");
	  histo_reweight = (TH1F*) file_reweight->Get("Weights");
	  histo_reweight->SetDirectory(0);
	  file_reweight->Close();
	}

	std::cout << "Progressed this far" << std::endl;
	vector<string> cutFlowLabels;
	jet_multiplicity = new TH1F("h_jet_multiplicity", "Jet Multiplicity before jet cut", 10, 0, 10);
	std::cout << "Jet multiplicty th1f created" << std::endl;
	
	cutFlowLabels.push_back("inputs");
	cutFlowLabels.push_back("2 leptons");
	cutFlowLabels.push_back("ee Channel");
	cutFlowLabels.push_back("OS");
	cutFlowLabels.push_back("2 or 3 Jets");
	cutFlowLabels.push_back("2 Jets");
	ee_OS_J2_bCuts = new DiLepChannel("ee_OS_J2_bCuts", cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.push_back("MET >60e3");
	cutFlowLabels.push_back("mll >50e3");
	cutFlowLabels.push_back("80e3 < mll < 100e3");
	ee_OS_J2_aCuts = new DiLepChannel("ee_OS_J2_aCuts",cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	ee_OS_J2_ZCR = new DiLepChannel("ee_OS_J2_ZCR", cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	emufromee_OS_J2_ZCR = new DiLepChannel("emufromee_OS_J2_ZCR",cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.push_back("1 lep_comb_cut");
	// ee_OS_J2_m_lj_cut = new DiLepChannel("ee_OS_J2_1m_lj_cut", cutFlowLabels, 2, 2, 0, f_out, data, eflavours);
	//ee_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("ee_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 2, 0, f_out, data, eflavours);
	//emufromee_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("emufromee_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 2, 0, f_out, data, eflavours);
	cutFlowLabels.push_back("2 lep_comb_cut");
	ee_OS_J2_m_lj_cut = new DiLepChannel("ee_OS_J2_m_lj_cut",cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	ee_OS_J2_ZCR_m_lj_cut = new DiLepChannel("ee_OS_J2_ZCR_m_lj_cut",cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	emufromee_OS_J2_ZCR_m_lj_cut = new DiLepChannel("emufromee_OS_J2_ZCR_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.erase(cutFlowLabels.begin() + 5, cutFlowLabels.end());
	cutFlowLabels.push_back("3 Jets");
	ee_OS_J3_bCuts = new DiLepChannel("ee_OS_J3_bCuts",cdi_file_path, cutFlowLabels, 3, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.push_back("MET >60e3");
	cutFlowLabels.push_back("mll >50e3");
	cutFlowLabels.push_back("80e3 < mll < 100e3");
	ee_OS_J3_aCuts = new DiLepChannel("ee_OS_J3_aCuts",cdi_file_path, cutFlowLabels, 3, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	ee_OS_J3_ZCR = new DiLepChannel("ee_OS_J3_ZCR", cdi_file_path, cutFlowLabels, 3, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	emufromee_OS_J3_ZCR = new DiLepChannel("emufromee_OS_J3_ZCR", cdi_file_path, cutFlowLabels, 3, 2, 0, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.erase(cutFlowLabels.begin() + 2, cutFlowLabels.end());

	cutFlowLabels.push_back("emu Channel");
	cutFlowLabels.push_back("OS");
	cutFlowLabels.push_back("2 or 3 Jets");
	cutFlowLabels.push_back("3 Jets");
	emu_OS_J3 = new DiLepChannel("emu_OS_J3", cdi_file_path, cutFlowLabels, 3, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	if (run_fakes && !data)
	{
	  emu_SS_J3 = new DiLepChannel("emu_SS_J3", cdi_file_path, cutFlowLabels, 3, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	}
	cutFlowLabels.pop_back();
	cutFlowLabels.push_back("2 Jets");
	//this is a channel we potentially want to fit later, so lets save the histograms for the fits!
	//and the chanel whre we ant the 2d cr distribution, so lets save it! :)
	std::cout << "About to begin dilep channel" << std::endl;
	std::cout << cutFlowLabels[0] << std::endl;
	std::cout << &f_out << std::endl;
	std::cout << data << std::endl;
	std::cout << eflavours[0] << std::endl;
	std::cout << m_n_boot_strap_min << std::endl;
	std::cout << m_n_boot_strap_max << std::endl;
	std::cout << m_treeReader->m_storeTracks << std::endl;
	std::cout << use_pt_bins_as_eta << std::endl;
	std::cout << hadronization << std::endl;
	std::cout << m_jl_CutValue << std::endl;
	std::cout << bTagSystName << std::endl;
	std::cout << save_fits << std::endl;
	emu_OS_J2= new DiLepChannel( "emu_OS_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits,true);
	std::cout << "Finished DilepChannel" << std::endl;
        emu_SS_J2= new DiLepChannel( "emu_SS_J2",cdi_file_path,cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
	if (run_fakes && !data){
	  emu_OS_NPmu_J2= new DiLepChannel( "emu_OS_NPmu_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_OS_NPel_J2= new DiLepChannel( "emu_OS_NPel_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_SS_NPmu_J2= new DiLepChannel( "emu_SS_NPmu_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
		emu_SS_NPel_J2= new DiLepChannel( "emu_SS_NPel_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
		emu_SS_2PL_J2= new DiLepChannel( "emu_SS_2PL_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
		emu_OS_0PL_J2= new DiLepChannel( "emu_OS_0PL_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
		emu_SS_0PL_J2= new DiLepChannel( "emu_SS_0PL_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
	}
	cutFlowLabels.push_back("1 lep_comb_cut");
	//this is a channel we potentially want to fit later, so lets save the histograms for the fits!
	// emu_OS_J2_1m_lj_cut = new DiLepChannel("emu_OS_J2_1m_lj_cut", cutFlowLabels, 2, 1, 1, f_out, save_fits);
	cutFlowLabels.push_back("2 lep_comb_cut");
	//this is a channel we potentially want to fit later, so lets save the histograms for the fits!
	emu_OS_J2_m_lj_cut = new DiLepChannel("emu_OS_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
	emu_OS_J2_CR_bl= new DiLepChannel( "emu_OS_J2_CR_bl",cdi_file_path, cutFlowLabels, 2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
	emu_OS_J2_CR_lb= new DiLepChannel( "emu_OS_J2_CR_lb",cdi_file_path, cutFlowLabels, 2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
	emu_OS_J2_CR_ll= new DiLepChannel( "emu_OS_J2_CR_ll",cdi_file_path, cutFlowLabels, 2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);

	if (run_fakes && !data)
	{
		emu_SS_J2_m_lj_cut = new DiLepChannel("emu_SS_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
		emu_OS_NPmu_J2_m_lj_cut = new DiLepChannel("emu_OS_NPmu_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
		emu_OS_NPel_J2_m_lj_cut = new DiLepChannel("emu_OS_NPel_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
		emu_SS_NPmu_J2_m_lj_cut = new DiLepChannel("emu_SS_NPmu_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
		emu_SS_NPel_J2_m_lj_cut = new DiLepChannel("emu_SS_NPel_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
		emu_OS_0PL_J2_m_lj_cut= new DiLepChannel( "emu_OS_0PL_J2_m_lj_cut",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
		emu_SS_0PL_J2_m_lj_cut= new DiLepChannel( "emu_SS_0PL_J2_m_lj_cut",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, save_fits);
	}

	cutFlowLabels.erase(cutFlowLabels.begin() + 2, cutFlowLabels.end());
	cutFlowLabels.push_back("mumu Channel");
	cutFlowLabels.push_back("OS");
	cutFlowLabels.push_back("2 or 3 Jets");
	cutFlowLabels.push_back("2 Jets");
	mumu_OS_J2_bCuts = new DiLepChannel("mumu_OS_J2_bCuts", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.push_back("MET >60e3");
	cutFlowLabels.push_back("mll >50e3");
	cutFlowLabels.push_back("80e3 < mll < 100e3");
	mumu_OS_J2_aCuts = new DiLepChannel("mumu_OS_J2_aCuts", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	mumu_OS_J2_ZCR = new DiLepChannel("mumu_OS_J2_ZCR", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	emufrommumu_OS_J2_ZCR = new DiLepChannel("emufrommumu_OS_J2_ZCR", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.push_back("1 lep_comb_cut");
	// mumu_OS_J2_1m_lj_cut = new DiLepChannel("mumu_OS_J2_1m_lj_cut", cutFlowLabels, 2, 0, 2, f_out);
	// mumu_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("mumu_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 0, 2, f_out);
	// emufrommumu_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("emufrommumu_OS_J2_ZCR_1m_lj_cut", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out);
	cutFlowLabels.push_back("2 lep_comb_cut");
	mumu_OS_J2_m_lj_cut = new DiLepChannel("mumu_OS_J2_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	mumu_OS_J2_ZCR_m_lj_cut = new DiLepChannel("mumu_OS_J2_ZCR_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	emufrommumu_OS_J2_ZCR_m_lj_cut = new DiLepChannel("emufrommumu_OS_J2_ZCR_m_lj_cut", cdi_file_path, cutFlowLabels, 2, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.erase(cutFlowLabels.begin() + 5, cutFlowLabels.end());
	cutFlowLabels.push_back("3 Jets");
	mumu_OS_J3_bCuts = new DiLepChannel("mumu_OS_J3_bCuts", cdi_file_path, cutFlowLabels, 3, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	cutFlowLabels.push_back("MET >60e3");
	cutFlowLabels.push_back("mll >50e3");
	cutFlowLabels.push_back("80e3 < mll < 100e3");
	mumu_OS_J3_aCuts = new DiLepChannel("mumu_OS_J3_aCuts", cdi_file_path, cutFlowLabels, 3, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	mumu_OS_J3_ZCR = new DiLepChannel("mumu_OS_J3_ZCR", cdi_file_path, cutFlowLabels, 3, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	emufrommumu_OS_J3_ZCR = new DiLepChannel("emufrommumu_OS_J3_ZCR", cdi_file_path, cutFlowLabels, 3, 0, 2, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	

	// cutFlowLabels.push_back("inputs");
	// cutFlowLabels.push_back("2 leptons");
	// cutFlowLabels.push_back("ee Channel");
	// cutFlowLabels.push_back("OS");
	// cutFlowLabels.push_back("2 or 3 Jets");
	// cutFlowLabels.push_back("2 Jets");
	// ee_OS_J2_bCuts = new DiLepChannel("ee_OS_J2_bCuts", cutFlowLabels, 2, 2, 0);
	// cutFlowLabels.push_back("MET >60e3");
	// cutFlowLabels.push_back("mll >50e3");
	// cutFlowLabels.push_back("80e3 < mll < 100e3");
	// ee_OS_J2_aCuts = new DiLepChannel("ee_OS_J2_aCuts", cutFlowLabels, 2, 2, 0);
	// ee_OS_J2_ZCR = new DiLepChannel("ee_OS_J2_ZCR", cutFlowLabels, 2, 2, 0);
	// emufromee_OS_J2_ZCR = new DiLepChannel("emufromee_OS_J2_ZCR", cutFlowLabels, 2, 2, 0);
	// cutFlowLabels.push_back("1 lep_comb_cut");
	// // ee_OS_J2_1m_lj_cut = new DiLepChannel("ee_OS_J2_1m_lj_cut", cutFlowLabels, 2, 2, 0);
	// // ee_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("ee_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 2, 0);
	// // emufromee_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("emufromee_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 2, 0);
	// cutFlowLabels.push_back("2 lep_comb_cut");
	// ee_OS_J2_m_lj_cut = new DiLepChannel("ee_OS_J2_m_lj_cut", cutFlowLabels, 2, 2, 0);
	// ee_OS_J2_ZCR_m_lj_cut = new DiLepChannel("ee_OS_J2_ZCR_m_lj_cut", cutFlowLabels, 2, 2, 0);
	// emufromee_OS_J2_ZCR_m_lj_cut = new DiLepChannel("emufromee_OS_J2_ZCR_m_lj_cut", cutFlowLabels, 2, 2, 0);
	// cutFlowLabels.erase(cutFlowLabels.begin() + 5, cutFlowLabels.end());
	// cutFlowLabels.push_back("3 Jets");
	// ee_OS_J3_bCuts = new DiLepChannel("ee_OS_J3_bCuts", cutFlowLabels, 3, 2, 0);
	// cutFlowLabels.push_back("MET >60e3");
	// cutFlowLabels.push_back("mll >50e3");
	// cutFlowLabels.push_back("80e3 < mll < 100e3");
	// ee_OS_J3_aCuts = new DiLepChannel("ee_OS_J3_aCuts", cutFlowLabels, 3, 2, 0);
	// ee_OS_J3_ZCR = new DiLepChannel("ee_OS_J3_ZCR", cutFlowLabels, 3, 2, 0);
	// emufromee_OS_J3_ZCR = new DiLepChannel("emufromee_OS_J3_ZCR", cutFlowLabels, 3, 2, 0);
	// cutFlowLabels.erase(cutFlowLabels.begin() + 2, cutFlowLabels.end());

	// cutFlowLabels.push_back("emu Channel");
	// cutFlowLabels.push_back("OS");
	// cutFlowLabels.push_back("2 or 3 Jets");
	// cutFlowLabels.push_back("3 Jets");
	// emu_OS_J3 = new DiLepChannel("emu_OS_J3", cutFlowLabels, 3, 1, 1);
	if (run_fakes && !data)
	  {
	    emu_SS_J3 = new DiLepChannel("emu_SS_J3", cdi_file_path, cutFlowLabels, 3, 1, 1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  }
	// cutFlowLabels.pop_back();
	// cutFlowLabels.push_back("2 Jets");
	// //this is a channel we potentially want to fit later, so lets save the histograms for the fits!
	// //and the chanel whre we ant the 2d cr distribution, so lets save it! :)
	// emu_OS_J2= new DiLepChannel( "emu_OS_J2",cutFlowLabels,2,1,1, save_fits,true);
	// emu_OS_J2_mz= new DiLepChannel( "emu_OS_J2_mz",cutFlowLabels,2,1,1);
	// ee_OS_J2= new DiLepChannel( "ee_OS_J2",cutFlowLabels,2,2,0);
	// mumu_OS_J2= new DiLepChannel( "mumu_OS_J2",cutFlowLabels,2,0,2);
 //        emu_SS_J2= new DiLepChannel( "emu_SS_J2",cutFlowLabels,2,1,1, save_fits);
	if (run_fakes && !data){
	  emu_OS_NPmu_J2= new DiLepChannel( "emu_OS_NPmu_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_OS_NPel_J2= new DiLepChannel( "emu_OS_NPel_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_SS_NPmu_J2= new DiLepChannel( "emu_SS_NPmu_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_SS_NPel_J2= new DiLepChannel( "emu_SS_NPel_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_SS_2PL_J2= new DiLepChannel( "emu_SS_2PL_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_OS_0PL_J2= new DiLepChannel( "emu_OS_0PL_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	  emu_SS_0PL_J2= new DiLepChannel( "emu_SS_0PL_J2",cdi_file_path, cutFlowLabels,2,1,1, f_out, data, eflavours,  m_n_boot_strap_min, m_n_boot_strap_max, m_treeReader->m_storeTracks, use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName);
	}
	// cutFlowLabels.push_back("1 lep_comb_cut");
	// //this is a channel we potentially want to fit later, so lets save the histograms for the fits!
	// // emu_OS_J2_1m_lj_cut = new DiLepChannel("emu_OS_J2_1m_lj_cut", cutFlowLabels, 2, 1, 1, save_fits);
	// cutFlowLabels.push_back("2 lep_comb_cut");
	// //this is a channel we potentially want to fit later, so lets save the histograms for the fits!
	// emu_OS_J2_m_lj_cut = new DiLepChannel("emu_OS_J2_m_lj_cut", cutFlowLabels, 2, 1, 1, save_fits);
	// emu_OS_J2_CR_bl= new DiLepChannel( "emu_OS_J2_CR_bl",cutFlowLabels, 2,1,1, save_fits);
	// emu_OS_J2_CR_lb= new DiLepChannel( "emu_OS_J2_CR_lb",cutFlowLabels, 2,1,1, save_fits);
	// emu_OS_J2_CR_ll= new DiLepChannel( "emu_OS_J2_CR_ll",cutFlowLabels, 2,1,1, save_fits);

	// if (run_fakes && !data)
	// {
	// 	emu_SS_J2_m_lj_cut = new DiLepChannel("emu_SS_J2_m_lj_cut", cutFlowLabels, 2, 1, 1);
	// 	emu_OS_NPmu_J2_m_lj_cut = new DiLepChannel("emu_OS_NPmu_J2_m_lj_cut", cutFlowLabels, 2, 1, 1);
	// 	emu_OS_NPel_J2_m_lj_cut = new DiLepChannel("emu_OS_NPel_J2_m_lj_cut", cutFlowLabels, 2, 1, 1);
	// 	emu_SS_NPmu_J2_m_lj_cut = new DiLepChannel("emu_SS_NPmu_J2_m_lj_cut", cutFlowLabels, 2, 1, 1);
	// 	emu_SS_NPel_J2_m_lj_cut = new DiLepChannel("emu_SS_NPel_J2_m_lj_cut", cutFlowLabels, 2, 1, 1);
	// 	emu_OS_0PL_J2_m_lj_cut= new DiLepChannel( "emu_OS_0PL_J2_m_lj_cut",cutFlowLabels,2,1,1, save_fits);
	// 	emu_SS_0PL_J2_m_lj_cut= new DiLepChannel( "emu_SS_0PL_J2_m_lj_cut",cutFlowLabels,2,1,1, save_fits);
	// }

	// cutFlowLabels.erase(cutFlowLabels.begin() + 2, cutFlowLabels.end());
	// cutFlowLabels.push_back("mumu Channel");
	// cutFlowLabels.push_back("OS");
	// cutFlowLabels.push_back("2 or 3 Jets");
	// cutFlowLabels.push_back("2 Jets");
	// mumu_OS_J2_bCuts = new DiLepChannel("mumu_OS_J2_bCuts", cutFlowLabels, 2, 0, 2);
	// cutFlowLabels.push_back("MET >60e3");
	// cutFlowLabels.push_back("mll >50e3");
	// cutFlowLabels.push_back("80e3 < mll < 100e3");
	// mumu_OS_J2_aCuts = new DiLepChannel("mumu_OS_J2_aCuts", cutFlowLabels, 2, 0, 2);
	// mumu_OS_J2_ZCR = new DiLepChannel("mumu_OS_J2_ZCR", cutFlowLabels, 2, 0, 2);
	// emufrommumu_OS_J2_ZCR = new DiLepChannel("emufrommumu_OS_J2_ZCR", cutFlowLabels, 2, 0, 2);
	// cutFlowLabels.push_back("1 lep_comb_cut");
	// // mumu_OS_J2_1m_lj_cut = new DiLepChannel("mumu_OS_J2_1m_lj_cut", cutFlowLabels, 2, 0, 2);
	// //    mumu_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("mumu_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 0, 2);
	// //    emufrommumu_OS_J2_ZCR_1m_lj_cut = new DiLepChannel("emufrommumu_OS_J2_ZCR_1m_lj_cut", cutFlowLabels, 2, 0, 2);
	// cutFlowLabels.push_back("2 lep_comb_cut");
	// mumu_OS_J2_m_lj_cut = new DiLepChannel("mumu_OS_J2_m_lj_cut", cutFlowLabels, 2, 0, 2);
	// mumu_OS_J2_ZCR_m_lj_cut = new DiLepChannel("mumu_OS_J2_ZCR_m_lj_cut", cutFlowLabels, 2, 0, 2);
	// emufrommumu_OS_J2_ZCR_m_lj_cut = new DiLepChannel("emufrommumu_OS_J2_ZCR_m_lj_cut", cutFlowLabels, 2, 0, 2);
	// cutFlowLabels.erase(cutFlowLabels.begin() + 5, cutFlowLabels.end());
	// cutFlowLabels.push_back("3 Jets");
	// mumu_OS_J3_bCuts = new DiLepChannel("mumu_OS_J3_bCuts", cutFlowLabels, 3, 0, 2);
	// cutFlowLabels.push_back("MET >60e3");
	// cutFlowLabels.push_back("mll >50e3");
	// cutFlowLabels.push_back("80e3 < mll < 100e3");
	// mumu_OS_J3_aCuts = new DiLepChannel("mumu_OS_J3_aCuts", cutFlowLabels, 3, 0, 2);
	// mumu_OS_J3_ZCR = new DiLepChannel("mumu_OS_J3_ZCR", cutFlowLabels, 3, 0, 2);
	// emufrommumu_OS_J3_ZCR = new DiLepChannel("emufrommumu_OS_J3_ZCR", cutFlowLabels, 3, 0, 2);
	std::cout << "Finished begin method" << std::endl;
}

void finalSelectionMC::SlaveBegin()
{
	// The SlaveBegin() function is called after the Begin() function.
	// When running with PROOF SlaveBegin() is called on each slave server.
	// The tree argument is deprecated (on PROOF 0 is passed).

	TString option = GetOption();
}

Bool_t finalSelectionMC::Process(Long64_t entry)
{
	// The Process() function is called for each entry in the tree (or possibly
	// keyed object in the case of PROOF) to be processed. The entry argument
	// specifies which entry in the currently loaded tree is to be processed.
	// It can be passed to either finalSelectionMC::GetEntry() or TBranch::GetEntry()
	// to read either all or the required parts of the data. When processing
	// keyed objects with PROOF, the object is already loaded and is available
	// via the fObject pointer.
	//
	// This function should contain the "body" of the analysis. It can contain
	// simple or elaborate selection criteria, run algorithms on the data
	// of the event and typically fill histograms.
	//
	// The processing can be stopped by calling Abort().
	//
	// Use fStatus to set the return value of TTree::Process().
	//
	// The return value is currently not used.

	m_treeReader->Process(entry);
	if (!(nE % 10000))
		cout << "Processing Event: " << nE + 1 << endl;
	nE++;
	//getting weight;
	//b_weight_mc->GetEntry(entry);
	//b_weight_pileup->GetEntry(entry);
	//b_weight_leptonSF->GetEntry(entry);
	//b_weight_jvt->GetEntry(entry);
	//	fChain->GetTree()->GetEntry(entry);

	int n_el = m_treeReader->el_charge->size();
	int n_mu = m_treeReader->mu_charge->size();

	TLorentzVector lep4vec;
	TLorentzVector seclep4vec;
	double weight = 1;
	if (!data)
	{
		weight = w_lumi;
		if (n_weight_shower != 0)
		{
			double weight_mc = m_treeReader->weights["weight_mc"];
			//std::cout << " Weight MC " << weight_mc << std::endl;
			if (n_weight_muRmuF == 0)
				weight = weight * m_treeReader->mc_generator_weights->at(n_weight_shower) ; // weight_mc */ weight_mc
			else
				weight = weight * m_treeReader->mc_generator_weights->at(n_weight_shower)  * (m_treeReader->mc_generator_weights->at(n_weight_muRmuF) / weight_mc);
		}
		for (auto const &w : weight_names_to_use)
		{
			if (m_treeReader->weights.find(w.c_str()) == m_treeReader->weights.end())
			{
				cout << "ERROR!!!: " << w << " not in TREE!!" << endl;
				exit(EXIT_FAILURE);
			}
			else
			{
				weight = weight * m_treeReader->weights[w.c_str()];
			}
		}
	}

	int nNonpromptel = 0, nNonpromptmu = 0;
	//all input events:
	// std::cout << "Adding to cutflow ... " << std::endl;
	ee_OS_J2_bCuts->addToCutflow(1, weight);
	ee_OS_J3_bCuts->addToCutflow(1, weight);
	ee_OS_J2_aCuts->addToCutflow(1, weight);
	ee_OS_J3_aCuts->addToCutflow(1, weight);
	emu_OS_J2->addToCutflow(1, weight);
	// emu_OS_J2_1m_lj_cut->addToCutflow(1, weight);
	emu_OS_J2_m_lj_cut->addToCutflow(1, weight);
	emu_OS_J2_CR_bl->addToCutflow(1, weight);
	emu_OS_J2_CR_lb->addToCutflow(1, weight);
	emu_OS_J2_CR_ll->addToCutflow(1, weight);
	emu_OS_J3->addToCutflow(1, weight);
	mumu_OS_J2_bCuts->addToCutflow(1, weight);
	mumu_OS_J3_bCuts->addToCutflow(1, weight);
	mumu_OS_J2_aCuts->addToCutflow(1, weight);
	mumu_OS_J3_aCuts->addToCutflow(1, weight);
	emu_SS_J2->addToCutflow(1, weight);
	if (run_fakes && !data)
	{
		emu_OS_NPmu_J2_m_lj_cut->addToCutflow(1, weight);
		emu_OS_NPmu_J2->addToCutflow(1, weight);
		emu_OS_NPel_J2_m_lj_cut->addToCutflow(1, weight);
		emu_OS_NPel_J2->addToCutflow(1, weight);
		emu_SS_J2_m_lj_cut->addToCutflow(1, weight);
		emu_SS_NPmu_J2_m_lj_cut->addToCutflow(1, weight);
		emu_SS_NPmu_J2->addToCutflow(1, weight);
		emu_SS_NPel_J2_m_lj_cut->addToCutflow(1, weight);
		emu_SS_NPel_J2->addToCutflow(1, weight);
		emu_SS_2PL_J2->addToCutflow(1, weight);
		emu_SS_J3->addToCutflow(1, weight);
		emu_OS_0PL_J2->addToCutflow(1, weight);
		emu_SS_0PL_J2->addToCutflow(1, weight);
		emu_OS_0PL_J2_m_lj_cut->addToCutflow(1, weight);
		emu_SS_0PL_J2_m_lj_cut->addToCutflow(1, weight);
	}

	//2 lepton cut:
	if (n_el + n_mu != 2)
		return kTRUE;

	ee_OS_J2_bCuts->addToCutflow(2, weight);
	ee_OS_J3_bCuts->addToCutflow(2, weight);
	ee_OS_J2_aCuts->addToCutflow(2, weight);
	ee_OS_J3_aCuts->addToCutflow(2, weight);
	emu_OS_J2->addToCutflow(2, weight);
	// emu_OS_J2_1m_lj_cut->addToCutflow(2, weight);
	emu_OS_J2_m_lj_cut->addToCutflow(2, weight);
	emu_OS_J2_CR_bl->addToCutflow(2, weight);
	emu_OS_J2_CR_lb->addToCutflow(2, weight);
	emu_OS_J2_CR_ll->addToCutflow(2, weight);
	emu_OS_J3->addToCutflow(2, weight);
	mumu_OS_J2_bCuts->addToCutflow(2, weight);
	mumu_OS_J3_bCuts->addToCutflow(2, weight);
	mumu_OS_J2_aCuts->addToCutflow(2, weight);
	mumu_OS_J3_aCuts->addToCutflow(2, weight);
	emu_SS_J2->addToCutflow(2, weight);
	if (run_fakes && !data)
	{
		emu_OS_NPmu_J2_m_lj_cut->addToCutflow(2, weight);
		emu_OS_NPmu_J2->addToCutflow(2, weight);
		emu_OS_NPel_J2_m_lj_cut->addToCutflow(2, weight);
		emu_OS_NPel_J2->addToCutflow(2, weight);
		emu_SS_J2_m_lj_cut->addToCutflow(2, weight);
		emu_SS_NPmu_J2_m_lj_cut->addToCutflow(2, weight);
		emu_SS_NPmu_J2->addToCutflow(2, weight);
		emu_SS_NPel_J2_m_lj_cut->addToCutflow(2, weight);
		emu_SS_NPel_J2->addToCutflow(2, weight);
		emu_SS_2PL_J2->addToCutflow(2, weight);
		emu_SS_J3->addToCutflow(2, weight);
		emu_OS_0PL_J2->addToCutflow(2, weight);
		emu_SS_0PL_J2->addToCutflow(2, weight);
		emu_OS_0PL_J2_m_lj_cut->addToCutflow(2, weight);
		emu_SS_0PL_J2_m_lj_cut->addToCutflow(2, weight);
	}
	// std::cout << "Added to cutflow 2 ... " << std::endl;
	//jet Multiplicity:
	//b_jet_pt->GetEntry(entry);
	int n_jet = m_treeReader->jet_pt->size();
	if (n_jet < 9)
		jet_multiplicity->Fill(n_jet, weight);
	else
		jet_multiplicity->Fill(9, weight);

	// cout<<"filled jet -muliplicity"<<endl;
	//el el Channel ************************************************************
	if ((n_el == 2) && (n_mu == 0))
	{
	  // cout<<"ee-chan"<<endl;
		ee_OS_J2_bCuts->addToCutflow(3, weight);
		ee_OS_J3_bCuts->addToCutflow(3, weight);
		ee_OS_J2_aCuts->addToCutflow(3, weight);
		ee_OS_J3_aCuts->addToCutflow(3, weight);
		lep4vec.SetPtEtaPhiM(m_treeReader->el_pt->at(0), m_treeReader->el_eta->at(0), m_treeReader->el_phi->at(0), 0);
		seclep4vec.SetPtEtaPhiM( m_treeReader->el_pt->at(1), m_treeReader->el_eta->at(1), m_treeReader->el_phi->at(1), 0);
		double mll = (lep4vec + seclep4vec).M();
		if (mll < m_ll_CutValue*1000)
		{
			return kTRUE;
		}

		if (mll < m_ll_CutValue*1000) { return kTRUE; }
		if ((m_treeReader->el_charge->at(0) * m_treeReader->el_charge->at(1)) > 0 && run_fakes)
		{
			return kTRUE;
		}
		else if ((m_treeReader->el_charge->at(0) * m_treeReader->el_charge->at(1)) < 0)
		{
			ee_OS_J2_bCuts->addToCutflow(4, weight);
			ee_OS_J3_bCuts->addToCutflow(4, weight);
			ee_OS_J2_aCuts->addToCutflow(4, weight);
			ee_OS_J3_aCuts->addToCutflow(4, weight);
		}
		else
		{
			return kTRUE;
		}
		//so only OS events left:
		//jets cuts:
		if (n_jet < 2 || n_jet > 3)
			return kTRUE;
		if (n_jet == 2){
		  if (m_treeReader->jet_m_jl->at(0)/1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1)/1000. < min_m_jl_CutValue) {
		    return kTRUE;
		  } 
		}

		ee_OS_J2_bCuts->addToCutflow(5, weight);
		ee_OS_J3_bCuts->addToCutflow(5, weight);
		ee_OS_J2_aCuts->addToCutflow(5, weight);
		ee_OS_J3_aCuts->addToCutflow(5, weight);
		if (n_jet == 2)
		{
			ee_OS_J2_aCuts->addToCutflow(6, weight);
			ee_OS_J2_bCuts->addToCutflow(6, weight);
		}
		else
		{
			ee_OS_J3_aCuts->addToCutflow(6, weight);
			ee_OS_J3_bCuts->addToCutflow(6, weight);
		}
		if (n_jet == 2)
		{
			ee_OS_J2_bCuts->AddEvent(m_treeReader, weight, entry);
			//min m_treeReader->jet_m_jl_cut:
			if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
			{
				return kTRUE; 
			}

			if ((mll > 80e3) && (mll < 100e3))
			{ //emu channel CR : SAME FLAVOUR (ee+mumu), OS, 80<mll<100, NO MET CUT!
				emufromee_OS_J2_ZCR->addToCutflow(9, weight);
				emufromee_OS_J2_ZCR->AddEvent(m_treeReader, weight, entry);

				//lets try the lj combi cut
				if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
				{
					// emufromee_OS_J2_ZCR_1m_lj_cut->addToCutflow(10, weight);
					// emufromee_OS_J2_ZCR_1m_lj_cut->AddEvent(m_treeReader, weight, entry);
					emufromee_OS_J2_ZCR_m_lj_cut->addToCutflow(10, weight);
					if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
					{
						emufromee_OS_J2_ZCR_m_lj_cut->addToCutflow(11, weight);
						emufromee_OS_J2_ZCR_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					}
				}
			}
		}
		else
		{
			ee_OS_J3_bCuts->AddEvent(m_treeReader, weight, entry);
			if ((mll > 80e3) && (mll < 100e3))
			{
				emufromee_OS_J3_ZCR->addToCutflow(9, weight);
				emufromee_OS_J3_ZCR->AddEvent(m_treeReader, weight, entry);
			}
		}
		//met cut:
		//b_met_met->GetEntry(entry);
		// if (met_met <= 60e3)
		// 	return kTRUE;
		if (n_jet == 2)
			ee_OS_J2_aCuts->addToCutflow(7, weight);
		else if (n_jet == 3)
			ee_OS_J3_aCuts->addToCutflow(7, weight);
		//mll cut:
		if (mll <= 50e3)
			return kTRUE;
		if (n_jet == 2)
			ee_OS_J2_aCuts->addToCutflow(8, weight);
		else if (n_jet == 3)
			ee_OS_J3_aCuts->addToCutflow(8, weight);

		if ((mll > 80e3) && (mll < 100e3))
		{
			if (n_jet == 2)
			{
				//min m_treeReader->jet_m_jl_cut:
				if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
				{
					return kTRUE; 
				}
				ee_OS_J2_ZCR->addToCutflow(9, weight);
				ee_OS_J2_ZCR->AddEvent(m_treeReader, weight, entry);
				//lets try the lj combi cut
				if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
				{
					// ee_OS_J2_ZCR_1m_lj_cut->addToCutflow(10, weight);
					// ee_OS_J2_ZCR_1m_lj_cut->AddEvent(m_treeReader, weight, entry);
					ee_OS_J2_ZCR_m_lj_cut->addToCutflow(10, weight);
					if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
					{
						ee_OS_J2_ZCR_m_lj_cut->addToCutflow(11, weight);
						ee_OS_J2_ZCR_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					}
				}
			}
			else
			{
				ee_OS_J3_ZCR->addToCutflow(9, weight);
				ee_OS_J3_ZCR->AddEvent(m_treeReader, weight, entry);
			}
		}
		else
		{

			if (n_jet == 2)
			{
				//min m_treeReader->jet_m_jl_cut:
				if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
				{
					return kTRUE; 
				}
				ee_OS_J2_aCuts->addToCutflow(9, weight);
				ee_OS_J2_aCuts->AddEvent(m_treeReader, weight, entry);
				//lets try the lj combi cut
				if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
				{
					// ee_OS_J2_1m_lj_cut->addToCutflow(10, weight);
					// ee_OS_J2_1m_lj_cut->AddEvent(m_treeReader, weight, entry);
					ee_OS_J2_m_lj_cut->addToCutflow(10, weight);
					if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
					{
						ee_OS_J2_m_lj_cut->addToCutflow(11, weight);
						ee_OS_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					}
				}
			}
			else
			{
				ee_OS_J3_aCuts->addToCutflow(9, weight);
				ee_OS_J3_aCuts->AddEvent(m_treeReader, weight, entry);
			}
		}
	}
	// std::cout << "Now at el mu channel ... " << std::endl;
	//el mu channel *************************************************************
	else if ((n_el == 1) && (n_mu == 1))
	{
	  // cout<<"emu-chan"<<endl;
		emu_OS_J2->addToCutflow(3, weight);
		// emu_OS_J2_1m_lj_cut->addToCutflow(3, weight);
		emu_OS_J2_m_lj_cut->addToCutflow(3, weight);
		emu_OS_J2_CR_bl->addToCutflow(3, weight);
		emu_OS_J2_CR_lb->addToCutflow(3, weight);
		emu_OS_J2_CR_ll->addToCutflow(3, weight);
		emu_OS_J3->addToCutflow(3, weight);
		emu_SS_J2->addToCutflow(3, weight);
		lep4vec.SetPtEtaPhiM(m_treeReader->mu_pt->at(0), m_treeReader->mu_eta->at(0), m_treeReader->mu_phi->at(0), 0);
		seclep4vec.SetPtEtaPhiM(m_treeReader->el_pt->at(0), m_treeReader->el_eta->at(0), m_treeReader->el_phi->at(0), 0);
		double mll = (lep4vec + seclep4vec).M();
		if (mll < m_ll_CutValue*1000) {
		  return kTRUE;
		} 
		if ((run_fakes || correctFakes) && !data)
		{
			if (!m_treeReader->el_true_isPrompt->at(0)) {
				nNonpromptel++;
				if (m_treeReader->el_true_type->at(0) == 6 && m_treeReader->el_true_origin->at(0) == 13) {	//these are Z->mumu events where a muon is reconstructed as an electron! - consider as prompt!
					nNonpromptel--;
				}
			}
			if (!m_treeReader->mu_true_isPrompt->at(0)) {
				nNonpromptmu++;
			}
		}
		if (run_fakes && !data)
		{
			emu_OS_NPmu_J2_m_lj_cut->addToCutflow(3, weight);
			emu_OS_NPmu_J2->addToCutflow(3, weight);
			emu_OS_NPel_J2_m_lj_cut->addToCutflow(3, weight);
			emu_OS_NPel_J2->addToCutflow(3, weight);
			emu_SS_J2_m_lj_cut->addToCutflow(3, weight);
			emu_SS_NPmu_J2_m_lj_cut->addToCutflow(3, weight);
			emu_SS_NPmu_J2->addToCutflow(3, weight);
			emu_SS_NPel_J2_m_lj_cut->addToCutflow(3, weight);
			emu_SS_NPel_J2->addToCutflow(3, weight);
			emu_SS_2PL_J2->addToCutflow(3, weight);
			emu_SS_J3->addToCutflow(3, weight);
			emu_OS_0PL_J2->addToCutflow(3, weight);
			emu_SS_0PL_J2->addToCutflow(3, weight);
			emu_OS_0PL_J2_m_lj_cut->addToCutflow(3, weight);
			emu_SS_0PL_J2_m_lj_cut->addToCutflow(3, weight);
		}

		// std::cout<< " mll: "<<mll<<" cutval: " << m_ll_CutValue*1000 <<std::endl;
		if (mll < m_ll_CutValue*1000)
		{
			// std::cout<< "cut on away mll: "<<mll<<std::endl;
			return kTRUE;
		}

		if ((m_treeReader->el_charge->at(0) * m_treeReader->mu_charge->at(0)) > 0)
		{
			emu_SS_J2->addToCutflow(4, weight);
			if (run_fakes && !data)
			{
				emu_SS_J2_m_lj_cut->addToCutflow(4, weight);
				emu_SS_J3->addToCutflow(4, weight);
				if (nNonpromptmu == 1 && nNonpromptel == 0)
				{
					emu_SS_NPmu_J2_m_lj_cut->addToCutflow(4, weight);
					emu_SS_NPmu_J2->addToCutflow(4, weight);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 0)
				{
					emu_SS_NPel_J2_m_lj_cut->addToCutflow(4, weight);
					emu_SS_NPel_J2->addToCutflow(4, weight);
				}
				else if (nNonpromptel == 0 && nNonpromptmu == 0)
				{
					emu_SS_2PL_J2->addToCutflow(4, weight);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 1)
				{
					emu_SS_0PL_J2->addToCutflow(4, weight);
					emu_SS_0PL_J2_m_lj_cut->addToCutflow(4, weight);
				}

			}

			if (n_jet < 2 || n_jet > 3)
			{
				return kTRUE;
			}
		  
			if (n_jet == 2){
			  if (m_treeReader->jet_m_jl->at(0)/1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1)/1000. < min_m_jl_CutValue) {
			    return kTRUE;
			  }
			}


			emu_SS_J2->addToCutflow(5, weight);
			if (run_fakes && !data)
			{
				emu_SS_J2_m_lj_cut->addToCutflow(5, weight);
				emu_SS_J3->addToCutflow(5, weight);
				if (nNonpromptmu == 1 && nNonpromptel == 0)
				{
					emu_SS_NPmu_J2_m_lj_cut->addToCutflow(5, weight);
					emu_SS_NPmu_J2->addToCutflow(5, weight);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 0)
				{
					emu_SS_NPel_J2_m_lj_cut->addToCutflow(5, weight);
					emu_SS_NPel_J2->addToCutflow(5, weight);
				}
				else if (nNonpromptel == 0 && nNonpromptmu == 0)
				{
					emu_SS_2PL_J2->addToCutflow(5, weight);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 1)
				{
					emu_SS_0PL_J2->addToCutflow(5, weight);
					emu_SS_0PL_J2_m_lj_cut->addToCutflow(5, weight);
				}
			}
			if (n_jet == 2)
			{
				//min m_treeReader->jet_m_jl_cut:
				if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
				{
					return kTRUE; 
				}
				emu_SS_J2->addToCutflow(6, weight);
				emu_SS_J2->AddEvent(m_treeReader, weight, entry);
				if (run_fakes && !data)
				{
					if (nNonpromptmu == 1 && nNonpromptel == 0)
					{
						emu_SS_NPmu_J2_m_lj_cut->addToCutflow(6, weight);
						emu_SS_NPmu_J2->addToCutflow(6, weight);
						emu_SS_NPmu_J2->AddEvent(m_treeReader, weight, entry);
					}
					else if (nNonpromptel == 1 && nNonpromptmu == 0)
					{
						emu_SS_NPel_J2_m_lj_cut->addToCutflow(6, weight);
						emu_SS_NPel_J2->addToCutflow(6, weight);
						emu_SS_NPel_J2->AddEvent(m_treeReader, weight, entry);
					}
					else if (nNonpromptel == 0 && nNonpromptmu == 0)
					{
						emu_SS_2PL_J2->addToCutflow(6, weight);
						emu_SS_2PL_J2->AddEvent(m_treeReader, weight, entry);
					}
					else if (nNonpromptel == 1 && nNonpromptmu == 1)
					{
						emu_SS_0PL_J2->addToCutflow(6, weight);
						emu_SS_0PL_J2->AddEvent(m_treeReader, weight, entry);
						emu_SS_0PL_J2_m_lj_cut->addToCutflow(6, weight);
					}
					//lets try the lj combi cut
					if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
					{
						emu_SS_J2_m_lj_cut->addToCutflow(7, weight);
						if (nNonpromptmu == 1 && nNonpromptel == 0)
						{
							emu_SS_NPmu_J2_m_lj_cut->addToCutflow(7, weight);
						}
						else if (nNonpromptel == 1 && nNonpromptmu == 0)
						{
							emu_SS_NPel_J2_m_lj_cut->addToCutflow(7, weight);
						}
						else if (nNonpromptel == 1 && nNonpromptmu == 1)
						{
							emu_SS_0PL_J2_m_lj_cut->addToCutflow(7, weight);
						}
						if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
						{
							emu_SS_J2_m_lj_cut->addToCutflow(8, weight);
							emu_SS_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
							if (nNonpromptmu == 1 && nNonpromptel == 0)
							{
								emu_SS_NPmu_J2_m_lj_cut->addToCutflow(8, weight);
								emu_SS_NPmu_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
							}
							else if (nNonpromptel == 1 && nNonpromptmu == 0)
							{
								emu_SS_NPel_J2_m_lj_cut->addToCutflow(8, weight);
								emu_SS_NPel_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
							}
							else if (nNonpromptel == 1 && nNonpromptmu == 1)
							{
								emu_SS_0PL_J2_m_lj_cut->addToCutflow(8, weight);
								emu_SS_0PL_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
							}
						}
					}
				}
			}
			else
			{
				if (run_fakes && !data)
				{
					emu_SS_J3->addToCutflow(6, weight);
					emu_SS_J3->AddEvent(m_treeReader, weight, entry);
				}
			}
			return kTRUE;
		}
		else if ((m_treeReader->el_charge->at(0) * m_treeReader->mu_charge->at(0)) < 0)
		{
			emu_OS_J2->addToCutflow(4, weight);
			emu_OS_J2_m_lj_cut->addToCutflow(4, weight);
			emu_OS_J2_CR_bl->addToCutflow(4, weight);
			emu_OS_J2_CR_lb->addToCutflow(4, weight);
			emu_OS_J2_CR_ll->addToCutflow(4, weight);
			emu_OS_J3->addToCutflow(4, weight);
			if (run_fakes && !data)
			{
				if (nNonpromptmu == 1 && nNonpromptel == 0)
				{
					emu_OS_NPmu_J2_m_lj_cut->addToCutflow(4, weight);
					emu_OS_NPmu_J2->addToCutflow(4, weight);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 0)
				{
					emu_OS_NPel_J2_m_lj_cut->addToCutflow(4, weight);
					emu_OS_NPel_J2->addToCutflow(4, weight);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 1)
				{
					emu_OS_0PL_J2->addToCutflow(4, weight);
					emu_OS_0PL_J2_m_lj_cut->addToCutflow(4, weight);
				}
			}
		}
		else
		{
			return kTRUE;
		}
		//Only OS events left now:
		if (n_jet < 2 || n_jet > 3)
			return kTRUE;
		  
		if (n_jet == 2){
		  if (m_treeReader->jet_m_jl->at(0)/1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue) {
		    return kTRUE;
		  }
		}
		emu_OS_J2->addToCutflow(5, weight);
		emu_OS_J2_m_lj_cut->addToCutflow(5, weight);
		emu_OS_J2_CR_bl->addToCutflow(5, weight);
		emu_OS_J2_CR_lb->addToCutflow(5, weight);
		emu_OS_J2_CR_ll->addToCutflow(5, weight);
		emu_OS_J3->addToCutflow(5, weight);
		if (run_fakes && !data)
		{
			if (nNonpromptmu == 1 && nNonpromptel == 0)
			{
				emu_OS_NPmu_J2_m_lj_cut->addToCutflow(5, weight);
				emu_OS_NPmu_J2->addToCutflow(5, weight);
			}
			else if (nNonpromptel == 1 && nNonpromptmu == 0)
			{
				emu_OS_NPel_J2_m_lj_cut->addToCutflow(5, weight);
				emu_OS_NPel_J2->addToCutflow(5, weight);
			}
			else if (nNonpromptel == 1 && nNonpromptmu == 1)
			{
				emu_OS_0PL_J2->addToCutflow(5, weight);
				emu_OS_0PL_J2_m_lj_cut->addToCutflow(5, weight);
			}
		}

		if (n_jet == 2)
		{
		  //		  if(reweight_pT && !data ){
		  if(reweight_pT &&  (!data && m_treeReader->jet_truthflav->at(0) == 5) ){
		  
		  if(reweight_pT){
		    weight*=histo_reweight->GetBinContent(histo_reweight->GetXaxis()->FindBin(m_treeReader->jet_pt->at(0)/1000)) ;
		  }
		  if(reweight_ip3d && !data){
		    weight*=histo_reweight->GetBinContent(histo_reweight->GetXaxis()->FindBin(m_treeReader->IP3D_ntrk->at(0))) ;
		  }
		  if(reweight_jf && !data){
		    weight*=histo_reweight->GetBinContent(histo_reweight->GetXaxis()->FindBin(m_treeReader->JF_ntrk->at(0))) ;
		  }
		  if(reweight_sv1 && !data){
		    weight*=histo_reweight->GetBinContent(histo_reweight->GetXaxis()->FindBin(m_treeReader->SV1_ntrk->at(0))) ;
		  }
		}
		  
		  //min m_treeReader->jet_m_jl_cut:
		  if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue){
		      return kTRUE; 
		    }			
		  if (correctFakes && nNonpromptel == 1 && nNonpromptmu == 0)
		    {
				if (m_treeReader->el_pt->at(0) / 1000. < 150.)
					weight *= NPel_bin1;
				else if (m_treeReader->el_pt->at(0) / 1000. >= 150. && m_treeReader->el_pt->at(0) / 1000. < 300.)
					weight *= NPel_bin2;
				else
					weight *= NPel_bin3;
			}
			emu_OS_J2->addToCutflow(6, weight);
			emu_OS_J2_m_lj_cut->addToCutflow(6, weight);
			emu_OS_J2_CR_bl->addToCutflow(6, weight);
			emu_OS_J2_CR_lb->addToCutflow(6, weight);
			emu_OS_J2_CR_ll->addToCutflow(6, weight);
			emu_OS_J2->AddEvent(m_treeReader, weight, entry);
			if(mll  < 80e3 || mll > 100e3 )
			  // emu_OS_J2_mz->AddEvent(m_treeReader, weight, entry);

			if (run_fakes && !data)
			{
				if (nNonpromptmu == 1 && nNonpromptel == 0)
				{
					emu_OS_NPmu_J2_m_lj_cut->addToCutflow(6, weight);
					emu_OS_NPmu_J2->addToCutflow(6, weight);
					emu_OS_NPmu_J2->AddEvent(m_treeReader, weight, entry);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 0)
				{
					emu_OS_NPel_J2_m_lj_cut->addToCutflow(6, weight);
					emu_OS_NPel_J2->addToCutflow(6, weight);
					emu_OS_NPel_J2->AddEvent(m_treeReader, weight, entry);
				}
				else if (nNonpromptel == 1 && nNonpromptmu == 1)
				{
					emu_OS_0PL_J2->addToCutflow(6, weight);
					emu_OS_0PL_J2_m_lj_cut->addToCutflow(6, weight);
					emu_OS_0PL_J2->AddEvent(m_treeReader, weight, entry);
				}
			}

			//lets try the lj combi cut
			if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
			{
				emu_OS_J2_m_lj_cut->addToCutflow(7, weight);
				emu_OS_J2_CR_bl->addToCutflow(7, weight);
				if (run_fakes && !data)
				{
					if (nNonpromptmu == 1 && nNonpromptel == 0)
					{
						emu_OS_NPmu_J2_m_lj_cut->addToCutflow(7, weight);
					}
					else if (nNonpromptel == 1 && nNonpromptmu == 0)
					{
						emu_OS_NPel_J2_m_lj_cut->addToCutflow(7, weight);
					}
					else if (nNonpromptel == 1 && nNonpromptmu == 1)
					{
						emu_OS_0PL_J2_m_lj_cut->addToCutflow(7, weight);
					}
				}
				if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
				{
					emu_OS_J2_m_lj_cut->addToCutflow(8, weight);
					emu_OS_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					if (run_fakes && !data)
					{
						if (nNonpromptmu == 1 && nNonpromptel == 0)
						{
							emu_OS_NPmu_J2_m_lj_cut->addToCutflow(8, weight);
							emu_OS_NPmu_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
						}
						else if (nNonpromptel == 1 && nNonpromptmu == 0)
						{
							emu_OS_NPel_J2_m_lj_cut->addToCutflow(8, weight);
							emu_OS_NPel_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
						}
						else if (nNonpromptel == 1 && nNonpromptmu == 1)
						{
							emu_OS_0PL_J2_m_lj_cut->addToCutflow(8, weight);
							emu_OS_0PL_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
						}
					}
				}else{
				  emu_OS_J2_CR_bl->addToCutflow(8, weight);
				  emu_OS_J2_CR_bl->AddEvent(m_treeReader, weight, entry);
				}
			}
			else
			{
				if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
				{
					emu_OS_J2_CR_lb->AddEvent(m_treeReader, weight, entry);
				}
				else
				{
					emu_OS_J2_CR_ll->AddEvent(m_treeReader, weight, entry);
				}
			}
		}
		else
		{
			emu_OS_J3->addToCutflow(6, weight);
			emu_OS_J3->AddEvent(m_treeReader, weight, entry);
		}
	}
	// std::cout << "Now mu mu channel ... " << std::endl;
	//mu mu channel *************************************************************
	else if ((n_el == 0) && (n_mu == 2))
	{
	  // cout<<"mumu-chan"<<endl;
		mumu_OS_J2_bCuts->addToCutflow(3, weight);
		mumu_OS_J3_bCuts->addToCutflow(3, weight);
		mumu_OS_J2_aCuts->addToCutflow(3, weight);
		mumu_OS_J3_aCuts->addToCutflow(3, weight);
		lep4vec.SetPtEtaPhiM(m_treeReader->mu_pt->at(0), m_treeReader->mu_eta->at(0), m_treeReader->mu_phi->at(0), 0);
		seclep4vec.SetPtEtaPhiM(m_treeReader->mu_pt->at(1), m_treeReader->mu_eta->at(1), m_treeReader->mu_phi->at(1), 0);
		double mll = (lep4vec + seclep4vec).M();
		if (mll < m_ll_CutValue*1000)
		{
			return kTRUE;
		}

		if ((m_treeReader->mu_charge->at(0) * m_treeReader->mu_charge->at(1)) > 0 && run_fakes)
		{
			return kTRUE;
		}
		else if ((m_treeReader->mu_charge->at(0) * m_treeReader->mu_charge->at(1)) < 0)
		{
			mumu_OS_J2_bCuts->addToCutflow(4, weight);
			mumu_OS_J3_bCuts->addToCutflow(4, weight);
			mumu_OS_J2_aCuts->addToCutflow(4, weight);
			mumu_OS_J3_aCuts->addToCutflow(4, weight);
		}
		else
		{
			return kTRUE;
		}
		//jets cuts:
		if (n_jet < 2 || n_jet > 3)
			return kTRUE;
		if (n_jet == 2){
		  if (m_treeReader->jet_m_jl->at(0)/1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1)/1000. < min_m_jl_CutValue) {
		    return kTRUE;
		  }
		}
		mumu_OS_J2_bCuts->addToCutflow(5, weight);
		mumu_OS_J3_bCuts->addToCutflow(5, weight);
		mumu_OS_J2_aCuts->addToCutflow(5, weight);
		mumu_OS_J3_aCuts->addToCutflow(5, weight);
		if (n_jet == 2)
		{
			mumu_OS_J2_bCuts->addToCutflow(6, weight);
			mumu_OS_J2_aCuts->addToCutflow(6, weight);
		}
		else
		{
			mumu_OS_J3_aCuts->addToCutflow(6, weight);
			mumu_OS_J3_bCuts->addToCutflow(6, weight);
		}
		if (n_jet == 2)
		{
			mumu_OS_J2_bCuts->AddEvent(m_treeReader, weight, entry);
			if ((mll > 80e3) && (mll < 100e3))
			{
				//min m_treeReader->jet_m_jl_cut:
				if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
				{
					return kTRUE; 
				}
				emufrommumu_OS_J2_ZCR->addToCutflow(9, weight);
				emufrommumu_OS_J2_ZCR->AddEvent(m_treeReader, weight, entry);

				//lets try the lj combi cut
				if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
				{
					emufrommumu_OS_J2_ZCR_m_lj_cut->addToCutflow(10, weight);
					if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
					{
						emufrommumu_OS_J2_ZCR_m_lj_cut->addToCutflow(11, weight);
						emufrommumu_OS_J2_ZCR_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					}
				}
			}
		}
		else
		{
			mumu_OS_J3_bCuts->AddEvent(m_treeReader, weight, entry);
			if ((mll > 80e3) && (mll < 100e3))
			{
				emufrommumu_OS_J3_ZCR->addToCutflow(9, weight);
				emufrommumu_OS_J3_ZCR->AddEvent(m_treeReader, weight, entry);
			}
		}
		//met cut:
		// b_met_met->GetEntry(entry);
		// if (met_met <= 60e3)
		// 	return kTRUE;
		if (n_jet == 2)
			mumu_OS_J2_aCuts->addToCutflow(7, weight);
		else if (n_jet == 3)
			mumu_OS_J3_aCuts->addToCutflow(7, weight);
		//mll cut:
		if (mll <= 50e3)
			return kTRUE;
		if (n_jet == 2)
			mumu_OS_J2_aCuts->addToCutflow(8, weight);
		else if (n_jet == 3)
			mumu_OS_J3_aCuts->addToCutflow(8, weight);

		if ((mll > 80e3) && (mll < 100e3))
		{
			if (n_jet == 2)
			{
				//min m_treeReader->jet_m_jl_cut:
				if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
				{
					return kTRUE; 
				}
				mumu_OS_J2_ZCR->addToCutflow(9, weight);
				mumu_OS_J2_ZCR->AddEvent(m_treeReader, weight, entry);

				//lets try the lj combi cut
				if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
				{
					mumu_OS_J2_ZCR_m_lj_cut->addToCutflow(10, weight);
					if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
					{
						mumu_OS_J2_ZCR_m_lj_cut->addToCutflow(11, weight);
						mumu_OS_J2_ZCR_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					}
				}
			}
			else
			{
				mumu_OS_J3_ZCR->addToCutflow(9, weight);
				mumu_OS_J3_ZCR->AddEvent(m_treeReader, weight, entry);
			}
		}
		else
		{
			if (n_jet == 2)
			{
				//min m_treeReader->jet_m_jl_cut:
				if (m_treeReader->jet_m_jl->at(0) / 1000. < min_m_jl_CutValue || m_treeReader->jet_m_jl->at(1) / 1000. < min_m_jl_CutValue)
				{
					return kTRUE; 
				}
				mumu_OS_J2_aCuts->AddEvent(m_treeReader, weight, entry);
				mumu_OS_J2_aCuts->addToCutflow(9, weight);
				//lets try the lj combi cut
				if (m_treeReader->jet_m_jl->at(0) / 1000. < m_jl_CutValue)
				{
					mumu_OS_J2_m_lj_cut->addToCutflow(10, weight);
					if (m_treeReader->jet_m_jl->at(1) / 1000. < m_jl_CutValue)
					{
						mumu_OS_J2_m_lj_cut->addToCutflow(11, weight);
						mumu_OS_J2_m_lj_cut->AddEvent(m_treeReader, weight, entry);
					}
				}
			}
			else
			{
				mumu_OS_J3_aCuts->AddEvent(m_treeReader, weight, entry);
				mumu_OS_J3_aCuts->addToCutflow(9, weight);
			}
		}
	}
	return kTRUE;
}

void finalSelectionMC::SlaveTerminate()
{
	// The SlaveTerminate() function is called after all entries or objects
	// have been processed. When running with PROOF SlaveTerminate() is called
	// on each slave server.
}

void finalSelectionMC::Terminate()
{
	// The Terminate() function is the last function to be called during
	// a query. It always runs on the client, it can be used to present
	// the results graphically or save the results to file.
	std::cout << "ran over " << nE << " events" << std::endl;
	TFile* file=TFile::Open(output.c_str(),"RECREATE");
	file->cd();
	jet_multiplicity->Write();
	// cout<<"wrote multiplicity" << endl;
	TVectorD *lumi_weight;
	lumi_weight = new TVectorD(1);
	lumi_weight[0][0] = w_lumi;
	lumi_weight->Write("applied_lumi_weight");
	// //cout<<"wrote lumiweight" << endl;
	ee_OS_J2_bCuts->Save(file);
	ee_OS_J3_bCuts->Save(file);
	ee_OS_J2_aCuts->Save(file);
	// ee_OS_J2_1m_lj_cut->Save(file);
	ee_OS_J2_m_lj_cut->Save(file);
	ee_OS_J3_aCuts->Save(file);
	// ////delete ee_OS_J3_aCuts;
	// cout<<"wrote ee-channel" << endl;
	emu_OS_J2->Save(file);
	// emu_OS_J2_mz->Save(file);
	// ee_OS_J2->Save(file);
	// mumu_OS_J2->Save(file);
	// emu_OS_J2_1m_lj_cut->Save(file);
	emu_OS_J2_m_lj_cut->Save(file);
	emu_OS_J2_CR_ll->Save(file);
	emu_OS_J2_CR_bl->Save(file);
	emu_OS_J2_CR_lb->Save(file);
	emu_OS_J3->Save(file);
	// //cout<<"deleting" << endl;
	// ////delete emu_OS_J3;
	// cout<<"wrote emu-channel" << endl;
	mumu_OS_J2_bCuts->Save(file);
	mumu_OS_J3_bCuts->Save(file);
	mumu_OS_J2_aCuts->Save(file);
	// mumu_OS_J2_1m_lj_cut->Save(file);
	mumu_OS_J2_m_lj_cut->Save(file);
	mumu_OS_J3_aCuts->Save(file);
	// //cout<<"deleting" << endl;
	// ////delete mumu_OS_J3_aCuts;
	// cout<<"wrote mumu_OS_J3_aCuts" << endl;
	// cout<<"wrote all" << endl;
	ee_OS_J2_ZCR->Save(file);
	ee_OS_J3_ZCR->Save(file);
	emufromee_OS_J2_ZCR->Save(file);
	// emufromee_OS_J2_ZCR_1m_lj_cut->Save(file);
	emufromee_OS_J2_ZCR_m_lj_cut->Save(file);
	emufromee_OS_J3_ZCR->Save(file);
	mumu_OS_J2_ZCR->Save(file);
	mumu_OS_J3_ZCR->Save(file);
	emufrommumu_OS_J2_ZCR->Save(file);
	// emufrommumu_OS_J2_ZCR_1m_lj_cut->Save(file);
	emufrommumu_OS_J2_ZCR_m_lj_cut->Save(file);
	emufrommumu_OS_J3_ZCR->Save(file);
	// ee_OS_J2_ZCR_1m_lj_cut->Save(file);
	ee_OS_J2_ZCR_m_lj_cut->Save(file);
	// mumu_OS_J2_ZCR_1m_lj_cut->Save(file);
	mumu_OS_J2_ZCR_m_lj_cut->Save(file);
	emu_SS_J2->Save(file);
	if (run_fakes && !data)
	{
		emu_SS_J2_m_lj_cut->Save(file);
		emu_SS_J3->Save(file);
		emu_OS_NPmu_J2_m_lj_cut->Save(file);
		emu_OS_NPel_J2_m_lj_cut->Save(file);
		emu_SS_NPmu_J2_m_lj_cut->Save(file);
		emu_SS_NPel_J2_m_lj_cut->Save(file);
		emu_OS_NPmu_J2->Save(file);
		emu_OS_NPel_J2->Save(file);
		emu_SS_NPmu_J2->Save(file);
		emu_SS_NPel_J2->Save(file);
		emu_SS_2PL_J2->Save(file);
		emu_SS_0PL_J2->Save(file);
		emu_OS_0PL_J2->Save(file);
		emu_SS_0PL_J2_m_lj_cut->Save(file);
		emu_OS_0PL_J2_m_lj_cut->Save(file);
	}

	std::cout << "TSelector: End." << endl;
	file->Close();
	std::cout << " Closing the TFile with name " << output.c_str()<<std::endl;
}
