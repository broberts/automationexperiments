// PtBinMeanContainer.h

#pragma once

#include <math.h>
#include <TDirectory.h>
#include <vector>
#include <string>
#include "PtBinSingle.h"

class Pt_bin_mean_container
{
public:
	TDirectory* dir;
	TDirectory* chan_dir;
	std::vector<Pt_bin_single*> v_single_pt_bins;
	const Double_t* bin_edges;
	bool data;
	int n_pt_bins;
	std::string m_name;
public:
	// Constructor
	Pt_bin_mean_container(std::string name, std::string taggerName, TDirectory* chan_dir, int n_pt_bins, const Double_t* pt_bins, int n_tagger, const Double_t* tagger_bins, bool data, std::vector<std::string> eflavours, std::string pt_label_jet1, std::string pt_label_jet2);
	
    // Fills something
	void Fill(Double_t* where_vector, double weight,  std::vector<int> *jet_truthflav);

    // Writes something
    void Write();

    ClassDef(Pt_bin_mean_container,1);
};
