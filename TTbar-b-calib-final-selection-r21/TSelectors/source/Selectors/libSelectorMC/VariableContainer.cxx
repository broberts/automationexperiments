// VariableContainer.cxx

#include "VariableContainer.h"

ClassImp(Variable_Container)

Variable_Container::Variable_Container(std::string name, int n_jets, int bins, double xmin, double xmax, bool data, std::vector<std::string> eflavours)
{
	this->name = name;
	this->m_data = data;
	if (m_data)
	{
		eflavours.clear();
		eflavours.push_back("data");
	}
	for (const auto &eventFlav : eflavours)
	{
		m_histograms[eventFlav] = new TH1F(("h_" + name + "_" + eventFlav).c_str(), (name + " with Jet Flavours: " + eventFlav).c_str(), bins, xmin, xmax);
		//cout<<"created: "<< ("h_" + name + "_" + flav) <<endl;
		m_histograms[eventFlav]->Sumw2();
	}
}

Variable_Container::Variable_Container(std::string name, int n_jets, int nbins, const double* xbins, bool data, std::vector<std::string> eflavours)
{
	this->name = name;
	this->m_data = data;
	if (m_data)
	{
		eflavours.clear();
		eflavours.push_back("data");
	}
	for (const auto &eventFlav : eflavours)
	{
		m_histograms[eventFlav] = new TH1F(("h_" + name + "_" + eventFlav).c_str(), (name + " with Jet Flavours: " + eventFlav).c_str(), nbins, xbins);
		//cout<<"created: "<< ("h_" + name + "_" + flav) <<endl;
		m_histograms[eventFlav]->Sumw2();
	}
}

Variable_Container::~Variable_Container()
{
}

void Variable_Container::Write()
{
	for (const auto &x : m_histograms)
		x.second->Write();
}

void Variable_Container::addEvent(double value, double weight, std::vector<int> *jet_truthflav)
{ //defing string flavour "bb","bl"...:
        std::string eventFlav = Variable_Container::getEventFlav(jet_truthflav);
	// std::cout<<"filling: "<< ("h_" + this->name + "_" + eventFlav) << std::endl;
	// std::cout<<"value: "<<value << std::endl;
	// std::cout << "weight: " << weight << std::endl;
	 //adding event
	m_histograms[eventFlav]->Fill(value, weight);
}

std::string Variable_Container::getEventFlav(std::vector<int> *jet_truthflav)
{
	std::string eventFlav = "";
	if (m_data)
	{
		eventFlav = "data";
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			if (jet_truthflav->at(i) == 4)
				eventFlav = eventFlav + "c";
			else if (jet_truthflav->at(i) == 5)
				eventFlav = eventFlav + "b";
			else
				eventFlav = eventFlav + "l"; //this is also tau jets!
		}
	}
	return eventFlav;
}
