// PtBinSingle.h

#pragma once

#include <THnSparse.h>
#include <TAxis.h>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <math.h>

class Pt_bin_single 
{
public:
	std::string name;
	Double_t pt_1_low, pt_1_high, pt_2_low, pt_2_high;
	std::map<std::string, THnSparseD *> h_pt_bin;
	bool data;
	std::vector<std::string> eflavours;
	std::string m_pt_label_jet1;
	std::string m_pt_label_jet2;

public:
	// Constructor
	Pt_bin_single(std::string name, std::string taggerName, int n_pt, Double_t pt_1_low, Double_t pt_1_high, Double_t pt_2_low, Double_t pt_2_high, int n_tagger, const Double_t* tagger_bins, bool data, std::vector<std::string> eflavours, std::string pt_label_jet1, std::string pt_label_jet2);

    // Fills something
	bool Fill(Double_t* where_vector, double weight,  std::vector<int> *jet_truthflav);

    // Writes something
    void Write();

    ClassDef(Pt_bin_single,1);
};
