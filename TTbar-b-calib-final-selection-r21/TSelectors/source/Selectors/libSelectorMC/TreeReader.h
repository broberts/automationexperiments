// finalSelectionMC.h
#pragma once

#include <TROOT.h>
#include <TSelector.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TObject.h>
#include <TList.h>
#include <TTree.h>
#include <TAxis.h>
#include <TChain.h>
#include <TLorentzVector.h>
#include <TBranch.h>
#include <TH1F.h>
#include <TH2F.h>
#include <THnSparse.h>
#include <TVectorD.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TStyle.h>
#include <TEventList.h>
#include <TH1D.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <string>
#include <set>
#include <algorithm>
#include <string>
#include <map>
#include <math.h>
#include <vector>

class TreeReader : public TSelector{

 public:
  bool m_storeTracks=false;
  TTree *fChain;               //!pointer to the analyzed TTree or TChain       
  
  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  std::vector<float> *mc_generator_weights;
  std::map<std::string, Float_t> weights;
  //  Float_t         weight_mc;
  //  Float_t         weight_pileup;
  //  Float_t         weight_leptonSF;
  //  Float_t         weight_jvt;
  //  Float_t         weight_pileup_UP;
  //  Float_t         weight_pileup_DOWN;
  //  Float_t         weight_leptonSF_EL_SF_Trigger_UP;
  //  Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
  //  Float_t         weight_leptonSF_EL_SF_Reco_UP;
  //  Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
  //  Float_t         weight_leptonSF_EL_SF_ID_UP;
  //  Float_t         weight_leptonSF_EL_SF_ID_DOWN;
  //  Float_t         weight_leptonSF_EL_SF_Isol_UP;
  //  Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
  //  Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
  //  Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
  //  Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
  //  Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
  //  Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
  //  Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
  //  Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
  //  Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
	//  Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
	//  Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
	//  Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
	//  Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
	//  Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
	//  Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
	//  Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
	//  Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
	//  Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
	//  Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
	//  Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
	//  Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
	//  Float_t         weight_indiv_SF_EL_Trigger;
	//  Float_t         weight_indiv_SF_EL_Trigger_UP;
	//  Float_t         weight_indiv_SF_EL_Trigger_DOWN;
	//  Float_t         weight_indiv_SF_EL_Reco;
	//  Float_t         weight_indiv_SF_EL_Reco_UP;
	//  Float_t         weight_indiv_SF_EL_Reco_DOWN;
	//  Float_t         weight_indiv_SF_EL_ID;
	//  Float_t         weight_indiv_SF_EL_ID_UP;
	//  Float_t         weight_indiv_SF_EL_ID_DOWN;
	//  Float_t         weight_indiv_SF_EL_Isol;
	//  Float_t         weight_indiv_SF_EL_Isol_UP;
	//  Float_t         weight_indiv_SF_EL_Isol_DOWN;
	//  Float_t         weight_indiv_SF_EL_ChargeID;
	//  Float_t         weight_indiv_SF_EL_ChargeID_UP;
	//  Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
	//  Float_t         weight_indiv_SF_EL_ChargeMisID;
	//  Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
	//  Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
	//  Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
	//  Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
	//  Float_t         weight_oldTriggerSF_MU_Trigger;
	//  Float_t         weight_oldTriggerSF_MU_Trigger_STAT_UP;
	//  Float_t         weight_oldTriggerSF_MU_Trigger_STAT_DOWN;
	//  Float_t         weight_oldTriggerSF_MU_Trigger_SYST_UP;
	//  Float_t         weight_oldTriggerSF_MU_Trigger_SYST_DOWN;
	//  Float_t         weight_indiv_SF_MU_ID;
	//  Float_t         weight_indiv_SF_MU_ID_STAT_UP;
	//  Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
	//  Float_t         weight_indiv_SF_MU_ID_SYST_UP;
	//  Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
	//  Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
	//  Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
	//  Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
	//  Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
	//  Float_t         weight_indiv_SF_MU_Isol;
	//  Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
	//  Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
	//  Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
	//  Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
	//  Float_t         weight_indiv_SF_MU_TTVA;
	//  Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
	//  Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
	//  Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
	//  Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
	//  Float_t         weight_jvt_UP;
	//  Float_t         weight_jvt_DOWN;
	ULong64_t eventNumber;
	UInt_t runNumber;
	UInt_t randomRunNumber;
	UInt_t mcChannelNumber;
	Float_t mu;
	Float_t averageIntPerXing;
	UInt_t backgroundFlags;
	UInt_t hasBadMuon;
	std::vector<float> *el_pt;
	std::vector<float> *el_eta;
	std::vector<float> *el_cl_eta;
	std::vector<float> *el_phi;
	std::vector<float> *el_e;
	std::vector<float> *el_charge;
	std::vector<float> *el_topoetcone20;
	std::vector<float> *el_ptvarcone20;
	std::vector<char> *el_CF;
	std::vector<float> *el_d0sig;
	std::vector<float> *el_delta_z0_sintheta;
	std::vector<int> *el_true_type;
	std::vector<int> *el_true_origin;
	std::vector<int> *el_true_firstEgMotherTruthType;
	std::vector<int> *el_true_firstEgMotherTruthOrigin;
	//std::vector<int> *el_true_firstEgMotherPdgId;
	std::vector<bool> *el_true_isPrompt;
	std::vector<float> *mu_pt;
	std::vector<float> *mu_eta;
	std::vector<float> *mu_phi;
	std::vector<float> *mu_e;
	std::vector<float> *mu_charge;
	std::vector<float> *mu_topoetcone20;
	std::vector<float> *mu_ptvarcone30;
	std::vector<float> *mu_d0sig;
	std::vector<float> *mu_delta_z0_sintheta;
	std::vector<int> *mu_true_type;
	std::vector<int> *mu_true_origin;
	std::vector<bool> *mu_true_isPrompt;
	std::vector<float> *jet_pt;
	std::vector<float> *jet_eta;
	std::vector<float> *jet_phi;
	std::vector<float> *jet_e;
	//std::vector<float> *jet_mv2c00;
	std::vector<float> *jet_mv2c10;
	//std::vector<float> *jet_mv2c20;
	//std::vector<float> *jet_ip3dsv1;
	std::vector<float> *jet_jvt;
	std::vector<char> *jet_passfjvt;
	std::vector<int> *jet_truthflav;
	std::vector<int> *jet_truthPartonLabel;
	std::vector<int> *weight_poisson;
	std::vector<char> *jet_isTrueHS;
	std::vector<double> *MV2c10Flip;
	//std::vector<float> *jet_MV2r;
	//std::vector<float> *jet_MV2rmu;
	std::vector<float> *jet_DL1;
	std::vector<float> *jet_DL1r;
	std::vector<float> *jet_DL1rmu;
	std::vector<float> *jet_DL1rmu_pb;
	std::vector<float> *jet_DL1rmu_pc;
	std::vector<float> *jet_DL1rmu_pu;
	std::vector<float> *jet_DL1_pb;
	std::vector<float> *jet_DL1_pc;
	std::vector<float> *jet_DL1_pu;
	std::vector<float> *jet_DL1r_pb;
	std::vector<float> *jet_DL1r_pc;
	std::vector<float> *jet_DL1r_pu;
	Float_t met_met;
	Float_t met_phi;
	Int_t nPV;
	Int_t emu_2015;
	Int_t emu_2016;
	Int_t ee_2015;
	Int_t ee_2016;
	Int_t mumu_2015;
	Int_t mumu_2016;
	Char_t HLT_e60_lhmedium_nod0;
	Char_t HLT_mu26_ivarmedium;
	Char_t HLT_e26_lhtight_nod0_ivarloose;
	Char_t HLT_e140_lhloose_nod0;
	Char_t HLT_mu20_iloose_L1MU15;
	Char_t HLT_mu50;
	Char_t HLT_e60_lhmedium;
	Char_t HLT_e24_lhmedium_L1EM20VH;
	Char_t HLT_e120_lhloose;
	std::vector<char> *el_trigMatch_HLT_e60_lhmedium_nod0;
	std::vector<char> *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
	std::vector<char> *el_trigMatch_HLT_e140_lhloose_nod0;
	std::vector<char> *el_trigMatch_HLT_e60_lhmedium;
	std::vector<char> *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
	std::vector<char> *el_trigMatch_HLT_e120_lhloose;
	std::vector<char> *mu_trigMatch_HLT_mu26_ivarmedium;
	std::vector<char> *mu_trigMatch_HLT_mu50;
	std::vector<char> *mu_trigMatch_HLT_mu20_iloose_L1MU15;
	std::vector<int> *IP2DNeg_ntrk;
	std::vector<double> *IP2DNeg_pb;
	std::vector<double> *IP2DNeg_pc;
	std::vector<double> *IP2DNeg_pu;
	std::vector<int> *IP2D_ntrk;
	std::vector<double> *IP2D_pb;
	std::vector<double> *IP2D_pc;
	std::vector<double> *IP2D_pu;
	std::vector<int> *IP3DNeg_ntrk;
	std::vector<double> *IP3DNeg_pb;
	std::vector<double> *IP3DNeg_pc;
	std::vector<double> *IP3DNeg_pu;
	std::vector<int> *IP3D_ntrk;
	std::vector<double> *IP3D_pb;
	std::vector<double> *IP3D_pc;
	std::vector<double> *IP3D_pu;
	std::vector<int> *JFFlip_N2Tpair;
	std::vector<float> *JFFlip_dRFlightDir;
	std::vector<float> *JFFlip_deltaeta;
	std::vector<float> *JFFlip_deltaphi;
	std::vector<float> *JFFlip_energyFraction;
	std::vector<float> *JFFlip_mass;
	std::vector<int> *JFFlip_nSingleTracks;
	std::vector<int> *JFFlip_nTracksAtVtx;
	std::vector<int> *JFFlip_nVTX;
	std::vector<int> *JFFlip_ntrk;
	std::vector<float> *JFFlip_significance3d;
	std::vector<int> *JF_N2Tpair;
	std::vector<float> *JF_dRFlightDir;
	std::vector<float> *JF_deltaeta;
	std::vector<float> *JF_deltaphi;
	std::vector<float> *JF_energyFraction;
	std::vector<float> *JF_mass;
	std::vector<int> *JF_nSingleTracks;
	std::vector<int> *JF_nTracksAtVtx;
	std::vector<int> *JF_nVTX;
	std::vector<int> *JF_ntrk;
	std::vector<float> *JF_significance3d;
	std::vector<float> *SV1Flip_L3d;
	std::vector<float> *SV1Flip_Lxy;
	std::vector<int> *SV1Flip_N2Tpair;
	std::vector<int> *SV1Flip_NGTinSvx;
	std::vector<float> *SV1Flip_deltaR;
	std::vector<float> *SV1Flip_efracsvx;
	std::vector<float> *SV1Flip_masssvx;
	std::vector<float> *SV1Flip_normdist;
	std::vector<int> *SV1Flip_ntrk;
	std::vector<float> *SV1_L3d;
	std::vector<float> *SV1_Lxy;
	std::vector<int> *SV1_N2Tpair;
	std::vector<int> *SV1_NGTinSvx;
	std::vector<float> *SV1_deltaR;
	std::vector<float> *SV1_efracsvx;
	std::vector<float> *SV1_masssvx;
	std::vector<float> *SV1_normdist;
	std::vector<int> *SV1_ntrk;
	std::vector<int> *HadronConeExclExtendedTruthLabelID;
	std::vector<float> *jet_m_jl= new std::vector<float>();
	std::vector<float> *lepton_m_lj= new std::vector<float>();

	// List of branches
	TBranch *b_mc_generator_weights;                        //!
	TBranch *b_weight_mc;                                   //!
	TBranch *b_weight_pileup;                               //!
	TBranch *b_weight_leptonSF;                             //!
	TBranch *b_weight_jvt;                                  //!
	TBranch *b_weight_pileup_UP;                            //!
	TBranch *b_weight_pileup_DOWN;                          //!
	TBranch *b_weight_poisson;                               //!
	TBranch *b_weight_leptonSF_EL_SF_Trigger_UP;            //!
	TBranch *b_weight_leptonSF_EL_SF_Trigger_DOWN;          //!
	TBranch *b_weight_leptonSF_EL_SF_Reco_UP;               //!
	TBranch *b_weight_leptonSF_EL_SF_Reco_DOWN;             //!
	TBranch *b_weight_leptonSF_EL_SF_ID_UP;                 //!
	TBranch *b_weight_leptonSF_EL_SF_ID_DOWN;               //!
	TBranch *b_weight_leptonSF_EL_SF_Isol_UP;               //!
	TBranch *b_weight_leptonSF_EL_SF_Isol_DOWN;             //!
	TBranch *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;       //!
	TBranch *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;     //!
	TBranch *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;       //!
	TBranch *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;     //!
	TBranch *b_weight_leptonSF_MU_SF_ID_STAT_UP;            //!
	TBranch *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;          //!
	TBranch *b_weight_leptonSF_MU_SF_ID_SYST_UP;            //!
	TBranch *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;          //!
	TBranch *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;      //!
	TBranch *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;    //!
	TBranch *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;      //!
	TBranch *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;    //!
	TBranch *b_weight_leptonSF_MU_SF_Isol_STAT_UP;          //!
	TBranch *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;        //!
	TBranch *b_weight_leptonSF_MU_SF_Isol_SYST_UP;          //!
	TBranch *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;        //!
	TBranch *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;          //!
	TBranch *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;        //!
	TBranch *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;          //!
	TBranch *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;        //!
	TBranch *b_weight_indiv_SF_EL_Trigger;                  //!
	TBranch *b_weight_indiv_SF_EL_Trigger_UP;               //!
	TBranch *b_weight_indiv_SF_EL_Trigger_DOWN;             //!
	TBranch *b_weight_indiv_SF_EL_Reco;                     //!
	TBranch *b_weight_indiv_SF_EL_Reco_UP;                  //!
	TBranch *b_weight_indiv_SF_EL_Reco_DOWN;                //!
	TBranch *b_weight_indiv_SF_EL_ID;                       //!
	TBranch *b_weight_indiv_SF_EL_ID_UP;                    //!
	TBranch *b_weight_indiv_SF_EL_ID_DOWN;                  //!
	TBranch *b_weight_indiv_SF_EL_Isol;                     //!
	TBranch *b_weight_indiv_SF_EL_Isol_UP;                  //!
	TBranch *b_weight_indiv_SF_EL_Isol_DOWN;                //!
	TBranch *b_weight_indiv_SF_EL_ChargeID;                 //!
	TBranch *b_weight_indiv_SF_EL_ChargeID_UP;              //!
	TBranch *b_weight_indiv_SF_EL_ChargeID_DOWN;            //!
	TBranch *b_weight_indiv_SF_EL_ChargeMisID;              //!
	TBranch *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;      //!
	TBranch *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;    //!
	TBranch *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;      //!
	TBranch *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;    //!
	TBranch *b_weight_oldTriggerSF_MU_Trigger;                  //!
	TBranch *b_weight_oldTriggerSF_MU_Trigger_STAT_UP;          //!
	TBranch *b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN;        //!
	TBranch *b_weight_oldTriggerSF_MU_Trigger_SYST_UP;          //!
	TBranch *b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN;        //!
	TBranch *b_weight_indiv_SF_MU_ID;                       //!
	TBranch *b_weight_indiv_SF_MU_ID_STAT_UP;               //!
	TBranch *b_weight_indiv_SF_MU_ID_STAT_DOWN;             //!
	TBranch *b_weight_indiv_SF_MU_ID_SYST_UP;               //!
	TBranch *b_weight_indiv_SF_MU_ID_SYST_DOWN;             //!
	TBranch *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;         //!
	TBranch *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;       //!
	TBranch *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;         //!
	TBranch *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;       //!
	TBranch *b_weight_indiv_SF_MU_Isol;                     //!
	TBranch *b_weight_indiv_SF_MU_Isol_STAT_UP;             //!
	TBranch *b_weight_indiv_SF_MU_Isol_STAT_DOWN;           //!
	TBranch *b_weight_indiv_SF_MU_Isol_SYST_UP;             //!
	TBranch *b_weight_indiv_SF_MU_Isol_SYST_DOWN;           //!
	TBranch *b_weight_indiv_SF_MU_TTVA;                     //!
	TBranch *b_weight_indiv_SF_MU_TTVA_STAT_UP;             //!
	TBranch *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;           //!
	TBranch *b_weight_indiv_SF_MU_TTVA_SYST_UP;             //!
	TBranch *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;           //!
	TBranch *b_weight_jvt_UP;                               //!
	TBranch *b_weight_jvt_DOWN;                             //!
	TBranch *b_eventNumber;                                 //!
	TBranch *b_runNumber;                                   //!
	TBranch *b_randomRunNumber;                             //!
	TBranch *b_mcChannelNumber;                             //!
	TBranch *b_mu;                                          //!
	TBranch *b_averageIntPerXing;                           //!
	TBranch *b_backgroundFlags;                             //!
	TBranch *b_hasBadMuon;                                  //!
	TBranch *b_el_pt;                                       //!
	TBranch *b_el_eta;                                      //!
	TBranch *b_el_cl_eta;                                   //!
	TBranch *b_el_phi;                                      //!
	TBranch *b_el_e;                                        //!
	TBranch *b_el_charge;                                   //!
	TBranch *b_el_topoetcone20;                             //!
	TBranch *b_el_ptvarcone20;                              //!
	TBranch *b_el_CF;                                       //!
	TBranch *b_el_d0sig;                                    //!
	TBranch *b_el_delta_z0_sintheta;                        //!
	TBranch *b_el_true_type;                                //!
	TBranch *b_el_true_origin;                              //!
	TBranch *b_el_true_firstEgMotherTruthType;		//!
	TBranch *b_el_true_firstEgMotherTruthOrigin;		//!
	//TBranch *b_el_true_firstEgMotherPdgId;			//!
	TBranch *b_el_true_isPrompt;				//!
	TBranch *b_mu_pt;                                       //!
	TBranch *b_mu_eta;                                      //!
	TBranch *b_mu_phi;                                      //!
	TBranch *b_mu_e;                                        //!
	TBranch *b_mu_charge;                                   //!
	TBranch *b_mu_topoetcone20;                             //!
	TBranch *b_mu_ptvarcone30;                              //!
	TBranch *b_mu_d0sig;                                    //!
	TBranch *b_mu_delta_z0_sintheta;                        //!
	TBranch *b_mu_true_type;                                //!
	TBranch *b_mu_true_origin;                              //!
	TBranch *b_mu_true_isPrompt;                            //!
	TBranch *b_jet_pt;                                      //!
	TBranch *b_jet_eta;                                     //!
	TBranch *b_jet_phi;                                     //!
	TBranch *b_jet_e;                                       //!
	//TBranch *b_jet_mv2c00;                                  //!
	TBranch *b_jet_mv2c10;                                  //!
	//TBranch *b_jet_mv2c20;                                  //!
	//TBranch *b_jet_ip3dsv1;                                 //!
	TBranch *b_jet_jvt;                                     //!
	TBranch *b_jet_passfjvt;                                //!
	TBranch *b_jet_truthflav;                               //!
	TBranch *b_jet_truthPartonLabel;                        //!
	TBranch *b_jet_isTrueHS;                                //!
	//   TBranch        *b_jet_isbtagged_MV2c10_77;   //!
	//   TBranch        *b_jet_isbtagged_MV2c10_HybBEff_77;   //!
	//   TBranch        *b_jet_isbtagged_MV2r_77;   //!
	//   TBranch        *b_jet_isbtagged_MV2r_HybBEff_77;   //!
	//   TBranch        *b_jet_isbtagged_MV2rmu_77;   //!
	//   TBranch        *b_jet_isbtagged_MV2rmu_HybBEff_77;   //!
	//   TBranch        *b_jet_isbtagged_DL1_77;   //!
	//   TBranch        *b_jet_isbtagged_DL1_HybBEff_77;   //!
	//   TBranch        *b_jet_isbtagged_DL1r_77;   //!
	//   TBranch        *b_jet_isbtagged_DL1r_HybBEff_77;   //!
	//   TBranch        *b_jet_isbtagged_DL1rmu_77;   //!
	//   TBranch        *b_jet_isbtagged_DL1rmu_HybBEff_77;   //!
	TBranch *b_jet_MV2r;                                //!
	TBranch *b_jet_MV2rmu;                               //!
	TBranch *b_jet_DL1;                                     //!
	TBranch *b_jet_DL1r;                                   //!
	TBranch *b_jet_DL1rmu;                                  //!
	TBranch *b_jet_DL1rmu_pb;                               //!
	TBranch *b_jet_DL1rmu_pc;                               //!
	TBranch *b_jet_DL1rmu_pu;                               //!
	TBranch *b_jet_DL1_pb;                                  //!
	TBranch *b_jet_DL1_pc;                                  //!
	TBranch *b_jet_DL1_pu;                                  //!
	TBranch *b_jet_DL1r_pb;                                //!
	TBranch *b_jet_DL1r_pc;                                //!
	TBranch *b_jet_DL1r_pu;                                //!
	TBranch *b_met_met;                                     //!
	TBranch *b_met_phi;                                     //!
	TBranch *b_nPV;                                         //!
	TBranch *b_emu_2015;                                    //!
	TBranch *b_emu_2016;                                    //!
	TBranch *b_ee_2015;                                     //!
	TBranch *b_ee_2016;                                     //!
	TBranch *b_mumu_2015;                                   //!
	TBranch *b_mumu_2016;                                   //!
	TBranch *b_HLT_e60_lhmedium_nod0;                       //!
	TBranch *b_HLT_mu26_ivarmedium;                         //!
	TBranch *b_HLT_e26_lhtight_nod0_ivarloose;              //!
	TBranch *b_HLT_e140_lhloose_nod0;                       //!
	TBranch *b_HLT_mu20_iloose_L1MU15;                      //!
	TBranch *b_HLT_mu50;                                    //!
	TBranch *b_HLT_e60_lhmedium;                            //!
	TBranch *b_HLT_e24_lhmedium_L1EM20VH;                   //!
	TBranch *b_HLT_e120_lhloose;                            //!
	TBranch *b_el_trigMatch_HLT_e60_lhmedium_nod0;          //!
	TBranch *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose; //!
	TBranch *b_el_trigMatch_HLT_e140_lhloose_nod0;          //!
	TBranch *b_el_trigMatch_HLT_e60_lhmedium;               //!
	TBranch *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;      //!
	TBranch *b_el_trigMatch_HLT_e120_lhloose;               //!
	TBranch *b_mu_trigMatch_HLT_mu26_ivarmedium;            //!
	TBranch *b_mu_trigMatch_HLT_mu50;                       //!
	TBranch *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;         //!
	TBranch *b_IP2DNeg_ntrk;                                //!
	TBranch *b_IP2DNeg_pb;                                  //!
	TBranch *b_IP2DNeg_pc;                                  //!
	TBranch *b_IP2DNeg_pu;                                  //!
	TBranch *b_IP2D_ntrk;                                   //!
	TBranch *b_IP2D_pb;                                     //!
	TBranch *b_IP2D_pc;                                     //!
	TBranch *b_IP2D_pu;                                     //!
	TBranch *b_IP3DNeg_ntrk;                                //!
	TBranch *b_IP3DNeg_pb;                                  //!
	TBranch *b_IP3DNeg_pc;                                  //!
	TBranch *b_IP3DNeg_pu;                                  //!
	TBranch *b_IP3D_ntrk;                                   //!
	TBranch *b_IP3D_pb;                                     //!
	TBranch *b_IP3D_pc;                                     //!
	TBranch *b_IP3D_pu;                                     //!
	TBranch *b_JFFlip_N2Tpair;                              //!
	TBranch *b_JFFlip_dRFlightDir;                          //!
	TBranch *b_JFFlip_deltaeta;                             //!
	TBranch *b_JFFlip_deltaphi;                             //!
	TBranch *b_JFFlip_energyFraction;                       //!
	TBranch *b_JFFlip_mass;                                 //!
	TBranch *b_JFFlip_nSingleTracks;                        //!
	TBranch *b_JFFlip_nTracksAtVtx;                         //!
	TBranch *b_JFFlip_nVTX;                                 //!
	TBranch *b_JFFlip_ntrk;                                 //!
	TBranch *b_JFFlip_significance3d;                       //!
	TBranch *b_JF_N2Tpair;                                  //!
	TBranch *b_JF_dRFlightDir;                              //!
	TBranch *b_JF_deltaeta;                                 //!
	TBranch *b_JF_deltaphi;                                 //!
	TBranch *b_JF_energyFraction;                           //!
	TBranch *b_JF_mass;                                     //!
	TBranch *b_JF_nSingleTracks;                            //!
	TBranch *b_JF_nTracksAtVtx;                             //!
	TBranch *b_JF_nVTX;                                     //!
	TBranch *b_JF_ntrk;                                     //!
	TBranch *b_JF_significance3d;                           //!
	TBranch *b_MV2c10Flip;                                  //!
	TBranch *b_SV1Flip_L3d;                                 //!
	TBranch *b_SV1Flip_Lxy;                                 //!
	TBranch *b_SV1Flip_N2Tpair;                             //!
	TBranch *b_SV1Flip_NGTinSvx;                            //!
	TBranch *b_SV1Flip_deltaR;                              //!
	TBranch *b_SV1Flip_efracsvx;                            //!
	TBranch *b_SV1Flip_masssvx;                             //!
	TBranch *b_SV1Flip_normdist;                            //!
	TBranch *b_SV1Flip_ntrk;                                //!
	TBranch *b_SV1_L3d;                                     //!
	TBranch *b_SV1_Lxy;                                     //!
	TBranch *b_SV1_N2Tpair;                                 //!
	TBranch *b_SV1_NGTinSvx;                                //!
	TBranch *b_SV1_deltaR;                                  //!
	TBranch *b_SV1_efracsvx;                                //!
	TBranch *b_SV1_masssvx;                                 //!
	TBranch *b_SV1_normdist;                                //!
	TBranch *b_SV1_ntrk;                                    //!
	TBranch *b_HadronConeExclExtendedTruthLabelID;          //!

public:
	using TSelector::Begin;
	using TSelector::SlaveBegin;

	// Default Constructor
	TreeReader();

    // Destructor                                                                                                                                                                                       
    virtual ~TreeReader();

	void initialize_m_ljs();

	virtual Int_t   Version() const
	{
		return 2;
	}

	// Called every time a loop on the tree starts, a convenient place to create your histograms.                                                                                                       
	virtual void    Begin();

	// Clled after Begin(), when on PROOF called only on the slave servers.                                                                                                                             
	virtual void    SlaveBegin();

	// called for each event, in this function you decide what to read and fill your histograms.                                                                                                        
	virtual Bool_t  Process(Long64_t entry);

	virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0)
	{
		return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0;
	}

	virtual void    SetOption(const char *option)
	{
		fOption = option;
	}

	virtual void    SetObject(TObject *obj)
	{
		fObject = obj;
	}

	virtual void    SetInputList(TList *input)
	{
		fInput = input;
	}

	virtual TList *GetOutputList() const
	{
		return fOutput;
	}

	// Called at the end of the loop on the tree, when on PROOF called only on the slave servers.                                                                                                       
	virtual void    SlaveTerminate(){};

	// called at the end of the loop on the tree, a convenient place to draw/fit your histograms.                                                                                                       
	virtual void    Terminate(){};


	Bool_t Notify()
	{
		// The Notify() function is called when a new file is opened. This                                                                                                                          
		// can be either for a new TTree in a TChain or when when a new TTree                                                                                                                       
		// is started when using PROOF. It is normally not necessary to make changes                                                                                                                
		// to the generated code, but the routine can be extended by the                                                                                                                            
		// user if needed. The return value is currently not used.                                                                                                                                  

		return kTRUE;
	}

        // ClassDef(TreeReader,0);       
        // };                                                                                                                                                                                                       // #endif                                                                                                                                                                                                  // #ifdef TreeReader_cxx           
	void Init(TTree *tree)
	{
		// The Init() function is called when the selector needs to initialize
		// a new tree or chain. Typically here the branch addresses and branch
		// pointers of the tree will be set.
		// It is normally not necessary to make changes to the generated
		// code, but the routine can be extended by the user if needed.
		// Init() will be called many times when running on PROOF
		// (once per file to be processed).

		// Set object pointer
		mc_generator_weights = 0;
		el_pt = 0;
		el_eta = 0;
		el_cl_eta = 0;
		el_phi = 0;
		el_e = 0;
		el_charge = 0;
		el_topoetcone20 = 0;
		el_ptvarcone20 = 0;
		el_CF = 0;
		el_d0sig = 0;
		el_delta_z0_sintheta = 0;
		el_true_type = 0;
		el_true_origin = 0;
		el_true_firstEgMotherTruthType= 0;
		el_true_firstEgMotherTruthOrigin = 0;
		//el_true_firstEgMotherPdgId = 0;
		el_true_isPrompt = 0;
		mu_pt = 0;
		mu_eta = 0;
		mu_phi = 0;
		mu_e = 0;
		mu_charge = 0;
		mu_topoetcone20 = 0;
		mu_ptvarcone30 = 0;
		mu_d0sig = 0;
		mu_delta_z0_sintheta = 0;
		mu_true_type = 0;
		mu_true_origin = 0;
		mu_true_isPrompt = 0;
		jet_pt = 0;
		jet_eta = 0;
		jet_phi = 0;
		jet_e = 0;
		//jet_mv2c00 = 0;
		jet_mv2c10 = 0;
		//jet_mv2c20 = 0;
		//jet_ip3dsv1 = 0;
		jet_jvt = 0;
		jet_passfjvt = 0;
		jet_truthflav = 0;
		jet_truthPartonLabel = 0;
		jet_isTrueHS = 0;
		// jet_isbtagged_MV2c10_77 = 0;
		// jet_isbtagged_MV2c10_HybBEff_77 = 0;
		// jet_isbtagged_MV2r_77 = 0;
		// jet_isbtagged_MV2r_HybBEff_77 = 0;
		// jet_isbtagged_MV2rmu_77 = 0;
		// jet_isbtagged_MV2rmu_HybBEff_77 = 0;
		// jet_isbtagged_DL1_77 = 0;
		// jet_isbtagged_DL1_HybBEff_77 = 0;
		// jet_isbtagged_DL1r_77 = 0;
		// jet_isbtagged_DL1r_HybBEff_77 = 0;
		// jet_isbtagged_DL1rmu_77 = 0;
		// jet_isbtagged_DL1rmu_HybBEff_77 = 0;
		//jet_MV2r = 0;
		//jet_MV2rmu = 0;
		jet_DL1 = 0;
		jet_DL1r = 0;
		jet_DL1rmu = 0;
		jet_DL1rmu_pb = 0;
		jet_DL1rmu_pc = 0;
		jet_DL1rmu_pu = 0;
		jet_DL1_pb = 0;
		jet_DL1_pc = 0;
		jet_DL1_pu = 0;
		jet_DL1r_pb = 0;
		jet_DL1r_pc = 0;
		jet_DL1r_pu = 0;
		nPV = 0;
		el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
		el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
		el_trigMatch_HLT_e140_lhloose_nod0 = 0;
		el_trigMatch_HLT_e60_lhmedium = 0;
		el_trigMatch_HLT_e24_lhmedium_L1EM20VH = 0;
		el_trigMatch_HLT_e120_lhloose = 0;
		mu_trigMatch_HLT_mu26_ivarmedium = 0;
		mu_trigMatch_HLT_mu50 = 0;
		mu_trigMatch_HLT_mu20_iloose_L1MU15 = 0;
		IP2DNeg_ntrk = 0;
		IP2DNeg_pb = 0;
		IP2DNeg_pc = 0;
		IP2DNeg_pu = 0;
		IP2D_ntrk = 0;
		IP2D_pb = 0;
		IP2D_pc = 0;
		IP2D_pu = 0;
		IP3DNeg_ntrk = 0;
		IP3DNeg_pb = 0;
		IP3DNeg_pc = 0;
		IP3DNeg_pu = 0;
		IP3D_ntrk = 0;
		IP3D_pb = 0;
		IP3D_pc = 0;
		IP3D_pu = 0;
		JFFlip_N2Tpair = 0;
		JFFlip_dRFlightDir = 0;
		JFFlip_deltaeta = 0;
		JFFlip_deltaphi = 0;
		JFFlip_energyFraction = 0;
		JFFlip_mass = 0;
		JFFlip_nSingleTracks = 0;
		JFFlip_nTracksAtVtx = 0;
		JFFlip_nVTX = 0;
		JFFlip_ntrk = 0;
		JFFlip_significance3d = 0;
		JF_N2Tpair = 0;
		JF_dRFlightDir = 0;
		JF_deltaeta = 0;
		JF_deltaphi = 0;
		JF_energyFraction = 0;
		JF_mass = 0;
		JF_nSingleTracks = 0;
		JF_nTracksAtVtx = 0;
		JF_nVTX = 0;
		JF_ntrk = 0;
		JF_significance3d = 0;
		MV2c10Flip = 0;
		SV1Flip_L3d = 0;
		SV1Flip_Lxy = 0;
		SV1Flip_N2Tpair = 0;
		SV1Flip_NGTinSvx = 0;
		SV1Flip_deltaR = 0;
		SV1Flip_efracsvx = 0;
		SV1Flip_masssvx = 0;
		SV1Flip_normdist = 0;
		SV1Flip_ntrk = 0;
		SV1_L3d = 0;
		SV1_Lxy = 0;
		SV1_N2Tpair = 0;
		SV1_NGTinSvx = 0;
		SV1_deltaR = 0;
		SV1_efracsvx = 0;
		SV1_masssvx = 0;
		SV1_normdist = 0;
		SV1_ntrk = 0;
		HadronConeExclExtendedTruthLabelID = 0;
	    weight_poisson = 0;
		// Set branch addresses and branch pointers
		if (!tree) return;
		fChain = tree;
		fChain->SetMakeClass(1);

		// wunderfull reg ex expression: fChain->SetBranchAddress\("(weight\w+)", &weight_\w+, &(\w+)\);
		//replace by: fChain->SetBranchAddress("$1", &weights["$1"], &$2);
		if (tree->GetListOfBranches()->FindObject("mc_generator_weights"))
		{
			fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
		}
	    if (tree->GetListOfBranches()->FindObject("weight_poisson"))
	    {
			fChain->SetBranchAddress("weight_poisson", &weight_poisson, &b_weight_poisson);
	    }

	    if (tree->GetListOfBranches()->FindObject("jet_truthflav"))
	    {
	        fChain->SetBranchAddress("weight_mc", &weights["weight_mc"], &b_weight_mc);
	        fChain->SetBranchAddress("weight_pileup", &weights["weight_pileup"], &b_weight_pileup);
		fChain->SetBranchAddress("weight_leptonSF", &weights["weight_leptonSF"], &b_weight_leptonSF);
		fChain->SetBranchAddress("weight_jvt", &weights["weight_jvt"], &b_weight_jvt);
	    }

		if (tree->GetListOfBranches()->FindObject("weight_pileup_UP"))
		{
			//cout << "Processung without individual weight Variations!"<<endl;
			fChain->SetBranchAddress("weight_pileup_UP", &weights["weight_pileup_UP"], &b_weight_pileup_UP);
			fChain->SetBranchAddress("weight_pileup_DOWN", &weights["weight_pileup_DOWN"], &b_weight_pileup_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weights["weight_leptonSF_EL_SF_Trigger_UP"], &b_weight_leptonSF_EL_SF_Trigger_UP);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weights["weight_leptonSF_EL_SF_Trigger_DOWN"], &b_weight_leptonSF_EL_SF_Trigger_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weights["weight_leptonSF_EL_SF_Reco_UP"], &b_weight_leptonSF_EL_SF_Reco_UP);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weights["weight_leptonSF_EL_SF_Reco_DOWN"], &b_weight_leptonSF_EL_SF_Reco_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weights["weight_leptonSF_EL_SF_ID_UP"], &b_weight_leptonSF_EL_SF_ID_UP);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weights["weight_leptonSF_EL_SF_ID_DOWN"], &b_weight_leptonSF_EL_SF_ID_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weights["weight_leptonSF_EL_SF_Isol_UP"], &b_weight_leptonSF_EL_SF_Isol_UP);
			fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weights["weight_leptonSF_EL_SF_Isol_DOWN"], &b_weight_leptonSF_EL_SF_Isol_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weights["weight_leptonSF_MU_SF_Trigger_STAT_UP"], &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weights["weight_leptonSF_MU_SF_Trigger_STAT_DOWN"], &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weights["weight_leptonSF_MU_SF_Trigger_SYST_UP"], &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weights["weight_leptonSF_MU_SF_Trigger_SYST_DOWN"], &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weights["weight_leptonSF_MU_SF_ID_STAT_UP"], &b_weight_leptonSF_MU_SF_ID_STAT_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weights["weight_leptonSF_MU_SF_ID_STAT_DOWN"], &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weights["weight_leptonSF_MU_SF_ID_SYST_UP"], &b_weight_leptonSF_MU_SF_ID_SYST_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weights["weight_leptonSF_MU_SF_ID_SYST_DOWN"], &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weights["weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP"], &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weights["weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN"], &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weights["weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP"], &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weights["weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN"], &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weights["weight_leptonSF_MU_SF_Isol_STAT_UP"], &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weights["weight_leptonSF_MU_SF_Isol_STAT_DOWN"], &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weights["weight_leptonSF_MU_SF_Isol_SYST_UP"], &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
		}
		if (tree->GetListOfBranches()->FindObject("weight_pileup_UP"))
		{
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weights["weight_leptonSF_MU_SF_Isol_SYST_DOWN"], &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weights["weight_leptonSF_MU_SF_TTVA_STAT_UP"], &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weights["weight_leptonSF_MU_SF_TTVA_STAT_DOWN"], &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weights["weight_leptonSF_MU_SF_TTVA_SYST_UP"], &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
			fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weights["weight_leptonSF_MU_SF_TTVA_SYST_DOWN"], &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);

			//  fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_UP", &weights["weight_indiv_SF_EL_Trigger_UP"], &b_weight_indiv_SF_EL_Trigger_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_DOWN", &weights["weight_indiv_SF_EL_Trigger_DOWN"], &b_weight_indiv_SF_EL_Trigger_DOWN);
			//
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weights["weight_indiv_SF_EL_Reco_UP"], &b_weight_indiv_SF_EL_Reco_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weights["weight_indiv_SF_EL_Reco_DOWN"], &b_weight_indiv_SF_EL_Reco_DOWN);
			//
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weights["weight_indiv_SF_EL_ID_UP"], &b_weight_indiv_SF_EL_ID_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weights["weight_indiv_SF_EL_ID_DOWN"], &b_weight_indiv_SF_EL_ID_DOWN);
			//
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weights["weight_indiv_SF_EL_Isol_UP"], &b_weight_indiv_SF_EL_Isol_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weights["weight_indiv_SF_EL_Isol_DOWN"], &b_weight_indiv_SF_EL_Isol_DOWN);
			//
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weights["weight_indiv_SF_EL_ChargeID_UP"], &b_weight_indiv_SF_EL_ChargeID_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weights["weight_indiv_SF_EL_ChargeID_DOWN"], &b_weight_indiv_SF_EL_ChargeID_DOWN);
			//
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weights["weight_indiv_SF_EL_ChargeMisID_STAT_UP"], &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weights["weight_indiv_SF_EL_ChargeMisID_STAT_DOWN"], &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weights["weight_indiv_SF_EL_ChargeMisID_SYST_UP"], &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
			//  fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weights["weight_indiv_SF_EL_ChargeMisID_SYST_DOWN"], &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);

			// fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_UP", &weights["weight_oldTriggerSF_MU_Trigger_STAT_UP"], &b_weight_oldTriggerSF_MU_Trigger_STAT_UP);
			// fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_STAT_DOWN", &weights["weight_oldTriggerSF_MU_Trigger_STAT_DOWN"], &b_weight_oldTriggerSF_MU_Trigger_STAT_DOWN);
			// fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_UP", &weights["weight_oldTriggerSF_MU_Trigger_SYST_UP"], &b_weight_oldTriggerSF_MU_Trigger_SYST_UP);
			// fChain->SetBranchAddress("weight_oldTriggerSF_MU_Trigger_SYST_DOWN", &weights["weight_oldTriggerSF_MU_Trigger_SYST_DOWN"], &b_weight_oldTriggerSF_MU_Trigger_SYST_DOWN);

			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weights["weight_indiv_SF_MU_ID_STAT_UP"], &b_weight_indiv_SF_MU_ID_STAT_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weights["weight_indiv_SF_MU_ID_STAT_DOWN"], &b_weight_indiv_SF_MU_ID_STAT_DOWN);
		}
		if (tree->GetListOfBranches()->FindObject("weight_pileup_UP"))
		{
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weights["weight_indiv_SF_MU_ID_SYST_UP"], &b_weight_indiv_SF_MU_ID_SYST_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weights["weight_indiv_SF_MU_ID_SYST_DOWN"], &b_weight_indiv_SF_MU_ID_SYST_DOWN);
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weights["weight_indiv_SF_MU_ID_STAT_LOWPT_UP"], &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weights["weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN"], &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weights["weight_indiv_SF_MU_ID_SYST_LOWPT_UP"], &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weights["weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN"], &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);

			fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weights["weight_indiv_SF_MU_Isol_STAT_UP"], &b_weight_indiv_SF_MU_Isol_STAT_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weights["weight_indiv_SF_MU_Isol_STAT_DOWN"], &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
			fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weights["weight_indiv_SF_MU_Isol_SYST_UP"], &b_weight_indiv_SF_MU_Isol_SYST_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weights["weight_indiv_SF_MU_Isol_SYST_DOWN"], &b_weight_indiv_SF_MU_Isol_SYST_DOWN);

			fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weights["weight_indiv_SF_MU_TTVA_STAT_UP"], &b_weight_indiv_SF_MU_TTVA_STAT_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weights["weight_indiv_SF_MU_TTVA_STAT_DOWN"], &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
			fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weights["weight_indiv_SF_MU_TTVA_SYST_UP"], &b_weight_indiv_SF_MU_TTVA_SYST_UP);
			fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weights["weight_indiv_SF_MU_TTVA_SYST_DOWN"], &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);

			fChain->SetBranchAddress("weight_jvt_UP", &weights["weight_jvt_UP"], &b_weight_jvt_UP);
			fChain->SetBranchAddress("weight_jvt_DOWN", &weights["weight_jvt_DOWN"], &b_weight_jvt_DOWN);
		}

		fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
		fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);

	    if (tree->GetListOfBranches()->FindObject("jet_truthflav"))
	    {
		       fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
		       fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
	    }

		fChain->SetBranchAddress("mu", &mu, &b_mu);
	    fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
	    fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
		fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
		fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
		fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
		fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
		fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
		fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
		fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
		fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
		fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
		fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
		fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
		fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
		fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);

		if (tree->GetListOfBranches()->FindObject("el_true_type"))
		{
			fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
			fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
	    	if (tree->GetListOfBranches()->FindObject("el_true_isPrompt"))
	    	{
	    		fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
	    		fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
	    		//fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
	    		fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
			}
		}

		fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
		fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
		fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
		fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
		fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
		fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
		fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
		fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
		fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);

		if (tree->GetListOfBranches()->FindObject("mu_true_type"))
		{
	    	fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
	    	fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
			if (tree->GetListOfBranches()->FindObject("mu_true_isPrompt"))
			{
		    		fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
			}
		}

		fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
		fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
		fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
		fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
		//fChain->SetBranchAddress("jet_mv2c00", &jet_mv2c00, &b_jet_mv2c00);
		fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
		//fChain->SetBranchAddress("jet_mv2c20", &jet_mv2c20, &b_jet_mv2c20);
		//fChain->SetBranchAddress("jet_ip3dsv1", &jet_ip3dsv1, &b_jet_ip3dsv1);
		fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
		fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);

		if (tree->GetListOfBranches()->FindObject("jet_truthflav"))
		{
	        fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
	    	fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
	    	fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
	    }

		//fChain->SetBranchAddress("jet_MV2r", &jet_MV2r, &b_jet_MV2r);
		//fChain->SetBranchAddress("jet_MV2rmu", &jet_MV2rmu, &b_jet_MV2rmu);
		fChain->SetBranchAddress("jet_DL1", &jet_DL1, &b_jet_DL1);
		fChain->SetBranchAddress("jet_DL1r", &jet_DL1r, &b_jet_DL1r);
		fChain->SetBranchAddress("jet_DL1rmu", &jet_DL1rmu, &b_jet_DL1rmu);
		fChain->SetBranchAddress("jet_DL1rmu_pb", &jet_DL1rmu_pb, &b_jet_DL1rmu_pb);
		fChain->SetBranchAddress("jet_DL1rmu_pc", &jet_DL1rmu_pc, &b_jet_DL1rmu_pc);
		fChain->SetBranchAddress("jet_DL1rmu_pu", &jet_DL1rmu_pu, &b_jet_DL1rmu_pu);
		fChain->SetBranchAddress("jet_DL1_pb", &jet_DL1_pb, &b_jet_DL1_pb);
		fChain->SetBranchAddress("jet_DL1_pc", &jet_DL1_pc, &b_jet_DL1_pc);
		fChain->SetBranchAddress("jet_DL1_pu", &jet_DL1_pu, &b_jet_DL1_pu);
		fChain->SetBranchAddress("jet_DL1r_pb", &jet_DL1r_pb, &b_jet_DL1r_pb);
		fChain->SetBranchAddress("jet_DL1r_pc", &jet_DL1r_pc, &b_jet_DL1r_pc);
		fChain->SetBranchAddress("jet_DL1r_pu", &jet_DL1r_pu, &b_jet_DL1r_pu);
		fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
		fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
		// fChain->SetBranchAddress("emu_2015", &emu_2015, &b_emu_2015);
		// fChain->SetBranchAddress("emu_2016", &emu_2016, &b_emu_2016);
		// fChain->SetBranchAddress("ee_2015", &ee_2015, &b_ee_2015);
		// fChain->SetBranchAddress("ee_2016", &ee_2016, &b_ee_2016);
		// fChain->SetBranchAddress("mumu_2015", &mumu_2015, &b_mumu_2015);
		// fChain->SetBranchAddress("mumu_2016", &mumu_2016, &b_mumu_2016);
	    //we don't need trigger variables for now -do we?
		// fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
		// fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
		// fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
		// fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
		// fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
		// fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
		// fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
		// fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
		// fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
		// fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
		// fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
		// fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
		// fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium, &b_el_trigMatch_HLT_e60_lhmedium);
		// fChain->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH, &b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
		// fChain->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose, &b_el_trigMatch_HLT_e120_lhloose);
		// fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
		// fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
		// fChain->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15, &b_mu_trigMatch_HLT_mu20_iloose_L1MU15);
		if(m_storeTracks)
		{
			fChain->SetBranchAddress("IP2DNeg_ntrk", &IP2DNeg_ntrk, &b_IP2DNeg_ntrk);
			fChain->SetBranchAddress("IP2DNeg_pb", &IP2DNeg_pb, &b_IP2DNeg_pb);
			fChain->SetBranchAddress("IP2DNeg_pc", &IP2DNeg_pc, &b_IP2DNeg_pc);
			fChain->SetBranchAddress("IP2DNeg_pu", &IP2DNeg_pu, &b_IP2DNeg_pu);
			fChain->SetBranchAddress("IP2D_ntrk", &IP2D_ntrk, &b_IP2D_ntrk);
			fChain->SetBranchAddress("IP2D_pb", &IP2D_pb, &b_IP2D_pb);
			fChain->SetBranchAddress("IP2D_pc", &IP2D_pc, &b_IP2D_pc);
			fChain->SetBranchAddress("IP2D_pu", &IP2D_pu, &b_IP2D_pu);
			fChain->SetBranchAddress("IP3DNeg_ntrk", &IP3DNeg_ntrk, &b_IP3DNeg_ntrk);
			fChain->SetBranchAddress("IP3DNeg_pb", &IP3DNeg_pb, &b_IP3DNeg_pb);
			fChain->SetBranchAddress("IP3DNeg_pc", &IP3DNeg_pc, &b_IP3DNeg_pc);
			fChain->SetBranchAddress("IP3DNeg_pu", &IP3DNeg_pu, &b_IP3DNeg_pu);
			fChain->SetBranchAddress("IP3D_ntrk", &IP3D_ntrk, &b_IP3D_ntrk);
			fChain->SetBranchAddress("IP3D_pb", &IP3D_pb, &b_IP3D_pb);
			fChain->SetBranchAddress("IP3D_pc", &IP3D_pc, &b_IP3D_pc);
			fChain->SetBranchAddress("IP3D_pu", &IP3D_pu, &b_IP3D_pu);
			fChain->SetBranchAddress("JFFlip_N2Tpair", &JFFlip_N2Tpair, &b_JFFlip_N2Tpair);
			fChain->SetBranchAddress("JFFlip_dRFlightDir", &JFFlip_dRFlightDir, &b_JFFlip_dRFlightDir);
			fChain->SetBranchAddress("JFFlip_deltaeta", &JFFlip_deltaeta, &b_JFFlip_deltaeta);
			fChain->SetBranchAddress("JFFlip_deltaphi", &JFFlip_deltaphi, &b_JFFlip_deltaphi);
			fChain->SetBranchAddress("JFFlip_energyFraction", &JFFlip_energyFraction, &b_JFFlip_energyFraction);
			fChain->SetBranchAddress("JFFlip_mass", &JFFlip_mass, &b_JFFlip_mass);
			fChain->SetBranchAddress("JFFlip_nSingleTracks", &JFFlip_nSingleTracks, &b_JFFlip_nSingleTracks);
			fChain->SetBranchAddress("JFFlip_nTracksAtVtx", &JFFlip_nTracksAtVtx, &b_JFFlip_nTracksAtVtx);
			fChain->SetBranchAddress("JFFlip_nVTX", &JFFlip_nVTX, &b_JFFlip_nVTX);
			fChain->SetBranchAddress("JFFlip_ntrk", &JFFlip_ntrk, &b_JFFlip_ntrk);
			fChain->SetBranchAddress("JFFlip_significance3d", &JFFlip_significance3d, &b_JFFlip_significance3d);
			fChain->SetBranchAddress("JF_N2Tpair", &JF_N2Tpair, &b_JF_N2Tpair);
			fChain->SetBranchAddress("JF_dRFlightDir", &JF_dRFlightDir, &b_JF_dRFlightDir);
			fChain->SetBranchAddress("JF_deltaeta", &JF_deltaeta, &b_JF_deltaeta);
			fChain->SetBranchAddress("JF_deltaphi", &JF_deltaphi, &b_JF_deltaphi);
			fChain->SetBranchAddress("JF_energyFraction", &JF_energyFraction, &b_JF_energyFraction);
			fChain->SetBranchAddress("JF_mass", &JF_mass, &b_JF_mass);
			fChain->SetBranchAddress("JF_nSingleTracks", &JF_nSingleTracks, &b_JF_nSingleTracks);
			fChain->SetBranchAddress("JF_nTracksAtVtx", &JF_nTracksAtVtx, &b_JF_nTracksAtVtx);
			fChain->SetBranchAddress("JF_nVTX", &JF_nVTX, &b_JF_nVTX);
			fChain->SetBranchAddress("JF_ntrk", &JF_ntrk, &b_JF_ntrk);
			fChain->SetBranchAddress("JF_significance3d", &JF_significance3d, &b_JF_significance3d);
			fChain->SetBranchAddress("SV1Flip_L3d", &SV1Flip_L3d, &b_SV1Flip_L3d);
			fChain->SetBranchAddress("SV1Flip_Lxy", &SV1Flip_Lxy, &b_SV1Flip_Lxy);
			fChain->SetBranchAddress("SV1Flip_N2Tpair", &SV1Flip_N2Tpair, &b_SV1Flip_N2Tpair);
			fChain->SetBranchAddress("SV1Flip_NGTinSvx", &SV1Flip_NGTinSvx, &b_SV1Flip_NGTinSvx);
			fChain->SetBranchAddress("SV1Flip_deltaR", &SV1Flip_deltaR, &b_SV1Flip_deltaR);
			fChain->SetBranchAddress("SV1Flip_efracsvx", &SV1Flip_efracsvx, &b_SV1Flip_efracsvx);
			fChain->SetBranchAddress("SV1Flip_masssvx", &SV1Flip_masssvx, &b_SV1Flip_masssvx);
			fChain->SetBranchAddress("SV1Flip_normdist", &SV1Flip_normdist, &b_SV1Flip_normdist);
			fChain->SetBranchAddress("SV1Flip_ntrk", &SV1Flip_ntrk, &b_SV1Flip_ntrk);
			fChain->SetBranchAddress("SV1_L3d", &SV1_L3d, &b_SV1_L3d);
			fChain->SetBranchAddress("SV1_Lxy", &SV1_Lxy, &b_SV1_Lxy);
			fChain->SetBranchAddress("SV1_N2Tpair", &SV1_N2Tpair, &b_SV1_N2Tpair);
			fChain->SetBranchAddress("SV1_NGTinSvx", &SV1_NGTinSvx, &b_SV1_NGTinSvx);
		}
		if(m_storeTracks)
		{
			fChain->SetBranchAddress("SV1_deltaR", &SV1_deltaR, &b_SV1_deltaR);
			fChain->SetBranchAddress("SV1_efracsvx", &SV1_efracsvx, &b_SV1_efracsvx);
			fChain->SetBranchAddress("SV1_masssvx", &SV1_masssvx, &b_SV1_masssvx);
			fChain->SetBranchAddress("SV1_normdist", &SV1_normdist, &b_SV1_normdist);
			fChain->SetBranchAddress("SV1_ntrk", &SV1_ntrk, &b_SV1_ntrk);
			fChain->SetBranchAddress("MV2c10Flip", &MV2c10Flip, &b_MV2c10Flip);
		}

		fChain->SetBranchAddress("HadronConeExclExtendedTruthLabelID", &HadronConeExclExtendedTruthLabelID, &b_HadronConeExclExtendedTruthLabelID);
	}

	ClassDef(TreeReader,1);
};
