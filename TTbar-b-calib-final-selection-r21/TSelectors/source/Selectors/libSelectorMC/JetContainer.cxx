// JetContainer.cxx

#include "JetContainer.h"

ClassImp(jet_Container)

jet_Container::jet_Container(std::string name, int n_jets, bool data, std::vector<std::string> eflavours)
{
	this->name = name;
	this->n_jets = n_jets;
	this->m_data = data;
	jet_pt = new Variable_Container(name + "_pt", n_jets, 58, 20, 600, m_data, eflavours);
	if (n_jets == 2)

	{
		double m_jl_bins [29];
		double first_bin =20;
		for (int i=0;i<20; i++)
		{
			m_jl_bins[i]=first_bin;
			first_bin=first_bin+20;
		}
		m_jl_bins[20]=430;
		m_jl_bins[21]=460;
		m_jl_bins[22]=490;
		m_jl_bins[23]=520;
		m_jl_bins[24]=550;
		m_jl_bins[25]=600;
		m_jl_bins[26]=650;
		m_jl_bins[27]=700;
		m_jl_bins[28]=800;
		jet_m_jl = new Variable_Container(name + "_m_jl", n_jets, 28,m_jl_bins, m_data, eflavours);
		jet_mv2c10_m_jl_cor = new Variable_ptBins_Correlation_Container(name, n_jets, "mv2c10", 20, getDArray(20, -1., 1.), "m_jl", 40, getDArray(40, 0., 400.), m_data, eflavours);
		jet_mv2c10_pt = new Variable_ptBins_Container(name + "_mv2c10_pt", n_jets, 20, getDArray(20, -1., 1.), m_data, eflavours);
	}
	jet_eta = new Variable_Container(name + "_eta", n_jets, 24, -2.5, 2.5, m_data, eflavours);
	jet_eta_pt_gt_60 = new Variable_Container(name + "_eta_pt_gt_60", n_jets, 24, -2.5, 2.5, m_data, eflavours);
	jet_phi = new Variable_Container(name + "_phi", n_jets, 28, -3.5, 3.5, m_data, eflavours);
	jet_mv2c10 = new Variable_Container(name + "_mv2c10", n_jets, 20, -1, 1, m_data, eflavours);
	//jet_MV2r = new Variable_Container(name + "_MV2r", n_jets, 20, -1, 1, m_data, eflavours);
	//jet_MV2rmu = new Variable_Container(name + "_MV2rmu", n_jets, 20, -1, 1, m_data, eflavours);
	jet_DL1 = new Variable_Container(name + "_DL1", n_jets, 38, -5, 14, m_data, eflavours);
	jet_DL1r = new Variable_Container(name + "_DL1r", n_jets, 56, -10, 18, m_data, eflavours);
	jet_DL1rmu = new Variable_Container(name + "_DL1rmu", n_jets, 38, -5, 14, m_data, eflavours);
	if (!m_data)
	{
		HadronConeExclExtendedTruthLabelID = new Variable_Container(name + "_HadronConeExclExtendedTruthLabelID", n_jets, 56, 0, 55, m_data, eflavours);
		double hadron_bins [8] ={0,4,5,15,44,54,55,56};
		jet_HadronConeExclExtendedTruthLabelID_pt = new Variable_ptBins_Container(name + "_HadronConeExclExtendedTruthLabelID_pt", n_jets, 7, hadron_bins, m_data, eflavours);
	}
}

jet_Container::~jet_Container()
{
}

void jet_Container::Write()
{
	jet_pt->Write();
	if (this->n_jets == 2)
	{
		jet_m_jl->Write();
		jet_mv2c10_m_jl_cor->Write();
		jet_mv2c10_pt->Write();
	}
	jet_eta->Write();
	jet_eta_pt_gt_60->Write();
	jet_phi->Write();
	jet_mv2c10->Write();
	//jet_MV2r->Write();
	//jet_MV2rmu->Write();
	jet_DL1->Write();
	jet_DL1r->Write();
	jet_DL1rmu->Write();
	if (!m_data)
	{
		HadronConeExclExtendedTruthLabelID->Write();
		jet_HadronConeExclExtendedTruthLabelID_pt->Write();
	}
}

std::vector<float> jet_Container::DL1Hand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction)                 
{

  std::vector<float> DL1;

  for(unsigned int i = 0 ; i < pb.size() ; ++i){
    float dl1=TMath::Log(pb[i]/( fraction*pc[i] + (1-fraction)*pu[i] ));
    DL1.push_back(dl1);
  }
  return DL1;

}

void jet_Container::addJet(int jet_n, TreeReader *selector, double weight)
{
  std::vector<float> DL1 = DL1Hand(*selector->jet_DL1_pb,*selector->jet_DL1_pc,*selector->jet_DL1_pu,0.018);
  std::vector<float> DL1r = DL1Hand(*selector->jet_DL1r_pb,*selector->jet_DL1r_pc,*selector->jet_DL1r_pu,0.018);
  std::vector<float> DL1rmu = DL1Hand(*selector->jet_DL1rmu_pb,*selector->jet_DL1rmu_pc,*selector->jet_DL1rmu_pu,0.018);

  //std::cout << "adding jet " << jet_n << std::endl;
  jet_pt->addEvent(selector->jet_pt->at(jet_n) / 1000, weight, selector->jet_truthflav);
  if (selector->jet_pt->at(jet_n) > 60000)
    jet_eta_pt_gt_60->addEvent(selector->jet_eta->at(jet_n), weight, selector->jet_truthflav);
  jet_eta->addEvent(selector->jet_eta->at(jet_n), weight, selector->jet_truthflav);
  jet_phi->addEvent(selector->jet_phi->at(jet_n), weight, selector->jet_truthflav);
  jet_mv2c10->addEvent(selector->jet_mv2c10->at(jet_n), weight, selector->jet_truthflav);
  //jet_MV2r->addEvent(selector->jet_MV2r->at(jet_n), weight, selector->jet_truthflav);
  //jet_MV2rmu->addEvent(selector->jet_MV2rmu->at(jet_n), weight, selector->jet_truthflav);
  jet_DL1->addEvent(DL1.at(jet_n), weight, selector->jet_truthflav);
  jet_DL1r->addEvent(DL1r.at(jet_n), weight, selector->jet_truthflav);
  jet_DL1rmu->addEvent(DL1rmu.at(jet_n), weight, selector->jet_truthflav);
  if (!m_data)
    HadronConeExclExtendedTruthLabelID->addEvent(selector->HadronConeExclExtendedTruthLabelID->at(jet_n), weight, selector->jet_truthflav);
  if (this->n_jets == 2)
    {
      jet_m_jl->addEvent(selector->jet_m_jl->at(jet_n) / 1000, weight, selector->jet_truthflav);
      jet_mv2c10_m_jl_cor->addEvent(selector->jet_mv2c10->at(jet_n), selector->jet_m_jl->at(jet_n) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
      jet_mv2c10_pt->addEvent(selector->jet_mv2c10->at(jet_n), selector->jet_pt, weight, selector->jet_truthflav);
      if (!m_data)
	jet_HadronConeExclExtendedTruthLabelID_pt->addEvent(selector->HadronConeExclExtendedTruthLabelID->at(jet_n), selector->jet_pt, weight, selector->jet_truthflav);
    }
}

double* jet_Container::getDArray(int bins, double lower, double upper)
{
  double interval = (upper - lower) / bins;
  double *ar = new double[bins + 1];

  for (int bin = 0; bin <= bins; bin++)
    ar[bin] = lower + bin * interval;
  return ar;
};
