// DilepChannel.h
#pragma once

#include "TreeReader.h"
#include "VariableContainer.h"
#include "JetContainer.h"
#include "HistsForFitContainer.h"
#include "JLMinMSumCombination.h"
#include "JLMinMSubCombination.h"
#include "JLdRCombination.h"
#include "XXContainer.h"
#include "VariablePtContainer.h"
#include "VariablePtBinsContainer.h"

class DiLepChannel
{
public:
  std::string name;
  bool m_save_fits = false;
  int n_jets;
  int n_el;    
  int n_mu;
  int n_lep;
  int m_n_boot_strap_min;
  int m_n_boot_strap_max;
  bool m_storeTracks;
  bool m_use_pt_bins_as_eta;
  bool m_data;

  TDirectory *channel_dir; 
  TH1F *cutflow; //!  

  Variable_Container *h_MV2_muinjet= NULL;
  Variable_Container *h_DL1_muinjet= NULL;
  Variable_Container *h_MV2r_muinjet= NULL;
  Variable_Container *h_DL1r_muinjet= NULL;
  Variable_Container *h_MV2rmu_muinjet= NULL;
  Variable_Container *h_DL1rmu_muinjet= NULL;
  Variable_Container *h_MV2_nomuinjet= NULL;
  Variable_Container *h_DL1_nomuinjet= NULL;
  Variable_Container *h_MV2r_nomuinjet= NULL;
  Variable_Container *h_DL1r_nomuinjet= NULL;
  Variable_Container *h_MV2rmu_nomuinjet= NULL;
  Variable_Container *h_DL1rmu_nomuinjet= NULL;

  Variable_Container *h_met= NULL;
  Variable_Container *h_mu= NULL;
  Variable_Container *h_mu_shifted= NULL;
  Variable_Container *h_nPV= NULL;
  Variable_Container *h_pT_eta1= NULL;
  Variable_Container *h_pT_eta2= NULL;
  Variable_Container *h_pT_eta3= NULL;
  Variable_Container *h_pT_eta4= NULL;
  Variable_Container *h_pT_eta5= NULL;
  Variable_Container *h_pT_eta6= NULL;
  Variable_Container *h_pT_eta7= NULL;
  Variable_Container *h_pT_eta8= NULL;
  Variable_Container *h_pT_eta9= NULL;
  Variable_Container *h_pT_eta10= NULL;
  Variable_Container *h_ptsublead_pTup= NULL;
  Variable_Container *h_ptsublead_pTdown= NULL;
  Variable_Container *h_etasublead_pTup= NULL;
  Variable_Container *h_etasublead_pTdown= NULL;
  Variable_Container *h_phisublead_pTup= NULL;
  Variable_Container *h_phisublead_pTdown= NULL;
  Variable_Container *h_eta_pTup= NULL;
  Variable_Container *h_eta_pTdown= NULL;
  Variable_Container *h_phi_pTup= NULL;
  Variable_Container *h_phi_pTdown= NULL;
  Variable_Container *h_mushifted_pTup= NULL;
  Variable_Container *h_mushifted_pTdown= NULL;
  Variable_Container *h_ip2dtrk_pTup= NULL;
  Variable_Container *h_ip3dtrk_pTup= NULL;
  Variable_Container *h_ip2dnegtrk_pTup= NULL;
  Variable_Container *h_ip3dnegtrk_pTup= NULL;
  Variable_Container *h_jffliptrk_pTup= NULL;
  Variable_Container *h_sv1fliptrk_pTup= NULL;
  Variable_Container *h_jftrk_pTup= NULL;
  Variable_Container *h_sv1trk_pTup= NULL;
  Variable_Container *h_ip2dtrk_pTdown= NULL;
  Variable_Container *h_ip3dtrk_pTdown= NULL;
  Variable_Container *h_ip2dnegtrk_pTdown= NULL;
  Variable_Container *h_ip3dnegtrk_pTdown= NULL;
  Variable_Container *h_jffliptrk_pTdown= NULL;
  Variable_Container *h_sv1fliptrk_pTdown= NULL;
  Variable_Container *h_jftrk_pTdown= NULL;
  Variable_Container *h_sv1trk_pTdown= NULL;

  Variable_Container *h_ip2dtrk_lead= NULL;
  Variable_Container *h_ip3dtrk_lead= NULL;
  Variable_Container *h_ip2dnegtrk_lead= NULL;
  Variable_Container *h_ip3dnegtrk_lead= NULL;
  Variable_Container *h_jffliptrk_lead= NULL;
  Variable_Container *h_sv1fliptrk_lead= NULL;
  Variable_Container *h_jftrk_lead= NULL;
  Variable_Container *h_sv1trk_lead= NULL;
  Variable_Container *h_ip2dtrk_sublead= NULL;
  Variable_Container *h_ip3dtrk_sublead= NULL;
  Variable_Container *h_ip2dnegtrk_sublead= NULL;
  Variable_Container *h_ip3dnegtrk_sublead= NULL;
  Variable_Container *h_jffliptrk_sublead= NULL;
  Variable_Container *h_sv1fliptrk_sublead= NULL;
  Variable_Container *h_jftrk_sublead= NULL;
  Variable_Container *h_sv1trk_sublead= NULL;
  Variable_pt_Container *h_pt_both= NULL;
  Variable_pt_Container *h_pt_eta_lead= NULL;
  Variable_pt_Container *h_pt_phi_lead= NULL;
  Variable_pt_Container *h_pt_ip2dtrk_lead= NULL;
  Variable_pt_Container *h_pt_ip3dtrk_lead= NULL;
  Variable_pt_Container *h_pt_eta_sublead= NULL;
  Variable_pt_Container *h_pt_phi_sublead= NULL;
  Variable_pt_Container *h_pt_ip2dtrk_sublead= NULL;
  Variable_pt_Container *h_pt_ip3dtrk_sublead= NULL;
  Hist_for_fit_Container *hist_for_fit_mv2c10_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_mv2c10_hybCut= NULL;
  Hist_for_fit_Container *hist_for_fit_mv2c10Flip_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_mv2c10Flip_hybCut= NULL;
  Hist_for_fit_Container *hist_for_fit_MV2r_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_MV2r_hybCut= NULL;
  Hist_for_fit_Container *hist_for_fit_MV2rmu_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_MV2rmu_hybCut= NULL;
  Hist_for_fit_Container *hist_for_fit_dl1_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_dl1_hybCut= NULL;
  Hist_for_fit_Container *hist_for_fit_DL1r_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_DL1r_hybCut= NULL;
  Hist_for_fit_Container *hist_for_fit_DL1rmu_fixedCut= NULL;
  Hist_for_fit_Container *hist_for_fit_DL1rmu_hybCut= NULL;
  JL_min_m_sum_Combination *jl_min_m_sum_Combination= NULL;
  JL_min_m_sub_Combination *jl_min_m_sub_Combination= NULL;
  JL_closest_dR_Combination *jl_closest_dR_Combination= NULL;

  Variable_ptBins_Container *m_jl_max= NULL;
  Variable_ptBins_Container *m_jl_sqsum= NULL;
  Variable_ptBins_Container *m_jl_sum= NULL;
  XX_Container *ll_con= NULL;

  std::vector<jet_Container *> jets;
  // std::vector<LJ_Container *> ljclosest_con;                                                                                                                                                       
  // std::vector<LJ_Container *> ljfarthest_con;                                                                                                                                                      
  std::vector<Variable_Container *> el_pt;
  std::vector<Variable_Container *> el_eta;
  std::vector<Variable_Container *> el_cl_eta;
  std::vector<Variable_Container *> el_phi;
  std::vector<Variable_Container *> mu_pt;
  std::vector<Variable_Container *> mu_eta;
  std::vector<Variable_Container *> mu_phi;
  std::vector<Variable_Container *> el_true_type;
  std::vector<Variable_Container *> el_true_origin;
  std::vector<Variable_Container *> mu_true_type;
  std::vector<Variable_Container *> mu_true_origin;
  
 public:
  // Constructor
  DiLepChannel(std::string name, std::string cdiPath, std::vector<std::string> cutFlowLabels, int n_jets, int n_el, int n_mu, TDirectory *f_out, bool data, std::vector<std::string> eflavours, int n_boot_strap_min, int n_boot_strap_max, bool storeTracks, bool use_pt_bins_as_eta,int hadronization, const double m_jl_CutValue, std::string bTagSystName, bool m_save_fits = false, bool save_cr_hist = false);
  
  // Destructor
  virtual ~DiLepChannel();
  
  // Adds to cutflow
  void addToCutflow(int bin, double weight);
  
  // Saves TFile
  void Save(TFile* file);

  // Adds event
  void AddEvent(TreeReader *selector, double weight, Long64_t entry);

  // Calculates DL1 score by hand
  std::vector<float> DL1Hand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction);

  ClassDef(DiLepChannel,1);
};
