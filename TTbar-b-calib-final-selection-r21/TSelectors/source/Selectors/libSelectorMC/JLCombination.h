// JLCombination.h

#pragma once

#include <string>
#include "XXContainer.h"

class JL_Combination
{
public:
	int n_jets  = 0;
	bool data;
	std::vector<std::string> eflavours;
	XX_Container* jet1_combination;
	XX_Container* jet2_combination;

public:
	// Constructor
	JL_Combination(std::string name, int n_jets, bool data, std::vector<std::string> eflavours);
	
	// Destructor
	~JL_Combination();

	// Writes to something
	void Write();

	ClassDef(JL_Combination,1);
};
