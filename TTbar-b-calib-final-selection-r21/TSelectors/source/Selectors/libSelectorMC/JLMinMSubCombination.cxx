// JLMinMSubCombination.cxx

#include "JLMinMSubCombination.h"

ClassImp(JL_min_m_sub_Combination)

JL_min_m_sub_Combination::JL_min_m_sub_Combination(std::string name, int n_jets,bool data, std::vector<std::string> eflavours) : JL_Combination::JL_Combination(name, n_jets, data, eflavours)
{
}

JL_min_m_sub_Combination::~JL_min_m_sub_Combination()
{
}

void JL_min_m_sub_Combination::Write()
{
	JL_Combination::Write();
}

void JL_min_m_sub_Combination::addEvent(TLorentzVector *lep_4vecArray, TLorentzVector *jet_4vecArray, double weight, std::vector<int> *jet_truthflav)
{
	if (this->n_jets == 2) {
		double m11, m22, m12, m21;
		m11 = (lep_4vecArray[0] + jet_4vecArray[0]).M();
		m22 = (lep_4vecArray[1] + jet_4vecArray[1]).M();
		m12 = (lep_4vecArray[0] + jet_4vecArray[1]).M();
		m21 = (lep_4vecArray[1] + jet_4vecArray[0]).M();
		if (abs(m11 - m22) < abs(m12 - m21)) {
			this->jet1_combination->addEvent(lep_4vecArray[0], jet_4vecArray[0], weight, jet_truthflav);
			this->jet2_combination->addEvent(lep_4vecArray[1], jet_4vecArray[1], weight, jet_truthflav);
			//std::cout<<"m11 "<<m11<< "sqrt: "<<sqrt(m11)<<std::endl;
		} else {
			this->jet1_combination->addEvent(lep_4vecArray[1], jet_4vecArray[0], weight, jet_truthflav);
			this->jet2_combination->addEvent(lep_4vecArray[0], jet_4vecArray[1], weight, jet_truthflav);
		}
	}
}
