// DiLepChannel.cxx

#include "DilepChannel.h"

ClassImp(DiLepChannel)

DiLepChannel::DiLepChannel(std::string name, std::string cdiPath, std::vector<std::string> cutFlowLabels, int n_jets, int n_el, int n_mu, TDirectory *f_out, bool data, std::vector<std::string> eflavours, int n_boot_strap_min, int n_boot_strap_max, bool storeTracks, bool use_pt_bins_as_eta, int hadronization, const double m_jl_CutValue, std::string bTagSystName, bool m_save_fits, bool save_cr_hist)
{
  std::cout << "Beginning setting attributes" << std::endl;
  this->name = name;
  this->m_save_fits = m_save_fits;
  channel_dir = f_out->GetDirectory(((name).c_str()));
  if (channel_dir == 0)
    channel_dir = f_out->mkdir(((name).c_str()));
  channel_dir->cd();

  this->n_jets = n_jets;
  this->n_el = n_el;
  this->n_mu = n_mu;
  this->n_lep = 2;
  this->m_n_boot_strap_min = n_boot_strap_min;
  this->m_n_boot_strap_max = n_boot_strap_max;
  this->m_storeTracks = storeTracks;
  this->m_use_pt_bins_as_eta = use_pt_bins_as_eta;
  this->m_data = data;
  //  int cs = 11;
  int cs = cutFlowLabels.size();
  //cout<<"cs"<<cs<<endl;
  cutflow = new TH1F(("h_" + name + "_CutFlow").c_str(), (name + "_CutFlow").c_str(), cs, 0, cs);
  cutflow->Sumw2();

  for (uint i = 0; i < cutFlowLabels.size(); i++)
    cutflow->GetXaxis()->SetBinLabel(i + 1, cutFlowLabels.at(i).c_str());
  if (this->n_jets == 2)
    {
      if (this->m_save_fits)
	{
	  //	  hist_for_fit_mv2c10_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "MV2c10", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, false, save_cr_hist);
	  // hist_for_fit_mv2c10_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "MV2c10", "HybBEff", channel_dir, false, save_cr_hist);
	  //				hist_for_fit_mv2c10Flip_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit_MV2c10Flip", n_jets, "MV2c10", "FixedCutBEff", true, save_cr_hist);
	  //				hist_for_fit_mv2c10Flip_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit_MV2c10Flip", n_jets, "MV2c10", "HybBEff", true, save_cr_hist);
	  //hist_for_fit_MV2r_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "MV2r", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, false, save_cr_hist);
	  // hist_for_fit_MV2r_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "MV2r", "HybBEff", channel_dir, false, save_cr_hist);
	  //hist_for_fit_MV2rmu_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "MV2rmu", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, false, save_cr_hist);
	  // hist_for_fit_MV2rmu_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "MV2rmu", "HybBEff", channel_dir, false, save_cr_hist);
	  hist_for_fit_dl1_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", cdiPath,  n_jets, "DL1", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, false, save_cr_hist);
	  // hist_for_fit_dl1_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "DL1", "HybBEff", channel_dir, false, save_cr_hist);
	  hist_for_fit_DL1r_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", cdiPath, n_jets, "DL1r", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, false, save_cr_hist);
	  // hist_for_fit_DL1r_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "DL1r", "HybBEff", channel_dir, false, save_cr_hist);
	  hist_for_fit_DL1rmu_fixedCut = new Hist_for_fit_Container(name + "_hist_for_fit", cdiPath, n_jets, "DL1rmu", "FixedCutBEff", channel_dir, m_data, eflavours, m_n_boot_strap_min, m_n_boot_strap_max, m_use_pt_bins_as_eta, hadronization, m_jl_CutValue, bTagSystName, false, save_cr_hist);
	  // hist_for_fit_DL1rmu_hybCut = new Hist_for_fit_Container(name + "_hist_for_fit", n_jets, "DL1rmu", "HybBEff", channel_dir, false, save_cr_hist);
	}
      std::cout << " Finished the hist for fits "  << std::endl;
      const Int_t NBINS_im = 20;
      const Double_t im_bins[NBINS_im + 1] = {0., 20., 40., 60., 80., 100., 120., 140., 160., 180., 200., 220., 240., 260., 280., 300., 320., 340., 360., 380., 400.};
      m_jl_max = new Variable_ptBins_Container(name + "_m_jl_max", n_jets, NBINS_im, im_bins, m_data, eflavours);
      m_jl_sqsum = new Variable_ptBins_Container(name + "_m_jl_sqsum", n_jets, NBINS_im, im_bins, m_data, eflavours);
      m_jl_sum = new Variable_ptBins_Container(name + "_m_jl_sum", n_jets, NBINS_im, im_bins, m_data, eflavours);
      jl_min_m_sum_Combination = new JL_min_m_sum_Combination(name + "_jl_min_m_sum_Combination", n_jets, m_data, eflavours);
      jl_min_m_sub_Combination = new JL_min_m_sub_Combination(name + "_jl_min_m_sub_Combination", n_jets, m_data, eflavours);
      jl_closest_dR_Combination = new JL_closest_dR_Combination(name + "_jl_closest_dR_Combination", n_jets, m_data, eflavours);
    }
  
  h_MV2_muinjet= new Variable_Container(name + "_MV2_muinjet",n_jets,20,-1,1,m_data,eflavours); 
  h_DL1_muinjet= new Variable_Container(name + "_DL1_muinjet",n_jets,25,-5,20, m_data, eflavours); 
  h_MV2r_muinjet= new Variable_Container(name + "_MV2r_muinjet",n_jets,20,-1,1, m_data, eflavours); 
  h_DL1r_muinjet= new Variable_Container(name + "_DL1r_muinjet",n_jets,35,-5,30, m_data, eflavours); 
  h_MV2rmu_muinjet= new Variable_Container(name + "_MV2rmu_muinjet",n_jets,20,-1,1, m_data, eflavours); 
  h_DL1rmu_muinjet= new Variable_Container(name + "_DL1rmu_muinjet",n_jets,25,-5,20, m_data, eflavours); 
  h_MV2_nomuinjet= new Variable_Container(name + "_MV2_nomuinjet",n_jets,20,-1,1, m_data, eflavours); 
  h_DL1_nomuinjet= new Variable_Container(name + "_DL1_nomuinjet",n_jets,25,-5,20, m_data, eflavours); 
  h_MV2r_nomuinjet= new Variable_Container(name + "_MV2r_nomuinjet",n_jets,20,-1,1, m_data, eflavours); 
  h_DL1r_nomuinjet= new Variable_Container(name + "_DL1r_nomuinjet",n_jets,35,-5,30, m_data, eflavours); 
  h_MV2rmu_nomuinjet= new Variable_Container(name + "_MV2rmu_nomuinjet",n_jets,20,-1,1, m_data, eflavours); 
  h_DL1rmu_nomuinjet= new Variable_Container(name + "_DL1rmu_nomuinjet",n_jets,25,-5,20, m_data, eflavours); 
  std::cout << "Problem in the nomuinjet checks" << std::endl;
  if( m_n_boot_strap_max == 0 ){
    ll_con = new XX_Container(name + "_ll", n_jets, m_data, eflavours);
    // lj_min_m_sum_Combination = new LJ_min_m_sum_Combination(name + "_min_m_sum_Combination", n_jets);
    // min_m_dif_Combination = new LJ_min_m_dif_Combination(name + "_min_m_dif_Combination", n_jets);
    h_met = new Variable_Container(name + "_met", n_jets, 40, 0, 400, m_data, eflavours);
    h_mu = new Variable_Container(name + "_mu", n_jets, 100, 0, 100, m_data, eflavours);
    h_mu_shifted = new Variable_Container(name + "_mu_shifted", n_jets, 100, 0, 100, m_data, eflavours);
    h_nPV = new Variable_Container(name + "_nPV", n_jets, 100, 0, 100, m_data, eflavours);
    h_pT_eta1 = new Variable_Container(name + "_pT_eta1", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta2 = new Variable_Container(name + "_pT_eta2", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta3 = new Variable_Container(name + "_pT_eta3", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta4 = new Variable_Container(name + "_pT_eta4", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta5 = new Variable_Container(name + "_pT_eta5", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta6 = new Variable_Container(name + "_pT_eta6", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta7 = new Variable_Container(name + "_pT_eta7", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta8 = new Variable_Container(name + "_pT_eta8", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta9 = new Variable_Container(name + "_pT_eta9", n_jets, 40, 0, 400, m_data, eflavours);
    h_pT_eta10 = new Variable_Container(name + "_pT_eta10", n_jets, 40, 0, 400, m_data, eflavours);
    h_ptsublead_pTup = new Variable_Container(name + "_ptsublead_ptup", n_jets, 100,0,600, m_data, eflavours);
    h_ptsublead_pTdown = new Variable_Container(name + "_ptsublead_ptdown", n_jets,100,0,600, m_data, eflavours);
    h_etasublead_pTup = new Variable_Container(name + "_etasublead_ptup", n_jets, 30,-3,3, m_data, eflavours);
    h_etasublead_pTdown = new Variable_Container(name + "_etasublead_ptdown", n_jets, 30,-3,3, m_data, eflavours);
    h_phisublead_pTup = new Variable_Container(name + "_phisublead_ptup", n_jets, 30,-3.1416,3.1416, m_data, eflavours);
    h_phisublead_pTdown = new Variable_Container(name + "_phisublead_ptdown", n_jets, 30,-3.1416,3.1416, m_data, eflavours);
    h_eta_pTup = new Variable_Container(name + "_eta_ptup", n_jets, 30,-3,3, m_data, eflavours);
    h_eta_pTdown = new Variable_Container(name + "_eta_ptdown", n_jets, 30,-3,3, m_data, eflavours);
    h_phi_pTup = new Variable_Container(name + "_phi_ptup", n_jets, 30,-3.1416,3.1416, m_data, eflavours);
    h_phi_pTdown = new Variable_Container(name + "_phi_ptdown", n_jets, 30,-3.1416,3.1416, m_data, eflavours);
    h_mushifted_pTup = new Variable_Container(name + "_mushifted_ptup", n_jets, 40,0,40, m_data, eflavours);
    h_mushifted_pTdown = new Variable_Container(name + "_mushifted_ptdown", n_jets, 40,0,40, m_data, eflavours);
       
    h_ip2dtrk_pTup = new Variable_Container(name + "_ip2dtrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dtrk_pTup = new Variable_Container(name + "_ip3dtrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_ip2dnegtrk_pTup = new Variable_Container(name + "_ip2dnegtrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dnegtrk_pTup = new Variable_Container(name + "_ip3dnegtrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_jftrk_pTup = new Variable_Container(name + "_jftrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_jffliptrk_pTup = new Variable_Container(name + "_jffliptrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_sv1trk_pTup = new Variable_Container(name + "_sv1trk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_sv1fliptrk_pTup = new Variable_Container(name + "_sv1fliptrk_ptup", n_jets, 20,0,20, m_data, eflavours);
    h_ip2dtrk_pTdown = new Variable_Container(name + "_ip2dtrk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dtrk_pTdown = new Variable_Container(name + "_ip3dtrk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_ip2dnegtrk_pTdown = new Variable_Container(name + "_ip2dnegtrk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dnegtrk_pTdown = new Variable_Container(name + "_ip3dnegtrk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_jftrk_pTdown = new Variable_Container(name + "_jftrk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_jffliptrk_pTdown = new Variable_Container(name + "_jffliptrk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_sv1trk_pTdown = new Variable_Container(name + "_sv1trk_ptdown", n_jets, 20,0,20, m_data, eflavours);
    h_sv1fliptrk_pTdown = new Variable_Container(name + "_sv1fliptrk_ptdown", n_jets, 20,0,20, m_data, eflavours);


    h_pt_both = new Variable_pt_Container(name + "_lead_sublead_ptbins", n_jets, 100,0,600,100,0,600,m_data,eflavours);

    h_pt_eta_lead = new Variable_pt_Container(name + "_eta_lead_ptbins", n_jets, 45, -4.5,4.5,100,0,600, m_data, eflavours);
    h_pt_phi_lead = new Variable_pt_Container(name + "_phi_lead_ptbins", n_jets, 45, -3.14,3.14,100,0,600, m_data, eflavours);
    h_pt_ip2dtrk_lead = new Variable_pt_Container(name + "_ip2dtrk_lead_ptbins", n_jets, 20,0,20,100,0,600, m_data, eflavours);
    h_pt_ip3dtrk_lead = new Variable_pt_Container(name + "_ip3dtrk_lead_ptbins", n_jets, 20,0,20,100,0,600, m_data, eflavours);
    h_pt_eta_sublead = new Variable_pt_Container(name + "_eta_sublead_ptbins", n_jets, 45, -4.5,4.5,100,0,600, m_data, eflavours);
    h_pt_phi_sublead = new Variable_pt_Container(name + "_phi_sublead_ptbins", n_jets, 45, -3.14,3.14,100,0,600, m_data, eflavours);
    h_pt_ip2dtrk_sublead = new Variable_pt_Container(name + "_ip2dtrk_sublead_ptbins", n_jets, 20,0,20,100,0,600, m_data, eflavours);
    h_pt_ip3dtrk_sublead = new Variable_pt_Container(name + "_ip3dtrk_sublead_ptbins", n_jets, 20,0,20,100,0,600, m_data, eflavours);

    h_ip2dtrk_lead = new Variable_Container(name + "_ip2dtrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dtrk_lead = new Variable_Container(name + "_ip3dtrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_ip2dnegtrk_lead = new Variable_Container(name + "_ip2dnegtrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dnegtrk_lead = new Variable_Container(name + "_ip3dnegtrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_jftrk_lead = new Variable_Container(name + "_jftrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_jffliptrk_lead = new Variable_Container(name + "_jffliptrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_sv1trk_lead = new Variable_Container(name + "_sv1trk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_sv1fliptrk_lead = new Variable_Container(name + "_sv1fliptrk_lead", n_jets, 20,0,20, m_data, eflavours);
    h_ip2dtrk_sublead = new Variable_Container(name + "_ip2dtrk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dtrk_sublead = new Variable_Container(name + "_ip3dtrk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_ip2dnegtrk_sublead = new Variable_Container(name + "_ip2dnegtrk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_ip3dnegtrk_sublead = new Variable_Container(name + "_ip3dnegtrk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_jftrk_sublead = new Variable_Container(name + "_jftrk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_jffliptrk_sublead = new Variable_Container(name + "_jffliptrk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_sv1trk_sublead = new Variable_Container(name + "_sv1trk_sublead", n_jets, 20,0,20, m_data, eflavours);
    h_sv1fliptrk_sublead = new Variable_Container(name + "_sv1fliptrk_sublead", n_jets, 20,0,20, m_data, eflavours);

  
    std::cout << "Bad loops" << std::endl;
    for (int i = 0; i < n_jets; i++)
      jets.push_back(new jet_Container(name + "_jet" + std::to_string(i + 1), n_jets, m_data, eflavours));
    for (int i = 0; i < n_el; i++)
      {
	std::string histName = name + "_el" + std::to_string(i + 1);
	if (name.find("SS") != std::string::npos) {
	  const int el_pt_bins = 3;
	  const int el_eta_bins = 4;
	  const double el_pt_edges[el_pt_bins+1] = {0.,150.,300.,600.};
	  const double el_eta_edges[el_eta_bins+1] = {-3.0,-1.0,0.,1.0,3.0};
	  el_pt.push_back(new Variable_Container(histName + "_pt", n_jets, el_pt_bins, el_pt_edges, m_data, eflavours));
	  el_eta.push_back(new Variable_Container(histName + "_eta", n_jets, el_eta_bins, el_eta_edges, m_data, eflavours));
	}
	else {
	  el_pt.push_back(new Variable_Container(histName + "_pt", n_jets, 60, 0, 600, m_data, eflavours));
	  el_eta.push_back(new Variable_Container(histName + "_eta", n_jets, 24, -3, 3, m_data, eflavours));
	}
	el_cl_eta.push_back(new Variable_Container(histName + "_cl_eta", n_jets, 24, -3, 3, m_data, eflavours));
	el_phi.push_back(new Variable_Container(histName + "_phi", n_jets, 28, -3.5, 3.5, m_data, eflavours));
	if (!m_data)
	  {
	    el_true_type.push_back(new Variable_Container(histName + "_true_type", n_jets, 39, 0, 38, m_data, eflavours));
	    el_true_origin.push_back(new Variable_Container(histName + "_true_origin", n_jets, 46, 0, 45, m_data, eflavours));
	  }
      }
    for (int i = 0; i < n_mu; i++)
      {
	std::string histName = name + "_mu" + std::to_string(i + 1);
	mu_pt.push_back(new Variable_Container(histName + "_pt", n_jets, 60, 0, 600, m_data, eflavours));
	mu_eta.push_back(new Variable_Container(histName + "_eta", n_jets, 24, -3, 3, m_data, eflavours));
	mu_phi.push_back(new Variable_Container(histName + "_phi", n_jets, 28, -3.5, 3.5, m_data, eflavours));
	if (!m_data)
	  {
	    mu_true_type.push_back(new Variable_Container(histName + "_true_type", n_jets, 39, 0, 38, m_data, eflavours));
	    mu_true_origin.push_back(new Variable_Container(histName + "_true_origin", n_jets, 46, 0, 45, m_data, eflavours));
	  }
      }
  }
  std::cout << "Finished init DiLepton " << std::endl; 
}

void DiLepChannel::addToCutflow(int bin, double weight)
{
  // std::cout<<"added to bin:"<<bin<<"  "<<weight<<std::endl;
  cutflow->Fill(bin - 1, weight);
}

std::vector<float> DiLepChannel::DL1Hand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction)
{

  std::vector<float> DL1;
  
  for(unsigned int i = 0 ; i < pb.size() ; ++i){
    float dl1=TMath::Log(pb[i]/( fraction*pc[i] + (1-fraction)*pu[i] ));
    DL1.push_back(dl1);
  }
  return DL1;

}

void DiLepChannel::Save(TFile* file)
{
  //cout<<"Saving: "<<this->name << endl;
  channel_dir = file->GetDirectory(((name).c_str()));
  if (channel_dir == 0)
    channel_dir = file->mkdir(((name).c_str()));
  channel_dir->cd();
  cutflow->Write();
  if(m_n_boot_strap_max == 0){
    ll_con->Write();
    h_met->Write();
    h_mu->Write();
    h_mu_shifted->Write();
    h_nPV->Write();
    // h_pT_eta1->Write();
    // h_pT_eta2->Write();
    // h_pT_eta3->Write();
    // h_pT_eta4->Write();
    // h_pT_eta5->Write();
    // h_pT_eta6->Write();
    // h_pT_eta7->Write();
    // h_pT_eta8->Write();
    // h_pT_eta9->Write();
    // h_pT_eta10->Write();
    // h_ptsublead_pTup->Write();
    // h_ptsublead_pTdown->Write();
    // h_etasublead_pTup->Write();
    // h_etasublead_pTdown->Write();
    // h_phisublead_pTup->Write();
    // h_phisublead_pTdown->Write();
    // h_eta_pTup->Write();
    // h_eta_pTdown->Write();
    // h_phi_pTup->Write();
    // h_phi_pTdown->Write();
    // h_mushifted_pTup->Write();
    // h_mushifted_pTdown->Write();
    if (m_storeTracks){
      h_ip2dtrk_pTup->Write();
      h_ip3dtrk_pTup->Write();
      h_ip2dnegtrk_pTup->Write();
      h_ip3dnegtrk_pTup->Write();
      h_jftrk_pTup->Write();
      h_jffliptrk_pTup->Write();
      h_sv1trk_pTup->Write();
      h_sv1fliptrk_pTup->Write();

      h_ip2dtrk_pTdown->Write();
      h_ip3dtrk_pTdown->Write();
      h_ip2dnegtrk_pTdown->Write();
      h_ip3dnegtrk_pTdown->Write();
      h_jftrk_pTdown->Write();
      h_jffliptrk_pTdown->Write();
      h_sv1trk_pTdown->Write();
      h_sv1fliptrk_pTdown->Write();


      h_ip2dtrk_lead->Write();
      h_ip3dtrk_lead->Write();
      h_ip2dnegtrk_lead->Write();
      h_ip3dnegtrk_lead->Write();
      h_jftrk_lead->Write();
      h_jffliptrk_lead->Write();
      h_sv1trk_lead->Write();
      h_sv1fliptrk_lead->Write();

      h_ip2dtrk_sublead->Write();
      h_ip3dtrk_sublead->Write();
      h_ip2dnegtrk_sublead->Write();
      h_ip3dnegtrk_sublead->Write();
      h_jftrk_sublead->Write();
      h_jffliptrk_sublead->Write();
      h_sv1trk_sublead->Write();
      h_sv1fliptrk_sublead->Write();
    }
  }
  if (this->n_jets == 2)
    {
      h_MV2_muinjet->Write();
      h_DL1_muinjet->Write();
      h_MV2r_muinjet->Write();
      h_DL1r_muinjet->Write();
      h_MV2rmu_muinjet->Write();
      h_DL1rmu_muinjet->Write();
      h_MV2_nomuinjet->Write();
      h_DL1_nomuinjet->Write();
      h_MV2r_nomuinjet->Write();
      h_DL1r_nomuinjet->Write();
      h_MV2rmu_nomuinjet->Write();
      h_DL1rmu_nomuinjet->Write();

      if(m_n_boot_strap_max == 0 )
	h_pt_both->Write();
      // h_pt_eta_lead->Write();
      // h_pt_phi_lead->Write();
      // h_pt_ip2dtrk_lead->Write();
      // h_pt_ip3dtrk_lead->Write();
      // h_pt_eta_sublead->Write();
      // h_pt_phi_sublead->Write();
      // h_pt_ip2dtrk_sublead->Write();
      // h_pt_ip3dtrk_sublead->Write();
	      
      // h_ip2dtrk_lead->Write();
      // h_ip3dtrk_lead->Write();
      // h_ip2dtrk_sublead->Write();
      // h_ip3dtrk_sublead->Write();
      if (this->m_save_fits)
	{
	  //hist_for_fit_mv2c10_fixedCut->Write();
	  // hist_for_fit_mv2c10_hybCut->Write();
	  //				hist_for_fit_mv2c10Flip_fixedCut->Write();
	  //				hist_for_fit_mv2c10Flip_hybCut->Write();
	  //	  hist_for_fit_MV2r_fixedCut->Write();
	  // hist_for_fit_MV2r_hybCut->Write();
	  // hist_for_fit_MV2rmu_fixedCut->Write();
	  // hist_for_fit_MV2rmu_hybCut->Write();
	  hist_for_fit_dl1_fixedCut->Write();
	  // hist_for_fit_dl1_hybCut->Write();
	  hist_for_fit_DL1r_fixedCut->Write();
	  // hist_for_fit_DL1r_hybCut->Write();
	  hist_for_fit_DL1rmu_fixedCut->Write();
	  // hist_for_fit_DL1rmu_hybCut->Write();
	}
      if(m_n_boot_strap_max == 0 ){
	m_jl_max->Write();
	m_jl_sqsum->Write();
	m_jl_sum->Write();
	jl_min_m_sum_Combination->Write();
	jl_min_m_sub_Combination->Write();
	jl_closest_dR_Combination->Write();
      }
    }
  if(m_n_boot_strap_max == 0){
    // lj_min_m_sum_Combination->Write();
    // min_m_dif_Combination->Write();
    for (int i = 0; i < n_jets; i++)
      jets.at(i)->Write();
    for (int i = 0; i < n_el; i++)
      {
	el_pt.at(i)->Write();
	el_eta.at(i)->Write();
	el_cl_eta.at(i)->Write();
	el_phi.at(i)->Write();
	if (!m_data)
	  {
	    el_true_type.at(i)->Write();
	    el_true_origin.at(i)->Write();
	  }
      }
    for (int i = 0; i < n_mu; i++)
      {
	mu_pt.at(i)->Write();
	mu_eta.at(i)->Write();
	mu_phi.at(i)->Write();
	if (!m_data)
	  {
	    mu_true_type.at(i)->Write();
	    mu_true_origin.at(i)->Write();
	  }
      }
  }
}

DiLepChannel::~DiLepChannel()
{
  // cout << "trying to delete: " << this->name << endl;
}

void DiLepChannel::AddEvent(TreeReader *selector, double weight, Long64_t entry)
{
  selector->fChain->GetTree()->GetEntry(entry);
  std::vector<float> DL1 = DL1Hand(*selector->jet_DL1_pb,*selector->jet_DL1_pc,*selector->jet_DL1_pu,0.018);
  std::vector<float> DL1r = DL1Hand(*selector->jet_DL1r_pb,*selector->jet_DL1r_pc,*selector->jet_DL1r_pu,0.018);
  std::vector<float> DL1rmu = DL1Hand(*selector->jet_DL1rmu_pb,*selector->jet_DL1rmu_pc,*selector->jet_DL1rmu_pu,0.018);

  if(m_n_boot_strap_max == 0){
    h_met->addEvent(selector->met_met / 1000, weight, selector->jet_truthflav);
    h_mu->addEvent(selector->mu, weight, selector->jet_truthflav);
    double mu_shift=selector->mu;
    if (!m_data){
      mu_shift=mu_shift*1.03;
    }
    h_mu_shifted->addEvent(mu_shift, weight, selector->jet_truthflav);
    h_nPV -> addEvent(selector -> nPV, weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 0 && fabs(selector->jet_eta->at(0)) < 0.3)
      h_pT_eta1->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 0.3 && fabs(selector->jet_eta->at(0)) < 0.6)
      h_pT_eta2->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 0.6 && fabs(selector->jet_eta->at(0)) < 0.9)
      h_pT_eta3->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 0.9 && fabs(selector->jet_eta->at(0)) < 1.2)
      h_pT_eta4->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 1.2 && fabs(selector->jet_eta->at(0)) < 1.5)
      h_pT_eta5->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 1.5 && fabs(selector->jet_eta->at(0)) < 1.8)
      h_pT_eta6->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 1.8 && fabs(selector->jet_eta->at(0)) < 2.1)
      h_pT_eta7->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 2.1 && fabs(selector->jet_eta->at(0)) < 2.4)
      h_pT_eta8->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 2.4 && fabs(selector->jet_eta->at(0)) < 2.7)
      h_pT_eta9->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_eta->at(0)) >= 2.7 && fabs(selector->jet_eta->at(0)) < 3.0)
      h_pT_eta10->addEvent(selector -> jet_pt->at(0) / 1000., weight, selector->jet_truthflav);
    if( fabs(selector->jet_pt->at(0) / 1000.) >= 100 ){
      h_ptsublead_pTup->addEvent(selector->jet_pt->at(1), weight, selector->jet_truthflav);
      h_etasublead_pTup->addEvent(selector->jet_eta->at(1), weight, selector->jet_truthflav);
      h_phisublead_pTup->addEvent(selector -> jet_phi->at(1), weight, selector->jet_truthflav);
      h_eta_pTup->addEvent(selector->jet_eta->at(0), weight, selector->jet_truthflav);
      h_phi_pTup->addEvent(selector -> jet_phi->at(0), weight, selector->jet_truthflav);
      h_mushifted_pTup->addEvent(mu_shift, weight, selector->jet_truthflav);
      if (m_storeTracks){
	h_ip2dtrk_pTup->addEvent(selector -> IP2D_ntrk->at(0), weight, selector->jet_truthflav);
	h_ip3dtrk_pTup->addEvent(selector -> IP3D_ntrk->at(0), weight, selector->jet_truthflav);
	h_ip2dnegtrk_pTup->addEvent(selector -> IP2DNeg_ntrk->at(0), weight, selector->jet_truthflav);
	h_ip3dnegtrk_pTup->addEvent(selector -> IP3DNeg_ntrk->at(0), weight, selector->jet_truthflav);
	h_jftrk_pTup->addEvent(selector -> JF_ntrk->at(0), weight, selector->jet_truthflav);
	h_jffliptrk_pTup->addEvent(selector -> JFFlip_ntrk->at(0), weight, selector->jet_truthflav);
	h_sv1trk_pTup->addEvent(selector -> SV1_ntrk->at(0), weight, selector->jet_truthflav);
	h_sv1fliptrk_pTup->addEvent(selector -> SV1Flip_ntrk->at(0), weight, selector->jet_truthflav);
      }
    }else{

      h_ptsublead_pTdown->addEvent(selector->jet_pt->at(1), weight, selector->jet_truthflav);
      h_etasublead_pTdown->addEvent(selector->jet_eta->at(1), weight, selector->jet_truthflav);
      h_phisublead_pTdown->addEvent(selector -> jet_phi->at(1), weight, selector->jet_truthflav);
      h_eta_pTdown->addEvent(selector -> jet_eta->at(0), weight, selector->jet_truthflav);
      h_phi_pTdown->addEvent(selector -> jet_phi->at(0), weight, selector->jet_truthflav);
      h_mushifted_pTdown->addEvent(mu_shift, weight, selector->jet_truthflav);
      if (m_storeTracks){
	h_ip2dtrk_pTdown->addEvent(selector -> IP2D_ntrk->at(0), weight, selector->jet_truthflav);
	h_ip3dtrk_pTdown->addEvent(selector -> IP3D_ntrk->at(0), weight, selector->jet_truthflav);
	h_ip2dnegtrk_pTdown->addEvent(selector -> IP2DNeg_ntrk->at(0), weight, selector->jet_truthflav);
	h_ip3dnegtrk_pTdown->addEvent(selector -> IP3DNeg_ntrk->at(0), weight, selector->jet_truthflav);
	h_jftrk_pTdown->addEvent(selector -> JF_ntrk->at(0), weight, selector->jet_truthflav);
	h_jffliptrk_pTdown->addEvent(selector -> JFFlip_ntrk->at(0), weight, selector->jet_truthflav);
	h_sv1trk_pTdown->addEvent(selector -> SV1_ntrk->at(0), weight, selector->jet_truthflav);
	h_sv1fliptrk_pTdown->addEvent(selector -> SV1Flip_ntrk->at(0), weight, selector->jet_truthflav);
      }
    }
  }


  if (this->n_jets == 2)
    {

      /*
      if(fabs(selector->jet_MV2r->at(0) - selector->jet_MV2rmu->at(0)) < 0.02  ){
	h_MV2_nomuinjet->addEvent(selector->jet_mv2c10->at(0), weight, selector->jet_truthflav);
	h_DL1_nomuinjet->addEvent(DL1.at(0), weight, selector->jet_truthflav);
	h_MV2r_nomuinjet->addEvent(selector->jet_MV2r->at(0), weight, selector->jet_truthflav);
	h_DL1r_nomuinjet->addEvent(DL1r.at(0), weight, selector->jet_truthflav);
	h_MV2rmu_nomuinjet->addEvent(selector->jet_MV2rmu->at(0), weight, selector->jet_truthflav);
	h_DL1rmu_nomuinjet->addEvent(DL1rmu.at(0), weight, selector->jet_truthflav);
      }else{	
	h_MV2_muinjet->addEvent(selector->jet_mv2c10->at(0), weight, selector->jet_truthflav);
	h_DL1_muinjet->addEvent(DL1.at(0), weight, selector->jet_truthflav);
	h_MV2r_muinjet->addEvent(selector->jet_MV2r->at(0), weight, selector->jet_truthflav);
	h_DL1r_muinjet->addEvent(DL1r.at(0), weight, selector->jet_truthflav);
	h_MV2rmu_muinjet->addEvent(selector->jet_MV2rmu->at(0), weight, selector->jet_truthflav);
	h_DL1rmu_muinjet->addEvent(DL1rmu.at(0), weight, selector->jet_truthflav);
      }
      if(fabs(selector->jet_MV2r->at(1) - selector->jet_MV2rmu->at(1)) < 0.02  ){
	h_MV2_nomuinjet->addEvent(selector->jet_mv2c10->at(1), weight, selector->jet_truthflav);
	h_DL1_nomuinjet->addEvent(DL1.at(1), weight, selector->jet_truthflav);
	h_MV2r_nomuinjet->addEvent(selector->jet_MV2r->at(1), weight, selector->jet_truthflav);
	h_DL1r_nomuinjet->addEvent(DL1r.at(1), weight, selector->jet_truthflav);
	h_MV2rmu_nomuinjet->addEvent(selector->jet_MV2rmu->at(1), weight, selector->jet_truthflav);
	h_DL1rmu_nomuinjet->addEvent(DL1rmu.at(1), weight, selector->jet_truthflav);
      }else{	
	h_MV2_muinjet->addEvent(selector->jet_mv2c10->at(1), weight, selector->jet_truthflav);
	h_DL1_muinjet->addEvent(DL1.at(1), weight, selector->jet_truthflav);
	h_MV2r_muinjet->addEvent(selector->jet_MV2r->at(1), weight, selector->jet_truthflav);
	h_DL1r_muinjet->addEvent(DL1r.at(1), weight, selector->jet_truthflav);
	h_MV2rmu_muinjet->addEvent(selector->jet_MV2rmu->at(1), weight, selector->jet_truthflav);
	h_DL1rmu_muinjet->addEvent(DL1rmu.at(1), weight, selector->jet_truthflav);
      }

      */

      if(m_n_boot_strap_max == 0 ){
	h_pt_both->addEvent(selector->jet_pt->at(0), selector->jet_pt->at(1), weight, selector->jet_truthflav);

	h_pt_eta_lead->addEvent(selector->jet_eta->at(0), selector->jet_pt->at(0), weight, selector->jet_truthflav);
	h_pt_phi_lead->addEvent(selector->jet_phi->at(0), selector->jet_pt->at(0), weight, selector->jet_truthflav);
	h_pt_eta_sublead->addEvent(selector->jet_eta->at(1), selector->jet_pt->at(1), weight, selector->jet_truthflav);
	h_pt_phi_sublead->addEvent(selector->jet_phi->at(1), selector->jet_pt->at(1), weight, selector->jet_truthflav);
	if (m_storeTracks){
	  h_pt_ip2dtrk_lead->addEvent(selector->IP2D_ntrk->at(0), selector->jet_pt->at(0), weight, selector->jet_truthflav);
	  h_pt_ip3dtrk_lead->addEvent(selector->IP3D_ntrk->at(0), selector->jet_pt->at(0), weight, selector->jet_truthflav);
	  h_pt_ip2dtrk_sublead->addEvent(selector->IP2D_ntrk->at(1), selector->jet_pt->at(1), weight, selector->jet_truthflav);
	  h_pt_ip3dtrk_sublead->addEvent(selector->IP3D_ntrk->at(1), selector->jet_pt->at(1), weight, selector->jet_truthflav);

	  h_ip2dtrk_lead->addEvent(selector->IP2D_ntrk->at(0), weight, selector->jet_truthflav);
	  h_ip3dtrk_lead->addEvent(selector->IP3D_ntrk->at(0), weight, selector->jet_truthflav);
	  h_ip2dtrk_sublead->addEvent(selector->IP2D_ntrk->at(1), weight, selector->jet_truthflav);
	  h_ip3dtrk_sublead->addEvent(selector->IP3D_ntrk->at(1), weight, selector->jet_truthflav);
	  h_ip2dnegtrk_lead->addEvent(selector->IP2DNeg_ntrk->at(0), weight, selector->jet_truthflav);
	  h_ip3dnegtrk_lead->addEvent(selector->IP3DNeg_ntrk->at(0), weight, selector->jet_truthflav);
	  h_ip2dnegtrk_sublead->addEvent(selector->IP2DNeg_ntrk->at(1), weight, selector->jet_truthflav);
	  h_ip3dnegtrk_sublead->addEvent(selector->IP3DNeg_ntrk->at(1), weight, selector->jet_truthflav);
	    
	  h_jftrk_lead->addEvent(selector->JF_ntrk->at(0), weight, selector->jet_truthflav);
	  h_jffliptrk_lead->addEvent(selector->JFFlip_ntrk->at(0), weight, selector->jet_truthflav);
	  h_jftrk_sublead->addEvent(selector->JF_ntrk->at(1), weight, selector->jet_truthflav);
	  h_jffliptrk_sublead->addEvent(selector->JFFlip_ntrk->at(1), weight, selector->jet_truthflav);
	  h_sv1trk_lead->addEvent(selector->SV1_ntrk->at(0), weight, selector->jet_truthflav);
	  h_sv1fliptrk_lead->addEvent(selector->SV1Flip_ntrk->at(0), weight, selector->jet_truthflav);
	  h_sv1trk_sublead->addEvent(selector->SV1_ntrk->at(1), weight, selector->jet_truthflav);
	  h_sv1fliptrk_sublead->addEvent(selector->SV1Flip_ntrk->at(1), weight, selector->jet_truthflav);
	}
	m_jl_max->addEvent(std::max(selector->jet_m_jl->at(0), selector->jet_m_jl->at(1)) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
	m_jl_sqsum->addEvent(sqrt(pow(selector->jet_m_jl->at(0), 2) + pow(selector->jet_m_jl->at(1), 2)) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
	m_jl_sum->addEvent((selector->jet_m_jl->at(0) + selector->jet_m_jl->at(1)) / 1000., selector->jet_pt, weight, selector->jet_truthflav);
      }
      if (this->m_save_fits)
	{
	  // cout << "saving fits: " << this->name << endl;
	  //hist_for_fit_mv2c10_fixedCut->addEvent(selector->jet_mv2c10, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  // hist_for_fit_mv2c10_hybCut->addEvent(selector->jet_mv2c10, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  //				std::vector<float> floatvecMV2c10Flip(selector->MV2c10Flip->begin(), selector->MV2c10Flip->end());
	  //				hist_for_fit_mv2c10Flip_fixedCut->addEvent(&floatvecMV2c10Flip, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  //				hist_for_fit_mv2c10Flip_hybCut->addEvent(&floatvecMV2c10Flip, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  //hist_for_fit_MV2r_fixedCut->addEvent(selector->jet_MV2r, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  // hist_for_fit_MV2r_hybCut->addEvent(selector->jet_MV2r, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  //hist_for_fit_MV2rmu_fixedCut->addEvent(selector->jet_MV2rmu, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  // hist_for_fit_MV2rmu_hybCut->addEvent(selector->jet_MV2rmu, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  hist_for_fit_dl1_fixedCut->addEvent(&DL1, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  // hist_for_fit_dl1_hybCut->addEvent(DL1, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  hist_for_fit_DL1r_fixedCut->addEvent(&DL1r, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  // hist_for_fit_DL1r_hybCut->addEvent(DL1r, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  hist_for_fit_DL1rmu_fixedCut->addEvent(&DL1rmu, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	  // hist_for_fit_DL1rmu_hybCut->addEvent(DL1rmu, selector->jet_pt, selector->jet_eta, selector->jet_m_jl, weight, selector->jet_truthflav, selector->weight_poisson);
	}
    }
  if(m_n_boot_strap_max == 0 ){
    for (int i = 0; i < n_mu; i++)
      {
	mu_pt.at(i)->addEvent(selector->mu_pt->at(i) / 1000, weight, selector->jet_truthflav);
	mu_eta.at(i)->addEvent(selector->mu_eta->at(i), weight, selector->jet_truthflav);
	mu_phi.at(i)->addEvent(selector->mu_phi->at(i), weight, selector->jet_truthflav);
	if (!m_data)
	  {
	    mu_true_type.at(i)->addEvent(selector->mu_true_type->at(i), weight, selector->jet_truthflav);
	    mu_true_origin.at(i)->addEvent(selector->mu_true_origin->at(i), weight, selector->jet_truthflav);
	  }
      }
    for (int i = 0; i < n_el; i++)
      {
	el_pt.at(i)->addEvent(selector->el_pt->at(i) / 1000, weight, selector->jet_truthflav);
	el_eta.at(i)->addEvent(selector->el_eta->at(i), weight, selector->jet_truthflav);
	el_cl_eta.at(i)->addEvent(selector->el_cl_eta->at(i), weight, selector->jet_truthflav);
	el_phi.at(i)->addEvent(selector->el_phi->at(i), weight, selector->jet_truthflav);
	if (!m_data)
	  {
	    el_true_type.at(i)->addEvent(selector->el_true_type->at(i), weight, selector->jet_truthflav);
	    el_true_origin.at(i)->addEvent(selector->el_true_origin->at(i), weight, selector->jet_truthflav);
	  }
      }
    for (int i = 0; i < n_jets; i++)
      {
	jets.at(i)->addJet(i, selector, weight);
      }
    TLorentzVector lep_4vec[2];
    TLorentzVector jet_4vec[3];
    for (int i = 0; i < n_jets; i++)
      jet_4vec[i].SetPtEtaPhiE(selector->jet_pt->at(i), selector->jet_eta->at(i), selector->jet_phi->at(i), selector->jet_e->at(i));
    if (n_el == 2)
      {
	lep_4vec[0].SetPtEtaPhiM(selector->el_pt->at(0), selector->el_eta->at(0), selector->el_phi->at(0), 0);
	lep_4vec[1].SetPtEtaPhiM(selector->el_pt->at(1), selector->el_eta->at(1), selector->el_phi->at(1), 0);
      }
    if ((n_el == 1) && (n_mu == 1))
      {
	if (selector->el_pt->at(0) > selector->mu_pt->at(0))
	  {
	    lep_4vec[0].SetPtEtaPhiM(selector->el_pt->at(0), selector->el_eta->at(0), selector->el_phi->at(0), 0);
	    lep_4vec[1].SetPtEtaPhiM(selector->mu_pt->at(0), selector->mu_eta->at(0), selector->mu_phi->at(0), 0);
	  }
	else
	  {
	    lep_4vec[1].SetPtEtaPhiM(selector->el_pt->at(0), selector->el_eta->at(0), selector->el_phi->at(0), 0);
	    lep_4vec[0].SetPtEtaPhiM(selector->mu_pt->at(0), selector->mu_eta->at(0), selector->mu_phi->at(0), 0);
	  }
      }
    if ((n_el == 0) && (n_mu == 2))
      {
	lep_4vec[0].SetPtEtaPhiM(selector->mu_pt->at(0), selector->mu_eta->at(0), selector->mu_phi->at(0), 0);
	lep_4vec[1].SetPtEtaPhiM(selector->mu_pt->at(1), selector->mu_eta->at(1), selector->mu_phi->at(1), 0);
      }
    ll_con->addEvent(lep_4vec[0], lep_4vec[1], weight, selector->jet_truthflav);
    if (this->n_jets == 2)
      {
	jl_min_m_sum_Combination->addEvent(lep_4vec, jet_4vec, weight, selector->jet_truthflav);
	jl_min_m_sub_Combination->addEvent(lep_4vec, jet_4vec, weight, selector->jet_truthflav);
	jl_closest_dR_Combination->addEvent(lep_4vec, jet_4vec, weight, selector->jet_truthflav);
      }
  }

}
