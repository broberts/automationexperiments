// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "TH1.h"
#include "TFile.h"
#include "TChain.h"
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include "libSelectorMC/finalSelectionMC.h"
#include "cxxopts.hpp"

using namespace std;

int main(int argc, char *argv[])
{
  double w_lumi = 1.;
  int hadronization=410470;
  int bsbin;
  string input_file, output_file, tChain_name,jet_collection,jet_collection_lightjets,cdi_file_path;
  bool data,run_fakes,boot_strap,apply_lf_calib,use_pt_bins_as_eta,save_fits,storeTracks,reweight_pT,reweight_ip3d,reweight_jf,reweight_sv1;
  std::string commandLineStr= "";
  for (int i=0;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));
  // cout << "commandLineStr: " << commandLineStr<< endl;
  try
  {
        cxxopts::Options options(argv[0], " - example command line options");
        options.positional_help("");
        options.add_options()
        ("i,input", "Input", cxxopts::value<std::string>(), "input_file")
        ("o,output", "outputFilename", cxxopts::value<std::string>()->default_value("test.root"), "output_file") //name
        ("c, cdiPath", "Path to CDI file to be used", cxxopts::value<std::string>(), "cdi_file_path")
        ("save_fit_input", "save the inputs for the fit later on.", cxxopts::value<bool>(save_fits))
        ("t,tsyst", "name of tChain Systematic", cxxopts::value<std::string>()->default_value("nominal"), "systName")
        ("w,isyst", "name of inner Systematic(weights)", cxxopts::value<std::string>()->default_value("nominal"), "isystName")
        ("f,NPLeptons_file", "NPLeptons_file", cxxopts::value<std::string>()->default_value(""), "NPLeptons_file")
        ("l,lumiWeight", "lumiWeight", cxxopts::value<double>(w_lumi), "lumiWeight")
        ("data", "run over data", cxxopts::value<bool>(data))
        ("boot_strap", "save boot strap weights. takes sapce and time!", cxxopts::value<bool>(boot_strap))
        ("bsbin", "choose which  boot strap weights to save!", cxxopts::value<int>(bsbin))
        ("run_fakes", "run over fakes", cxxopts::value<bool>(run_fakes))
        ("apply_lf_calib", "apply_lf_calib", cxxopts::value<bool>(apply_lf_calib))
        ("storeTracks", "storeTracks", cxxopts::value<bool>(storeTracks))
        ("jet_collection", "jet_collection", cxxopts::value<string>(jet_collection))
        ("jet_collection_lf", "jet_collection_lf", cxxopts::value<string>(jet_collection_lightjets))
        ("reweight_pT", "reweight_pT", cxxopts::value<bool>(reweight_pT))
        ("reweight_ip3d", "Reweight the histograms", cxxopts::value<bool>(reweight_ip3d))
        ("reweight_jf", "Reweight the histograms", cxxopts::value<bool>(reweight_jf))
        ("reweight_sv1", "Reweight the histograms", cxxopts::value<bool>(reweight_sv1))
        ("hadronization", "hadronization use for mc/mc sf", cxxopts::value<int>(hadronization), "int")
        ("use_pt_bins_as_eta", "run calibration with eta bins instead of pT.", cxxopts::value<bool>(use_pt_bins_as_eta) )
        ("h, help", "Print help")
        ;

        options.parse(argc, argv);

  if (options.count("help")) {
  	cout << "Where input_file is a text file with the filenames to be added to the Chain for the TSelectors" << endl;
  	cout << "TChain_name = syst name or nominal!" << endl;
  	cout << "if TChain_name ==  nominal you can specify here an inner systematic like weight_leptonSF_EL_SF_Trigger_UP" << endl;
  	std::cout << options.help({ "" }) << std::endl;
  	exit(0);
  }
  if (options.count("input")) {
    std::cout << "Input = " << options["input"].as<std::string>() << std::endl;
    input_file = options["input"].as<std::string>();
  } else {
    std::cout << "please give input file!" << std::endl;
    std::cout << options.help({ "" }) << std::endl;
    exit(0);
  }
  if (options.count("cdiPath")){
    std::cout << "CDI Path = " << options["cdiPath"].as<std::string>() << std::endl;
    cdi_file_path = options["cdiPath"].as<std::string>();
  }
  else {
    std::cout << "please give CDI path!" << std::endl;
    std::cout << options.help({""}) << std::endl;
    exit(0);
  }
  std::cout << "Output = " << options["output"].as<std::string>() << std::endl;
  output_file = options["output"].as<std::string>();
  std::cout << "tChain_name = " << options["tsyst"].as<std::string>() << std::endl;
  tChain_name = options["tsyst"].as<std::string>();
  std::cout << "inner_Systematic = " << options["isyst"].as<std::string>() << std::endl;
  std::cout << "NPLeptons_file = " << options["NPLeptons_file"].as<std::string>() << std::endl;
  std::cout << "w_lumi = " << w_lumi << std::endl;
  } catch (const cxxopts::OptionException& e) {
    std::cout << "error parsing options: " << e.what() << std::endl;
    exit(1);
  }

  TChain* chain = new TChain(tChain_name.c_str());
  ifstream myfile(input_file);
  string line;
  if (myfile.is_open())
  {
    while (getline(myfile, line))
    {
      cout << "Adding File: " << line << "|" << endl;
      chain->Add(line.c_str());
    }
    myfile.close();
  }
  else
  {
    cout << "Could not open inputfile: " << input_file << endl;
    return 1;
  }

  finalSelectionMC* selectorMC = new finalSelectionMC();
  std::cout << "selector formed" << std::endl;
  selectorMC->output = output_file;
  std::cout << "set output attribute" << std::endl;
  selectorMC->Begin(commandLineStr.c_str());
  std::cout << "Begin method done" << std::endl;
  //  TFile *MyFile = TFile::Open(output_file.c_str(), "RECREATE");
  cout << "Beginning TChain processing ... " << endl;
  chain->Process(selectorMC, commandLineStr.c_str());
  // cout << "finished TSelector" << endl;
  // if (MyFile->IsOpen()) {
  //   cout << "Outputfile still open successfully" << endl;
  //   MyFile->Close();
  //   std::cout << "Ouputfile closed." << endl;
  // }
  delete selectorMC;
  cout << "deleted TSelector" << endl;
  delete chain;
  cout << "deleted chain" << endl;
  return 0;
}
