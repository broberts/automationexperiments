rm -rf build
mkdir build
cd build
acmSetup AnalysisBase,21.2.115,here
acm find_packages
acm compile
cmake ../source
make -j
cd ../
