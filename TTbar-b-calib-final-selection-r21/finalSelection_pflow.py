import ROOT
import os
import subprocess
from options_file import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import TChain, TSelector, TTree, TCanvas, TH1F
import argparse

def Is_AFII(Input_File):
  Is_AFII = False
  if "a875" in Input_File:
      Is_AFII = True
  else:
      Is_AFII = False
  return Is_AFII

def getDsid(fileName):
    dsid=fileName.split('/')[-1].split(".")[2]
    print("disd is:" + str(dsid))
    print("fileName is:" + str(fileName))
    if (len(dsid) is not 6) and (len(dsid) is not 8):
        raise DsidNotFound('Dsid in Filename not Found! Filename:'+fileName)
    return int(dsid)

def getWeight_lumi(dsid, inFile, isyst):
    print("calculating Sum of weights")
    n_generated = 0
    print(inFile)
    for line in file(inFile):
        print(line)
        rootfile=ROOT.TFile(line.split('\n')[0])
        sumWeightsChain=rootfile.Get("sumWeights")
        for jentry in range(0,sumWeightsChain.GetEntries()):
            sumWeightsChain.GetEntry( jentry )
            if args.isyst=="weight_mc_rad_UP" and "4104" in str(dsid):
                print("using different generator weights!: " , 5, 193, "due to ", "weight_mc_rad_UP in ttbar")
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(5)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(193)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(0)
            elif args.isyst=="weight_mc_rad_DOWN" and "4104" in str(dsid):
                print( "using different generator weights!: ", 6, 194,  "due to ", "weight_mc_rad_DOWN in ttbar")
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(6)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(194)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(0)
            elif args.isyst=="weight_mc_rad_UP" and "41064" in str(dsid):
                print( "using different generator weights!: ", 5, 142,  "due to ", "weight_mc_rad_DOWN in single) top")
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(5)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(142)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(0)
            elif args.isyst=="weight_mc_rad_DOWN" and "41064" in str(dsid):
                print( "using different generator weights!: ", 6, 143,  "due to ", "weight_mc_rad_DOWN in single) top")
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(6)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(143)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(0)
            elif args.isyst=="weight_mc_fsr_UP":
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(198)
            elif args.isyst=="weight_mc_fsr_DOWN":
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(199)
            else:
                n_generated +=  sumWeightsChain.totalEventsWeighted
        rootfile.Close()
    print("sum_of_weights: " + str(n_generated))
    kfac=-1
    crossec=-1
        #calculatin w_lumi
        #from anna top mc 15: 410000  crossection: 377.9932 kfaktor: 1.1949   pythia  kfaktor needid to crrect for NNlo
    with open(os.path.expandvars('$WorkDir_DIR')+"/../../../TopDataPreparation/XSection-MC15-13TeV.data") as f:
        for line in f:
            if str(dsid) in line:
                spli=line.split()
                if not spli[0] == "#":
                    kfac=float(spli[2])
                    crossec=float(spli[1])
    if kfac == -1:
        print("kfac not found!")
        print( "were looking for dsid" + str(dsid))
        raise kfacNotFound('kfac in XSection-MC15-13TeV.data not Found!')
    if args.combine_datasets:
        if "r9364" in inFile:
            lumi_data = options.data_1516_lumi
        elif "r10201" in inFile:
            lumi_data = options.data_17_lumi
        elif "r10724" in inFile:
            lumi_data = options.data_18_lumi
    else:
        lumi_data = options.data_lumi

    if args.combine_datasets:
      if any( x in inFile for x in [".410654.",".410655."]):
        if "r10201" in inFile:
          lumi_data = options.data_17_lumi + options.data_18_lumi
        elif "r9364" in inFile:
          lumi_data = options.data_1516_lumi
        else:
          lumi_data = options.data_lumi

    lumi_mc = n_generated/(crossec * kfac)
    w_lumi=0
    if lumi_mc > 0:
        w_lumi = lumi_data/lumi_mc
    print("crossec "+str(crossec)+"; kfac "+str(kfac)+"; sum_of_weights: "+ str(n_generated)+"; w_lumi: ",str(w_lumi))
    return w_lumi

def runOverFile(inFile,outdir,outputFilename, args):
    elistsOut=outdir+outputFilename
    print("running over inFile: " + str(inFile))
    #print "outputfile: " + elistsOut
    w_lumi=1
    if not (args.data):
        #claculating w_lumi:
        dsid=getDsid(fline)
        w_lumi=getWeight_lumi(dsid,inFile, args.isyst)
    log=None;
    if args.log:
        log=open(elistsOut+".log","w")
    additional_options=[]
    if args.save_fit_input:
        additional_options.append("--save_fit_input")
    if args.use_pt_bins_as_eta:
        additional_options.append("--use_pt_bins_as_eta")
    if args.storeTracks:
        additional_options.append("--storeTracks")    
    if args.run_fakes:
        additional_options.append("--run_fakes")    
    if args.data:
        print("running on data:")
        additional_options.append("--data")
        p=subprocess.Popen([os.path.expandvars('RunTSelectorMC'),"-i", inFile,"-o",elistsOut, "--cdiPath", options.cdiPath, "-l" ,"1"]+additional_options,stdout=subprocess.PIPE)
        for stdout_line in iter(p.stdout.readline, ""):
            print(stdout_line.rstrip('\n'))
            if "deleted chain" in stdout_line:
                p.kill()
        p.stdout.close()
    else:
        is_AFII = Is_AFII(inFile);
        print("Is afii transferred correctly?: "+ str(is_AFII))
        if(((args.tsyst == "CategoryReduction_JET_JER_DataVsMC_MC16__1down") or (args.tsyst == "CategoryReduction_JET_JER_DataVsMC_MC16__1up")) and (is_AFII == True)):
            # This and next if statement are for AFII dependent systematics       
            args.tsyst = "nominal"
        elif(((args.tsyst == "CategoryReduction_JET_JER_DataVsMC_AFII__1up") or (args.tsyst == "CategoryReduction_JET_JER_DataVsMC_AFII__1down")) and (is_AFII == False)):
            args.tsyst = "nominal"
        elif(((args.tsyst == "CategoryReduction_JET_PunchThrough_MC16__1down") or (args.tsyst == "CategoryReduction_JET_PunchThrough_MC16__1up")) and (is_AFII == True)):
            args.tsyst = "nominal"
        elif(((args.tsyst == "CategoryReduction_JET_PunchThrough_AFII__1down") or (args.tsyst == "CategoryReduction_JET_PunchThrough_AFII__1up")) and (is_AFII == False)):
            args.tsyst = "nominal"
        print("args.tsyst should have changed: " + str(args.tsyst))
        print("running on MC "+ str(args.tsyst) +" "+ str(args.isyst) + ": ")
        if args.jet_collection:
            additional_options.append("--jet_collection %s"%args.jet_collection)
        if args.jet_collection_lf:
            additional_options.append("--jet_collection_lf %s"%args.jet_collection_lf)
        if args.apply_lf_calib:
            additional_options.append("--apply_lf_calib")
        if args.reweight_pT:
            additional_options.append("--reweight_pT")
        if args.reweight_ip3d:
            additional_options.append("--reweight_ip3d")
        if args.reweight_sv1:
            additional_options.append("--reweight_sv1")
        if args.reweight_jf:
            additional_options.append("--reweight_jf")
        if args.boot_strap:
            additional_options.append("--boot_strap")
            additional_options.append("--bsbin %d"%args.bsbin)
        if args.correctFakes:
            p=subprocess.Popen([os.path.expandvars('RunTSelectorMC'), "-l",str(w_lumi),"-i", inFile,"-o",elistsOut, "--cdiPath", options.cdiPath,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization,"-f",NPLeptonsFile]+additional_options, stdout=subprocess.PIPE) #,"-n",NormfactorsFile
        else:
            p=subprocess.Popen([os.path.expandvars('RunTSelectorMC'), "-l",str(w_lumi),"-i", inFile,"-o",elistsOut, "--cdiPath", options.cdiPath,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization]+additional_options, stdout=subprocess.PIPE) #,"-n",NormfactorsFile
        for stdout_line in iter(p.stdout.readline, ""):
            print(stdout_line.rstrip('\n'))
            if "deleted chain" in stdout_line:
                p.kill()
        p.stdout.close()
    # if not args.runParallel:
    #     p.wait()
    print("back in py")


def DrawCutflow(name,outdir, elists):
    print("**************************************")
    print("Drawing Cutflow: ")
    cutFlow = elists.Get(name)
    c = TCanvas()
    cutFlow.Draw()
    outputfile = outdir + "cutflow/"
    if not os.path.exists(outputfile):
        os.makedirs(outputfile)
    c.SaveAs((outputfile + name.replace("/","_") + ".png"))
    print( "Events in first bin: " + str(cutFlow.GetBinContent(1)))
    print( "Events in 3 bin channel: " + str(cutFlow.GetBinContent(3)))
    print( "Events in 4 bin sign: " + str(cutFlow.GetBinContent(4)))
    print( "Events in 5 bin 2 or 3 jets: " + str(cutFlow.GetBinContent(5)))
    print( "Events in 6 bin 2 jets: " + str(cutFlow.GetBinContent(6)))
    print( "Events in 7 bin MET: " + str(cutFlow.GetBinContent(7)))
    print( "Events in 9 bin mll: " + str(cutFlow.GetBinContent(9)))

def validateCmdLineInput():
    # Initial check for systematics, please add extra IF statements where appropriate
    if (args.tsyst != "nominal") & (args.isyst != "nominal"):
        print("you can only provide inner_systematic when you run over nominal!")
        return False
    else:
        return True

def getOutputFilename(inFile):
    # getting the ouput file name
    inpath, inFileName = os.path.split(inFile)
    sampleName = inFileName[:-4]
    if args.sFile:
        sampleName = inFileName[:-9]
        return sampleName
    if args.test:
        sampleName = sampleName + "_test"
        return sampleName
    NPLeptonsFile=""
    if args.correctFakes:
        NPLeptonsFile = outFolderName + "NPLeptons/compare_1PL_DatavsMC_nominal.root"
        return NPLeptonsFile
    return sampleName

def getOutputDirectory(outputFolderName, sampleName):
    # Get the output directory
    outdir = outFolderName + sampleName + "/"
    return outdir

# In main:
parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts.')
parser.add_argument('input_file',
                    help='input file for example: ../selecting_inputs/user.jschmoec.410000.PowhegPythiaEvtGen.DAOD_FTAG2.e3698_s2608_s2183_r7725_r7676_p2669.anaTopStyle-17-06-06_output.root.txt')
parser.add_argument('--rrS', action='store_true',
                    help='rerun the Event Selection (default: false)')
parser.add_argument('--data', action='store_true',
                    help='the input is data (default: false)')
parser.add_argument('--save_fit_input', action='store_true',
                    help='Save the histogras for the fit,takes longer and uses more space.')
parser.add_argument('--test', action='store_true',
                    help='only run on two files for testing (default: false)')
parser.add_argument('--dPlots', action='store_true',
                    help='draw Comparison Plots, takes a while. (default: false)')
parser.add_argument('--sFile', action='store_true',
                    help='The input is a single File and not a file with Folders as usual (default: false)')
parser.add_argument('--boot_strap', action='store_true',
                    help='Run over Bootstrapweights -takes much more time and space, but neccessary for mc-stat unc.')
parser.add_argument('--bsbin', action='store',type=int,default=0,
                    help='Run over bootstrap bin. 10 bins to add bootstrap weights. bsbin 0 means to run no boot_strap.')
parser.add_argument('-t',"--tsyst",default="nominal",
                    help='name of systematic tree to run over. default=nominal.')
parser.add_argument('-w',"--isyst",default="nominal",
                    help='only valid if you run on nominal. You can specify inner systematics here like weight_leptonSF_EL_SF_Trigger_UP(weights)')
parser.add_argument('--correctFakes', action='store_true',
                    help='Scale factors are applied to correct weights of SS events with a non-prompt electron - have to be generated first! (default: false)')
parser.add_argument('--run_fakes', action='store_true',
                    help='Generate  fake scale factors (default: false).')
parser.add_argument('--apply_lf_calib', action='store_true',
                    help='apply calibration for lf-jets. Not fully supportet jet, since not all taggers hafe pseudo continious lf-sf available.' )
parser.add_argument('--jet_collection', action='store',default='AntiKt4EMPFlowJets_BTagging201903',
                    help='Jet collection that is being considered in the analysis.')
parser.add_argument('--jet_collection_lf', action='store',default='AntiKt4EMPFlowJets_BTagging201903',
                    help='Jet collection that is considered to get the efficiency scale factors for light jets when option --apply_lf_calib is used. Currently, CDI only supports SF for emtopo jets.')
parser.add_argument('--storeTracks', action='store_true',
                    help='Store track information.')
parser.add_argument('--reweight_pT', action='store_true',
                    help='Reweight the pT of the first leading jet')
parser.add_argument('--reweight_ip3d', action='store_true',
                    help='Reweight the pT of the first leading jet')
parser.add_argument('--reweight_jf', action='store_true',
                    help='Reweight the pT of the first leading jet')
parser.add_argument('--reweight_sv1', action='store_true',
                    help='Reweight the pT of the first leading jet')
parser.add_argument('--hadronization', default="410470",
                    help='hadronization model used for the Generator. needed for MC/MC sf. https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalib2017#MC_MC_Scale_Factors_for_Analysis')
parser.add_argument('--log', action='store_true',
                    help='Create log files, nice option for batch submission! (default: false)')
parser.add_argument('--runParallel', action='store_true',
                    help='if you want to run all jubs in parallel. (default: false)')
parser.add_argument('--use_pt_bins_as_eta', action='store_true',
                    help='you can tweak the pt bins to hold eta. Check if sf are flat in eta.  (default: false)')
parser.add_argument('--o', action='store',default=options.output_dir,dest='output_dir',
                    help='Output directory to overwrite directory from options file.')
parser.add_argument('--combine_datasets',action='store_true',
                    help='Want to create a dir to run over all your data not just one year!')


args = parser.parse_args()
inFile = args.input_file
print("inFile: "), inFile

if(validateCmdLineInput() == False):
    # Exit script if method returns false
    exit(3)

# Get the current working dir to use as a global var in methods
analysis_dir = "/".join(options.input_selection_dir.split("/")[:-3])+"/"
print(analysis_dir)

outFolderName = args.output_dir
sampleName = getOutputFilename(inFile)
outdir = getOutputDirectory(outFolderName,sampleName)
if not os.path.exists(outdir):
    # Create output directory  if it doesn't exist
    os.makedirs(outdir)

if (args.tsyst=='nominal') & (args.isyst != ""):
    # Get the systematics requested from the cmd line
    if args.data:
        systematic_name='data'
    elif args.correctFakes:
        systematic_name='correctFakes'
    else:
        systematic_name=args.isyst #we run with differnt weights (a inner systematic)
else:
    systematic_name=args.tsyst #we run over a differnt tree (a tree systematic)
    args.isyst="nominal"

fn = 0
if args.sFile:
    inFile=inFileName
    runOverFile(inFile,outdir, sampleName, args)
    fn = fn + 1
else:
    for fline in file(inFile):
        print("Adding file: " + str(fline))
        inDir=inFile[:-4]   #subtracting '.txt'
        # flineWithoutNameSpace=fline.split(":")[1]
        flineWithoutNameSpace=fline.split('/')[-1]
        flineWithoutNameSpace=flineWithoutNameSpace[0:-1].replace(' ','')
        filesForChainFileName=flineWithoutNameSpace+".txt"
        outputFilename=filesForChainFileName[0:-16]+ "_" + systematic_name+ "_elist.root"
        if (args.rrS or not(os.path.exists(outdir+outputFilename))):
            runOverFile(inDir+'/'+filesForChainFileName,outdir,outputFilename, args)
        fn = fn + 1
    print("In " + str(fn) + " Input Containers")

    #creating a histogram with the Lumi_weight:
    if not args.data:
        w_lumi_hist=TH1F('w_lumi_hist','Lumi_weight per disd.',fn,0,fn)
    #Doing the Combination for the full sample:
    fn=0
    out_files=""
    for fline in file(inFile):
        #print "Adding file: " + fline
        flineWithoutNameSpace=fline.split('/')[-1]
        flineWithoutNameSpace=flineWithoutNameSpace[0:-1].replace(' ','')
        filesForChainFileName=flineWithoutNameSpace+".txt"
        outputFilename=filesForChainFileName[0:-16]+ "_" + systematic_name+ "_elist.root"
        single_output_file_path=outdir+outputFilename
        if args.log:
            #lets check the log file by the last line:
            print("    checking:" + single_output_file_path +".log")
            with open(single_output_file_path+".log", 'r') as fh:
                for line in fh:
                    last_line = line
                print("last_line: " + str(last_line))
                if "deleted chain" in last_line:
                    print("success! passed logtest!")
                else:
                    print("logtest failed!")
                    print("last_line: " + str(last_line))
                    print("check: " + str(single_output_file_path)+".log")
                    # exit(3)
                    fh.close()
                    print("lets try again: " + str(single_output_file_path))
                    command="mv "+single_output_file_path+".log"+" "+single_output_file_path+"_1attempt_"+".log"
                    print(command)
                    os.system(command)
                    runOverFile(inDir+'/'+filesForChainFileName,outdir,outputFilename, args)
                    with open(single_output_file_path+".log", 'r') as fh2:
                        for line2 in fh2:
                            pass
                        last_line = line2
                        print("last_line: " + str(last_line))
                        if "deleted chain" in last_line:
                            print("succes! passed logtest!")
                        else:
                            print("logtest failed again!")
                            print("last_line: " + str(last_line))
                            print("check: "+ str(single_output_file_path)+".log")
                            exit(3)
        #lets check if we can open correctly the outward path:
        print("Test if the output file opens correctly:")
        print(single_output_file_path)
        single_output_file = ROOT.TFile(single_output_file_path, "read")
        h_emu_OS_J2_CutFlow=single_output_file.Get("h_emu_OS_J2_CutFlow")
        out_files=out_files+single_output_file_path+" "
        fn = fn + 1
        single_output_file.Close()
        print("Passed")
        #filling the histo with the applied lumi weight:
        if not args.data:
            single_output_file = ROOT.TFile(single_output_file_path, "read")
            w_lumi_hist.AddBinContent(fn,single_output_file.Get("applied_lumi_weight")[0])
            dsid=getDsid(fline)
            w_lumi_hist.GetXaxis().SetBinLabel(fn,str(dsid))
            single_output_file.Close();
    print("combining output files: ")
    combiName=sampleName+ "_" + systematic_name +"_combination.root"
    if args.bsbin != 0 :
        combiName=sampleName+ "_" + systematic_name +"_bootStrapBin"+str(args.bsbin)+"_combination.root"
    command="hadd -f "+outdir+combiName+" "+out_files
    os.system(command)
    print("In " + str(fn) + " Input Containers")
    if not args.data:
        c = TCanvas()
        outfile= ROOT.TFile(outdir+combiName,"update")
        w_lumi_hist.Write()
        w_lumi_hist.SetMaximum(3)
        w_lumi_hist.Draw()
        outfile.Close()
        #c.SaveAs(outdir+"lumi_weight.pdf")
    #lets remove the elist files to save some disc space:
    command="rm -rf "+out_files
    print(command)
    os.system(command)

if args.dPlots:
    print("Drawing Cutflows:")
    f_comb = ROOT.TFile(outdir+combiName, "read")
    p_outdir = outdir + "ComparisonPlots/"
    if not os.path.exists(p_outdir):
        os.makedirs(p_outdir)
    c = TCanvas()
    jet_multiplicity = f_comb.Get("h_jet_multiplicity")
    jet_multiplicity.Draw()
    c.SaveAs(p_outdir + "h_jet_multiplicity.png")
    c.SaveAs(p_outdir + "h_jet_multiplicity.pdf")
    #if not args.data:
    ##"h_"+"ee_OS_J2_jet1"+"_flav"+to_string(flavours[flav])+"_pt"
    DrawCutflow("ee_OS_J2_aCuts/h_ee_OS_J2_aCuts_CutFlow", p_outdir ,f_comb)
    DrawCutflow("ee_OS_J3_aCuts/h_ee_OS_J3_aCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J2_aCuts/h_mumu_OS_J2_aCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J3_aCuts/h_mumu_OS_J3_aCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("emu_OS_J2/h_emu_OS_J2_CutFlow", p_outdir, f_comb)
    DrawCutflow("emu_OS_J3/h_emu_OS_J3_CutFlow", p_outdir, f_comb)
    DrawCutflow("ee_OS_J2_bCuts/h_ee_OS_J2_bCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("ee_OS_J3_bCuts/h_ee_OS_J3_bCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J2_bCuts/h_mumu_OS_J2_bCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J3_bCuts/h_mumu_OS_J3_bCuts_CutFlow", p_outdir, f_comb)
    f_comb.Close()
print("End.")
