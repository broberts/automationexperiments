import ROOT
import os
import copy
import array
from options_file import *
from ROOT import TChain, TSelector, TTree, TCanvas, TH1F
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)

process = "FTAG2_ttbar_PhPy8"
inputfile = options.input_selection_dir + process + ".txt"
output_file=ROOT.TFile("output.root","recreate")
lumi = options.data_lumi
nTotalEventsWeighted = 0
nTotalEventsWeighted_0 = 0
nTotalEventsWeighted_radUnc = 0
nTotalEventsWeighted_198 = 0
nTotalEventsWeighted_199 = 0
SumOfWeights = 0
SumOfWeights_0 = 0
SumOfWeights_5 = 0
SumOfWeights_193 = 0
SumOfWeights_198 = 0
SumOfWeights_199 = 0
SumOfWeights_radUnc_correct = 0
SumOfWeights_radUnc_wrong = 0

output_file.cd()
h_weights=ROOT.TH1D("h_weights","histogram with individual event weights",10000,0,1000)
h_weights_0=ROOT.TH1D("h_weights_0","histogram with individual event weights",10000,0,10)
h_weights_198=ROOT.TH1D("h_weights_198","histogram with individual event weights",10000,0,100)
h_weights_199=ROOT.TH1D("h_weights_199","histogram with individual event weights",10000,0,100)
dsid = 0
chain = "nominal"
# h_weights.Fill(0.5)
# h_weights.Fill(0.6)
# h_weights.Fill(0.5)
# h_weights.Fill(0.5)
print "Running on " + process
for fline in file(inputfile):
    print "##########################################################"
    print "Running over " + fline.replace('\n', '')
    dsid = fline.split(".")[3]
    
    print "DSID = " + str(dsid)
    with open("../AnalysisTop-21.2.X/grid/TopDataPreparation/XSection-MC15-13TeV.data") as f:
        for xline in f:
            if str(dsid) in xline:
                spli = xline.split()
                if not spli[0] == "#":
                    XS = float(spli[1])
                    kfactor = float(spli[2])
    print "XS = " + str(XS)
    print "kfactor = " + str(kfactor)
    print
    sumWeightsChain = TChain("sumWeights")
    treeChain = TChain(chain)
    for line in file("/nfs/dust/atlas/user/schmoecj/b-jet-calib-code/bjets_ttbardilepton_PDF_cleanup/TTbar-b-calib-final-selection-r21/ntuple_input_files/r21-2-29_18-07-03_mc16a/FTAG2_ttbar_PhPy8/user.jgeisen.410472.PhPy8EG.DAOD_FTAG2.e6348_e5984_s3126_r9364_r9315_p3563.AT21-2-29.18-05-14_v2_output_root.txt"):
        print "Running on file " + line.replace('\n', '')
        infile = ROOT.TFile(line.replace('\n', ''), "read")
        sumWeightsChain.AddFile(line[:-1])
        treeChain.AddFile(line[:-1])
        infile.Close()
    print "Running over sumWeights"
    for ientry in range(sumWeightsChain.GetEntries()):
        sumWeightsChain.GetEntry(ientry)
        nTotalEventsWeighted += sumWeightsChain.totalEventsWeighted
        nTotalEventsWeighted_0 += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(0)
        nTotalEventsWeighted_radUnc += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(5) * sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(193) / sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(0)
        nTotalEventsWeighted_198 += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(198)
        nTotalEventsWeighted_199 += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(199)
    print "Finished sumWeights after " + str(sumWeightsChain.GetEntries()) + " entries"
    print "Running over " + chain
    for jentry in range(treeChain.GetEntries()):
        if True:
            treeChain.GetEntry(jentry)
            w_mc=treeChain.weight_mc
            SumOfWeights += w_mc 
            h_weights.Fill(w_mc)
            SumOfWeights_0 += treeChain.mc_generator_weights.at(0)
            h_weights_0.Fill(treeChain.mc_generator_weights.at(0)/w_mc)

            SumOfWeights_5 += treeChain.mc_generator_weights.at(5)
            SumOfWeights_193 += treeChain.mc_generator_weights.at(193)
            SumOfWeights_radUnc_correct += treeChain.mc_generator_weights.at(5)*treeChain.mc_generator_weights.at(193)/treeChain.mc_generator_weights.at(0)
            w_198=treeChain.mc_generator_weights.at(198)
            SumOfWeights_198 += w_198
            h_weights_198.Fill(w_198/w_mc)
            w_199=treeChain.mc_generator_weights.at(199)
            SumOfWeights_199 += w_199
            h_weights_199.Fill(w_199/w_mc)
            if jentry%10000==0:
                print "Running over entry " + str(jentry)
                print "weights", treeChain.weight_mc, treeChain.mc_generator_weights.at(0),treeChain.mc_generator_weights.at(5),treeChain.mc_generator_weights.at(193),treeChain.mc_generator_weights.at(198) ,treeChain.mc_generator_weights.at(199)
    print "Finished " + chain + " after " + str(treeChain.GetEntries()) + " events"
    #SumOfWeights_radUnc_wrong = SumOfWeights_5*SumOfWeights_193/SumOfWeights_0
    print "nTotalEventsWeighted = " + str(nTotalEventsWeighted)
    print "nTotalEventsWeighted_0 = " + str(nTotalEventsWeighted_0)
    print "nTotalEventsWeighted_radUnc = " + str(nTotalEventsWeighted_radUnc)
    print "nTotalEventsWeighted_198 = " + str(nTotalEventsWeighted_198)
    print "nTotalEventsWeighted_199 = " + str(nTotalEventsWeighted_199)
    weight = XS * kfactor * lumi / nTotalEventsWeighted
    weight_0 = XS * kfactor * lumi / nTotalEventsWeighted_0
    weight_radUnc = XS * kfactor * lumi / nTotalEventsWeighted_radUnc
    print "Final weight factor = " + str(weight)
    print "Final weight factor_0 = " + str(weight_0)
    print "Final weight factor_radUnc = " + str(weight_radUnc)
    print "------------------------------"
    print "SumOfWeights = " + str(SumOfWeights)
    # sumofweight = XS * kfactor * lumi / SumOfWeights
    if chain == "nominal":
        print "SumOfWeights_0 = " + str(SumOfWeights_0)
        print "SumOfWeights_5 = " + str(SumOfWeights_5)
        print "SumOfWeights_193 = " + str(SumOfWeights_193)
        print "SumOfWeights_198 = " + str(SumOfWeights_198)
        print "SumOfWeights_199 = " + str(SumOfWeights_199)
        print "SumOfWeights_radUnc_correct = " + str(SumOfWeights_radUnc_correct)
        #print "SumOfWeights_radUnc_wrong = " + str(SumOfWeights_radUnc_wrong)
    #     sumofweight_0 = XS * kfactor * lumi / SumOfWeights_0
    #     sumofweight_radUnc_correct = XS * kfactor * lumi / SumOfWeights_radUnc_correct
    #     sumofweight_radUnc_wrong = XS * kfactor * lumi / SumOfWeights_radUnc_wrong
    # print "New weight factor = " + str(sumofweight)
    # if chain == "nominal" :
    #     print "New weight factor_0 = " + str(sumofweight_0)
    #     print "New weight factor_radUnc_correct = " + str(sumofweight_radUnc_correct)
    #     print "New weight factor_radUnc_wrong = " + str(sumofweight_radUnc_wrong)
output_file.cd()
h_weights.Write()
h_weights_0.Write()
h_weights_198.Write()
h_weights_199.Write()
output_file.Close()
print "Done."
