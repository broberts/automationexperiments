import ROOT
import os
import sys
import argparse
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
sys.path.insert(0, '../')
from options_file import *

parser = argparse.ArgumentParser(
    description='Subtracts a chosen MC channel from another chosen data channel:')
parser.add_argument('-s',"--syst_name",default="nominal",
                    help='name of systematic syst to run over. default=nominal.')
parser.add_argument('-i',"--input_channel",default="emu_SS_J2",
                    help='Name of the input data channel. default=emu_SS_J2.')
parser.add_argument('-m',"--subtract_channel",default="emu_SS_2PL_J2",
                    help='Name of the MC channel to subtract from data. default=emu_SS_2PL_J2.')
parser.add_argument('-d',"--destination_channel",default="emu_SS_NPel_J2",
                    help='Name of the data channel to store the output into. default=emu_SS_NPel_J2.')
args = parser.parse_args()

syst = args.syst_name
inputchanneldir_name = args.input_channel
subtractdir_name = args.subtract_channel
destinationdir_name = args.destination_channel

outdir=options.output_dir
dataName=options.data_name

MCsamples=[]
MCsamples.append(options.Diboson_sample)
MCsamples.append(options.singleTop_sample)
MCsamples.append(options.Wjets_sample)
MCsamples.append(options.ZJets_sample)
MCsamples.append(options.ttb_sample)

for sample in MCsamples:
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_" + syst + "_combination.root"

datafile_name = outdir+dataName+"/"+dataName+"_data_combination.root"
if os.path.exists(datafile_name):
	datafile = ROOT.TFile(datafile_name, "read")
else:
	print "Input data file not found: ", datafile_name
	exit(1)
print datafile_name

outputfile_name=outdir+dataName+"/"+dataName+"_data_modified_" + syst + "_combination.root"
outputfile=ROOT.TFile(outputfile_name,"recreate")
outputfile.mkdir(destinationdir_name)

Flavourlabels=[	'bb','bc','bl','cb','cc','cl','lb','lc','ll' ]

indir_index = 0
subdir_index = 0

data_keyList=datafile.GetListOfKeys()
for i in xrange(0, data_keyList.GetSize()):
	data_obj=datafile.Get(data_keyList.At(i).GetName())
	data_className=data_obj.ClassName()
	if data_className[:2]=="TD":
		if data_obj.GetName() == inputchanneldir_name:
			indir_index = i
			break
data_td_keyList = data_obj.GetListOfKeys()
for h in xrange(0, data_td_keyList.GetSize()):
	data_td_obj=data_obj.Get(data_td_keyList.At(h).GetName())
	data_td_className=data_td_obj.ClassName()
	if data_td_className[:3]=="TH1":
		data_td_histname=data_td_obj.GetName()
		hist = data_td_obj
		hist.SetName(data_td_histname.replace(inputchanneldir_name,destinationdir_name))
		print hist.GetName()
		for sample in MCsamples:
			if os.path.exists(sample.root_file_path):
				inFile=ROOT.TFile(sample.root_file_path,"read")
			else:
				print "Input file not found: ", sample.root_file_path
				exit(1)
			mc_keyList=inFile.GetListOfKeys()
			if subdir_index == 0:
				for i in xrange(0, mc_keyList.GetSize()):
					mc_obj=inFile.Get(mc_keyList.At(i).GetName())
					mc_className=mc_obj.ClassName()
					if mc_className[:2]=="TD":
						mc_td_name=mc_obj.GetName()
						if  mc_td_name == subtractdir_name:
							subdir_index = i
							break
			else:
				mc_obj=inFile.Get(mc_keyList.At(subdir_index).GetName()) #should be the same position in every MC sample
			mc_td_histname = data_td_histname.replace(inputchanneldir_name,subtractdir_name)
			if "CutFlow" in data_td_histname:
				mc_hist = mc_obj.Get(mc_td_histname) #no split in flavours here
			else:
				mc_hist = mc_obj.Get(mc_td_histname.replace("data","bb"))
				for flav in Flavourlabels:
					if flav == 'bb':
						continue
					mc_hist.Add(mc_obj.Get(mc_td_histname.replace("data",flav)))
			hist.Add(mc_hist,-1.)
		outputfile.cd(destinationdir_name)
		hist.Write()
outputfile.Close()
print "Finished: ",outputfile_name
