import ROOT
import os
import sys
import math
import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
sys.path.insert(0, '../')
sys.path.insert(0, os.getcwd())
from options_file import *

outdir=options.output_dir
dataName=options.data_name

MCsamples=[]
MCsamples.append(options.Diboson_sample)
MCsamples.append(options.singeTop_sample)
MCsamples.append(options.Wjets_sample)
MCsamples.append(options.ZJets_sample)
MCsamples.append(options.ttb_sample)
for sample in MCsamples:
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_nominal_combination.root"

Channels=[	'emu_OS_J2',
		'emu_SS_J2',
		'emu_SS_2PL_J2',
		'emu_SS_NPel_J2',
		'emu_SS_NPmu_J2',
]

if not os.path.exists(outdir+"NPLeptons/"):
    os.makedirs(outdir+"NPLeptons/")
outputfile_name=outdir+"NPLeptons/compare_1PL_DatavsMC_nominal.root"
outputfile=ROOT.TFile(outputfile_name,"recreate")

datafile = ROOT.TFile(outdir+dataName+"/"+dataName+"_data_combination.root", "read")

Flavourlabels=[	'bb','bc','bl','cb','cc','cl','lb','lc','ll' ]
new_binedges_el = [0.,150.,300.,600.]
new_ngroup_el = len(new_binedges_el)-1
new_binedges_mu = [0.,60.,80.,600.]
new_ngroup_mu = len(new_binedges_mu)-1

hist_OS_data_elpt = ROOT.TH1D("hist_OS_data_elpt","hist_OS_data_elpt",60, 0, 600)
hist_OS_data_elpt.Sumw2()
hist_OS_data_elpt.GetXaxis().SetTitle("Electron p_{T} [GeV]")
hist_OS_data_elpt.GetYaxis().SetTitle("Number of events")
hist_OS_data_mupt = ROOT.TH1D("hist_OS_data_mupt","hist_OS_data_mupt",60, 0, 600)
hist_OS_data_mupt.Sumw2()
hist_OS_data_mupt.GetXaxis().SetTitle("Muon p_{T} [GeV]")
hist_OS_data_mupt.GetYaxis().SetTitle("Number of events")
hist_SS_data_elpt = ROOT.TH1D("hist_SS_data_elpt","hist_SS_data_elpt", new_ngroup_el, array('d', new_binedges_el))
hist_SS_data_elpt.Sumw2()
hist_SS_data_elpt.GetXaxis().SetTitle("Electron p_{T} [GeV]")
hist_SS_data_elpt.GetYaxis().SetTitle("Number of events")
hist_SS_data_mupt = ROOT.TH1D("hist_SS_data_mupt","hist_SS_data_mupt",60, 0, 600)
hist_SS_data_mupt.Sumw2()
hist_SS_data_mupt.GetXaxis().SetTitle("Muon p_{T} [GeV]")
hist_SS_data_mupt.GetYaxis().SetTitle("Number of events")
hist_OS_MC2PL_elpt = ROOT.TH1D("hist_OS_MC2PL_elpt","hist_OS_MC2PL_elpt",60, 0, 600)
hist_OS_MC2PL_elpt.Sumw2()
hist_OS_MC2PL_elpt.GetXaxis().SetTitle("Electron p_{T} [GeV]")
hist_OS_MC2PL_elpt.GetYaxis().SetTitle("Number of events")
hist_OS_MC2PL_mupt = ROOT.TH1D("hist_OS_MC2PL_mupt","hist_OS_MC2PL_mupt",60, 0, 600)
hist_OS_MC2PL_mupt.Sumw2()
hist_OS_MC2PL_mupt.GetXaxis().SetTitle("Muon p_{T} [GeV]")
hist_OS_MC2PL_mupt.GetYaxis().SetTitle("Number of events")
hist_SS_MC2PL_elpt = ROOT.TH1D("hist_SS_MC2PL_elpt","hist_SS_MC2PL_elpt",new_ngroup_el, array('d', new_binedges_el))
hist_SS_MC2PL_elpt.Sumw2()
hist_SS_MC2PL_elpt.GetXaxis().SetTitle("Electron p_{T} [GeV]")
hist_SS_MC2PL_elpt.GetYaxis().SetTitle("Number of events")
hist_SS_MC2PL_mupt = ROOT.TH1D("hist_SS_MC2PL_mupt","hist_SS_MC2PL_mupt",60, 0, 600)
hist_SS_MC2PL_mupt.Sumw2()
hist_SS_MC2PL_mupt.GetXaxis().SetTitle("Muon p_{T} [GeV]")
hist_SS_MC2PL_mupt.GetYaxis().SetTitle("Number of events")
hist_SS_NPel_pt = ROOT.TH1D("hist_SS_NPel_pt","hist_SS_NPel_pt",new_ngroup_el, array('d', new_binedges_el))
hist_SS_NPel_pt.Sumw2()
hist_SS_NPel_pt.GetXaxis().SetTitle("Electron p_{T} [GeV]")
hist_SS_NPel_pt.GetYaxis().SetTitle("Number of events")
hist_SS_NPmu_pt = ROOT.TH1D("hist_SS_NPmu_pt","hist_SS_NPmu_pt",60, 0, 600)
hist_SS_NPmu_pt.Sumw2()
hist_SS_NPmu_pt.GetXaxis().SetTitle("Muon p_{T} [GeV]")
hist_SS_NPmu_pt.GetYaxis().SetTitle("Number of events")

def ReadLeptonPT(channelname):
	histelptname = channelname + "/h_" + channelname + "_el1_pt"
	histmuptname = channelname + "/h_" + channelname + "_mu1_pt"	
	if "emu_OS_J2" == channelname:
		hist_OS_data_elpt.Add(datafile.Get(histelptname + "_data"))
		hist_OS_data_mupt.Add(datafile.Get(histmuptname + "_data"))
		for sample in MCsamples:
			MCInputfile = ROOT.TFile(sample.root_file_path, "read")
			for flavour in Flavourlabels:
				hist_OS_MC2PL_elpt.Add(MCInputfile.Get(histelptname + "_" + flavour))
				hist_OS_MC2PL_mupt.Add(MCInputfile.Get(histmuptname + "_" + flavour))
	elif "emu_SS_J2" == channelname:
		hist_SS_data_elpt.Add(datafile.Get(histelptname + "_data"))
		hist_SS_data_mupt.Add(datafile.Get(histmuptname + "_data"))
	elif "emu_SS_2PL_J2" == channelname:
		for sample in MCsamples:
			MCInputfile = ROOT.TFile(sample.root_file_path, "read")
			for flavour in Flavourlabels:
				hist_SS_MC2PL_elpt.Add(MCInputfile.Get(histelptname + "_" + flavour))
				hist_SS_MC2PL_mupt.Add(MCInputfile.Get(histmuptname + "_" + flavour))
	else:
		for sample in MCsamples:
			MCInputfile = ROOT.TFile(sample.root_file_path, "read")
			if "el" in channelname:
				for flavour in Flavourlabels:
					hist_SS_NPel_pt.Add(MCInputfile.Get(histelptname + "_" + flavour))
			else:
				for flavour in Flavourlabels:
					hist_SS_NPmu_pt.Add(MCInputfile.Get(histmuptname + "_" + flavour))

#Loop over all channels and get lepton pT histograms for each individually
for channel in Channels:
	print '####################################################'
	print 'Read lepton pT histograms for channel: ' + channel
	ReadLeptonPT(channel)
	print

#Rebinning histograms in lepton pT, depending on flavour
hist_OS_data_elpt_rebinned = hist_OS_data_elpt.Rebin(new_ngroup_el,"hist_OS_data_elpt_rebinned",array('d', new_binedges_el))
hist_OS_data_mupt_rebinned = hist_OS_data_mupt.Rebin(new_ngroup_mu,"hist_OS_data_mupt_rebinned",array('d', new_binedges_mu))
hist_SS_data_elpt_rebinned = hist_SS_data_elpt.Rebin(new_ngroup_el,"hist_SS_data_elpt_rebinned",array('d', new_binedges_el))
hist_SS_data_mupt_rebinned = hist_SS_data_mupt.Rebin(new_ngroup_mu,"hist_SS_data_mupt_rebinned",array('d', new_binedges_mu))
hist_OS_MC2PL_elpt_rebinned = hist_OS_MC2PL_elpt.Rebin(new_ngroup_el,"hist_OS_MC2PL_elpt_rebinned",array('d', new_binedges_el))
hist_OS_MC2PL_mupt_rebinned = hist_OS_MC2PL_mupt.Rebin(new_ngroup_mu,"hist_OS_MC2PL_elpt_rebinned",array('d', new_binedges_mu))
hist_SS_MC2PL_elpt_rebinned = hist_SS_MC2PL_elpt.Rebin(new_ngroup_el,"hist_SS_MC2PL_elpt_rebinned",array('d', new_binedges_el))
hist_SS_MC2PL_mupt_rebinned = hist_SS_MC2PL_mupt.Rebin(new_ngroup_mu,"hist_SS_MC2PL_elpt_rebinned",array('d', new_binedges_mu))
hist_SS_NPel_pt_rebinned = hist_SS_NPel_pt.Rebin(new_ngroup_el,"hist_SS_NPel_pt_rebinned",array('d', new_binedges_el))
hist_SS_NPmu_pt_rebinned = hist_SS_NPmu_pt.Rebin(new_ngroup_mu,"hist_SS_NPmu_pt_rebinned",array('d', new_binedges_mu))

hist_SS_data_elpt_rebinned.Add(hist_SS_MC2PL_elpt_rebinned,-1.0)
hist_SS_data_mupt_rebinned.Add(hist_SS_MC2PL_mupt_rebinned,-1.0)

styles = [ 20,23,22,28,25,31,24,27 ] #https://root.cern.ch/doc/v608/classTAttMarker.html
colours = [ 1,28,6,5,8,42,4,2 ]
hist_OS_data_elpt_rebinned.SetLineWidth(2)
hist_OS_data_elpt_rebinned.SetMarkerSize(0.5)
hist_OS_data_elpt_rebinned.SetMarkerStyle(styles[0])
hist_OS_data_elpt_rebinned.SetFillColor(colours[0])
hist_OS_data_elpt_rebinned.SetMarkerColor(colours[0])
hist_OS_data_elpt_rebinned.SetLineColor(colours[0])
hist_OS_data_elpt_rebinned.GetXaxis().SetTitleSize(1.5)
hist_OS_data_elpt_rebinned.GetXaxis().SetLabelSize(0.08)
hist_OS_data_elpt_rebinned.GetYaxis().SetTitleSize(0.05)
hist_OS_data_elpt_rebinned.GetYaxis().SetTitleOffset(1.5)
hist_OS_data_mupt_rebinned.SetLineWidth(2)
hist_OS_data_mupt_rebinned.SetMarkerSize(0.5)
hist_OS_data_mupt_rebinned.SetMarkerStyle(styles[0])
hist_OS_data_mupt_rebinned.SetFillColor(colours[0])
hist_OS_data_mupt_rebinned.SetMarkerColor(colours[0])
hist_OS_data_mupt_rebinned.SetLineColor(colours[0])
hist_OS_data_mupt_rebinned.GetXaxis().SetTitleSize(1.5)
hist_OS_data_mupt_rebinned.GetXaxis().SetLabelSize(0.08)
hist_OS_data_mupt_rebinned.GetYaxis().SetTitleSize(0.05)
hist_OS_data_mupt_rebinned.GetYaxis().SetTitleOffset(1.5)
hist_SS_data_elpt_rebinned.SetLineWidth(2)
hist_SS_data_elpt_rebinned.SetMarkerSize(0.5)
hist_SS_data_elpt_rebinned.SetMarkerStyle(styles[0])
hist_SS_data_elpt_rebinned.SetFillColor(colours[0])
hist_SS_data_elpt_rebinned.SetMarkerColor(colours[0])
hist_SS_data_elpt_rebinned.SetLineColor(colours[0])
hist_SS_data_elpt_rebinned.GetXaxis().SetTitleSize(1.5)
hist_SS_data_elpt_rebinned.GetXaxis().SetLabelSize(0.08)
hist_SS_data_elpt_rebinned.GetYaxis().SetTitleSize(0.05)
hist_SS_data_elpt_rebinned.GetYaxis().SetTitleOffset(1.1)
hist_SS_data_mupt_rebinned.SetLineWidth(2)
hist_SS_data_mupt_rebinned.SetMarkerSize(0.5)
hist_SS_data_mupt_rebinned.SetMarkerStyle(styles[0])
hist_SS_data_mupt_rebinned.SetFillColor(colours[0])
hist_SS_data_mupt_rebinned.SetMarkerColor(colours[0])
hist_SS_data_mupt_rebinned.SetLineColor(colours[0])
hist_SS_data_mupt_rebinned.GetXaxis().SetTitleSize(1.5)
hist_SS_data_mupt_rebinned.GetXaxis().SetLabelSize(0.08)
hist_SS_data_mupt_rebinned.GetYaxis().SetTitleSize(0.05)
hist_SS_data_mupt_rebinned.GetYaxis().SetTitleOffset(1.1)
hist_OS_MC2PL_elpt_rebinned.SetLineWidth(1)
hist_OS_MC2PL_elpt_rebinned.SetMarkerSize(0.5)
hist_OS_MC2PL_elpt_rebinned.SetMarkerStyle(styles[6])
hist_OS_MC2PL_elpt_rebinned.SetFillColor(colours[6])
hist_OS_MC2PL_elpt_rebinned.SetMarkerColor(colours[6])
hist_OS_MC2PL_elpt_rebinned.SetLineColor(colours[6])
hist_OS_MC2PL_mupt_rebinned.SetLineWidth(1)
hist_OS_MC2PL_mupt_rebinned.SetMarkerSize(0.5)
hist_OS_MC2PL_mupt_rebinned.SetMarkerStyle(styles[6])
hist_OS_MC2PL_mupt_rebinned.SetFillColor(colours[6])
hist_OS_MC2PL_mupt_rebinned.SetMarkerColor(colours[6])
hist_OS_MC2PL_mupt_rebinned.SetLineColor(colours[6])
hist_SS_MC2PL_elpt_rebinned.SetLineWidth(1)
hist_SS_MC2PL_elpt_rebinned.SetMarkerSize(0.5)
hist_SS_MC2PL_elpt_rebinned.SetMarkerStyle(styles[6])
hist_SS_MC2PL_elpt_rebinned.SetFillColor(colours[6])
hist_SS_MC2PL_elpt_rebinned.SetMarkerColor(colours[6])
hist_SS_MC2PL_elpt_rebinned.SetLineColor(colours[6])
hist_SS_MC2PL_mupt_rebinned.SetLineWidth(1)
hist_SS_MC2PL_mupt_rebinned.SetMarkerSize(0.5)
hist_SS_MC2PL_mupt_rebinned.SetMarkerStyle(styles[6])
hist_SS_MC2PL_mupt_rebinned.SetFillColor(colours[6])
hist_SS_MC2PL_mupt_rebinned.SetMarkerColor(colours[6])
hist_SS_MC2PL_mupt_rebinned.SetLineColor(colours[6])
hist_SS_NPel_pt_rebinned.SetLineWidth(1)
hist_SS_NPel_pt_rebinned.SetMarkerSize(0.5)
hist_SS_NPel_pt_rebinned.SetMarkerStyle(styles[7])
hist_SS_NPel_pt_rebinned.SetFillColor(colours[7])
hist_SS_NPel_pt_rebinned.SetMarkerColor(colours[7])
hist_SS_NPel_pt_rebinned.SetLineColor(colours[7])
hist_SS_NPmu_pt_rebinned.SetLineWidth(1)
hist_SS_NPmu_pt_rebinned.SetMarkerSize(0.5)
hist_SS_NPmu_pt_rebinned.SetMarkerStyle(styles[7])
hist_SS_NPmu_pt_rebinned.SetFillColor(colours[7])
hist_SS_NPmu_pt_rebinned.SetMarkerColor(colours[7])
hist_SS_NPmu_pt_rebinned.SetLineColor(colours[7])

#Plot histogram
import atlas_labels as al
AtlasStyle = al.atlasStyle()
AtlasStyle.SetErrorX(0.5)
AtlasStyle.SetEndErrorSize( 0 )
AtlasStyle.SetPadTopMargin(0.05)
AtlasStyle.SetPadRightMargin(  0.12)
AtlasStyle.SetPadBottomMargin( 0.20)
AtlasStyle.SetPadLeftMargin(   0.08)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()

c1 = TCanvas("c1", "Data vs. MC in emu+2j SS", 1200, 800)
c1.Divide(2,1)
c1.cd(1)

mainPad  = TPad("mainPad", "top", 0.0, 0.3, 1.0, 1.0)
mainPad.SetTopMargin(0.05)
mainPad.SetBottomMargin(0.010)
mainPad.SetRightMargin(0.026)
mainPad.SetLeftMargin(0.15)
mainPad.Draw()
ratioPad  = TPad("ratioPad", "bottom", 0.0, 0.0, 1.0, 0.30)
ratioPad.SetTopMargin(0.04)
ratioPad.SetBottomMargin(0.40)
ratioPad.SetRightMargin(0.026)
ratioPad.SetLeftMargin(0.15)
ratioPad.Draw()

#Setup legend
leg = ROOT.TLegend(0.65,0.63,0.85,0.75)
leg.SetTextSize(0.033)
leg.AddEntry(hist_SS_data_elpt_rebinned, "Data - MC (2PL)", "pl")
leg.AddEntry(hist_SS_NPel_pt_rebinned, "MC (NPel)", "pl")
leg.SetShadowColor(0)
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetLineColor(0)
leg.SetFillStyle(0)
leg.SetBorderSize(0)

mainPad.cd()

hist_SS_data_elpt_rebinned.Draw("P E")
hist_SS_NPel_pt_rebinned.Draw("P E SAME")
leg.Draw("SAME")
l1 = ROOT.TLatex()
l1.SetTextSize(0.04)
l1.SetTextColor(kBlack)
l1.SetNDC()
l1.DrawLatex(0.53,0.87,"#bf{ATLAS} Internal")
l1.DrawLatex(0.53,0.82, "#sqrt{s} = 13 TeV, L_{int}=36.1 fb^{-1}")
l1.DrawLatex(0.53,0.77, "emu+2j SS channel")

ratioPad.cd()

hRatio = hist_SS_data_elpt_rebinned.Clone()
hRatio.Divide(hist_SS_NPel_pt_rebinned)
hRatio.GetYaxis().SetTitle("")
hRatio.GetYaxis().SetLabelSize(0.095)
hRatio.GetXaxis().SetLabelSize(0.095)
hRatio.GetXaxis().SetTitleSize(0.10)
hRatio.GetYaxis().SetNdivisions(804)
hRatio.GetYaxis().SetTickLength(0.05)
hRatio.GetXaxis().SetTickLength(0.07)
hRatio.SetLineColor(hist_SS_data_elpt_rebinned.GetLineColor())
hRatio.SetLineStyle(hist_SS_data_elpt_rebinned.GetLineStyle())
hRatio.SetAxisRange(0.1, 5.0, "Y")
hRatio.Draw("P E SAME") 

myLine = ROOT.TLine() 
myLine.SetLineStyle(1) 
myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

l2 = ROOT.TLatex()
l2.SetTextAlign(12)
l2.SetTextSize(0.10)
l2.SetTextAngle(90) 
l2.SetNDC()
l2.DrawLatex(0.065, 0.50, "Data/MC")

c1.cd(2)

mainPad2  = TPad("mainPad2", "top", 0.0, 0.3, 1.0, 1.0)
mainPad2.SetTopMargin(0.05)
mainPad2.SetBottomMargin(0.010)
mainPad2.SetRightMargin(0.026)
mainPad2.SetLeftMargin(0.15)
mainPad2.Draw()
ratioPad2  = TPad("ratioPad2", "bottom", 0.0, 0.0, 1.0, 0.30)
ratioPad2.SetTopMargin(0.04)
ratioPad2.SetBottomMargin(0.40)
ratioPad2.SetRightMargin(0.026)
ratioPad2.SetLeftMargin(0.15)
ratioPad2.Draw()

#Setup legend
leg2 = ROOT.TLegend(0.65,0.63,0.85,0.75)
leg2.SetTextSize(0.033)
leg2.AddEntry(hist_SS_data_mupt_rebinned, "Data - MC(2PL)", "pl")
leg2.AddEntry(hist_SS_NPmu_pt_rebinned, "MC (NPmu)", "pl")
leg2.SetShadowColor(0)
leg2.SetTextFont(42)
leg2.SetFillColor(0)
leg2.SetLineColor(0)
leg2.SetFillStyle(0)
leg2.SetBorderSize(0)

mainPad2.cd()

hist_SS_data_mupt_rebinned.GetYaxis().SetRangeUser(-20.,1.2*hist_SS_data_mupt_rebinned.GetBinContent(hist_SS_data_mupt_rebinned.GetMaximumBin()))
hist_SS_data_mupt_rebinned.Draw("P E")
hist_SS_NPmu_pt_rebinned.Draw("P E SAME")
leg2.Draw("SAME")
l1.DrawLatex(0.53,0.87,"#bf{ATLAS} Internal")
l1.DrawLatex(0.53,0.82, "#sqrt{s} = 13 TeV, L_{int}=36.1 fb^{-1}")
l1.DrawLatex(0.53,0.77, "emu+2j SS channel")

ratioPad2.cd()

hRatio2 = hist_SS_data_mupt_rebinned.Clone()
hRatio2.Divide(hist_SS_NPmu_pt_rebinned)
hRatio2.GetYaxis().SetTitle("")
hRatio2.GetYaxis().SetLabelSize(0.095)
hRatio2.GetXaxis().SetLabelSize(0.095)
hRatio2.GetXaxis().SetTitleSize(0.10)
hRatio2.GetYaxis().SetNdivisions(804)
hRatio2.GetYaxis().SetTickLength(0.05)
hRatio2.GetXaxis().SetTickLength(0.07)
hRatio2.SetLineColor(hist_SS_data_mupt_rebinned.GetLineColor())
hRatio2.SetLineStyle(hist_SS_data_mupt_rebinned.GetLineStyle())
hRatio2.SetAxisRange(0.1, 5.0, "Y")
hRatio2.Draw("P E SAME") 

myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

l2.DrawLatex(0.065, 0.50, "Data/MC")

#Write plots to files
outname = outdir+"NPLeptons/compare_1PL_DatavsMC_SS_nominal"
c1.SaveAs(outname + ".pdf")
c1.SaveAs(outname + ".png")

#Write plots to root file
outputfile.cd()
c1.Write()
c1.Close()

c2 = TCanvas("c2", "Data vs. MC in emu+2j OS", 1200, 800)
c2.Divide(2,1)
c2.cd(1)

mainPad  = TPad("mainPad", "top", 0.0, 0.3, 1.0, 1.0)
mainPad.SetTopMargin(0.05)
mainPad.SetBottomMargin(0.010)
mainPad.SetRightMargin(0.026)
mainPad.SetLeftMargin(0.15)
mainPad.Draw()
ratioPad  = TPad("ratioPad", "bottom", 0.0, 0.0, 1.0, 0.30)
ratioPad.SetTopMargin(0.04)
ratioPad.SetBottomMargin(0.40)
ratioPad.SetRightMargin(0.026)
ratioPad.SetLeftMargin(0.15)
ratioPad.Draw()

#Setup legend
leg = ROOT.TLegend(0.65,0.63,0.85,0.75)
leg.SetTextSize(0.033)
leg.AddEntry(hist_OS_data_elpt_rebinned, "Data", "pl")
leg.AddEntry(hist_OS_MC2PL_elpt_rebinned, "MC", "pl")
leg.SetShadowColor(0)
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetLineColor(0)
leg.SetFillStyle(0)
leg.SetBorderSize(0)

mainPad.cd()

hist_OS_data_elpt_rebinned.Draw("P E")
hist_OS_MC2PL_elpt_rebinned.Draw("P E SAME")
leg.Draw("SAME")
l1 = ROOT.TLatex()
l1.SetTextSize(0.04)
l1.SetTextColor(kBlack)
l1.SetNDC()
l1.DrawLatex(0.53,0.87,"#bf{ATLAS} Internal")
l1.DrawLatex(0.53,0.82, "#sqrt{s} = 13 TeV, L_{int}=36.1 fb^{-1}")
l1.DrawLatex(0.53,0.77, "emu+2j OS channel")

ratioPad.cd()

hRatio = hist_OS_data_elpt_rebinned.Clone()
hRatio.Divide(hist_OS_MC2PL_elpt_rebinned)
hRatio.GetYaxis().SetTitle("")
hRatio.GetYaxis().SetLabelSize(0.095)
hRatio.GetXaxis().SetLabelSize(0.095)
hRatio.GetXaxis().SetTitleSize(0.10)
hRatio.GetYaxis().SetNdivisions(804)
hRatio.GetYaxis().SetTickLength(0.05)
hRatio.GetXaxis().SetTickLength(0.07)
hRatio.SetLineColor(hist_OS_data_elpt_rebinned.GetLineColor())
hRatio.SetLineStyle(hist_OS_data_elpt_rebinned.GetLineStyle())
hRatio.SetAxisRange(0.85, 1.15, "Y")
hRatio.Draw("P E SAME") 

myLine = ROOT.TLine() 
myLine.SetLineStyle(1) 
myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

l2 = ROOT.TLatex()
l2.SetTextAlign(12)
l2.SetTextSize(0.10)
l2.SetTextAngle(90) 
l2.SetNDC()
l2.DrawLatex(0.065, 0.50, "Data/MC")

c2.cd(2)

mainPad2  = TPad("mainPad2", "top", 0.0, 0.3, 1.0, 1.0)
mainPad2.SetTopMargin(0.05)
mainPad2.SetBottomMargin(0.010)
mainPad2.SetRightMargin(0.026)
mainPad2.SetLeftMargin(0.15)
mainPad2.Draw()
ratioPad2  = TPad("ratioPad2", "bottom", 0.0, 0.0, 1.0, 0.30)
ratioPad2.SetTopMargin(0.04)
ratioPad2.SetBottomMargin(0.40)
ratioPad2.SetRightMargin(0.026)
ratioPad2.SetLeftMargin(0.15)
ratioPad2.Draw()

#Setup legend
leg2 = ROOT.TLegend(0.65,0.63,0.85,0.75)
leg2.SetTextSize(0.033)
leg2.AddEntry(hist_OS_data_mupt_rebinned, "Data", "pl")
leg2.AddEntry(hist_OS_MC2PL_mupt_rebinned, "MC", "pl")
leg2.SetShadowColor(0)
leg2.SetTextFont(42)
leg2.SetFillColor(0)
leg2.SetLineColor(0)
leg2.SetFillStyle(0)
leg2.SetBorderSize(0)

mainPad2.cd()

hist_OS_data_mupt_rebinned.GetYaxis().SetRangeUser(-20.,1.2*hist_OS_data_mupt_rebinned.GetBinContent(hist_OS_data_mupt_rebinned.GetMaximumBin()))
hist_OS_data_mupt_rebinned.Draw("P E")
hist_OS_MC2PL_mupt_rebinned.Draw("P E SAME")
leg2.Draw("SAME")
l1.DrawLatex(0.53,0.87,"#bf{ATLAS} Internal")
l1.DrawLatex(0.53,0.82, "#sqrt{s} = 13 TeV, L_{int}=36.1 fb^{-1}")
l1.DrawLatex(0.53,0.77, "emu+2j OS channel")

ratioPad2.cd()

hRatio2 = hist_OS_data_mupt_rebinned.Clone()
hRatio2.Divide(hist_OS_MC2PL_mupt_rebinned)
hRatio2.GetYaxis().SetTitle("")
hRatio2.GetYaxis().SetLabelSize(0.095)
hRatio2.GetXaxis().SetLabelSize(0.095)
hRatio2.GetXaxis().SetTitleSize(0.10)
hRatio2.GetYaxis().SetNdivisions(804)
hRatio2.GetYaxis().SetTickLength(0.05)
hRatio2.GetXaxis().SetTickLength(0.07)
hRatio2.SetLineColor(hist_OS_data_mupt_rebinned.GetLineColor())
hRatio2.SetLineStyle(hist_OS_data_mupt_rebinned.GetLineStyle())
hRatio2.SetAxisRange(0.85, 1.15, "Y")
hRatio2.Draw("P E SAME") 

myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

l2.DrawLatex(0.065, 0.50, "Data/MC")

#Write plots to files
outname = outdir+"NPLeptons/compare_1PL_DatavsMC_OS_nominal"
c2.SaveAs(outname + ".pdf")
c2.SaveAs(outname + ".png")

print
print "Content of emu+2j SS electron pT plot"
for bin in range(new_ngroup_el):
	print "'Data - MC (2PL)' bin " + str(bin+1) + ": " + str(hist_SS_data_elpt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_SS_data_elpt_rebinned.GetBinError(bin+1))
	print "'MC (NPel)' bin " + str(bin+1) + ": " + str(hist_SS_NPel_pt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_SS_NPel_pt_rebinned.GetBinError(bin+1))
print
print "Content of emu+2j SS muon pT plot"
for bin in range(new_ngroup_mu):
	print "'Data - MC (2PL)' bin " + str(bin+1) + ": " + str(hist_SS_data_mupt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_SS_data_mupt_rebinned.GetBinError(bin+1))
	print "'MC (NPel)' bin " + str(bin+1) + ": " + str(hist_SS_NPmu_pt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_SS_NPmu_pt_rebinned.GetBinError(bin+1))
print
print "Content of emu+2j OS electron pT plot"
for bin in range(new_ngroup_el): #the "Data - MC (1PL)" is strictly speaking approximated to Data, since MC (1PL) is << 1% of Data.
	print "'Data - MC (1PL)' bin " + str(bin+1) + ": " + str(hist_OS_data_elpt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_OS_data_elpt_rebinned.GetBinError(bin+1))
	print "'MC (2PL)' bin " + str(bin+1) + ": " + str(hist_OS_MC2PL_elpt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_OS_MC2PL_elpt_rebinned.GetBinError(bin+1))
print
print "Content of emu+2j OS muon pT plot"
for bin in range(new_ngroup_mu):
	print "'Data - MC (1PL)' bin " + str(bin+1) + ": " + str(hist_OS_data_mupt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_OS_data_mupt_rebinned.GetBinError(bin+1))
	print "'MC (2PL)' bin " + str(bin+1) + ": " + str(hist_OS_MC2PL_mupt_rebinned.GetBinContent(bin+1)) + " +/- " + str(hist_OS_MC2PL_mupt_rebinned.GetBinError(bin+1))
print
#Write histograms and plots to root file
outputfile.cd()

c2.Write()
c2.Close()
ratiohist = hist_SS_data_elpt_rebinned.Clone("hist_SS_data_minus_MC2PL_over_MC1PL")
ratiohist.SetTitle("hist_SS_data_minus_MC2PL_over_MC1PL")
ratiohist.Divide(hist_SS_NPel_pt_rebinned)
ratiohist.Write()
hist_OS_data_elpt_rebinned.Write()
hist_OS_data_mupt_rebinned.Write()
hist_SS_data_elpt_rebinned.Write()
hist_SS_data_mupt_rebinned.Write()
hist_OS_MC2PL_elpt_rebinned.Write()
hist_OS_MC2PL_mupt_rebinned.Write()
hist_SS_MC2PL_elpt_rebinned.Write()
hist_SS_MC2PL_mupt_rebinned.Write()
hist_SS_NPel_pt_rebinned.Write()
hist_SS_NPmu_pt_rebinned.Write()

outputfile.Close()
print "Histograms and plots written to file " + outputfile_name
