import ROOT
import os
import sys
import math
import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import argparse
sys.path.insert(0, '../')
from options_file import *

outdir=options.output_dir
dataName=options.data_name

MCsamples=[]
MCsamples.append(options.Diboson_sample)
MCsamples.append(options.singeTop_sample)
MCsamples.append(options.Wjets_sample)
MCsamples.append(options.ZJets_sample)
MCsamples.append(options.ttb_sample)
for sample in MCsamples:
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_correctFakes_combination.root"

Channels=[	'emu_OS_J2',
		'emu_OS_NPel_J2',
		'emu_OS_NPmu_J2',
		'emu_SS_J2',
		'emu_SS_NPel_J2',
		'emu_SS_NPmu_J2',
]

datafile = ROOT.TFile(outdir+dataName+"/"+dataName+"_data_combination.root", "read")
Flavourlabels=[	'bb','bc','bl','cb','cc','cl','lb','lc','ll' ]

def ReadCutflow(channelname):
	histname = channelname + "/h_" + channelname + "_CutFlow"
	histptname = channelname + "/h_" + channelname
	histetaname = channelname + "/h_" + channelname
	if "NPel" in channelname:
		histptname += "_el1_pt"
		histetaname += "_el1_eta"
	else:
		histptname += "_mu1_pt"
		histetaname += "_mu1_eta"
	maxbin = 6 #this is the default cutflow bin for emu+2j
	if "m_lj" in channelname:
		maxbin = 8 #this is for emu+2j after applying both m_lj cuts
	for bin in range(1,maxbin+1):
		print "Yields in cutflow bin: " + str(bin)
		if channelname == "emu_OS_J2" or channelname == "emu_SS_J2":
			DataHist = datafile.Get(histname)
			DataYields = DataHist.GetBinContent(bin)
			sigmaDataYields = DataHist.GetBinError(bin)
			print "Number of events in data: " + str(DataYields) + ": '+/-: " + str(sigmaDataYields)
		MCYieldsTotal = 0
		sigmaMCYieldsTotal = 0
		for sample in MCsamples:
			MCInputfile = ROOT.TFile(sample.root_file_path, "read")
			MCHist = MCInputfile.Get(histname)
			MCYields = MCHist.GetBinContent(bin)
			sigmaMCYields = MCHist.GetBinError(bin)
			MCYieldsTotal += MCYields
			sigmaMCYieldsTotal += sigmaMCYields * sigmaMCYields
			print "Number of events in MC sample '" + sample.name + "': " + str(MCYields) + ": '+/-: " + str(sigmaMCYields)
		sigmaMCYieldsTotal = math.sqrt(sigmaMCYieldsTotal)
		print "Total number of events in MC: " + str(MCYieldsTotal) + ": '+/-: " + str(sigmaMCYieldsTotal)
		print "--------------------------------------------------"

#Loop over all channels and compute yields for each individually
for channel in Channels:
	print '####################################################'
	print 'Read yields for channel: ' + channel
	ReadCutflow(channel)
	print
print "Done"
