import ROOT
import os
import sys
import math
import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import argparse
sys.path.insert(0, '../')
from options_file import *

outdir=options.output_dir
dataName=options.data_name

MCsamples=[]
#MCsamples.append(options.Diboson_sample)
#MCsamples.append(options.singeTop_sample)
#MCsamples.append(options.Wjets_sample)
#MCsamples.append(options.ZJets_sample)
MCsamples.append(options.ttb_sample)
for sample in MCsamples:
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_nominal_combination.root"

Channels=[	'emu_OS_J2',
		'emu_OS_NPel_J2',
		#'emu_OS_NPmu_J2',
		#'emu_OS_0PL_J2',
		#'emu_OS_0PL_J2_m_lj_cut',
		#'emu_SS_J2',
		#'emu_SS_NPel_J2',
		#'emu_SS_NPmu_J2',
]

if not os.path.exists(outdir+"NPLeptons/"):
    os.makedirs(outdir+"NPLeptons/")
#outputfile_name=outdir+"NPLeptons/NPLeptons_nominal.root"
#outputfile=ROOT.TFile(outputfile_name,"recreate")

datafile = ROOT.TFile(outdir+dataName+"/"+dataName+"_data_combination.root", "read")
Flavourlabels=[	'bb','bc','bl','cb','cc','cl','lb','lc','ll' ]

new_binedges_elpt = [0.,150.,300.,600.]
new_ngroup_elpt = len(new_binedges_elpt)-1
new_binedges_mupt = [0.,60.,80.,600.]
new_ngroup_mupt = len(new_binedges_mupt)-1
new_binedges_eleta = [-3.0,-1.,0.,1.,3.0]
new_ngroup_eleta = len(new_binedges_eleta)-1
new_binedges_mueta = [-3.0,-1.,0.,1.,3.0]
new_ngroup_mueta = len(new_binedges_mueta)-1
hist_OS_NPel_pt = ROOT.TH1D("hist_OS-SS_NPel_pt","hist_OS-SS_NPel_pt",60, 0, 600)
hist_OS_NPel_pt.Sumw2()
hist_OS_NPmu_pt = ROOT.TH1D("hist_OS-SS_NPmu_pt","hist_OS-SS_NPmu_pt",60, 0, 600)
hist_OS_NPmu_pt.Sumw2()
hist_SS_NPel_pt = ROOT.TH1D("hist_SS_NPel_pt","hist_SS_NPel_pt",new_ngroup_elpt,array('d', new_binedges_elpt))
hist_SS_NPel_pt.Sumw2()
hist_SS_NPmu_pt = ROOT.TH1D("hist_SS_NPmu_pt","hist_SS_NPmu_pt",60, 0, 600)
hist_SS_NPmu_pt.Sumw2()
hist_OS_NPel_eta = ROOT.TH1D("hist_OS-SS_NPel_eta","hist_OS-SS_NPel_eta",24, -3.0, 3.0)
hist_OS_NPel_eta.Sumw2()
hist_OS_NPmu_eta = ROOT.TH1D("hist_OS-SS_NPmu_eta","hist_OS-SS_NPmu_eta",24, -3.0, 3.0)
hist_OS_NPmu_eta.Sumw2()
hist_SS_NPel_eta = ROOT.TH1D("hist_SS_NPel_eta","hist_SS_NPel_eta",new_ngroup_eleta,array('d', new_binedges_eleta))
hist_SS_NPel_eta.Sumw2()
hist_SS_NPmu_eta = ROOT.TH1D("hist_SS_NPmu_eta","hist_SS_NPmu_eta",24, -3.0, 3.0)
hist_SS_NPmu_eta.Sumw2()

def ReadCutflow(channelname):
	histname = channelname + "/h_" + channelname + "_CutFlow"
	histptname = channelname + "/h_" + channelname
	histetaname = channelname + "/h_" + channelname
	if "NPel" in channelname:
		histptname += "_el1_pt"
		histetaname += "_el1_eta"
	else:
		histptname += "_mu1_pt"
		histetaname += "_mu1_eta"
	maxbin = 6 #this is the default cutflow bin for emu+2j
	if "m_lj" in channelname:
		maxbin = 8 #this is for emu+2j after applying both m_lj cuts
	for bin in range(1,maxbin+1):
		print "Yields in cutflow bin: " + str(bin)
		if channelname == "emu_OS_J2" or channelname == "emu_SS_J2":
			DataHist = datafile.Get(histname)
			DataYields = DataHist.GetBinContent(bin)
			sigmaDataYields = DataHist.GetBinError(bin)
			print "Number of events in data: " + str(DataYields) + ": '+/-: " + str(sigmaDataYields)
		MCYieldsTotal = 0
		sigmaMCYieldsTotal = 0
		for sample in MCsamples:
			MCInputfile = ROOT.TFile(sample.root_file_path, "read")
			MCHist = MCInputfile.Get(histname)
			MCYields = MCHist.GetBinContent(bin)
			sigmaMCYields = MCHist.GetBinError(bin)
			MCYieldsTotal += MCYields
			sigmaMCYieldsTotal += sigmaMCYields * sigmaMCYields
			print "Number of events in MC sample '" + sample.name + "': " + str(MCYields) + ": '+/-: " + str(sigmaMCYields)
		sigmaMCYieldsTotal = math.sqrt(sigmaMCYieldsTotal)
		print "Total number of events in MC: " + str(MCYieldsTotal) + ": '+/-: " + str(sigmaMCYieldsTotal)
		print "--------------------------------------------------"
	if "NP" in channelname:
		for sample in MCsamples:
			MCInputfile = ROOT.TFile(sample.root_file_path, "read")
			if "OS" in channelname:
				if "el" in channelname:
					for flavour in Flavourlabels:
						hist_OS_NPel_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
						hist_OS_NPel_eta.Add(MCInputfile.Get(histetaname + "_" + flavour))
				else:
					for flavour in Flavourlabels:
						hist_OS_NPmu_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
						hist_OS_NPmu_eta.Add(MCInputfile.Get(histetaname + "_" + flavour))
			else:
				if "el" in channelname:
					for flavour in Flavourlabels:
						hist_SS_NPel_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
						hist_SS_NPel_eta.Add(MCInputfile.Get(histetaname + "_" + flavour))
				else:
					for flavour in Flavourlabels:
						hist_SS_NPmu_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
						hist_SS_NPmu_eta.Add(MCInputfile.Get(histetaname + "_" + flavour))

#Loop over all channels and compute yields for each individually
for channel in Channels:
	print '####################################################'
	print 'Read yields for channel: ' + channel
	ReadCutflow(channel)
	print

hist_OS_NPel_pt.GetXaxis().SetTitle("Non-prompt lepton p_{T} [GeV]")
hist_OS_NPel_pt.GetYaxis().SetTitle("OS/SS ratio [a.u.]")
hist_OS_NPmu_pt.GetXaxis().SetTitle("Non-prompt lepton p_{T} [GeV]")
hist_OS_NPmu_pt.GetYaxis().SetTitle("OS/SS ratio [a.u.]")
hist_OS_NPel_eta.GetXaxis().SetTitle("Non-prompt lepton #eta")
hist_OS_NPel_eta.GetYaxis().SetTitle("OS/SS ratio [a.u.]")
hist_OS_NPmu_eta.GetXaxis().SetTitle("Non-prompt lepton #eta")
hist_OS_NPmu_eta.GetYaxis().SetTitle("OS/SS ratio [a.u.]")
hist_OS_NPel_rebinned_pt = hist_OS_NPel_pt.Rebin(new_ngroup_elpt,"hist_OS-SS_NPel_rebinned_pt",array('d', new_binedges_elpt))
hist_OS_NPmu_rebinned_pt = hist_OS_NPmu_pt.Rebin(new_ngroup_mupt,"hist_OS-SS_NPmu_rebinned_pt",array('d', new_binedges_mupt))
hist_OS_NPel_rebinned_eta = hist_OS_NPel_eta.Rebin(new_ngroup_eleta,"hist_OS-SS_NPel_rebinned_eta",array('d', new_binedges_eleta))
hist_OS_NPmu_rebinned_eta = hist_OS_NPmu_eta.Rebin(new_ngroup_mueta,"hist_OS-SS_NPmu_rebinned_eta",array('d', new_binedges_mueta))
print "After rebinning, before dividing OS by SS histograms."
print "Content of rebinned OS NPel histogram:"
for bin in range(new_ngroup_elpt):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPel_rebinned_pt.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPel_rebinned_pt.GetBinError(bin+1))
print
for bin in range(new_ngroup_eleta):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPel_rebinned_eta.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPel_rebinned_eta.GetBinError(bin+1))
print
print "Content of rebinned OS NPmu histogram:"
for bin in range(new_ngroup_mupt):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPmu_rebinned_pt.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPmu_rebinned_pt.GetBinError(bin+1))
print
for bin in range(new_ngroup_mueta):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPmu_rebinned_eta.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPmu_rebinned_eta.GetBinError(bin+1))
print
hist_SS_NPel_rebinned_pt = hist_SS_NPel_pt.Rebin(new_ngroup_elpt,"hist_SS_NPel_rebinned_pt",array('d', new_binedges_elpt))
hist_SS_NPmu_rebinned_pt = hist_SS_NPmu_pt.Rebin(new_ngroup_mupt,"hist_SS_NPmu_rebinned_pt",array('d', new_binedges_mupt))
hist_SS_NPel_rebinned_eta = hist_SS_NPel_eta.Rebin(new_ngroup_eleta,"hist_SS_NPel_rebinned_eta",array('d', new_binedges_eleta))
hist_SS_NPmu_rebinned_eta = hist_SS_NPmu_eta.Rebin(new_ngroup_mueta,"hist_SS_NPmu_rebinned_eta",array('d', new_binedges_mueta))
print "Content of rebinned SS NPel histogram:"
for bin in range(new_ngroup_elpt):
	print "Bin " + str(bin+1) + ": " + str(hist_SS_NPel_rebinned_pt.GetBinContent(bin+1)) + " +/- " + str(hist_SS_NPel_rebinned_pt.GetBinError(bin+1))
print
for bin in range(new_ngroup_eleta):
	print "Bin " + str(bin+1) + ": " + str(hist_SS_NPel_rebinned_eta.GetBinContent(bin+1)) + " +/- " + str(hist_SS_NPel_rebinned_eta.GetBinError(bin+1))
print
print "Content of rebinned SS NPmu histogram:"
for bin in range(new_ngroup_mupt):
	print "Bin " + str(bin+1) + ": " + str(hist_SS_NPmu_rebinned_pt.GetBinContent(bin+1)) + " +/- " + str(hist_SS_NPmu_rebinned_pt.GetBinError(bin+1))
print
for bin in range(new_ngroup_mueta):
	print "Bin " + str(bin+1) + ": " + str(hist_SS_NPmu_rebinned_eta.GetBinContent(bin+1)) + " +/- " + str(hist_SS_NPmu_rebinned_eta.GetBinError(bin+1))
print
######## NOT saving unrebinned histograms anymore because of different binning between OS and SS
#hist_OS_NPel_pt.Divide(hist_SS_NPel_pt)
#hist_OS_NPmu_pt.Divide(hist_SS_NPmu_pt)
hist_OS_NPel_rebinned_pt.Divide(hist_SS_NPel_rebinned_pt)
hist_OS_NPmu_rebinned_pt.Divide(hist_SS_NPmu_rebinned_pt)
#hist_OS_NPel_eta.Divide(hist_SS_NPel_eta)
#hist_OS_NPmu_eta.Divide(hist_SS_NPmu_eta)
hist_OS_NPel_rebinned_eta.Divide(hist_SS_NPel_rebinned_eta)
hist_OS_NPmu_rebinned_eta.Divide(hist_SS_NPmu_rebinned_eta)
print "Divided OS by SS histograms."
print "Content of rebinned OS/SS NPel histogram:"
for bin in range(new_ngroup_elpt):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPel_rebinned_pt.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPel_rebinned_pt.GetBinError(bin+1))
print
for bin in range(new_ngroup_eleta):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPel_rebinned_eta.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPel_rebinned_eta.GetBinError(bin+1))
print
print "Content of rebinned OS/SS NPmu histogram:"
for bin in range(new_ngroup_mupt):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPmu_rebinned_pt.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPmu_rebinned_pt.GetBinError(bin+1))
print
for bin in range(new_ngroup_mueta):
	print "Bin " + str(bin+1) + ": " + str(hist_OS_NPmu_rebinned_eta.GetBinContent(bin+1)) + " +/- " + str(hist_OS_NPmu_rebinned_eta.GetBinError(bin+1))
print
'''
outputfile.cd()
#hist_OS_NPel_pt.Write()
hist_OS_NPel_rebinned_pt.Write()
#hist_OS_NPmu_pt.Write()
hist_OS_NPmu_rebinned_pt.Write()
#hist_OS_NPel_eta.Write()
hist_OS_NPel_rebinned_eta.Write()
#hist_OS_NPmu_eta.Write()
hist_OS_NPmu_rebinned_eta.Write()
outputfile.Close()
print "Histograms written to file " + outputfile_name
'''
print "Done"
