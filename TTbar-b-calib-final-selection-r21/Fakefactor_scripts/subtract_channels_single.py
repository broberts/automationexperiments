import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse

def subtract_channels(in_files):
    for inFile_name in in_files:
        if os.path.exists(inFile_name):
            inFile=ROOT.TFile(inFile_name,"read")
	else:
            print "input file not Found!: ", inFile_name
	    exit(1)

	save_i = 0
	save_j = 0
	channeldir_name = "emu_SS_J2"
	subtractdir_name = "emu_SS_2PL_J2"
    	outFile_name=inFile_name[:-17]+"_modified_combination.root"
    	outFile=ROOT.TFile(outFile_name,"recreate")
    	keyList=inFile.GetListOfKeys()
    	for i in xrange(0, keyList.GetSize()):
    	    obj=inFile.Get(keyList.At(i).GetName())
    	    className=obj.ClassName()
    	    if className[:2]=="TD":
		td_name=obj.GetName()
    	        if  td_name == channeldir_name:
		    save_i = i
		    if save_j > 0:
			break
		elif td_name == subtractdir_name:
		    save_j = i
		    if save_i > 0:
			break
	outFile.mkdir(channeldir_name)
	chan1 = inFile.Get(keyList.At(save_i).GetName())
	chan2 = inFile.Get(keyList.At(save_j).GetName())
	td_keyList1=chan1.GetListOfKeys()
	td_keyList2=chan2.GetListOfKeys()
	for k in xrange(0, td_keyList1.GetSize()):
		td_obj1 = chan1.Get(td_keyList1.At(k).GetName())
		td_className1 = td_obj1.ClassName()
		if td_className1[:3]=="TH1":
			td_obj2 = chan2.Get(td_keyList2.At(k).GetName())
			td_obj1 = td_obj1 - td_obj2
			outFile.cd(channeldir_name)
			td_obj1.Write()
	outFile.Close()
        print "Finished: ",outFile_name

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Subtract emu_SS_2PL_J2 from emu_SS_J2.')
    parser.add_argument('input_files',nargs='*',
                        help='input files')
    args = parser.parse_args()
    subtract_channels(args.input_files)
