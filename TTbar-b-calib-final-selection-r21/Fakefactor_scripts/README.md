These scripts are used for pursuing fake / non-prompt lepton studies in the emu_OS_J2 channel

To use them, you first have to run finalSelection over your nominal samples with the --run_fakes option
One exception is "combine_histos_for_fit_Fakes.py", which requires --run_fakes as well as --save_fit_input

1. combine_histos_for_fit_Fakes.py produces plots comparing "Data - MC(1 prompt lepton)" vs. "MC(2 prompt leptons)" and "Data - MC(2 prompt leptons)" vs "MC(1 prompt lepton)", each against leading jet pT, subleading jet pT, leading jet MV2c10 bins and subleading jet MV2c10 bins
2. compare_1PL_2PL_Data.py produces the same comparisons as above, but against the corresponding non-prompt lepton pT (either electron "NPel" or muon "NPmu")
	- this script is essential for taking into account non-prompt electrons in emu_OS_J2
	- first, run finalSelection with --run_fakes over all of your samples, then run this script, then run finalSelection with --correctFakes over all your samples
3. compare_NPLcutflows.py prints cutflows for all channels and produces ratio plots for emu_OS_J2_X/emu_SS_J2_X where X = NPel or NPmu. The ratios are plotted against the corresponding non-prompt lepton pT and eta
4. compare_NPLcutflows_correctFakes.py prints cutflows for the same channels, but AFTER --correctFakes was run (see point 2.)
	- for this script you actually need to run with --run_fakes --correctFakes to have the correct binning in the SS channels
5. plot_OS-SS_ratio_wrt_generators.py does essentially the same as compare_NPLcutflows.py, but produces the plots for each individual process/sample
6. plot_lepton_truthinfo.py plots truth variables important for asserting whether leptons are prompt or not
	- these are el_true_type, el_true_origin, mu_true_type, mu_true_origin
	- they are plotted for the main OS and SS channels, as well as the NPel and NPmu channels
7. subtract_channels_single.py allows to subtract the histograms of one channel from another channel in a single input file, e.g. emu_SS_2PL_J2 from emu_SS_J2 to end up with emu_SS_1PL_J2 for plots
8. subtract_channels_data.py allows to subtract the summed MC histograms in one channel from the data histograms in another channel, useful for plotting Data - MC(2PL) vs. MC(1PL)
