import ROOT
import os
import sys
import math
import copy
import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
sys.path.insert(0, '../')
from options_file import *

dataName=options.data_name
outdir=options.output_dir

flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
#flavours={'bb':0}
#flavours={'bc':0,'bl':0}
#flavours={'cb':0,'lb':0}
#flavours={'cc':0,'cl':0,'lc':0,'ll':0}
flavourskeys=flavours.keys()
ttb_sample=options.ttb_sample
samples=options.other_samples[:]
samples.append(options.singeTop_sample)
samples.append(ttb_sample)
taggers=["DL1","DL1r"]#,"DL1","DL1mu","DL1rnn","MV2c10rnn","MV2c10mu","MV2c10Flip_MV2c10"]
workingpoints=["FixedCutBEff"]#,"HybBEff"]
channels=[channel('emu_SS_J2')]
#axis = 2
#bin = 9
#if not os.path.exists(options.plot_dir+"combination_fakes_"+ttb_sample.name+"_PT"+str(axis-1)+"_"+str(bin)+"/"):
#    os.makedirs(options.plot_dir+"combination_fakes_"+ttb_sample.name+"_PT"+str(axis-1)+"_"+str(bin)+"/")
if not os.path.exists(options.plot_dir+"combination_fakes_"+ttb_sample.name+"/"):
    os.makedirs(options.plot_dir+"combination_fakes_"+ttb_sample.name+"/")

#syst_to_run_over= options.inner_systematics_in_nominal_tree+options.tree_systematics
syst_to_run_over=[]
syst_to_run_over.append("nominal")
NBINS_mv2c10 = 5
mv2c10_bins = [ 1, 2, 3, 4, 5, 6 ]
NBINS_pt = 9
pt_bins = [ 20., 30., 40., 60., 85., 110., 140., 175., 250., 600. ]
styles = [ 20,23,22,28,25,31,24,27 ] #https://root.cern.ch/doc/v608/classTAttMarker.html
colours = [ 1,28,6,5,8,42,4,2 ]
#Setup AtlasStyle for plots
import atlas_labels as al
AtlasStyle = al.atlasStyle()
AtlasStyle.SetErrorX(0.5)
AtlasStyle.SetEndErrorSize( 0 )
AtlasStyle.SetPadTopMargin(0.05)
AtlasStyle.SetPadRightMargin(  0.05)
AtlasStyle.SetPadBottomMargin( 0.16)
AtlasStyle.SetPadLeftMargin(   0.16)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()
print "#########################"
for syst in syst_to_run_over:
	#outfolder=options.plot_dir+"combination_fakes_"+ttb_sample.name+"_PT"+str(axis-1)+"_"+str(bin)+"/"
	outfolder=options.plot_dir+"combination_fakes_"+ttb_sample.name+"/"
	ouputfile_name=outfolder+ttb_sample.name+"_combination_for_fit_"+syst+".root"
	outputfile=ROOT.TFile(ouputfile_name,"recreate")
	
	for c in channels:
		for tagger in taggers:
			for workingpoint in workingpoints:
				combined_tagger_name=c.name+"_hist_for_fit_"+tagger+"_"+workingpoint
				flavours_copy=flavours.copy()
				flavours_copy_0PL=flavours.copy()
				flavours_copy_1PL=flavours.copy()
				flavours_copy_2PL=flavours.copy()
				hff_l=ROOT.TH2D("hff_"+"MC_combined_"+combined_tagger_name+"_light","hff_"+"MC_combined_"+combined_tagger_name+"_light", NBINS_mv2c10, array('d', mv2c10_bins), NBINS_pt, array('d', pt_bins))
				hff_b=hff_l.Clone("hff_"+"MC_combined_"+combined_tagger_name+"_b")
				hff_l_1PL=hff_l.Clone("hff_"+"MC_combined_1PL_"+combined_tagger_name+"_light")
				hff_b_1PL=hff_l.Clone("hff_"+"MC_combined_1PL_"+combined_tagger_name+"_b")
				hff_l_2PL=hff_l.Clone("hff_"+"MC_combined_2PL_"+combined_tagger_name+"_light")
				hff_b_2PL=hff_l.Clone("hff_"+"MC_combined_2PL_"+combined_tagger_name+"_b")
				datafile=ROOT.TFile(outdir+dataName+"/"+dataName+"_data_combination.root", "read")
				print outdir+dataName+"/"+dataName+"_data_combination.root"
				print c.name+"/hf4_"+combined_tagger_name
				datahist=datafile.Get(c.name+"/hf4_"+combined_tagger_name+"_data").Clone("hf4_"+"data_"+combined_tagger_name)
				for s in samples:
					print "----------------------"
					print "Running over " +outdir+s.name+"/"+s.name+"_"+syst+"_combination.root"
					infile=ROOT.TFile(outdir+s.name+"/"+s.name+"_"+syst+"_combination.root", "read")
					hff_l.Add(infile.Get(c.name+"/hff_"+combined_tagger_name+"_light"))
					hff_b.Add(infile.Get(c.name+"/hff_"+combined_tagger_name+"_b"))
					hff_l_1PL.Add(infile.Get(c.name.replace("_J","_NPel_J")+"/hff_"+combined_tagger_name.replace("_J","_NPel_J")+"_light"))
					hff_b_1PL.Add(infile.Get(c.name.replace("_J","_NPel_J")+"/hff_"+combined_tagger_name.replace("_J","_NPel_J")+"_b"))
					hff_l_2PL.Add(infile.Get(c.name.replace("_J","_2PL_J")+"/hff_"+combined_tagger_name.replace("_J","_2PL_J")+"_light"))
					hff_b_2PL.Add(infile.Get(c.name.replace("_J","_2PL_J")+"/hff_"+combined_tagger_name.replace("_J","_2PL_J")+"_b"))
					for flav in flavourskeys:
						if flavours_copy[flav]==0:
							flavours_copy[flav]=infile.Get(c.name+"/hf4_"+combined_tagger_name+"_"+flav).Clone("hf4_"+"MC_combined_"+combined_tagger_name+"_"+flav)
							flavours_copy_1PL[flav]=infile.Get(c.name.replace("_J","_NPel_J")+"/hf4_"+combined_tagger_name.replace("_J","_NPel_J")+"_"+flav).Clone("hf4_"+"MC_combined_"+combined_tagger_name.replace("_J","_NPel_J")+"_"+flav)
							flavours_copy_2PL[flav]=infile.Get(c.name.replace("_J","_2PL_J")+"/hf4_"+combined_tagger_name.replace("_J","_2PL_J")+"_"+flav).Clone("hf4_"+"MC_combined_"+combined_tagger_name.replace("_J","_2PL_J")+"_"+flav)
				        	else:
							hist=infile.Get(c.name+"/hf4_"+combined_tagger_name+"_"+flav).Clone("h_"+s.name+"_"+combined_tagger_name+"_"+flav)
							hist_1PL=infile.Get(c.name.replace("_J","_NPel_J")+"/hf4_"+combined_tagger_name.replace("_J","_NPel_J")+"_"+flav).Clone("h_"+s.name+"_"+combined_tagger_name.replace("_J","_NPel_J")+"_"+flav)
							hist_2PL=infile.Get(c.name.replace("_J","_2PL_J")+"/hf4_"+combined_tagger_name.replace("_J","_2PL_J")+"_"+flav).Clone("h_"+s.name+"_"+combined_tagger_name.replace("_J","_2PL_J")+"_"+flav)
							flavours_copy[flav].Add(hist)
							flavours_copy_1PL[flav].Add(hist_1PL)
							flavours_copy_2PL[flav].Add(hist_2PL)
				datahist_1PL = datahist.Clone("hf4_"+"data_1PL_"+combined_tagger_name)
				datahist_2PL = datahist.Clone("hf4_"+"data_2PL_"+combined_tagger_name)
				outputfile.cd()
				datahist.Write()
				hff_l.Write()
				hff_b.Write()
				hff_l_1PL.Write()
				hff_b_1PL.Write()
				hff_l_2PL.Write()
				hff_b_2PL.Write()
				flavours_copy['bb'].Write()
				flavours_copy['bl'].Add(flavours_copy['bc'])
				flavours_copy['bl'].Write()
				flavours_copy['lb'].Add(flavours_copy['cb'])
				flavours_copy['lb'].Write()
				flavours_copy['ll'].Add(flavours_copy['cc'])
				flavours_copy['ll'].Add(flavours_copy['cl'])
				flavours_copy['ll'].Add(flavours_copy['lc'])
				flavours_copy['ll'].Write()
				datahist_2PL.Add(flavours_copy_1PL['bb'],-1)
				flavours_copy_1PL_comb = copy.deepcopy(flavours_copy_1PL['bb'])
				flavours_copy_1PL['bb'].Write()
				flavours_copy_1PL['bl'].Add(flavours_copy_1PL['bc'])
				datahist_2PL.Add(flavours_copy_1PL['bl'],-1)
				flavours_copy_1PL_comb.Add(flavours_copy_1PL['bl'])
				flavours_copy_1PL['bl'].Write()
				flavours_copy_1PL['lb'].Add(flavours_copy_1PL['cb'])
				datahist_2PL.Add(flavours_copy_1PL['lb'],-1)
				flavours_copy_1PL_comb.Add(flavours_copy_1PL['lb'])
				flavours_copy_1PL['lb'].Write()
				flavours_copy_1PL['ll'].Add(flavours_copy_1PL['cc'])
				flavours_copy_1PL['ll'].Add(flavours_copy_1PL['cl'])
				flavours_copy_1PL['ll'].Add(flavours_copy_1PL['lc'])
				flavours_copy_1PL_comb.Add(flavours_copy_1PL['ll'])
				datahist_2PL.Add(flavours_copy_1PL['ll'],-1)
				flavours_copy_1PL['ll'].Write()
				datahist_1PL.Add(flavours_copy_2PL['bb'],-1)
				flavours_copy_2PL_comb = copy.deepcopy(flavours_copy_2PL['bb'])
				flavours_copy_2PL['bb'].Write()
				flavours_copy_2PL['bl'].Add(flavours_copy_2PL['bc'])
				datahist_1PL.Add(flavours_copy_2PL['bl'],-1)
				flavours_copy_2PL_comb.Add(flavours_copy_2PL['bl'])
				flavours_copy_2PL['bl'].Write()
				flavours_copy_2PL['lb'].Add(flavours_copy_2PL['cb'])
				datahist_1PL.Add(flavours_copy_2PL['lb'],-1)
				flavours_copy_2PL_comb.Add(flavours_copy_2PL['lb'])
				flavours_copy_2PL['lb'].Write()
				flavours_copy_2PL['ll'].Add(flavours_copy_2PL['cc'])
				flavours_copy_2PL['ll'].Add(flavours_copy_2PL['cl'])
				flavours_copy_2PL['ll'].Add(flavours_copy_2PL['lc'])
				flavours_copy_2PL_comb.Add(flavours_copy_2PL['ll'])
				datahist_1PL.Add(flavours_copy_2PL['ll'],-1)
				flavours_copy_2PL['ll'].Write()
				datahist_1PL.Write()
				datahist_2PL.Write()
				'''
				for mv2bin in [1,5]:
					if mv2bin == 1:
						print "First bin (100%-85%)"
					else:
						print "Last bin (60%-0%)"
					for ptbin1 in range(1,10):
						for ptbin2 in range(1,ptbin1+1):
							flavours_copy_1PL_comb.GetAxis(2).SetRange(ptbin1,ptbin1)
							flavours_copy_1PL_comb.GetAxis(3).SetRange(ptbin2,ptbin2)
							flavours_copy_2PL_comb.GetAxis(2).SetRange(ptbin1,ptbin1)
							flavours_copy_2PL_comb.GetAxis(3).SetRange(ptbin2,ptbin2)
							datahist_1PL.GetAxis(2).SetRange(ptbin1,ptbin1)
							datahist_1PL.GetAxis(3).SetRange(ptbin2,ptbin2)
							datahist_2PL.GetAxis(2).SetRange(ptbin1,ptbin1)
							datahist_2PL.GetAxis(3).SetRange(ptbin2,ptbin2)
							data_1PLbin = datahist_1PL.Projection(0).GetBinContent(mv2bin)
							data_2PLbin = datahist_2PL.Projection(0).GetBinContent(mv2bin)
							MC_1PLbin = flavours_copy_1PL_comb.Projection(0).GetBinContent(mv2bin)
							MC_2PLbin = flavours_copy_2PL_comb.Projection(0).GetBinContent(mv2bin)
							if data_1PLbin > MC_1PLbin:
								print "Data_1PL("+str(ptbin1)+","+str(ptbin2)+"): " + str(data_1PLbin)
								print "MC_1PL("+str(ptbin1)+","+str(ptbin2)+"): " + str(MC_1PLbin)
							if data_2PLbin > MC_2PLbin:
								print "Data_2PL("+str(ptbin1)+","+str(ptbin2)+"): " + str(data_2PLbin)
								print "MC_2PL("+str(ptbin1)+","+str(ptbin2)+"): " + str(MC_2PLbin)
				'''
				for Proj in range(4):
					Projection_MC1PL = flavours_copy_1PL_comb.Projection(Proj)
					Projection_MC2PL = flavours_copy_2PL_comb.Projection(Proj)
					Projection_data1PL = datahist_1PL.Projection(Proj)
					Projection_data2PL = datahist_2PL.Projection(Proj)
					XTitle = "Leading jet MV2c10 PC bins"
					if Proj == 1:
						XTitle = "Subleading jet MV2c10 PC bins"
					elif Proj == 2:
						XTitle = "Leading jet p_{T} [GeV]"
					elif Proj == 3:
						XTitle = "Subleading jet p_{T} [GeV]"

					###Plot histograms below
					Projection_data1PL.SetLineWidth(2)
					Projection_data1PL.SetMarkerSize(0.5)
					Projection_data1PL.SetMarkerStyle(styles[0])
					Projection_data1PL.SetFillColor(colours[0])
					Projection_data1PL.SetMarkerColor(colours[0])
					Projection_data1PL.SetLineColor(colours[0])
					Projection_data1PL.GetXaxis().SetTitle(XTitle)
					Projection_data1PL.GetYaxis().SetTitle("Number of events")
					Projection_data1PL.GetXaxis().SetTitleSize(1.5)
					Projection_data1PL.GetXaxis().SetLabelSize(0.08)
					Projection_data1PL.GetYaxis().SetTitleSize(0.05)
					Projection_data1PL.GetYaxis().SetTitleOffset(1.1)
					Projection_data2PL.SetLineWidth(2)
					Projection_data2PL.SetMarkerSize(0.5)
					Projection_data2PL.SetMarkerStyle(styles[0])
					Projection_data2PL.SetFillColor(colours[0])
					Projection_data2PL.SetMarkerColor(colours[0])
					Projection_data2PL.SetLineColor(colours[0])
					Projection_data2PL.GetXaxis().SetTitle(XTitle)
					Projection_data2PL.GetYaxis().SetTitle("Number of events")
					Projection_data2PL.GetXaxis().SetTitleSize(1.5)
					Projection_data2PL.GetXaxis().SetLabelSize(0.08)
					Projection_data2PL.GetYaxis().SetTitleSize(0.05)
					Projection_data2PL.GetYaxis().SetTitleOffset(1.1)

					Projection_MC2PL.SetLineWidth(1)
					Projection_MC2PL.SetMarkerSize(0.5)
					Projection_MC2PL.SetMarkerStyle(styles[6])
					Projection_MC2PL.SetFillColor(colours[6])
					Projection_MC2PL.SetMarkerColor(colours[6])
					Projection_MC2PL.SetLineColor(colours[6])
					Projection_MC1PL.SetLineWidth(1)
					Projection_MC1PL.SetMarkerSize(0.5)
					Projection_MC1PL.SetMarkerStyle(styles[7])
					Projection_MC1PL.SetFillColor(colours[7])
					Projection_MC1PL.SetMarkerColor(colours[7])
					Projection_MC1PL.SetLineColor(colours[7])

					#Setup TCanvas and TPads for 1PL
					c2 = TCanvas("c2", "Data SS 1PL estimation", 450, 450)

					mainPad  = TPad("mainPad", "top", 0.0, 0.3, 1.0, 1.0)
					mainPad.SetTopMargin(0.05)
					mainPad.SetBottomMargin(0.015)
					mainPad.SetRightMargin(0.026)
					mainPad.SetLeftMargin(0.15)
					mainPad.Draw()
					ratioPad  = TPad("ratioPad", "bottom", 0.0, 0.0, 1.0, 0.30)
					ratioPad.SetTopMargin(0.04)
					ratioPad.SetBottomMargin(0.40)
					ratioPad.SetRightMargin(0.026)
					ratioPad.SetLeftMargin(0.15)
					ratioPad.Draw()

					#Setup legend
					leg = ROOT.TLegend(0.60,0.78,0.80,0.9)
					leg.SetTextSize(0.033)
					leg.AddEntry(Projection_data1PL, "Data - MC(2PL)", "pl")
					leg.AddEntry(Projection_MC1PL, "MC(1PL)", "pl")
					leg.SetShadowColor(0)
					leg.SetTextFont(42)
					leg.SetFillColor(0)
					leg.SetLineColor(0)
					leg.SetFillStyle(0)
					leg.SetBorderSize(0)

					mainPad.cd()

					Projection_data1PL.Draw("P E")
					Projection_MC1PL.Draw("P E SAME")
					leg.Draw("SAME")
					l1 = ROOT.TLatex()
					l1.SetTextSize(0.04)
					l1.SetTextColor(kBlack)
					l1.SetNDC()
					l1.DrawLatex(0.40,0.85,"#bf{ATLAS} Internal")
					l1.DrawLatex(0.40,0.80, "#sqrt{s} = 13 TeV")
					l1.DrawLatex(0.40,0.75, c.name)

					ratioPad.cd()

					hRatio = Projection_data1PL.Clone()
					hRatio.Divide(Projection_MC1PL)
					hRatio.GetYaxis().SetTitle("")
					hRatio.GetYaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetTitleSize(0.10)
					hRatio.GetYaxis().SetNdivisions(804)
					hRatio.GetYaxis().SetTickLength(0.05)
					hRatio.GetXaxis().SetTickLength(0.07)
					hRatio.SetLineColor(Projection_data1PL.GetLineColor())
					hRatio.SetLineStyle(Projection_data1PL.GetLineStyle())
					hRatio.SetAxisRange(0.5, 2.5, "Y")
					hRatio.Draw("P E SAME") 

					myLine = ROOT.TLine() 
					myLine.SetLineStyle(1) 
					myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

					l2 = ROOT.TLatex()
					l2.SetTextAlign(12)
					l2.SetTextSize(0.10)
					l2.SetTextAngle(90) 
					l2.SetNDC()
					l2.DrawLatex(0.065, 0.50, "Data/MC")

					outname = outfolder+"Projection_"+str(Proj)+"_1PL"
					c2.SaveAs(outname + ".root")
					c2.SaveAs(outname + ".pdf")
					c2.SaveAs(outname + ".png")
					c2.Close()

					### 1PL normalised
					c3 = TCanvas("c3", "Data SS 1PL estimation (normalised)", 450, 450)

					mainPad  = TPad("mainPad", "top", 0.0, 0.3, 1.0, 1.0)
					mainPad.SetTopMargin(0.05)
					mainPad.SetBottomMargin(0.015)
					mainPad.SetRightMargin(0.026)
					mainPad.SetLeftMargin(0.15)
					mainPad.Draw()
					ratioPad  = TPad("ratioPad", "bottom", 0.0, 0.0, 1.0, 0.30)
					ratioPad.SetTopMargin(0.04)
					ratioPad.SetBottomMargin(0.40)
					ratioPad.SetRightMargin(0.026)
					ratioPad.SetLeftMargin(0.15)
					ratioPad.Draw()

					mainPad.cd()

					Projection_data1PL.Scale(1./Projection_data1PL.GetSumOfWeights())
					Projection_MC1PL.Scale(1./Projection_MC1PL.GetSumOfWeights())
					Projection_data1PL.GetYaxis().SetRangeUser(0.,1.)
					Projection_data1PL.GetYaxis().SetTitle("Arbitrary units")
					Projection_data1PL.Draw("P E")
					Projection_MC1PL.Draw("P E SAME")
					leg.Draw("SAME")
					l1.DrawLatex(0.40,0.85,"#bf{ATLAS} Internal")
					l1.DrawLatex(0.40,0.80, "#sqrt{s} = 13 TeV")
					l1.DrawLatex(0.40,0.75, c.name)

					ratioPad.cd()

					hRatio = Projection_data1PL.Clone()
					hRatio.Divide(Projection_MC1PL)
					hRatio.GetYaxis().SetTitle("")
					hRatio.GetYaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetTitleSize(0.10)
					hRatio.GetYaxis().SetNdivisions(804)
					hRatio.GetYaxis().SetTickLength(0.05)
					hRatio.GetXaxis().SetTickLength(0.07)
					hRatio.SetLineColor(Projection_data1PL.GetLineColor())
					hRatio.SetLineStyle(Projection_data1PL.GetLineStyle())
					hRatio.SetAxisRange(0., 2., "Y")
					hRatio.Draw("P E SAME") 

					myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

					l2.DrawLatex(0.065, 0.50, "Data/MC")

					outname = outfolder+"Projection_"+str(Proj)+"_1PL_normalised"
					c3.SaveAs(outname + ".root")
					c3.SaveAs(outname + ".pdf")
					c3.SaveAs(outname + ".png")
					c3.Close()

					### 2PL plot
					c4 = TCanvas("c4", "Data SS 2PL estimation", 450, 450)

					mainPad  = TPad("mainPad", "top", 0.0, 0.3, 1.0, 1.0)
					mainPad.SetTopMargin(0.05)
					mainPad.SetBottomMargin(0.015)
					mainPad.SetRightMargin(0.026)
					mainPad.SetLeftMargin(0.15)
					mainPad.Draw()
					ratioPad  = TPad("ratioPad", "bottom", 0.0, 0.0, 1.0, 0.30)
					ratioPad.SetTopMargin(0.04)
					ratioPad.SetBottomMargin(0.40)
					ratioPad.SetRightMargin(0.026)
					ratioPad.SetLeftMargin(0.15)
					ratioPad.Draw()

					leg = ROOT.TLegend(0.60,0.78,0.80,0.90)
					leg.SetTextSize(0.033)
					leg.AddEntry(Projection_data2PL, "Data - MC(1PL)", "pl")
					leg.AddEntry(Projection_MC2PL, "MC(2PL)", "pl")
					leg.SetShadowColor(0)
					leg.SetTextFont(42)
					leg.SetFillColor(0)
					leg.SetLineColor(0)
					leg.SetFillStyle(0)
					leg.SetBorderSize(0)

					mainPad.cd()

					Projection_data2PL.Draw("P E")
					Projection_MC2PL.Draw("P E SAME")
					leg.Draw("SAME")
					l1.DrawLatex(0.40,0.85,"#bf{ATLAS} Internal")
					l1.DrawLatex(0.40,0.80, "#sqrt{s} = 13 TeV")
					l1.DrawLatex(0.40,0.75, c.name)

					ratioPad.cd()

					hRatio = Projection_data2PL.Clone()
					hRatio.Divide(Projection_MC2PL)
					hRatio.GetYaxis().SetTitle("")
					hRatio.GetYaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetTitleSize(0.10)
					hRatio.GetYaxis().SetNdivisions(804)
					hRatio.GetYaxis().SetTickLength(0.05)
					hRatio.GetXaxis().SetTickLength(0.07)
					hRatio.SetLineColor(Projection_data2PL.GetLineColor())
					hRatio.SetLineStyle(Projection_data2PL.GetLineStyle())
					hRatio.SetAxisRange(0.5, 2.5, "Y")
					hRatio.Draw("P E SAME") 

					myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

					l2.DrawLatex(0.065, 0.50, "Data/MC")

					outname = outfolder+"Projection_"+str(Proj)+"_2PL"
					c4.SaveAs(outname + ".root")
					c4.SaveAs(outname + ".pdf")
					c4.SaveAs(outname + ".png")
					c4.Close()

					### 2PL normalised
					c5 = TCanvas("c5", "Data SS 1PL estimation (normalised)", 450, 450)

					mainPad  = TPad("mainPad", "top", 0.0, 0.3, 1.0, 1.0)
					mainPad.SetTopMargin(0.05)
					mainPad.SetBottomMargin(0.015)
					mainPad.SetRightMargin(0.026)
					mainPad.SetLeftMargin(0.15)
					mainPad.Draw()
					ratioPad  = TPad("ratioPad", "bottom", 0.0, 0.0, 1.0, 0.30)
					ratioPad.SetTopMargin(0.04)
					ratioPad.SetBottomMargin(0.40)
					ratioPad.SetRightMargin(0.026)
					ratioPad.SetLeftMargin(0.15)
					ratioPad.Draw()

					mainPad.cd()

					Projection_data2PL.Scale(1./Projection_data2PL.GetSumOfWeights())
					Projection_MC2PL.Scale(1./Projection_MC2PL.GetSumOfWeights())
					Projection_data2PL.GetYaxis().SetRangeUser(0.,1.)
					Projection_data2PL.GetYaxis().SetTitle("Arbitrary units")
					Projection_data2PL.Draw("P E")
					Projection_MC2PL.Draw("P E SAME")
					leg.Draw("SAME")
					l1.DrawLatex(0.40,0.85,"#bf{ATLAS} Internal")
					l1.DrawLatex(0.40,0.80, "#sqrt{s} = 13 TeV")
					l1.DrawLatex(0.40,0.75, c.name)

					ratioPad.cd()

					hRatio = Projection_data2PL.Clone()
					hRatio.Divide(Projection_MC2PL)
					hRatio.GetYaxis().SetTitle("")
					hRatio.GetYaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetLabelSize(0.095)
					hRatio.GetXaxis().SetTitleSize(0.10)
					hRatio.GetYaxis().SetNdivisions(804)
					hRatio.GetYaxis().SetTickLength(0.05)
					hRatio.GetXaxis().SetTickLength(0.07)
					hRatio.SetLineColor(Projection_data2PL.GetLineColor())
					hRatio.SetLineStyle(Projection_data2PL.GetLineStyle())
					hRatio.SetAxisRange(0., 2., "Y")
					hRatio.Draw("P E SAME") 

					myLine.DrawLine(hRatio.GetXaxis().GetXmin(), 1., hRatio.GetXaxis().GetXmax(), 1.)

					l2.DrawLatex(0.065, 0.50, "Data/MC")

					outname = outfolder+"Projection_"+str(Proj)+"_2PL_normalised"
					c5.SaveAs(outname + ".root")
					c5.SaveAs(outname + ".pdf")
					c5.SaveAs(outname + ".png")
					c5.Close()

	#print "#########################"
	print "Histograms written to Outputfile " + ouputfile_name
	outputfile.Close()
print "Done."
