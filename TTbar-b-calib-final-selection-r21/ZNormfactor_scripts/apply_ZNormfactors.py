import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse

def applyScalefactors(in_files, norm_file,nom_up_down):
    if os.path.exists(norm_file):
        normFactorFile=ROOT.TFile(norm_file,"read")
        h_normfactors=normFactorFile.Get("h_normfactors")
        outFile_name_add=""#"_nominal"
        ZJ_emu_OS_J2=h_normfactors.GetBinContent(1)
        ZJ_emu_OS_J2_m_lj_cut=h_normfactors.GetBinContent(2)
        ttbar_emu_OS_J2=h_normfactors.GetBinContent(3)
        ttbar_emu_OS_J2_m_lj_cut=h_normfactors.GetBinContent(6)
        if nom_up_down=="UP":
            outFile_name_add="_UP"
            ZJ_emu_OS_J2 += h_normfactors.GetBinError(1)
            ZJ_emu_OS_J2_m_lj_cut += h_normfactors.GetBinError(2)
    	    ttbar_emu_OS_J2 += h_normfactors.GetBinError(3)
    	    ttbar_emu_OS_J2_m_lj_cut += h_normfactors.GetBinError(6)
        elif nom_up_down=="DOWN":
            outFile_name_add="_DOWN"
            ZJ_emu_OS_J2 -= h_normfactors.GetBinError(1)
            ZJ_emu_OS_J2_m_lj_cut -= h_normfactors.GetBinError(2)
    	    ttbar_emu_OS_J2 -= h_normfactors.GetBinError(3)
    	    ttbar_emu_OS_J2_m_lj_cut -= h_normfactors.GetBinError(6)
        elif nom_up_down=="CR":
            ttbar_CRUP_emu_OS_J2=h_normfactors.GetBinContent(4)
            ttbar_CRDOWN_emu_OS_J2=h_normfactors.GetBinContent(5)
            ttbar_CRUP_emu_OS_J2_m_lj_cut=h_normfactors.GetBinContent(7)
            ttbar_CRDOWN_emu_OS_J2_m_lj_cut=h_normfactors.GetBinContent(8)

    else:
        print "normFactorFile not Found!: ", norm_file
        exit(1)

    for inFile_name in in_files:
        if os.path.exists(inFile_name):
            inFile=ROOT.TFile(inFile_name,"read")
	else:
            print "input file not Found!: ", inFile_name
	    exit(1)
    	if "Zjets" in inFile_name:
    		outFile_name=inFile_name[:-17]+"_ZJsf"+outFile_name_add+"_combination.root"
    	elif "ttbar" in inFile_name:
    		outFile_name=inFile_name[:-17]+"_TTBARsf"+outFile_name_add+"_combination.root"
    		if nom_up_down=="CR":
    			outFile_CRUP_name=inFile_name[:-17]+"_TTBARsf_CRUP_combination.root"
    			outFile_CRDOWN_name=inFile_name[:-17]+"_TTBARsf_CRDOWN_combination.root"
    	else:
    		print "Input file "+inFile_name+" contains neither 'ttbar' nor 'Zjets' -> no need to renormalise!"
    		continue
    	outFile=ROOT.TFile(outFile_name,"recreate")
    	if "ttbar" in inFile_name and nom_up_down=="CR":
    		outFile_CRUP=ROOT.TFile(outFile_CRUP_name,"recreate")
    		outFile_CRDOWN=ROOT.TFile(outFile_CRDOWN_name,"recreate")
    	keyList=inFile.GetListOfKeys()
    	for i in xrange(0, keyList.GetSize()):
    	    obj=inFile.Get(keyList.At(i).GetName())
    	    className=obj.ClassName()
    	    if className[:2]=="TD":
		td_name=obj.GetName()
    	        if td_name == "emu_OS_J2_m_lj_cut":
		    td_keyList=obj.GetListOfKeys()
		    for j in xrange(0, td_keyList.GetSize()):
		        td_obj=obj.Get(td_keyList.At(j).GetName())
			td_className=td_obj.ClassName()
			if td_className[:2]=="TH":
        		        if "Zjets" in inFile_name:
        			    #print "Applying emu_OS_J2_m_lj_cut-SF ", ZJ_emu_OS_J2_m_lj_cut ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        			    outFile.cd()
        			    td_obj.Scale(ZJ_emu_OS_J2_m_lj_cut)
        		            td_obj.Write()
				elif "ttbar" in inFile_name:
        			    #print "Applying emu_OS_J2-SF ", ttbar_emu_OS_J2_m_lj_cut ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        			    outFile.cd()
        			    td_obj.Scale(ttbar_emu_OS_J2_m_lj_cut)
        		            td_obj.Write()
        			    if nom_up_down=="CR":
        				#print "Applying emu_OS_J2-SF (CRUP) ", ttbar_CRUP_emu_OS_J2_m_lj_cut ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        				outFile_CRUP.cd()
        				td_obj.Scale(ttbar_CRUP_emu_OS_J2_m_lj_cut)
        				td_obj.Write()
        				#print "Applying emu_OS_J2-SF (CRDOWN) ", ttbar_CRDOWN_emu_OS_J2_m_lj_cut ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        				outFile_CRDOWN.cd()
        				td_obj.Scale(ttbar_CRDOWN_emu_OS_J2_m_lj_cut)
        				td_obj.Write()
		elif td_name == "emu_OS_J2":
		    td_keyList=obj.GetListOfKeys()
		    for j in xrange(0, td_keyList.GetSize()):
		        td_obj=obj.Get(td_keyList.At(j).GetName())
			td_className=td_obj.ClassName()
			if td_className[:2]=="TH":
				if "Zjets" in inFile_name:
        			    #print "Applying emu_OS_J2-SF ", ZJ_emu_OS_J2 ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        			    outFile.cd()
        			    td_obj.Scale(ZJ_emu_OS_J2)
        		            td_obj.Write()
 				elif "ttbar" in inFile_name:
        			    #print "Applying emu_OS_J2-SF ", ttbar_emu_OS_J2 ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        			    outFile.cd()
        			    td_obj.Scale(ttbar_emu_OS_J2)
        		            td_obj.Write()
        			    if nom_up_down=="CR":
        				#print "Applying emu_OS_J2-SF (CRUP) ", ttbar_CRUP_emu_OS_J2 ," to ClassName: ",td_className," Name: ",td_obj.GetName();
					outFile_CRUP.cd()
					td_obj.Scale(ttbar_CRUP_emu_OS_J2)
        				td_obj.Write()
        				#print "Applying emu_OS_J2-SF (CRDOWN) ", ttbar_CRDOWN_emu_OS_J2 ," to ClassName: ",td_className," Name: ",td_obj.GetName();
        				outFile_CRDOWN.cd()
        				td_obj.Scale(ttbar_CRDOWN_emu_OS_J2)
        				td_obj.Write()

        print "Finished: ",outFile_name
    	if "ttbar" in inFile_name and nom_up_down=="CR":
	       print "Finished: ",outFile_CRUP_name
	       print "Finished: ",outFile_CRDOWN_name

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Draw all plots in file.')
    parser.add_argument('input_files',nargs='*',
                        help='input files')
    parser.add_argument('-n',"--normFile",default="Normfactors_nominal.root",
                        help='name of systematic tree to run over. default=Normfactors_nominal.root.')
    parser.add_argument('-s',"--sfSyst",default="nominal",
                        help='provide UP or DOWN if you want to estimate sf uncertanty')

    args = parser.parse_args()
    applyScalefactors(args.input_files, args.normFile,args.sfSyst)
