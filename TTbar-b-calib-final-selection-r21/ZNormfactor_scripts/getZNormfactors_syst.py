import ROOT
import os
import subprocess
import math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse
from apply_scalefactors import applyScalefactors
from options_file import *

#nominal
# p=subprocess.Popen("python getNormfactors.py"+" -a", shell=True)
# p.wait()
#z+jet samples

for syst_sample in options.syst_samples_ZJets:
    p=subprocess.Popen("python getZNormfactors.py"+" -z "+syst_sample.name+" -a", shell=True)
    p.wait()
for syst in options.inner_systematics_in_nominal_tree+options.tree_systematics:
    p=subprocess.Popen("python getZNormfactors.py"+" -s "+syst+" -a", shell=True)
    p.wait()

subprocess.Popen("python getZNormfactors.py -s correctFakes -a", shell=True)
