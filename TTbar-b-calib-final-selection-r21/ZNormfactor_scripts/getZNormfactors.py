import ROOT
import os
import sys
import math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse
from apply_ZNormfactors import applyScalefactors
sys.path.insert(0, '../')
from options_file import *

parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts. specify ONE!! of the following options:')
parser.add_argument('-t',"--ttbar_syst_sample",default="",
                    help='name of ttbar_syst sample to run over.')
parser.add_argument('-z',"--zJets_syst_sample",default="",
                    help='name of zJets_syst sample to run over.')
parser.add_argument('-s',"--syst_name",default="nominal",
                    help='name of systematic syst to run over. default=nominal.')
parser.add_argument('-a',"--apply",action='store_true',
                    help='aplly zJet directly. default=false.')
args = parser.parse_args()

outdir=options.output_dir
dataName=options.data_name

if args.ttbar_syst_sample == "":
    ttb_sample = options.ttb_sample
    syst_name = args.syst_name
else:
    ttb_sample = sample(ttbar_syst_sample)
    syst_name = args.ttbar_syst_sample
if args.zJets_syst_sample == "":
    zJets_sample = options.ZJets_sample
else:
    zJets_sample = sample(args.zJets_syst_sample)
    syst_name = args.zJets_syst_sample
    zJets_sample.root_file_path=outdir + zJets_sample.name+"/" + zJets_sample.name + "_" + "nominal" + "_combination.root"


ttb_sample.systematic = args.syst_name

MCsamples=[]
MCsamples.append(options.Diboson_sample)
MCsamples.append(options.singeTop_sample)
MCsamples.append(options.Wjets_sample)
MCsamples.append(zJets_sample)
MCsamples.append(ttb_sample)
for sample in MCsamples:
    sample.systematic = args.syst_name
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_" + args.syst_name + "_combination.root"

Channels=[	'emu_OS_J2'
]

if not os.path.exists(outdir+"Normfactors/"):
    os.makedirs(outdir+"Normfactors/")
outputfile_name=outdir+"Normfactors/Normfactors_" + syst_name + ".root"
outputfile=ROOT.TFile(outputfile_name,"recreate")
datafile = ROOT.TFile(outdir+dataName+"/"+dataName+"_data_combination.root", "read")

h_normfactors = ROOT.TH1D("h_normfactors","h_normfactors_"+syst_name,8,1,9)
h_normfactors.GetXaxis().SetBinLabel(1, "Z+jets emu+2j");
h_normfactors.GetXaxis().SetBinLabel(2, "Z+jets_mlj emu+2j");
h_normfactors.GetXaxis().SetBinLabel(3, "SR emu+2j");
h_normfactors.GetXaxis().SetBinLabel(4, "SR emu+2j CR up");
h_normfactors.GetXaxis().SetBinLabel(5, "SR emu+2j CR down");
h_normfactors.GetXaxis().SetBinLabel(6, "SR_mlj emu+2j");
h_normfactors.GetXaxis().SetBinLabel(7, "SR_mlj emu+2j CR up");
h_normfactors.GetXaxis().SetBinLabel(8, "SR_mlj emu+2j CR down");

def ComputeNormfactorEMU(channelname):

	### Fakes determination: ###
	histname_Fakes = channelname.replace("OS","SS") + "/h_" + channelname.replace("OS","SS") + "_CutFlow"
	cutFlowData_Fakes = datafile.Get(histname_Fakes)
	nData_Fakes = cutFlowData_Fakes.GetBinContent(6)
	sigmaData_Fakes = cutFlowData_Fakes.GetBinError(6)
	print "Fakes events in Data: " + str(nData_Fakes) + ": '+/-: " + str(sigmaData_Fakes)
	nMC_Fakes_Total = 0
	sigmaMC_Fakes_Total = 0
	for sample in MCsamples:
		infile = ROOT.TFile(sample.root_file_path, "read")
		cutFlowMC_Fakes = infile.Get(histname_Fakes)
		nMC_Fakes = cutFlowMC_Fakes.GetBinContent(6)
		nMC_Fakes_Total += nMC_Fakes
		sigmaMC_Fakes = cutFlowMC_Fakes.GetBinError(6)
		sigmaMC_Fakes_Total += sigmaMC_Fakes*sigmaMC_Fakes
		print "Fakes events in MC sample " + sample.name + ": " + str(nMC_Fakes) + ": '+/-: " + str(sigmaMC_Fakes)
	sigmaMC_Fakes_Total = math.sqrt(sigmaMC_Fakes_Total)
	print "Total Fakes events in MC: " + str(nMC_Fakes_Total) + ": '+/-: " + str(sigmaMC_Fakes_Total)
	print "------------------------------------------"

	### derive Z+jet SF ###
	if ("J2" in channelname):
		histname_CR_e = "emufromee_OS_J2_ZCR/h_emufromee_OS_J2_ZCR_CutFlow"
		histname_CR_mu = "emufrommumu_OS_J2_ZCR/h_emufrommumu_OS_J2_ZCR_CutFlow"
	else:
		histname_CR_e = "emufromee_OS_J3_ZCR/h_emufromee_OS_J3_ZCR_CutFlow"
		histname_CR_mu = "emufrommumu_OS_J3_ZCR/h_emufrommumu_OS_J3_ZCR_CutFlow"
	cutFlowData_CR_e = datafile.Get(histname_CR_e)
	cutFlowData_CR_mu = datafile.Get(histname_CR_mu)
	nData_CR = cutFlowData_CR_e.GetBinContent(9) + cutFlowData_CR_mu.GetBinContent(9)
	sigmaData_CR = math.sqrt(cutFlowData_CR_e.GetBinError(9)*cutFlowData_CR_e.GetBinError(9) + cutFlowData_CR_mu.GetBinError(9)*cutFlowData_CR_mu.GetBinError(9))
	print "Data events in Z+jets CR: " + str(nData_CR) + ": '+/-: " + str(sigmaData_CR)
	nMC_CR_Total = 0
	sigmaMC_CR_Total = 0
	for sample in MCsamples:
		infile = ROOT.TFile(sample.root_file_path, "read")
		cutFlowMC_CR_e = infile.Get(histname_CR_e)
		cutFlowMC_CR_mu = infile.Get(histname_CR_mu)
		nMC_CR = cutFlowMC_CR_e.GetBinContent(9) + cutFlowMC_CR_mu.GetBinContent(9)
		nMC_CR_Total += nMC_CR
		sigmaMC_CR = math.sqrt(cutFlowMC_CR_e.GetBinError(9)*cutFlowMC_CR_e.GetBinError(9) + cutFlowMC_CR_mu.GetBinError(9)*cutFlowMC_CR_mu.GetBinError(9))
		sigmaMC_CR_Total += sigmaMC_CR*sigmaMC_CR
		print "MC events in Z+jets CR in sample " + sample.name + ": " + str(nMC_CR) + ": '+/-: " + str(sigmaMC_CR)
	sigmaMC_CR_Total = math.sqrt(sigmaMC_CR_Total)
	print "Total MC events in Z+jets CR: " + str(nMC_CR_Total) + ": '+/-: " + str(sigmaMC_CR_Total)
	normfactor_CR = nData_CR/nMC_CR_Total
	sigmanormfactor_CR = normfactor_CR*math.sqrt(math.pow(sigmaData_CR/nData_CR,2)+math.pow(sigmaMC_CR_Total/nMC_CR_Total,2))
	print "-> Z+jets normfactor: " + str(normfactor_CR) + ": '+/-: " + str(sigmanormfactor_CR)
	print "------------------------------------------"

	### derive Z+jets scale factor after m_lj cut
	if "J2" in channelname:
		histname_CR_mlj_e = "emufromee_OS_J2_ZCR_m_lj_cut/h_emufromee_OS_J2_ZCR_m_lj_cut_CutFlow"
		histname_CR_mlj_mu = "emufrommumu_OS_J2_ZCR_m_lj_cut/h_emufrommumu_OS_J2_ZCR_m_lj_cut_CutFlow"
		cutFlowData_CR_mlj_e = datafile.Get(histname_CR_mlj_e)
		cutFlowData_CR_mlj_mu = datafile.Get(histname_CR_mlj_mu)
		nData_CR_mlj = cutFlowData_CR_mlj_e.GetBinContent(11) + cutFlowData_CR_mlj_mu.GetBinContent(11)
		sigmaData_CR_mlj = math.sqrt(cutFlowData_CR_mlj_e.GetBinError(11)*cutFlowData_CR_mlj_e.GetBinError(11) + cutFlowData_CR_mlj_mu.GetBinError(11)*cutFlowData_CR_mlj_mu.GetBinError(11))
		print "Data events in Z+jets CR after m_lj cut: " + str(nData_CR_mlj) + ": '+/-: " + str(sigmaData_CR_mlj)
		nMC_CR_mlj_Total = 0
		sigmaMC_CR_mlj_Total = 0
		for sample in MCsamples:
			infile = ROOT.TFile(sample.root_file_path, "read")
			cutFlowMC_CR_mlj_e = infile.Get(histname_CR_mlj_e)
			cutFlowMC_CR_mlj_mu = infile.Get(histname_CR_mlj_mu)
			nMC_CR_mlj = cutFlowMC_CR_mlj_e.GetBinContent(11) + cutFlowMC_CR_mlj_mu.GetBinContent(11)
			nMC_CR_mlj_Total += nMC_CR_mlj
			sigmaMC_CR_mlj = math.sqrt(cutFlowMC_CR_mlj_e.GetBinError(11)*cutFlowMC_CR_mlj_e.GetBinError(11) + cutFlowMC_CR_mlj_mu.GetBinError(11)*cutFlowMC_CR_mlj_mu.GetBinError(11))
			sigmaMC_CR_mlj_Total += sigmaMC_CR_mlj*sigmaMC_CR_mlj
			print "MC events in Z+jets CR after m_lj cut in sample " + sample.name + ": " + str(nMC_CR_mlj) + ": '+/-: " + str(sigmaMC_CR_mlj)
		sigmaMC_CR_mlj_Total = math.sqrt(sigmaMC_CR_mlj_Total)
		print "Total MC events in Z+jets CR after m_lj cut: " + str(nMC_CR_mlj_Total) + ": '+/-: " + str(sigmaMC_CR_mlj_Total)
		normfactor_CR_mlj = nData_CR_mlj/nMC_CR_mlj_Total
		sigmanormfactor_CR_mlj = normfactor_CR_mlj*math.sqrt(math.pow(sigmaData_CR_mlj/nData_CR_mlj,2)+math.pow(sigmaMC_CR_mlj_Total/nMC_CR_mlj_Total,2))
		print "-> Z+jets normfactor after m_lj cut: " + str(normfactor_CR_mlj) + ": '+/-: " + str(sigmanormfactor_CR_mlj)
		print "------------------------------------------"

	### derive ttbar SF while applying Z+jet SF ###
	histname_SR = channelname + "/h_" + channelname + "_CutFlow"
	cutFlowData_SR = datafile.Get(histname_SR)
	nData_SR = cutFlowData_SR.GetBinContent(6)
	sigmaData_SR = cutFlowData_SR.GetBinError(6)
	print "Data events in SR: " + str(nData_SR) + ": '+/-: " + str(sigmaData_SR)
	nMC_SR_Total = 0
	nMC_SR_Total_CRUP = 0
	nMC_SR_Total_CRDO = 0
	sigmaMC_SR_Total = 0
	sigmaMC_SR_Total_CRUP = 0
	sigmaMC_SR_Total_CRDO = 0
	for sample in MCsamples:
		infile = ROOT.TFile(sample.root_file_path, "read")
		cutFlowMC_SR = infile.Get(histname_SR)
		nMC_SR = cutFlowMC_SR.GetBinContent(6)
		sigmaMC_SR = cutFlowMC_SR.GetBinError(6)
		### apply Z+jet SF here! ###
		if ("Zjets" in sample.name):
			nMC_SR_Total += nMC_SR*normfactor_CR
			nMC_SR_Total_CRUP += nMC_SR*(normfactor_CR+sigmanormfactor_CR)
			nMC_SR_Total_CRDO += nMC_SR*(normfactor_CR-sigmanormfactor_CR)
			sigmaMC_SR_Total += sigmaMC_SR*sigmaMC_SR*normfactor_CR*normfactor_CR + nMC_SR*nMC_SR*sigmanormfactor_CR*sigmanormfactor_CR
			sigmaMC_SR_Total_CRUP += sigmaMC_SR*sigmaMC_SR*(normfactor_CR+sigmanormfactor_CR)*(normfactor_CR+sigmanormfactor_CR) + nMC_SR*nMC_SR*sigmanormfactor_CR*sigmanormfactor_CR
			sigmaMC_SR_Total_CRDO += sigmaMC_SR*sigmaMC_SR*(normfactor_CR-sigmanormfactor_CR)*(normfactor_CR-sigmanormfactor_CR) + nMC_SR*nMC_SR*sigmanormfactor_CR*sigmanormfactor_CR
			print "MC events in SR from sample " + sample.name + " after scaling: " + str(nMC_SR*normfactor_CR) + ": '+/-: " + str(math.sqrt(sigmaMC_SR*sigmaMC_SR*normfactor_CR*normfactor_CR + nMC_SR*nMC_SR*sigmanormfactor_CR*sigmanormfactor_CR))
			print "CR up: " + str(nMC_SR*(normfactor_CR+sigmanormfactor_CR))  + ": '+/-: " + str(math.sqrt(sigmaMC_SR*sigmaMC_SR*(normfactor_CR+sigmanormfactor_CR)*(normfactor_CR+sigmanormfactor_CR) + nMC_SR*nMC_SR*sigmanormfactor_CR*sigmanormfactor_CR))
			print "CR down: " + str(nMC_SR*(normfactor_CR-sigmanormfactor_CR))  + ": '+/-: " + str(math.sqrt(sigmaMC_SR*sigmaMC_SR*(normfactor_CR-sigmanormfactor_CR)*(normfactor_CR-sigmanormfactor_CR) + nMC_SR*nMC_SR*sigmanormfactor_CR*sigmanormfactor_CR))
		else:
			nMC_SR_Total += nMC_SR
			nMC_SR_Total_CRUP += nMC_SR
			nMC_SR_Total_CRDO += nMC_SR
			sigmaMC_SR_Total += sigmaMC_SR*sigmaMC_SR
			sigmaMC_SR_Total_CRUP += sigmaMC_SR*sigmaMC_SR
			sigmaMC_SR_Total_CRDO += sigmaMC_SR*sigmaMC_SR
			print "MC events in SR from sample " + sample.name + " " + sample.systematic +": " + str(nMC_SR) + ": '+/-: " + str(sigmaMC_SR)
	sigmaMC_SR_Total = math.sqrt(sigmaMC_SR_Total)
	sigmaMC_SR_Total_CRUP = math.sqrt(sigmaMC_SR_Total_CRUP)
	sigmaMC_SR_Total_CRDO = math.sqrt(sigmaMC_SR_Total_CRDO)
	normfactor_SR = nData_SR/nMC_SR_Total
	normfactor_SR_CRUP = nData_SR/nMC_SR_Total_CRUP
	normfactor_SR_CRDO = nData_SR/nMC_SR_Total_CRDO
	sigmanormfactor_SR = normfactor_SR*math.sqrt(math.pow(sigmaData_SR/nData_SR,2)+math.pow(sigmaMC_SR_Total/nMC_SR_Total,2))
	sigmanormfactor_SR_CRUP = normfactor_SR_CRUP*math.sqrt(math.pow(sigmaData_SR/nData_SR,2)+math.pow(sigmaMC_SR_Total_CRUP/nMC_SR_Total_CRUP,2))
	sigmanormfactor_SR_CRDO = normfactor_SR_CRDO*math.sqrt(math.pow(sigmaData_SR/nData_SR,2)+math.pow(sigmaMC_SR_Total_CRDO/nMC_SR_Total_CRDO,2))
	print "Total MC events in SR: " + str(nMC_SR_Total) + ": '+/-: " + str(sigmaMC_SR_Total)
	print "-> Nominal ttbar signal normfactor: " + str(normfactor_SR) + ": '+/-: " + str(sigmanormfactor_SR)
	print "-> CR up ttbar signal normfactor: " + str(normfactor_SR_CRUP)
	print "-> CR down ttbar signal normfactor: " + str(normfactor_SR_CRDO)
	print "------------------------------------------"

	### derive ttbar SF after m_lj cut while applying Z+jet SF after m_lj cut ###
	if ("emu_OS_J2" in channelname):
		histname_SR_mlj = channelname + "_m_lj_cut/h_" + channelname + "_m_lj_cut_CutFlow"
		cutFlowData_SR_mlj = datafile.Get(histname_SR_mlj)
		nData_SR_mlj = cutFlowData_SR_mlj.GetBinContent(8)
		sigmaData_SR_mlj = cutFlowData_SR_mlj.GetBinError(8)
		print "Data events in SR after m_lj cut: " + str(nData_SR_mlj) + ": '+/-: " + str(sigmaData_SR_mlj)
		nMC_SR_mlj_Total = 0
		nMC_SR_mlj_Total_CRUP = 0
		nMC_SR_mlj_Total_CRDO = 0
		sigmaMC_SR_mlj_Total = 0
		sigmaMC_SR_mlj_Total_CRUP = 0
		sigmaMC_SR_mlj_Total_CRDO = 0
		for sample in MCsamples:
			infile = ROOT.TFile(sample.root_file_path, "read")
			cutFlowMC_SR_mlj = infile.Get(histname_SR_mlj)
			nMC_SR_mlj = cutFlowMC_SR_mlj.GetBinContent(8)
			sigmaMC_SR_mlj = cutFlowMC_SR_mlj.GetBinError(8)
			### apply Z+jet SF here! ###
			if ("Zjets" in sample.name):
				nMC_SR_mlj_Total += nMC_SR_mlj*normfactor_CR_mlj
				nMC_SR_mlj_Total_CRUP += nMC_SR_mlj*(normfactor_CR_mlj+sigmanormfactor_CR_mlj)
				nMC_SR_mlj_Total_CRDO += nMC_SR_mlj*(normfactor_CR_mlj-sigmanormfactor_CR_mlj)
				sigmaMC_SR_mlj_Total += sigmaMC_SR_mlj*sigmaMC_SR_mlj*normfactor_CR_mlj*normfactor_CR_mlj + nMC_SR_mlj*nMC_SR_mlj*sigmanormfactor_CR_mlj*sigmanormfactor_CR_mlj
				sigmaMC_SR_mlj_Total_CRUP += sigmaMC_SR_mlj*sigmaMC_SR_mlj*(normfactor_CR_mlj+sigmanormfactor_CR_mlj)*(normfactor_CR_mlj+sigmanormfactor_CR_mlj) + nMC_SR_mlj*nMC_SR_mlj*sigmanormfactor_CR_mlj*sigmanormfactor_CR_mlj
				sigmaMC_SR_mlj_Total_CRDO += sigmaMC_SR_mlj*sigmaMC_SR_mlj*(normfactor_CR_mlj-sigmanormfactor_CR_mlj)*(normfactor_CR_mlj-sigmanormfactor_CR_mlj) + nMC_SR_mlj*nMC_SR_mlj*sigmanormfactor_CR_mlj*sigmanormfactor_CR_mlj
				print "MC events in SR after m_lj cut from sample " + sample.name + " after scaling: " + str(nMC_SR_mlj*normfactor_CR_mlj) + ": '+/-: " + str(math.sqrt(sigmaMC_SR_mlj*sigmaMC_SR_mlj*normfactor_CR_mlj*normfactor_CR_mlj + nMC_SR_mlj*nMC_SR_mlj*sigmanormfactor_CR_mlj*sigmanormfactor_CR_mlj))
				print "CR up: " + str(nMC_SR_mlj*(normfactor_CR_mlj+sigmanormfactor_CR_mlj)) + ": '+/-: " + str(math.sqrt(sigmaMC_SR_mlj*sigmaMC_SR_mlj*(normfactor_CR_mlj+sigmanormfactor_CR_mlj)*(normfactor_CR_mlj+sigmanormfactor_CR_mlj) + nMC_SR_mlj*nMC_SR_mlj*sigmanormfactor_CR_mlj*sigmanormfactor_CR_mlj))
				print "CR down: " + str(nMC_SR_mlj*(normfactor_CR_mlj-sigmanormfactor_CR_mlj)) + ": '+/-: " + str(math.sqrt(sigmaMC_SR_mlj*sigmaMC_SR_mlj*(normfactor_CR_mlj-sigmanormfactor_CR_mlj)*(normfactor_CR_mlj-sigmanormfactor_CR_mlj) + nMC_SR_mlj*nMC_SR_mlj*sigmanormfactor_CR_mlj*sigmanormfactor_CR_mlj))
			else:
				nMC_SR_mlj_Total += nMC_SR_mlj
				nMC_SR_mlj_Total_CRUP += nMC_SR_mlj
				nMC_SR_mlj_Total_CRDO += nMC_SR_mlj
				sigmaMC_SR_mlj_Total += sigmaMC_SR_mlj*sigmaMC_SR_mlj
				sigmaMC_SR_mlj_Total_CRUP += sigmaMC_SR_mlj*sigmaMC_SR_mlj
				sigmaMC_SR_mlj_Total_CRDO += sigmaMC_SR_mlj*sigmaMC_SR_mlj
				print "MC events in SR after m_lj cut from sample " + sample.name + " " + sample.systematic +": " + str(nMC_SR_mlj)
		sigmaMC_SR_mlj_Total = math.sqrt(sigmaMC_SR_mlj_Total)
		sigmaMC_SR_mlj_Total_CRUP = math.sqrt(sigmaMC_SR_mlj_Total_CRUP)
		sigmaMC_SR_mlj_Total_CRDO = math.sqrt(sigmaMC_SR_mlj_Total_CRDO)
		normfactor_SR_mlj = nData_SR_mlj/nMC_SR_mlj_Total
		normfactor_SR_mlj_CRUP = nData_SR_mlj/nMC_SR_mlj_Total_CRUP
		normfactor_SR_mlj_CRDO = nData_SR_mlj/nMC_SR_mlj_Total_CRDO
		sigmanormfactor_SR_mlj = normfactor_SR_mlj*math.sqrt(math.pow(sigmaData_SR_mlj/nData_SR_mlj,2)+math.pow(sigmaMC_SR_mlj_Total/nMC_SR_mlj_Total,2))
		sigmanormfactor_SR_mlj_CRUP = normfactor_SR_mlj_CRUP*math.sqrt(math.pow(sigmaData_SR_mlj/nData_SR_mlj,2)+math.pow(sigmaMC_SR_mlj_Total_CRUP/nMC_SR_mlj_Total_CRUP,2))
		sigmanormfactor_SR_mlj_CRDO = normfactor_SR_mlj_CRDO*math.sqrt(math.pow(sigmaData_SR_mlj/nData_SR_mlj,2)+math.pow(sigmaMC_SR_mlj_Total_CRDO/nMC_SR_mlj_Total_CRDO,2))
		print "Total MC events in SR after m_lj cut: " + str(nMC_SR_mlj_Total) + ": '+/-: " + str(sigmaMC_SR_mlj_Total)
		print "-> Nominal ttbar signal normfactor after m_lj cut: " + str(normfactor_SR_mlj) + ": '+/-: " + str(sigmanormfactor_SR_mlj)
		print "-> CR up ttbar signal normfactor after m_lj cut: " + str(normfactor_SR_mlj_CRUP)
		print "-> CR down ttbar signal normfactor after m_lj cut: " + str(normfactor_SR_mlj_CRDO)
		print "------------------------------------------"

	### Filling histograms ###
	h_normfactors.Fill(1, normfactor_CR)
	h_normfactors.SetBinError(1, sigmanormfactor_CR)
	h_normfactors.Fill(2, normfactor_CR_mlj)
	h_normfactors.SetBinError(2, sigmanormfactor_CR_mlj)
	h_normfactors.Fill(3, normfactor_SR)
	h_normfactors.SetBinError(3, sigmanormfactor_SR)
	h_normfactors.Fill(4, normfactor_SR_CRUP)
	h_normfactors.SetBinError(4, sigmanormfactor_SR_CRUP)
	h_normfactors.Fill(5, normfactor_SR_CRDO)
	h_normfactors.SetBinError(5, sigmanormfactor_SR_CRDO)
	h_normfactors.Fill(6, normfactor_SR_mlj)
	h_normfactors.SetBinError(6, sigmanormfactor_SR_mlj)
	h_normfactors.Fill(7, normfactor_SR_mlj_CRUP)
	h_normfactors.SetBinError(7, sigmanormfactor_SR_mlj_CRUP)
	h_normfactors.Fill(8, normfactor_SR_mlj_CRDO)
	h_normfactors.SetBinError(8, sigmanormfactor_SR_mlj_CRDO)


#Loop over all channels and compute norm factor for each individually
for channel in Channels:
	print '####################################################'
	print 'Compute normalisation factors for channel: ' + channel
	if ("emu" in channel):
		ComputeNormfactorEMU(channel)
	else:
		print "Can only run over emu channels!"
	print

#Plot histogram
import atlas_labels as al
AtlasStyle = al.atlasStyle()
AtlasStyle.SetErrorX(0.5)
AtlasStyle.SetEndErrorSize( 0 )
AtlasStyle.SetPadTopMargin(0.05)
AtlasStyle.SetPadRightMargin(  0.12)
AtlasStyle.SetPadBottomMargin( 0.20)
AtlasStyle.SetPadLeftMargin(   0.08)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()

c1 = TCanvas("c1", "MC Normalisation scale factors", 1200, 800)

mainPad  = TPad("mainPad", "top",0.0, 0.0, 1.0, 1.00)
mainPad.SetBottomMargin(0.1)
mainPad.Draw()
mainPad.cd()

h_normfactors.SetMarkerStyle(20)
h_normfactors.SetMarkerSize(0.6)
h_normfactors.SetTitle("MC Normalisation scale factors")
h_normfactors.GetYaxis().SetTitle("Data/MC Ratio")
h_normfactors.GetYaxis().SetTitleOffset(1.)
h_normfactors.Draw()
outputfile.cd()
c1.Write()
c1.Close()

#Write histogram to file
outputfile.cd()
h_normfactors.Write()
outputfile.Close()
print "Normfactors written to file " + outputfile_name
if args.apply:
    print "applying now:"
    applyScalefactors([zJets_sample.root_file_path], outputfile_name,"nominal")
    if syst_name=="nominal":
        applyScalefactors([zJets_sample.root_file_path], outputfile_name,"UP")
        applyScalefactors([zJets_sample.root_file_path], outputfile_name,"DOWN")
print "Done"
