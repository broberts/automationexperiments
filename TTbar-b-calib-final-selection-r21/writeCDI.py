import ROOT
import os
import math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse
from options_file import *
import os

def GetInputFile(PathToInput, DataName, Tagger, WorkingPoint):
	if m_lj_cut:
		if args.fit_plot_type == "Nominal":
			InputFilename = PathToInput+"FitPlot_r21_"+DataName+"_"+Tagger+"_"+WorkingPoint+"_emu_OS_J2_m_lj_cut_fconst_nominal.root"
		else:
			InputFilename = PathToInput+"FinalFitPlots_r21_"+DataName+"_"+Tagger+"_"+WorkingPoint+"_emu_OS_J2_m_lj_cut_fconst.root"
	else:
		if args.fit_plot_type == "Nominal":
			InputFilename = PathToInput+"FinalFitPlots_r21_"+DataName+"_"+Tagger+"_"+WorkingPoint+"_emu_OS_J2.root"
		else:
			InputFilename = PathToInput+"FitPlot_r21_"+DataName+"_"+Tagger+"_"+WorkingPoint+"_emu_OS_J2_nominal.root"
	return InputFilename

def EnsureCorrectFormatingForFitTypeArg():
	if args.fit_plot_type == "Syst":
		pass
	elif args.fit_plot_type == "pseudo":
		args.fit_plot_type = "Pseudo"
	elif args.fit_plot_type == "nominal":
		args.fit_plot_type = "Nominal"
	else:
		print("Formatting not correct. Arg should be Syst (default), Pseudo or Nominal")

def GetOutputFilename(FitPlotType,DataName,ATRel,Tagger,Cut,Version, JetCollection, WorkingPoint="WP_not_specified"):
	# btag_ttbarPDF_<year release (eg: mc16a)>_<version (eg: v1.0)>_<AT release>_Tagger_WP_Cut_JetCollection_Pseudo(continous or not).txt
	ModATRel = ATRel.split("rel")[-1].split("_")[1]
	if args.fit_plot_type == "Syst":
		OutputFilename = "btag_ttbarPDF_" + DataName + "_" + ModATRel+"_"+Version+"_"+Tagger+"_"+Cut+"_"+WorkingPoint+"_"+JetCollection+".txt"
	elif args.fit_plot_type == "Nominal":
		OutputFilename = "btag_ttbarPDF_" + DataName + "_" + ModATRel+"_"+Version+"_"+Tagger+"_"+Cut+"_"+WorkingPoint+"_"+JetCollection+"_"+args.fit_plot_type+".txt"
	else:
		OutputFilename = "btag_ttbarPDF_" + DataName + "_" + ModATRel+"_"+Version+"_"+Tagger+"_"+Cut+"_"+JetCollection+"_Continous.txt"
	return OutputFilename

def GetXString(StringType, KeyList, KeyDir, IBin, TotalRelativeXError, XString, WorkingPoint):
	# String types: SYST, PDF, STAT
	# Need to include more options for pseudo to save space at the bottom
	for ikey in xrange(1, KeyList.GetSize()):
		obj=KeyDir.Get(KeyList.At(ikey).GetName())
		ClassName=obj.ClassName()
		if ClassName[:2]=="TH":
			HistName=obj.GetName();
			# If it is pdf or syst:
			if StringType != "STAT":
				if HistName.find("syst_Error_rel")>0 and HistName.find(WorkingPoint)>0 and HistName.find("unused")==-1 and HistName.find("MET_SoftTrk")==-1:
					HistSyst = KeyDir.Get(HistName)
					SystVal = HistSyst.GetBinContent(IBin+1)
					if (abs(SystVal) < threshold):
						print "Dropping this systematic, because its absolute value "+str(abs(SystVal))+" is smaller than threshold "+str(threshold)
						continue
					TotalRelativeXError += math.pow(SystVal,2)
					print "Added:\t"+HistName, str(round(100*SystVal,2))+"%)" , "current value: " ,str(round(100*math.sqrt(TotalRelativeXError),2))+"%)"
					SystNameAT=(HistName.replace("_syst_Error_rel","")).replace("e_b_"+WorkingPoint+"_","")
					# Starts to differ here:
					if StringType == "SYST":
						SystNameAT=SystNameAT.replace("JET_21NP_","").replace("CategoryReduction_JET_","").replace("FTAG2_","FT_EFF_")
						SystNameCP=SystNameAT.replace("weight_","")
						if SystNameCP in ATNames2Replace:
							SystNameCP=ATNames2Replace[SystNameCP]
					elif StringType == "PDF":
						SystNameAT=SystNameAT.replace("mc_shower","PDF4LHC")
						SystNameCP=SystNameAT.replace("weight_","")
						np=int(SystNameCP.replace("PDF4LHC_np_",""))
						SystNameCP="FT_EFF_PDF4LHC_np_"+str(np-111)
					XString += "\t\tsys("+SystNameCP+","+str(round(100*SystVal,2))+"%)\n"
			# If it is stat only:
			else:
				if HistName.find("stat_np")>0 and HistName.find("Error_rel")>0 and HistName.find(WorkingPoint)>0:
					print "Reading stat histogam:\t"+HistName
					HistStat = KeyDir.Get(HistName)
					StatVal = HistStat.GetBinContent(ibin+1)
					if (abs(StatVal) < threshold):
						print "Dropping this statistical error, because its absolute value "+str(abs(StatVal))+" is smaller than threshold "+str(threshold)
						continue
					TotalRelativeXError += math.pow(StatVal,2)
					XString += "\t\tsys("+(HistName.replace("_Error_rel","")).replace("e_b_"+WorkingPoint+"_","FT_EFF_"+"b-PDF_")+","+str(round(100*StatVal,2))+"%)\n"
	return XString, TotalRelativeXError

parser = argparse.ArgumentParser(
   description='Writes the scale factors and relative uncetainties into .txt files for the CDI file')
parser.add_argument('-ft', "--fit_plot_type", default="Syst", type=str,
   help='Choose between Syst(FitPlot) for systematic files, Nominal for nominal FitPlot files and Pseudo for pseudo-continious.')
#parser.add_argument('-t',"--threshold",default="0.0",
#        help='Sets the threshold at which relative systematic uncertainties are dropped.')
args = parser.parse_args()
EnsureCorrectFormatingForFitTypeArg()

# Set up options:
taggers=options.taggers
cuts=options.cuts
workingpoints=options.WPs
calib_jet_collection = "AntiKt4EMPFlowJets"
#be carefull if you change this! do you really use the new one? Change it in the TSelectors!!
CDI_jet_collection = "AntiKt4EMPFlowJets"
cdi_input_name="2017-21-13TeV-MC16-CDI-2018-10-19_v1.root"
cdi_input_path="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/"
version = "v1.0_73_SF"
AT_rel = options.version_tag.split("/")[0]
if args.fit_plot_type == "Syst" or args.fit_plot_type == "Pseudo":
	# By default they all should be set to true but can turn off if youre having problems
	include_PDF = True
	include_systs = True
	include_stat = True
else:
	include_PDF = False
	include_systs = False
	include_stat = False
m_lj_cut=False
pt_binedges=["20","30","40","60","85","110","140","175","250","400"]
nbins = len(pt_binedges)

# Create a dir for output txtfiles:
data_name=options.data_name
outdir_path = ("/").join(options.input_selection_dir.split("/")[:-2]) + "/"
outdir_folder = "CDIFiles/" + data_name + "/"
outdir = outdir_path + outdir_folder
if not os.path.exists(outdir):
    cmd = "mkdir -p " + outdir
    os.system(cmd)

# Where are the root files you want to get from?
workdir=options.plot_dir+"FitPlots_3SB/"
if m_lj_cut or args.fit_plot_type == "Nominal":
    workdir=options.plot_dir+"FitPlots_3SB/"

# Set threshold, whatever this is?
#if args.threshold:
#    threshold = args.threshold
threshold = -1

# Replace some syst names
ATNames2Replace={
    "pileup" : "PRW_DATASF",
    "jvt" : "JET_JvtEfficiency",
    "leptonSF_MU_SF_Trigger_STAT" : "MUON_EFF_TrigStatUncertainty",
    "leptonSF_MU_SF_Trigger_SYST" : "MUON_EFF_TrigSystUncertainty",
    "leptonSF_MU_SF_ID_STAT" : "MUON_EFF_RECO_STAT",
    "leptonSF_MU_SF_ID_SYST" : "MUON_EFF_RECO_SYS",
    "leptonSF_MU_SF_ID_STAT_LOWPT" : "MUON_EFF_RECO_STAT_LOWPT",
    "leptonSF_MU_SF_ID_SYST_LOWPT" : "MUON_EFF_RECO_SYS_LOWPT",
    "leptonSF_MU_SF_TTVA_STAT" : "MUON_EFF_TTVA_STAT",
    "leptonSF_MU_SF_TTVA_SYST" : "MUON_EFF_TTVA_SYS",
    "leptonSF_MU_SF_Isol_STAT" : "MUON_EFF_ISO_STAT",
    "leptonSF_MU_SF_Isol_SYST" : "MUON_EFF_ISO_SYS",
    "leptonSF_EL_SF_Trigger" : "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
    "leptonSF_EL_SF_Reco" : "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
    "leptonSF_EL_SF_ID" : "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
    "leptonSF_EL_SF_Isol" : "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
    "hdamp_mc_rad" : "FT_EFF_ttbar_PhPy8Rad",
    "MC_stat_nominal" : "FT_EFF_MC_stat_nominal",
    "mc_fsr" : "FT_EFF_ttbar_FSR",
    "misstagLight_up" : "FT_EFF_misstagLight_up",
    "clTestMoMc_nstat" : "FT_EFF_closure",
    "correctFakes" : "FT_EFF_correctFakes",
}

# Loop over all taggers and working points (Fixed, hybrid, etc)
for t in taggers:
    for c in cuts:
		# Get input file and check it exists:
		input_filename = GetInputFile(workdir,data_name, t, c)
		print("File running over: " + input_filename)
		if not os.path.isfile(input_filename):
			print "File "+input_filename+" not found!"
			break
		print("Congrats! The files exists .. ")

		# Get all the objecs in the root file
		input_file=ROOT.TFile(input_filename,"read")
		keyList=input_file.GetListOfKeys()
		# Pseudo continuous has slight differences:
		if args.fit_plot_type != "Pseudo":
			for wp in workingpoints:
				# Create a new file
				output_filename = GetOutputFilename(args.fit_plot_type, data_name, AT_rel, t, c, version,calib_jet_collection, wp)
				output_file = open(outdir + output_filename, "w")

				# Only applies to Syst fit plots:
				if args.fit_plot_type == "Syst":
					# Look in sys dir
					sys_dir=input_file.Get("e_b_systematics_wp_"+wp)
					sys_keyList=sys_dir.GetListOfKeys()
					# Look in PDF dir
					pdf_sys_dir=input_file.Get("e_b_pdf_systematics_wp_"+wp)
					pdf_sys_keyList=pdf_sys_dir.GetListOfKeys()
					# Look in stat dir
					stat_dir=input_file.Get("stats_wp_"+wp)
					stat_keyList=stat_dir.GetListOfKeys()
				else:
					# Look in sf and b eff folder
					sf_dir = "sf_and_beff"

				cutvalue = c+"_"+wp+"_"+cdi_input_name.replace(".root","")
				if c == "FixedCutBEff":
					CDI_file = ROOT.TFile(cdi_input_path+cdi_input_name,"read")
					cutval = CDI_file.Get(t+"/" + CDI_jet_collection + "/" + c + "_"+wp+"/cutvalue")
					cutvalue = cutval.Max()
					# Legacy code
					#if (wp == "85"):
					#    cutvalue = 0.175847
					#elif (wp == "77"):
					#    cutvalue = 0.4
					#elif (wp == "70"):
					#    cutvalue = 0.6
					#else:
					#    cutvalue = 0.8
				CDIstring = "Analysis(ttbar_PDF,bottom,"+t+","+c+"_"+wp+"," + calib_jet_collection + "){\n\n"
				CDIstring += "\tmeta_data_s (Hadronization, Pythia8EvtGen)\n"
				CDIstring += "\tmeta_data_s (OperatingPoint, "+str(cutvalue)+")\n\n"
				# Loop over each pt bin
				for ibin in range(0, nbins-1):
					CDIstring += "\tbin("+pt_binedges[ibin]+"<pt<"+pt_binedges[ibin+1]+",0<abseta<2.5)\n"
					CDIstring += "\t{\n"
					total_relative_syst_error = 0.
					total_relative_stat_error = 0.
					stat_string = ""
					sys_string = ""
					if include_systs == True:
						# Loop over syst folder:
						sys_string, total_relative_syst_error = GetXString("SYST", sys_keyList, sys_dir, ibin, total_relative_syst_error, sys_string, wp)
					if include_PDF == True:
						# Loop over PDF folder
						sys_string, total_relative_syst_error = GetXString("PDF", pdf_sys_keyList, pdf_sys_dir, ibin, total_relative_syst_error, sys_string, wp)
					if include_stat == True:
						# Loop over stat folder
						stat_string, total_relative_stat_error = GetXString("STAT", stat_keyList, stat_dir, ibin, total_relative_stat_error, stat_string, wp)
						sys_string += stat_string
					#Get SF here
					if args.fit_plot_type == "Nominal":
						string_nominal = "sf_b_"+wp+"_Postfit"
						hist_nominal = input_file.Get(sf_dir + "/" + string_nominal)
						central_value = hist_nominal.GetBinContent(ibin+1)
						totalstat_value = hist_nominal.GetBinError(ibin+1)
						totalstat_value /= central_value
					elif args.fit_plot_type == "Syst":
						string_nominal = "sf_b_"+wp+"_Postfit"
						hist_nominal = input_file.Get(string_nominal)
						central_value = hist_nominal.GetBinContent(ibin+1)
						totalstat_value = hist_nominal.GetBinError(ibin+1)
						totalstat_value /= central_value
						string_totalsys = "sf_b_"+wp+"_syst_Error_combined_rel"
						hist_totalsys = input_file.Get(string_totalsys)
						totalsys_value = hist_totalsys.GetBinContent(ibin+1)
						totalerr_value = math.sqrt(math.pow(totalsys_value,2) + math.pow(totalstat_value,2))
						total_relative_error = math.sqrt(total_relative_syst_error + total_relative_stat_error)
						total_relative_syst_error = math.sqrt(total_relative_syst_error)
						total_relative_stat_error = math.sqrt(total_relative_stat_error)
						print "Summed up total relative syst. error for bin "+str(ibin)+":\t"+str(100.*total_relative_syst_error)+"%"
						print "Summed up total relative stat. error for bin "+str(ibin)+":\t"+str(100.*total_relative_stat_error)+"%"
						print "Total error for bin "+str(ibin)+":\t"+str(100.*total_relative_error)+"%"
						print "Cross check: reading total relative syst. error for bin "+str(ibin)+" from file: "+str(100.*totalsys_value)+"%"
						print "Cross check: reading total relative stat. error for bin "+str(ibin)+" from file: "+str(100.*totalstat_value)+"%"
						print "Cross check: reading total relative error for bin "+str(ibin)+" from file: "+str(100.*totalerr_value)+"%"
		                #CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(round(100.*total_relative_stat_error,2))+"%)\n"
		                #CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(-999)+")\n"
		                # CDIstring += "\t\tmeta_data(N_jets total,-999,-999)\n"
		                # CDIstring += "\t\tmeta_data(N_jets tagged,-999,-999)\n"
					CDIstring += "\t\tcentral_value("+str(round(central_value,4))+","+str(0)+")\n"
					CDIstring += sys_string
					CDIstring += stat_string
					CDIstring += "\t}\n"
				CDIstring += "}"
				print "Writing to file "+output_filename
				output_file.write(CDIstring)
				output_file.close()
			input_file.Close()
			print "File "+input_filename+" closed."
		else:
			# This is the case of pseudo continious
			# Need to add 0 to wps:
			workingpoints_plus=list(workingpoints)
			workingpoints_plus = list(reversed(workingpoints))
			workingpoints_plus.append("0")

			# create file
			output_filename = GetOutputFilename(args.fit_plot_type, data_name, AT_rel, t, c, version, calib_jet_collection)
			output_file = open(outdir + output_filename, "w")
			CDIstring = "Analysis(ttbar_PDF,bottom,"+t+","+"Continuous"+","+ calib_jet_collection + "){\n\n"
			CDIstring += "\tmeta_data_s (Hadronization, Pythia8EvtGen)\n"
			for wp in workingpoints:
				cutvalue = c+"_"+wp+"_"+cdi_input_name.replace(".root","")
				if c== "FixedCutBEff":
					cdiFile=ROOT.TFile(cdi_input_path+cdi_input_name,"read")
					cutval=cdiFile.Get(t+"/"+CDI_jet_collection+"/FixedCutBEff_"+wp+"/cutvalue")
					cutvalue=cutval.Max()
				CDIstring += "\tmeta_data_s (TagWeightBinEdge,"+c+"_"+wp+","+str(cutvalue)+")\n"
			CDIstring +="\n"
			#now starting block for all the 3d bins:
			for pt_bin in range(1, nbins):
				wp_n=0
				tagweight_name=""
				for wp in workingpoints_plus:
					wp_n +=1
					if wp=="0":
						tagweight_name += "tagweight"
					else:
						tagweight_name += "tagweight<"+c+"_"+wp

					CDIstring += "\tbin("+pt_binedges[pt_bin-1]+"<pt<"+pt_binedges[pt_bin]+",0<abseta<2.5," + tagweight_name + ")\n"
					#set tagweighname for next round
					tagweight_name=c+"_"+wp+"<"
					CDIstring += "\t{\n"
					# List of systs:
					total_relative_syst_error = 0.
					# sys_string = ""
					# sys_dir=input_file.Get("p_b_systematics_pt_"+str(pt_bin))
					# sys_keyList=sys_dir.GetListOfKeys()
					# Lists of pdfs:
					# pdf_sys_dir=input_file.Get("p_b_pdf_systematics_pt_"+str(pt_bin))
					# pdf_sys_keyList=pdf_sys_dir.GetListOfKeys()
					# List of stats:
					total_relative_stat_error = 0.
					# stat_string = ""
					# stat_dir=input_file.Get("stats_pt_"+str(pt_bin))
					# stat_keyList=stat_dir.GetListOfKeys()
					# First loop over systs:
					# for ikey in xrange(1, sys_keyList.GetSize()):
					# 	obj=sys_dir.Get(sys_keyList.At(ikey).GetName())
					# 	className=obj.ClassName()
					# 	if className[:2]=="TH":
					# 		hist_name=obj.GetName();
					# 		if hist_name.find("_syst_Error_rel")>0 and hist_name.find(str(pt_bin))>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
					# 			#print "Reading syst histogam:\t"+hist_name
					# 			# hist_sys = sys_dir.Get(hist_name)
					# 			# sys_value = hist_sys.GetBinContent(wp_n)
					# 			if (abs(sys_value) < threshold):
					# 				print "Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold)
					# 				continue
					# 			total_relative_syst_error += math.pow(sys_value,2)
					# 			syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("p_b_"+"pt_"+str(pt_bin)+"_","")
					# 			syst_name_anaTop=syst_name_anaTop.replace("JET_21NP_","").replace("CategoryReduction_JET_","").replace("FTAG2_","FT_EFF_")
					# 			syst_name_cp=syst_name_anaTop.replace("weight_","")
					# 			if syst_name_cp in ATNames2Replace:
					# 				syst_name_cp=ATNames2Replace[syst_name_cp]
					# 			sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,4))+"%)\n"
					# Loop over PDFs:
					# for ikey in xrange(1, pdf_sys_keyList.GetSize()):
					# 	obj=pdf_sys_dir.Get(pdf_sys_keyList.At(ikey).GetName())
					# 	className=obj.ClassName()
					# 	if className[:2]=="TH":
					# 		hist_name=obj.GetName();
					# 		if hist_name.find("_syst_Error_rel")>0 and hist_name.find(str(pt_bin))>0 and hist_name.find("unused")==-1 and hist_name.find("MET_SoftTrk")==-1:
					# 			#print "Reading syst histogam:\t"+hist_name
					# 			hist_sys = pdf_sys_dir.Get(hist_name)
					# 			sys_value = hist_sys.GetBinContent(wp_n)
					# 			if (abs(sys_value) < threshold):
					# 				print "Dropping this systematic, because its absolute value "+str(abs(sys_value))+" is smaller than threshold "+str(threshold)
					# 				continue
					# 			total_relative_syst_error += math.pow(sys_value,2)
					# 			syst_name_anaTop=(hist_name.replace("_syst_Error_rel","")).replace("p_b_"+"pt_"+str(pt_bin)+"_","")
					# 			syst_name_anaTop=syst_name_anaTop.replace("mc_shower","PDF4LHC")
					# 			syst_name_cp=syst_name_anaTop.replace("weight_","")
					# 			np=int(syst_name_cp.replace("PDF4LHC_np_",""))
					# 			syst_name_cp="FT_EFF_PDF4LHC_np_"+str(np-111)
					# 			sys_string += "\t\tsys("+syst_name_cp+","+str(round(100*sys_value,4))+"%)\n"
					# Loop over stats:
					# for ikey in xrange(1, stat_keyList.GetSize()):
					# 	obj=stat_dir.Get(stat_keyList.At(ikey).GetName())
					# 	className=obj.ClassName()
					# 	if className[:2]=="TH":
					# 		hist_name=obj.GetName();
					# 		if hist_name.find("Error_rel")>0 and hist_name.find(str(pt_bin))>0:
					# 			#print "Reading stat histogam:\t"+hist_name
					# 			hist_stat = stat_dir.Get(hist_name)
					# 			stat_value = hist_stat.GetBinContent(wp_n)
					# 			if (abs(stat_value) < threshold):
					# 				print "Dropping this statistical error, because its absolute value "+str(abs(stat_value))+" is smaller than threshold "+str(threshold)
					# 				continue
					# 			total_relative_stat_error += math.pow(stat_value,2)
					# 			sys_string += "\t\tsys("+(hist_name.replace("_Error_rel","")).replace("p_b_pt_"+str(pt_bin)+"_","FT_EFF_b-PDF_")+","+str(round(100*stat_value,4))+"%)\n"
					#Get SF here
					string_nominal_pre = "pdf/p_b_pt_"+str(pt_bin)+"_Prefit"
					string_nominal_post = "pdf/p_b_pt_"+str(pt_bin)+"_Postfit"
					hist_nominal = input_file.Get(string_nominal_post)
					hist_nominal2 = input_file.Get(string_nominal_pre)
					central_value = hist_nominal.GetBinContent(wp_n)
					central_value2 = hist_nominal2.GetBinContent(wp_n)
					central_value = central_value / central_value2
					totalstat_value = hist_nominal.GetBinError(wp_n)
					totalstat_value2 = hist_nominal2.GetBinError(wp_n)
					totalstat_value = pow(pow(totalstat_value,2) + pow(totalstat_value,2),0.5)
					totalstat_value /= central_value
					# string_totalsys = "p_b_sf_pt_"+str(pt_bin)+"_syst_Error_combined_rel"
					# hist_totalsys = input_file.Get(string_totalsys)
					# totalsys_value = hist_totalsys.GetBinContent(wp_n)
					# totalerr_value = math.sqrt(math.pow(totalsys_value,2) + math.pow(totalstat_value,2))
					# total_relative_error = math.sqrt(total_relative_syst_error + total_relative_stat_error)
					# total_relative_syst_error = math.sqrt(total_relative_syst_error)
					# total_relative_stat_error = math.sqrt(total_relative_stat_error)
					# print "Summed up total relative syst. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+":\t"+str(100.*total_relative_syst_error)+"%"
					# print "Summed up total relative stat. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+":\t"+str(100.*total_relative_stat_error)+"%"
					# print "Total error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+":\t"+str(100.*total_relative_error)+"%"
					# print "Cross check: reading total relative syst. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(100.*totalsys_value)+"%"
					# print "Cross check: reading total relative stat. error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(100.*totalstat_value)+"%"
					# print "Cross check: reading total relative error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(100.*totalerr_value)+"%"
					# print "Cross check: absolut stat error for pt_bin "+str(pt_bin)+" wp_bin "+str(wp_n)+" from file: "+str(central_value*totalstat_value)+" from here: "+str(total_relative_stat_error*central_value)
					#CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(round(100.*total_relative_stat_error,2))+"%)\n"
					#CDIstring += "\t\tcentral_value("+str(round(central_value,2))+","+str(-999)+")\n"
					CDIstring += "\t\tcentral_value("+str(round(central_value,4))+","+str(0)+")\n"
					# CDIstring += "\t\tmeta_data(N_jets total,-999,-999)\n"
					# CDIstring += "\t\tmeta_data(N_jets tagged,-999,-999)\n"
					# CDIstring += sys_string
					CDIstring += "\t}\n"
			CDIstring += "}"
			print "Writing to file "+output_filename
			output_file.write(CDIstring)
			output_file.close()
			input_file.Close()
			print "File "+input_filename+" closed."
print "Done"
