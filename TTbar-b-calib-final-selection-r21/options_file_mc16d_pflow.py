import ROOT
import os


#sample with plot options
class sample:
    _lastFillColor =0
    _fillColors=[ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,6,17,18]
    def __init__(self, name, boot_strap_available = False):
        self.name = name
        self.markerStyle = 1
        self.fillStyle = 1001
        self.lineColor = 1
        self.fillColor = sample._fillColors[sample._lastFillColor]
        sample._lastFillColor=sample._lastFillColor+1
        self.lineStyle = 1
        self.root_file_path = ""
        self.boot_strap_available = boot_strap_available
        self.systematic= "nominal"
        if "Py8" in name:
            self.hadronization=410470
        if "Sherpa22" in name:
            self.hadronization=410250
        if "HW7" in name:
            self.hadronization=410558
        if "aMCnlo" in name:
            self.hadronization=410470

    def setHistOptions(self,hist):
        hist.SetMarkerStyle(self.markerStyle)
        hist.SetFillStyle(self.fillStyle);
        hist.SetLineColor(self.lineColor);
        hist.SetFillColor(self.fillColor);
        hist.SetLineStyle(self.lineStyle);
    def addToLegend(self,hist,legend):
        legend.AddEntry(hist,self.name[6:],"F")

class channel:
    def __init__(self,name="ee_OS_J2"):
        self.name = name

class systematic:
    def __init__(self,name="nominal",do_bootstrap=False):
        self.name = name
        self.do_bootstrap=do_bootstrap       #if systematic appears bumpy- you can calc mc stat unc of systematic here and smooth it later

#defining options for dircetories
class options_container:
    def __init__(self):
        self.name="Summary of options"
        # Calibration related attributes
        self.taggers = ["MV2c10", "DL1", "MV2rmu", "MV2r","DL1rmu","DL1r"]
        self.cuts=["FixedCutBEff"]
        self.WPs=["60", "70", "77", "85"]
        # File set up attributes
        self.AT_rel="21.2.93"
        self.version_tag="rel_"+self.AT_rel+"_mc16d/"
        self.dust_dir="/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/b-jet-calib-syst/"+self.version_tag
        self.output_dir= self.dust_dir+self.AT_rel +"_mc16d/"
        self.condor_dir= self.dust_dir+"htc_submit/"
        self.input_selection_dir= "/atlas/solis/BTagging/bjets_ttbardilepton_PDF_2/TTbar-b-calib-final-selection-r21/"+self.version_tag
        self.plot_dir= self.dust_dir+"plots-"+self.AT_rel+"_mc16d/"
        self.www_dir="/afs/cern.ch/user/a/alopezso/www/b-jet-eff-ttbarPDF/"
        #the sample list. Change names here!!!
        self.data_name="data17"
        self.data_year="17"
        self.data_lumi = 43800.0
        self.singeTop_sample=sample('FTAG2_singletop_PowPy8')
        self.ZJets_sample=sample('FTAG2_Zjets_Sherpa221');
        self.Diboson_sample=sample('FTAG2_Diboson_Sherpa222');
        self.Wjets_sample=sample('FTAG2_Wjets_Sherpa221')
#        self.ttV_sample=sample('FTAG2_ttV_aMCnlo')
        other_s=[self.Diboson_sample,self.ZJets_sample,self.Wjets_sample]
        self.other_samples=list(other_s)
        #ttb=sample('FTAG2_ttbar_aMcPy8')
        #ttb=sample('FTAG2_ttbar_PowHW7')
        #ttb=sample("FTAG2_ttbar_Sherpa221")
        # ttb=sample('FTAG2_ttbar_PhPy8_AF2')
        ttb=sample('FTAG2_ttbar_PhPy8')
        ttb.fillColor=0
        self.ttb_sample=ttb
        other_s.append(self.singeTop_sample)
        other_s.append(ttb)
#        other_s.append(self.ttV_sample)
        self.nominal_samples=list(other_s)
        for s in self.nominal_samples:
             s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"
             s.boot_strap_available=True;
        #the channel names. change names here!!
        self.channels=[channel('emu_OS_J2'),channel('emu_OS_J2_m_lj_cut')]
#        self.channels=[channel('emu_OS_J2'),channel('emu_OS_J3'),channel('ee_OS_J2_aCuts'),channel('ee_OS_J3_aCuts'),channel('mumu_OS_J2_aCuts'),channel('mumu_OS_J3_aCuts'),channel('emu_OS_J2_m_lj_cut')]
        self.try_cut_ratio_in_xxcon_plot=0  #=0 to have now cutraio label

        ##set plot limits
        self.plot_lim_standrad=0#[2,20e4]
        self.plot_lim_pt=0#[2,20e3]
        self.plot_lim_m=0#[2,20e3]
        self.plot_lim_mT=0#[2,20e3]
        self.syst_samples = [
            sample('FTAG2_ttbar_aMcPy8',True),
            sample('FTAG2_ttbar_PowHW7',True),
            sample("FTAG2_ttbar_Sherpa221",True),
            sample('FTAG2_ttbar_PhPy8_AF2',True),
#            sample('FTAG2_ttbar_PhPy8_hdamp3mtop',True),
            # sample('FTAG2_ttbar_PhPy8_nonallhad'),
        ]
        for s in self.syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"
        self.rad_up_sample=sample('FTAG2_ttbar_PhPy8_hdamp3mtop')
        self.rad_up_sample.systematic="weight_mc_rad_UP"
        self.rad_up_sample.boot_strap_available=True
        self.rad_down_sample=sample('FTAG2_ttbar_PhPy8')
        self.rad_down_sample.systematic="weight_mc_rad_DOWN"
        self.rad_down_sample.boot_strap_available=True
        self.rad_samples=[
            self.rad_down_sample,
            self.rad_up_sample,
        ]
        self.fsr_up_sample=sample('FTAG2_ttbar_PhPy8')
        self.fsr_up_sample.systematic="weight_mc_fsr_UP"
        self.fsr_up_sample.boot_strap_available=True
        self.fsr_down_sample=sample('FTAG2_ttbar_PhPy8')
        self.fsr_down_sample.systematic="weight_mc_fsr_DOWN"
        self.fsr_down_sample.boot_strap_available=True
        self.fsr_samples=[
            self.fsr_down_sample,
            self.fsr_up_sample,
        ]
        self.syst_samples_ZJets = [
#            sample("FTAG2_Zjets_PowPy8"),
#            sample("FTAG2_Zjets_MGPy8"),
        ]
        self.fake_estimation=False
        for s in self.syst_samples_ZJets:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"
        self.syst_samples_Diboson = [
 #           sample("FTAG2_Diboson_PowPy8"),
        ]
        for s in self.syst_samples_Diboson:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"
        self.syst_samples_singletop = [
 #           sample('FTAG2_singletop_PowPy8_DS')
        ]
        for s in self.syst_samples_singletop:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"
        self.rad_samples_singletop = [
            # sample('FTAG2_singletop_PowPy6_rad_down'),
            # sample('FTAG2_singletop_PowPy6_rad_up')
        ]
        self.inner_systematics_in_nominal_tree = [
            systematic("weight_pileup_UP"),
            systematic("weight_pileup_DOWN"),
            systematic("weight_leptonSF_EL_SF_Trigger_UP"),
            systematic("weight_leptonSF_EL_SF_Trigger_DOWN"),
            systematic("weight_leptonSF_EL_SF_Reco_UP"),
            systematic("weight_leptonSF_EL_SF_Reco_DOWN"),
            systematic("weight_leptonSF_EL_SF_ID_UP"),
            systematic("weight_leptonSF_EL_SF_ID_DOWN"),
            systematic("weight_leptonSF_EL_SF_Isol_UP"),
            systematic("weight_leptonSF_EL_SF_Isol_DOWN"),
            systematic("weight_leptonSF_MU_SF_Trigger_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_Trigger_STAT_DOWN"),
            systematic("weight_leptonSF_MU_SF_Trigger_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_Trigger_SYST_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN"),
            systematic("weight_leptonSF_MU_SF_Isol_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_Isol_STAT_DOWN"),
            systematic("weight_leptonSF_MU_SF_Isol_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_Isol_SYST_DOWN"),
            systematic("weight_leptonSF_MU_SF_TTVA_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_TTVA_STAT_DOWN"),
            systematic("weight_leptonSF_MU_SF_TTVA_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_TTVA_SYST_DOWN"),
            systematic("weight_jvt_UP"),
            systematic("weight_jvt_DOWN"),
            # systematic("FT_EFF_Light_systematics__1up"),
            # systematic("FT_EFF_Light_systematics__1down"),
            systematic("FT_EFF_Eigen_C_0__1down"),
            systematic("FT_EFF_Eigen_C_0__1up"),
            systematic("FT_EFF_Eigen_C_1__1down"),
            systematic("FT_EFF_Eigen_C_1__1up"),
            systematic("FT_EFF_Eigen_C_2__1down"),
            systematic("FT_EFF_Eigen_C_2__1up"),
            systematic("FT_EFF_Eigen_C_3__1down"),
            systematic("FT_EFF_Eigen_C_3__1up"),
            systematic("FT_EFF_Eigen_C_4__1down"),
            systematic("FT_EFF_Eigen_C_4__1up"),
            systematic("FT_EFF_Eigen_C_5__1down"),
            systematic("FT_EFF_Eigen_C_5__1up"),
            systematic("FT_EFF_Eigen_C_6__1down"),
            systematic("FT_EFF_Eigen_C_6__1up"),
            systematic("FT_EFF_Eigen_C_7__1down"),
            systematic("FT_EFF_Eigen_C_7__1up"),
            systematic("FT_EFF_Eigen_C_8__1down"),
            systematic("FT_EFF_Eigen_C_8__1up"),
            systematic("FT_EFF_Eigen_C_9__1down"),
            systematic("FT_EFF_Eigen_C_9__1up"),
            systematic("FT_EFF_Eigen_C_10__1down"),
            systematic("FT_EFF_Eigen_C_10__1up"),
            systematic("FT_EFF_Eigen_C_11__1down"),
            systematic("FT_EFF_Eigen_C_11__1up"),
            systematic("FT_EFF_Eigen_C_12__1down"),
            systematic("FT_EFF_Eigen_C_12__1up"),
            systematic("FT_EFF_Eigen_C_13__1down"),
            systematic("FT_EFF_Eigen_C_13__1up"),
            systematic("FT_EFF_Eigen_C_14__1down"),
            systematic("FT_EFF_Eigen_C_14__1up"),
            systematic("FT_EFF_Eigen_C_15__1down"),
            systematic("FT_EFF_Eigen_C_15__1up"),
            systematic("FT_EFF_Eigen_C_16__1down"),
            systematic("FT_EFF_Eigen_C_16__1up"),
            systematic("FT_EFF_Eigen_C_17__1down"),
            systematic("FT_EFF_Eigen_C_17__1up"),
            systematic("FT_EFF_Eigen_C_18__1down"),
            systematic("FT_EFF_Eigen_C_18__1up"),
            systematic("FT_EFF_Eigen_C_19__1down"),
            systematic("FT_EFF_Eigen_C_19__1up"),
            systematic("FT_EFF_Eigen_Light_0__1down"),
            systematic("FT_EFF_Eigen_Light_0__1up"),
            systematic("FT_EFF_Eigen_Light_1__1down"),
            systematic("FT_EFF_Eigen_Light_1__1up"),
            systematic("FT_EFF_Eigen_Light_2__1down"),
            systematic("FT_EFF_Eigen_Light_2__1up"),
            systematic("FT_EFF_Eigen_Light_3__1down"),
            systematic("FT_EFF_Eigen_Light_3__1up"),
            systematic("FT_EFF_Eigen_Light_4__1down"),
            systematic("FT_EFF_Eigen_Light_4__1up"),
            systematic("FT_EFF_Eigen_Light_5__1down"),
            systematic("FT_EFF_Eigen_Light_5__1up"),
            systematic("FT_EFF_Eigen_Light_6__1down"),
            systematic("FT_EFF_Eigen_Light_6__1up"),
            systematic("FT_EFF_Eigen_Light_7__1down"),
            systematic("FT_EFF_Eigen_Light_7__1up"),
            systematic("FT_EFF_Eigen_Light_8__1down"),
            systematic("FT_EFF_Eigen_Light_8__1up"),
            systematic("FT_EFF_Eigen_Light_9__1down"),
            systematic("FT_EFF_Eigen_Light_9__1up"),
            systematic("FT_EFF_Eigen_Light_10__1down"),
            systematic("FT_EFF_Eigen_Light_10__1up"),
            systematic("FT_EFF_Eigen_Light_11__1down"),
            systematic("FT_EFF_Eigen_Light_11__1up"),
            systematic("FT_EFF_Eigen_Light_12__1down"),
            systematic("FT_EFF_Eigen_Light_12__1up"),
            systematic("FT_EFF_Eigen_Light_13__1down"),
            systematic("FT_EFF_Eigen_Light_13__1up"),
            systematic("FT_EFF_Eigen_Light_14__1down"),
            systematic("FT_EFF_Eigen_Light_14__1up"),
            systematic("FT_EFF_Eigen_Light_15__1down"),
            systematic("FT_EFF_Eigen_Light_15__1up"),
            systematic("FT_EFF_Eigen_Light_16__1down"),
            systematic("FT_EFF_Eigen_Light_16__1up"),
            systematic("FT_EFF_Eigen_Light_17__1down"),
            systematic("FT_EFF_Eigen_Light_17__1up"),
            systematic("FT_EFF_Eigen_Light_18__1down"),
            systematic("FT_EFF_Eigen_Light_18__1up") ,
            systematic("FT_EFF_Eigen_Light_19__1down"),
            systematic("FT_EFF_Eigen_Light_19__1up"),
            ]
        self.tree_systematics = [
            systematic("EG_RESOLUTION_ALL__1down"),
            systematic("EG_RESOLUTION_ALL__1up"),
            systematic("EG_SCALE_ALL__1down"),
            systematic("EG_SCALE_ALL__1up"),
            systematic("CategoryReduction_JET_BJES_Response__1down"),
            systematic("CategoryReduction_JET_BJES_Response__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Detector1__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Detector1__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Mixed1__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Mixed1__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Mixed2__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Mixed2__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Mixed3__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Mixed3__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling1__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling1__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling2__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling2__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling3__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling3__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling4__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Modelling4__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical1__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical1__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical2__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical2__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical3__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical3__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical4__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical4__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical5__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical5__1up"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical6__1down"),
            systematic("CategoryReduction_JET_EffectiveNP_Statistical6__1up"),
            systematic("CategoryReduction_JET_EtaIntercalibration_Modelling__1down", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_Modelling__1up", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_TotalStat__1down", True),
            systematic("CategoryReduction_JET_EtaIntercalibration_TotalStat__1up", True),
            systematic("CategoryReduction_JET_Flavor_Composition__1down", True),
            systematic("CategoryReduction_JET_Flavor_Composition__1up", True),
            systematic("CategoryReduction_JET_Flavor_Response__1down", True),
            systematic("CategoryReduction_JET_Flavor_Response__1up", True),
            systematic("CategoryReduction_JET_Pileup_OffsetMu__1down"),
            systematic("CategoryReduction_JET_Pileup_OffsetMu__1up"),
            systematic("CategoryReduction_JET_Pileup_OffsetNPV__1down", True),
            systematic("CategoryReduction_JET_Pileup_OffsetNPV__1up", True),
            systematic("CategoryReduction_JET_Pileup_PtTerm__1down"),
            systematic("CategoryReduction_JET_Pileup_PtTerm__1up"),
            systematic("CategoryReduction_JET_Pileup_RhoTopology__1down", True),
            systematic("CategoryReduction_JET_Pileup_RhoTopology__1up", True),
            systematic("CategoryReduction_JET_SingleParticle_HighPt__1down"),
            systematic("CategoryReduction_JET_PunchThrough_MC16__1down"),
            systematic("CategoryReduction_JET_PunchThrough_MC16__1up"),
            systematic("CategoryReduction_JET_JER_DataVsMC__1down",True),
            systematic("CategoryReduction_JET_JER_DataVsMC__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_1__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_1__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_2__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_2__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_3__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_3__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_4__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_4__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_5__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_5__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_6__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_6__1up",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down",True),
            systematic("CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up",True),
            systematic("MET_SoftTrk_ResoPara"),
            systematic("MET_SoftTrk_ResoPerp"),
            systematic("MET_SoftTrk_ScaleDown"),
            systematic("MET_SoftTrk_ScaleUp"),
            systematic("MUON_ID__1down"),
            systematic("MUON_ID__1up"),
            systematic("MUON_MS__1down"),
            systematic("MUON_MS__1up"),
            systematic("MUON_SAGITTA_RESBIAS__1down"),
            systematic("MUON_SAGITTA_RESBIAS__1up"),
            systematic("MUON_SAGITTA_RHO__1down"),
            systematic("MUON_SAGITTA_RHO__1up"),
            systematic("MUON_SCALE__1down"),
            systematic("MUON_SCALE__1up"),
            ]
        self.pdf_systematics=["weight_mc_shower_np_11",]
        for np in xrange(112,142):
            self.pdf_systematics.append("weight_mc_shower_np_"+str(np))
options=options_container()
